angular.module('ciklus').controller('AgendaCtrl', function($scope, SessionService, EstabelecimentoService, SocioService, $ionicModal, $state, $sce, $cordovaGeolocation, NotificacaoService) {

  if (SessionService.get('socio')) {
    $scope.user = JSON.parse(SessionService.get('socio'));
  } else {
    $scope.user = null;
  }
  $scope.modalDia = {};
  $scope.agenda = [];
  $scope.diaAtual = new Date().getDay();
  $scope.urlImagens = imagens;
  $scope.localizacao = {};
  $scope.posicaoAtual = {};
  $scope.dadosCarregados = false;
  $scope.validadoOnesignal = false;

  $scope.$on('$ionicView.enter', function() {
    if(window.cordova) {
      window.ga.trackView('Agenda');
    }
  });

  if (window.cordova) {
    var options = {timeout: 10000, enableHighAccuracy: true};
    $cordovaGeolocation.getCurrentPosition(options).then(function(position) {
      $scope.posicaoAtual = position;
    });
  }

  $scope.obterDistanciaEstabelecimento = function(promocao) {
    if (window.cordova && $scope.posicaoAtual && promocao.latitude && promocao.longitude) {
      var myLocation = new google.maps.LatLng($scope.posicaoAtual.coords.latitude, $scope.posicaoAtual.coords.longitude);
      var myDestination = new google.maps.LatLng(promocao.latitude, promocao.longitude);
      promocao.distancia = (google.maps.geometry.spherical.computeDistanceBetween(myLocation, myDestination) / 1000).toFixed(2);
      return '- ' + (google.maps.geometry.spherical.computeDistanceBetween(myLocation, myDestination) / 1000).toFixed(2) + "km";
    }
  };

  $scope.trustSrc = function(src) {
    return $sce.trustAsResourceUrl(src);
  };

  $scope.fecharModalDia = function() {
    $scope.modalDia.hide();
  };

  $scope.obterLocalizacao = function() {
    if (window.cordova) {
      var options = {timeout: 10000, enableHighAccuracy: true};
      $cordovaGeolocation.getCurrentPosition(options).then(function(position) {
        $scope.localizacao = position;
        $scope.agenda.forEach(function(x) {
          x.forEach(function(y) {
            y.distancia = $scope.obterDistanciaEstabelecimento($scope.localizacao, y);
          });
        });
      }, function(error){
        console.log("Could not get location");
      });
    }
  };

  $scope.atualizarAgenda = function(dia) {
    if (dia == null) {
      dia = $scope.diaAtual;
    }
    $scope.dadosCarregados = false;

    EstabelecimentoService.buscarAgenda(dia).then(function(data) {
      $scope.agenda = data;
      $scope.dadosCarregados = true;
    });
  };

  $scope.irParaCategoria = function(id) {
    $state.go('tab.estabelecimentosagenda', { categoria: id });
  };

  $scope.atualizarAgenda(null);

  $scope.buscarNomeDia = function() {
    switch($scope.diaAtual) {
      case 0:
        return "Domingo";
        break;
      case 1:
        return "Segunda-feira";
        break;
      case 2:
        return "Terça-feira";
        break;
      case 3:
        return "Quarta-feira";
        break;
      case 4:
        return "Quinta-feira";
        break;
      case 5:
        return "Sexta-feira";
        break;
      case 6:
        return "Sábado";
        break;
      default:
        return "";
        break;
    }
  };

  $scope.classeDia = function(dia) {
    if ($scope.diaAtual == dia)
      return "pink";
  };

  $scope.trocarDiaPara = function(dia) {
    $scope.diaAtual = dia;
    $scope.atualizarAgenda(dia);
    $scope.modalDia.hide();
  };

  $scope.formatarDias = function(promocao) {
    var dias = [];
    if (promocao.segundaFeira)
      dias.push('Seg');
    if (promocao.tercaFeira)
      dias.push('Ter');
    if (promocao.quartaFeira)
      dias.push('Qua');
    if (promocao.quintaFeira)
      dias.push('Qui');
    if (promocao.sextaFeira)
      dias.push('Sex');
    if (promocao.sabado)
      dias.push('Sab');
    if (promocao.tercaFeira)
      dias.push('Dom');

    return dias.join(', ');
  };

  $scope.trocarDia = function() {
    $scope.modalDia.show();
  };

  $scope.irParaEstabelecimento = function(promocao) {
    $state.go('tab.estabelecimentoagenda', { id: promocao.id });
  };

  $ionicModal.fromTemplateUrl('templates/modal-trocar-dia.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modalDia = modal;
  });

})