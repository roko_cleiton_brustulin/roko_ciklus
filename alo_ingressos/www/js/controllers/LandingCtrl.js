angular.module('ciklus').controller('LandingCtrl', function($scope, $ionicModal, $state, SocioService, $ionicPopup, SessionService, IuguService, $ionicLoading, EmailService, PlanoService) {
  $scope.modalCadastro = {};
  $scope.modalBemVindo = {};
  $scope.novaSenha = { email: '', cpf: '' };
  $scope.cadastro = { passo: 1, senha: '' };
  $scope.login = { cpf: '', senha: '' };
  $scope.planos = [];
  $scope.emailInvalido = false;
  $scope.convite = {nome: '', email: ''};
  $scope.pularPagamento = false;

  // $scope.$on('$ionicView.beforeEnter', function() {
  //   if (SessionService.get('socio')) {
  //     var socio = JSON.parse(SessionService.get('socio'));
  //     if (socio.id) {
  //       if (socio.emailValido == true){
  //         $state.go('tab.categorias');
  //       }else{
  //         $state.go('landing');
  //         $ionicPopup.show({
  //           title: 'E-mail não confirmado',
  //           template: 'O seu e-mail ainda não foi confirmado.<br><br>Acesse a sua caixa de e-mail e clique no link para validação antes de efetuar o login.',
  //           buttons: [
  //             { text: 'OK' }
  //           ]
  //         });
  //       }
  //     }
  //   }
  // });

  $scope.$on('$ionicView.enter', function() {
    if(window.cordova) {
      setTimeout(function() {
        window.ga.trackView('Landing');
      }, 1500);
    }
  });


  $scope.criarConta = function(flag) {
    if(flag == true){
      $scope.pularPagamento = true;
    }
    $scope.cadastro.passo = 1;
    $scope.modalCadastro.show();
  };

  $scope.buscarCEP = function() {

    $ionicLoading.show();

    $scope.cadastro.dados.rua     = "";
    $scope.cadastro.dados.bairro  = "";
    $scope.cadastro.dados.cidade  = "";
    $scope.cadastro.dados.uf      = "";

    SocioService.buscarCEP($scope.cadastro.dados.cep).then(function(result) {
      $ionicLoading.hide();
      if(result == "erro"){
        $ionicPopup.show({
          title: 'CEP incorreto!',
          template: 'O CEP que você digitou está incorreto.',
          buttons: [
            { text: 'OK' }
          ]
        });
      }else if(result.erro == true){
        $ionicPopup.show({
          title: 'CEP incorreto!',
          template: 'O CEP que você digitou está incorreto.',
          buttons: [
            { text: 'OK' }
          ]
        });
      } else {
        $scope.cadastro.dados.rua     = result.logradouro;
        $scope.cadastro.dados.bairro  = result.bairro;
        $scope.cadastro.dados.cidade  = result.localidade;
        $scope.cadastro.dados.uf      = result.uf;
        if (result.logradouro) {
          $ionicPopup.show({
            title: 'CEP encontrado!',
            template: 'Confira as informações e digite o número para completar o seu cadastro',
            buttons: [
              { text: 'OK' }
            ]
          });
        } else {
          $ionicPopup.show({
            title: 'CEP encontrado!',
            template: 'Digite a Rua, Bairro e Número para completar o seu cadastro',
            buttons: [
              { text: 'OK' }
            ]
          });
        }
      }
    })
  };

  $scope.verBeneficios = function() {
    $state.go('tab.categorias'); 
  };

  $scope.click = function(event){
    alert("Cliked");
  };

  $scope.irParaDestino = function(src) {
      window.open(src,  '_system', 'location=no');
  };

  $scope.validarVoucher = function() {
    $ionicLoading.show();
    SocioService.validarVoucher($scope.cadastro.voucher).then(function(result) {
      if (result.length > 0) {
        result = result[0];

        $scope.voucherFrequencia = result.plano.frequencia;
        if (result.utilizado) {
          $ionicPopup.show({
            title: 'Convite já utilizado',
            template: 'O código que você digitou já foi utilizado.',
            buttons: [
              { text: 'OK' }
            ]
          });
          $ionicLoading.hide();
          return;
        } else {
          var expiracao = result.expiracao.split('-');
          var expiracaoObj = new Date(expiracao[0], expiracao[1], expiracao[2].substring(0, 2));
          var hoje = new Date();
          if (expiracaoObj.valueOf() < hoje.valueOf()) {
            $ionicPopup.show({
              title: 'Convite expirado!',
              template: 'O código que você digitou está expirado.',
              buttons: [
                { text: 'OK' }
              ]
            });
            $ionicLoading.hide();
            return;
          } else {
              $ionicLoading.hide();
              $ionicPopup.show({
              title: 'Parabéns!',
              template: 'O código que você digitou é válido e dá direito a ' + result.plano.frequencia + ' dias grátis.',
              buttons: [
                { text: 'OK',
                  onTap: function(e) {
                    $scope.voucherValido = true;
                    $scope.voucherObj = result;
                    $scope.efetivarCadastro();
                  }
                }
              ]
            });
          }
        }
      } else {
        $ionicLoading.hide();
        $ionicPopup.show({
          title: 'Convite inválido',
          template: 'Ops, parece que o código que você digitou é inválido.',
          buttons: [
            { text: 'OK' }
          ]
        });
      }
    });
  };

  $scope.esqueceuSenha = function() {
    $ionicModal.fromTemplateUrl('templates/modal-esqueceu-senha.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modalEsqueceuSenha = modal;
      $scope.modalEsqueceuSenha.show();
    });
  };

  $scope.fecharEsqueceuSenha = function() {
    $scope.modalEsqueceuSenha.hide();
  };

  $scope.gerarNovaSenha = function() {
    var cpf = $scope.formatarCpf($scope.novaSenha.cpf);
    if (cpf.length == 14) {
      if ($scope.validarEmail($scope.novaSenha.email)) {
        $ionicLoading.show();
        SocioService.gerarSenha(cpf, $scope.novaSenha.email).then(function(res) {
          if (res) {
            $ionicLoading.hide();
            $ionicPopup.show({
              title: 'Pronto!',
              subTitle: 'Uma nova senha foi gerada e enviada para seu e-mail.',
              scope: $scope,
              buttons: [
                { text: 'Ok',
                onTap: function(e) {
                  $scope.modalEsqueceuSenha.hide();
                }
              }
              ]
            });
          } else {
            $ionicLoading.hide();
            $ionicPopup.show({
              title: 'Atenção!',
              subTitle: 'Dados inválidos, por favor, verifique.',
              scope: $scope,
              buttons: [
                { text: 'Ok' }
              ]
            });
          }
        });
      } else {
        $ionicPopup.show({
          title: 'Atenção!',
          subTitle: 'Dados inválidos, por favor, verifique.',
          scope: $scope,
          buttons: [
            { text: 'Ok' }
          ]
        });  
      }
    } else {
      $ionicPopup.show({
        title: 'Atenção!',
        subTitle: 'Dados inválidos, por favor, verifique.',
        scope: $scope,
        buttons: [
          { text: 'Ok' }
        ]
      });
    }
  };

  $scope.convite = function(){
    if ($scope.convite.nome && $scope.convite.email) {
      $ionicLoading.show();
      EmailService.enviar($scope.convite.nome, $scope.convite.email, 'Solicitacao de convite via app').then(function(data) {
        $ionicLoading.hide();
        $ionicPopup.show({
          title: 'Obrigado!',
          template: 'Seu  nome já está na nossa lista de espera.<br><br>Agora é só aguardar!<br><br>Obrigado pelo interesse.',
          buttons: [
            { text: 'Ok',
             onTap: function(e) {
              $scope.modalCadastro.hide();
             }
            }
          ]
        });
      })
    } else {
      $ionicPopup.show({
        title: 'Ops',
        template: 'Preencha seu Nome Completo e e-mail.',
        buttons: [
          { text: 'OK' }
        ]
      });
    }
  }

  $scope.validarEmail = function(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (re.test(email)) {
      return true;
    } else {
      $ionicPopup.show({
        title: 'Atenção!',
        subTitle: 'E-mail inválido, por favor, verifique.',
        scope: $scope,
        buttons: [
          { text: 'Ok' }
        ]
      });
      return false;
    }
  };

  $scope.formatarCpf = function(cpf) {
    cpf = cpf.toString();
    while (cpf.length < 11) {
      cpf = "0" + cpf;
    }
    cpf = cpf.slice(0, 3) + '.' + cpf.slice(3, 6) + '.' + cpf.slice(6, 9) + '-' + cpf.slice(9, 11);
    return cpf;
  };

  $scope.login = function() {

    if ($scope.login.cpf && $scope.login.senha) {
      $ionicLoading.show();
      SocioService.login($scope.formatarCpf($scope.login.cpf), $scope.login.senha).then(function(data) {
        $ionicLoading.hide();
        if (data.length > 0) {
          if (data[0].emailValido == false){
            $ionicPopup.show({
              title: 'E-mail não confirmado',
              template: 'O seu e-mail ainda não foi confirmado.<br><br>Acesse a sua caixa de e-mail e clique no link para validação antes de efetuar o login.',
              buttons: [
                { text: 'OK' }
              ]
            });
          }else{
            SocioService.atualizarOnesignal(data[0].id);
            SessionService.persist('socio', data[0]);
            $scope.dadosCarregados = true;
            $scope.socioLogado = true;
            $state.go('tab.categorias');
            $scope.modalCadastro.hide();
          }
        } else {
          $ionicPopup.show({
            title: 'Login inválido',
            template: 'CPF e/ou senha inválidos, tente novamente.',
            buttons: [
              { text: 'OK' }
            ]
          });
        }
      });
    } else {
      $ionicPopup.show({
        title: 'Ops',
        template: 'Preencha seu CPF e senha.',
        buttons: [
          { text: 'OK' }
        ]
      });
    }
  };

  $scope.validarCPF = function(cpf) {
    cpf = cpf.replace(/[^\d]+/g,'');
    if(cpf == '') return false;
    // Elimina CPFs invalidos conhecidos
    if (cpf.length != 11 ||
        cpf == "00000000000" ||
        cpf == "11111111111" ||
        cpf == "22222222222" ||
        cpf == "33333333333" ||
        cpf == "44444444444" ||
        cpf == "55555555555" ||
        cpf == "66666666666" ||
        cpf == "77777777777" ||
        cpf == "88888888888" ||
        cpf == "99999999999")
            return false;
    // Valida 1o digito
    add = 0;
    for (i=0; i < 9; i ++)
        add += parseInt(cpf.charAt(i)) * (10 - i);
        rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;
        if (rev != parseInt(cpf.charAt(9)))
            return false;
    // Valida 2o digito
    add = 0;
    for (i = 0; i < 10; i ++)
        add += parseInt(cpf.charAt(i)) * (11 - i);
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)
        rev = 0;
    if (rev != parseInt(cpf.charAt(10)))
        return false;
    return true;
  };


  $scope.cadastrarAuxiliar = function() {
      
    if ($scope.cadastro.dados.cpf) {
      $scope.cadastro.dados.cpf = $scope.cadastro.dados.cpf.toString();
      if ($scope.cadastro.dados.cpf.length < 11) {
        $scope.cadastro.dados.cpf = $scope.cadastro.dados.cpf.padZero(11, '0');
      }
    }

    $scope.cadastro.dados.telefone = $scope.cadastro.dados.telefone.toString();
    if ($scope.cadastro.dados.telefone){
      if ($scope.cadastro.dados.telefone.length < 8){
        $ionicPopup.show({
          title: 'Telefone incorreto',
          template: 'Favor preencher o telefone com DDD',
          buttons: [
            { text: 'OK' }
          ]
        });
        return "";
      }
    }else{
      $ionicPopup.show({
          title: 'Telefone não preenchido',
          template: 'Favor preencher o telefone com DDD',
          buttons: [
            { text: 'OK' }
          ]
        });
      return "";
    }

    if ($scope.validarEmail($scope.cadastro.dados.email)) {
      if ($scope.cadastro.dados.nome.length > 1
        && $scope.cadastro.dados.cpf.length == 11
        && $scope.cadastro.dados.termo){
        $ionicLoading.show();
        var cpf = $scope.cadastro.dados.cpf;
        $scope.cadastro.dados.cpf = cpf.substring(0, 3) + '.' + cpf.substring(3, 6) + '.' + cpf.substring(6, 9) + '-' + cpf.substring(9, 11);
        if ($scope.validarCPF($scope.cadastro.dados.cpf)) {
              SocioService.verificarCPF($scope.cadastro.dados.cpf).then(function(resultado) {
                if (resultado) {
                    // Registra na tabela auxiliar e vai para o CEP
                    SocioService.cadastrarAuxiliar($scope.cadastro.dados).then(function(socioAux) {
                      $scope.cadastro.dados.id = socioAux.id;
                      $scope.cadastro.passo = 2;
                      $ionicLoading.hide();
                    });

                } else {
                  $scope.cadastro.dados.cpf = '';
                  $ionicLoading.hide();
                  $ionicPopup.show({
                    title: 'CPF encontrado',
                    template: 'Esse CPF já está cadastrado no Clube Alô Ingressos.',
                    buttons: [
                      { text: 'OK' }
                    ]
                  });
                }
              });
        } else {
          $ionicLoading.hide();
          $ionicPopup.show({
            title: 'CPF inválido',
            template: 'Por favor, verifique seu CPF.',
            buttons: [
              { text: 'OK' }
            ]
          });
        }
      } else {
        $ionicPopup.show({
          title: 'Dados inválidos',
          template: 'Por favor, verifique os dados preenchidos.',
          buttons: [
            { text: 'OK' }
          ]
        });
      }
    }
  };


 $scope.cadastrarAuxiliarCEP = function() {

    if ($scope.cadastro.dados.cep && 
      $scope.cadastro.dados.rua &&
      $scope.cadastro.dados.bairro &&
      $scope.cadastro.dados.cidade &&
      $scope.cadastro.dados.uf &&
      $scope.cadastro.dados.numero ) {
    
      $scope.cadastro.dados.cep = $scope.cadastro.dados.cep.toString();
    
      $ionicLoading.show();
        
        // Atualiza tabela auxiliar com CEP e vai para os planos
        SocioService.cadastrarAuxiliarCEP($scope.cadastro.dados).then(function(socioAux) {
          $scope.cadastro.dados.id = socioAux.id;
          
          if($scope.pularPagamento){
            //Direciona para Efetivação do Cadastro pois não passa pelo pagamento (Cadastro Simples - Baixar ingresso)
            $scope.cadastro.dados.cupom = 0; //indica que foi criado usando o Baixe o seu Ingresso
            $scope.efetivarCadastro();
          }else{
            //Direciona para Modal PLANOS
            PlanoService.buscarPlanos().then(function(response) {
               $scope.planos = response;
               $scope.mostrarPlanos = $scope.planos.length;
               $scope.cadastro.passo = 3;
               $ionicLoading.hide();
            });  
          }
        });
    } else {
      $ionicPopup.show({
        title: 'Informações incompletas',
        template:'Complete todas as informações e tente novamente',
        buttons: [{
          text: 'Ok'
        }]
      });
      $ionicLoading.hide();
      return;
    }    
  }; 

  $scope.realizarPagamento = function() {
    $scope.cadastro.dados.cartao.numero = $scope.cadastro.dados.cartao.numero.toString();
    $scope.cadastro.dados.cartao.cvv = $scope.cadastro.dados.cartao.cvv.toString();
    if ($scope.cadastro.dados.cartao.nome.length > 3 && $scope.cadastro.dados.cartao.numero.length == 16
      && ($scope.cadastro.dados.cartao.cvv.length == 3 || $scope.cadastro.dados.cartao.cvv.length == 4)
      && $scope.cadastro.dados.cartao.mes.length == 2 && $scope.cadastro.dados.cartao.ano.length == 2) {
      var cartao = $scope.cadastro.dados.cartao;
      $ionicLoading.show();
      IuguService.associarContaPagamento($scope.cadastro.dados, cartao, $scope.cadastro.plano.idPlataforma).then(function(pagamento) {
        var resultado = pagamento;
        if (resultado) {
          if (resultado.current_transaction.status == "paid") {

            $scope.cadastro.dados.idIugu = resultado.id;
            $scope.efetivarCadastro();

          } else {
            $ionicLoading.hide();
            $ionicPopup.show({
              title: 'Erro ao realizar pagamento',
              template: 'A transação não foi bem sucedida.',
              buttons: [
                { text: 'OK' }
              ]
            });  
          }
        } else {
          $ionicLoading.hide();
          $ionicPopup.show({
            title: 'Erro ao realizar pagamento',
            template: 'Por favor, verifique os dados preenchidos e tente novamente.',
            buttons: [
              { text: 'OK' }
            ]
          });
        }
      }, function(err) {
        $ionicLoading.hide();
        $ionicPopup.show({
          title: 'Erro ao realizar pagamento',
          template: 'Por favor, verifique os dados preenchidos e tente novamente.',
          buttons: [
            { text: 'OK' }
          ]
        });
      });
    } else {
      $ionicLoading.hide();
      $ionicPopup.show({
        title: 'Dados inválidos',
        template: 'Por favor, verifique os dados preenchidos e tente novamente.',
        buttons: [
          { text: 'OK' }
        ]
      });
    }
  };


  $scope.efetivarCadastro = function(){

    if ($scope.voucherValido) {
      var hoje =  new Date();
      hoje.setDate(hoje.getDate() + $scope.voucherFrequencia);
      var mes = hoje.getMonth() + 1;
      var dia = hoje.getDate();
      var ano = hoje.getFullYear();

      if (mes.length < 2) mes = '0' + mes;
      if (dia.length < 2) dia = '0' + dia;
      ativoAte = [ano, mes, dia].join('-');

      $scope.cadastro.dados.ativoAte = ativoAte;
    }
    
    $scope.cadastro.dados.empresa     = empresa;
    $scope.cadastro.dados.emailValido = false;
    delete $scope.cadastro.dados.id;
    SocioService.cadastrar($scope.cadastro.dados).then(function(response) {
      if (response.id > 0) {
        var socio = response;
        if (socio.id > 0) {

          SocioService.excluirAuxiliar(socio.cpf);

          // seta voucher como utilizado.
          if ($scope.voucherValido) {
            SocioService.atualizarVoucher($scope.voucherObj.id, true).then(function(response) {
              socio.cupom = $scope.voucherObj.id;
              SocioService.atualizar(socio).then(function(d) {
              });
            });
          } 

          SocioService.atualizarOnesignal(socio.id);

          $ionicLoading.hide();
          $ionicPopup.show({
            template: '<input type="password" ng-model="cadastro.senha">',
            title: 'Crie uma senha',
            scope: $scope,
            buttons: [
              {
                text: '<b>Salvar</b>',
                type: 'button-positive',
                onTap: function(e) {
                  if ($scope.cadastro.senha.length > 3) {
                    
                    $ionicLoading.show();

                    SocioService.atualizarCodigo(socio.id);
                    SocioService.alterarSenha(socio.id, $scope.cadastro.senha).then(function(response) {
                      SessionService.persist('socio', socio);
                      SocioService.atualizarOnesignal(socio.id);
                      $scope.cadastro = { passo: 1, senha: '' };
                      $scope.voucherValido = false;
                      // $scope.modalCadastro.hide();
                      // if($scope.pularPagamento){
                      //   $state.go('tab.pedidos');
                      // }else{
                      //   $scope.modalBemVindo.show();
                      // }
 
                      EmailService.enviar_email_validacao(socio.nome, socio.email, socio.id).then(function(result){
                        $ionicLoading.hide();
                        $ionicPopup.show({
                          title: 'Parabéns',
                          template: 'Seu cadastro foi realizado com sucesso<br><br>Foi enviado um e-mail para ' + socio.email + '.<br><br>Acesse a sua caixa de e-mail e clique no link para validação antes de efetuar o login.',
                          buttons: [
                            {
                              text: '<b>OK</b>',
                              type: 'button-positive',
                              onTap: function(e) {
                                $scope.modalCadastro.hide();
                              }
                            }
                          ]
                        });
                      }, function(err) {
                        $ionicLoading.hide();
                        console.log('erro ao enviar e-mail');
                      });
                    });
                  } else {
                    e.preventDefault();
                    $ionicPopup.show({
                      template: 'Sua senha deve ter pelo menos 4 caracteres.',
                      buttons: [
                        {
                          text: '<b>Salvar</b>',
                          type: 'button-positive'
                        }
                      ]
                    });
                  }
                }
              }
            ]
          });
        }
      }
    });
  }

  $scope.fecharBemVindo = function() {
    $state.go('tab.categorias');
    $scope.modalBemVindo.hide();
  };

  // USADO QUANDO BUSCAVA OS PLANOS DO PAGAR.ME
  // $scope.formatarPreco = function(preco) {
  //   preco = preco.toString();
  //   if (preco.length > 1) {
  //     var precoFormatado = preco.substr(0, preco.length - 2) + ','
  //                         + preco.substr(preco.length - 2, preco.length - 1);
  //     return precoFormatado;
  //   } else {
  //     return "";
  //   }
  // };

  $scope.formatarPreco = function(preco) {
    preco = preco.toString();
    return preco;
    // if (preco.length > 1) {
    //   var precoFormatado = preco.substr(0, preco.length - 2) + ','
    //                       + preco.substr(preco.length - 2, preco.length - 1);
    //   return precoFormatado;
    // } else {
    //   return "";
    // }
  };

  $scope.irLogin = function() {
    $scope.modalCadastro.show();
    $scope.cadastro.passo = 5;
  };

  $scope.selecionarPlano = function(plano) {
    $scope.cadastro.plano = plano;
    $scope.cadastro.passo = 4; //CARTÃO DE CRÉDITO
  };

  $scope.irParaHome = function() {
    $state.go('tab.categorias');
    $scope.modalCadastro.hide();
  };

  $ionicModal.fromTemplateUrl('templates/cadastro.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modalCadastro = modal;
  });

  $ionicModal.fromTemplateUrl('templates/modal-bem-vindo.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modalBemVindo = modal;
  });

  $scope.fecharCadastro = function() {
    if ($scope.cadastro.passo == 5){
      $scope.cadastro.passo = 1;
      $scope.modalCadastro.hide();
    }else{
      if ($scope.cadastro.passo > 1) {
      $scope.cadastro.passo = $scope.cadastro.passo - 1;
      } else {
        $scope.modalCadastro.hide();
      }
    }
  };

})