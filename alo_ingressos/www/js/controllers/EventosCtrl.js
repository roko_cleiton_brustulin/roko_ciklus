angular.module('ciklus').controller('EventosCtrl', function($scope, EstabelecimentoService, $state, $ionicLoading, $ionicModal, SessionService) {
  $scope.eventos = [];
  $scope.orderOptionVar = "data";
  $scope.cidade = localStorage.getItem('cidade');

  EstabelecimentoService.buscarEventos().then(function (response) {
    $scope.eventos = response;
  });

  $scope.formatarData = function(data) {
    var d = data.split('-');
    return d[2].substring(0, 2) + '/' + d[1] + '/' + d[0];
  };

  $scope.recuperarImagem = function(evento) {
    if (evento.foto) {
      return imagens + evento.foto.id + '.' + evento.foto.extensao;
    }
  };

  $scope.irParaEvento = function(evento) {
    $state.go('tab.evento', { evento: evento });
  };
  
  $scope.irParaDestino = function(src) {
      window.open(src,  '_system', 'location=no');
  };

  $scope.getOrder = function() {
    return $scope.orderOptionVar;
  };

  $scope.orderOption = function(option) {
    switch (option) {
      case 0:
        $scope.orderOptionVar = 'titulo';
        break;
      case 1:
        $scope.orderOptionVar = 'data';
        break;
    }
  };

  $scope.recuperarFiltroAtivo = function(option) {
    var opt = "";
    switch (option) {
      case 0:
        opt = 'data';
        break;
      case 1:
        opt = 'titulo';
        break;
    }

    if ($scope.orderOptionVar == opt)
      return "bg-pink white";
  };

  $scope.pullToRefresh = function() {
    // $ionicLoading.show();
    $scope.eventos  = [];
    
    EstabelecimentoService.buscarEventos().then(function (response) {
      $scope.eventos = response;
      if ($scope.eventos.length == 0) {
        $scope.semEventos = true;
      }
      $scope.$broadcast('scroll.refreshComplete');
      // $ionicLoading.hide();
    });
  };


  $scope.trocarCidade = function() {  
    EstabelecimentoService.buscarCidadesEventos().then(function(cidades) {
      $scope.cidades = cidades.rows;
      $ionicModal.fromTemplateUrl('templates/modal-trocar-cidade.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.modalCidade = modal;
        $scope.modalCidade.show();
      });
    });
  };

  $scope.fecharModalCidade = function() {
    $scope.modalCidade.hide();
  };

  $scope.trocarCidadePara = function(cidade) {
    SessionService.persist('cidade', cidade);
    $scope.cidade = cidade;
    $scope.modalCidade.hide();
  };

})