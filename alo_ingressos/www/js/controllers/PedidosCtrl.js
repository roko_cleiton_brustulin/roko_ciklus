angular.module('ciklus').controller('PedidosCtrl', function($scope, SessionService, SocioService, AloService, $state, $ionicLoading, $ionicPopup) {
  
  if (SessionService.get('socio')) {
    $scope.socio = JSON.parse(SessionService.get('socio'));
  } else {
    $scope.socio = null;
  }
  $scope.dadosCarregados = false;
  $scope.socioLogado = false;


  $scope.$on('$ionicView.enter', function() {

    if(window.cordova) {
      window.ga.trackView('Pedidos');
    }

    if($scope.pedidos == undefined){

      $ionicLoading.show();

      var socio = SessionService.get('socio');
      if(!socio){
        $scope.socioLogado      = false;
        $scope.dadosCarregados  = true;
        $ionicLoading.hide();
      } else {
        $scope.socio = JSON.parse(socio);
        if(navigator.onLine){
          SocioService.buscar($scope.socio.id).then(function(socio) {
            $scope.socio = socio;
            SessionService.persist('socio', $scope.socio);
          });
        }
        $scope.buscarPedidos();
      }
    }
  });


  $scope.buscarPedidos = function(){

    email = $scope.socio.email;
    cpf   = $scope.socio.cpf;

    if(navigator.onLine){

      $scope.flagOnline = 'Online';

      AloService.login(email, cpf).then(function(result){
        // console.log(result)
        if(result == 401){ //usuário não encontrado
          $scope.pedidos = [];
          $scope.socioLogado = true;
          $scope.dadosCarregados = true;
          $ionicLoading.hide();
        } else {
          AloService.buscarPedidos(email, cpf).then(function(result){
            // console.log(result)
            if(result == 401){ //usuário não encontrado
              $scope.pedidos = [];
            } else {
              $scope.pedidos = result;
              SessionService.persist('pedidos', result);
            };
            $scope.socioLogado = true;
            $scope.dadosCarregados = true;
            $ionicLoading.hide();
          });
        };
      });

    }else{

      $scope.flagOnline = 'Offline';

      var pedidos     = SessionService.get('pedidos');
      $scope.pedidos  = JSON.parse(pedidos);
      // console.log($scope.pedidos);
      if(pedidos){
        $scope.socioLogado = true;
        $scope.dadosCarregados = true;
        $ionicLoading.hide();
      }else{
        $scope.socioLogado      = false;
        $scope.dadosCarregados  = true;
        $ionicLoading.hide();
      }
    }

  }


  $scope.irParaPedido = function(pedido) {
    $state.go('tab.pedido', {pedido:pedido});
  };


  $scope.pullToRefresh = function() {
    $scope.pedidos  = [];
    $scope.dadosCarregados  = false;
    $scope.buscarPedidos();
    $scope.$broadcast('scroll.refreshComplete');
  };

  $scope.irCadastro = function(){ 
    $state.go('landing');
  };

  $scope.irLogin = function(){
    $state.go('landing');
  }; 

  $scope.irParaDestino = function(src) {
    window.open(src,  '_system', 'location=no');
  };

})