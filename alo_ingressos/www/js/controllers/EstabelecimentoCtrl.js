angular.module('ciklus').controller('EstabelecimentoCtrl', function($scope, $stateParams, EstabelecimentoService, $sce, $cordovaGeolocation, $ionicLoading) {
  $scope.estabelecimento = { };
  $scope.urlImagens = imagens;
  $scope.mostrandoMapa = false;
  $scope.dadosCarregados = false;

  // $ionicLoading.show({
  //   template: 'Carregando...'
  // });
  $ionicLoading.show();

  $scope.$on('$ionicView.enter', function() {
    if(window.cordova) {
      window.ga.trackView('Estabelecimento');
    }
  });

  EstabelecimentoService.buscarPorId($stateParams.id).then(function(estabelecimento) {
    $scope.mostrandoMapa = false;
    $scope.estabelecimento = estabelecimento;
    $scope.estabelecimento.descricao = $scope.estabelecimento.descricao.replace(/(?:\r\n|\r|\n)/g, '<br />');
    $scope.obterDistanciaEstabelecimento();
    $scope.dadosCarregados = true;
    $ionicLoading.hide();
  });

  $scope.recuperarHorarios1 = function(promocao) {
     var horarios = "";
     if (promocao.horario1 && promocao.horario2) {
       horarios = promocao.horario1 + ' - ' + promocao.horario2;
     }
    return horarios;
  };

  $scope.recuperarHorarios2 = function(promocao) {
    var horarios = "";
    if (promocao.horario3 && promocao.horario4) {
      if (horarios != "") {
        horarios += " & ";
      }
      horarios = promocao.horario1 + ' - ' + promocao.horario2;
      horarios = promocao.horario3 + ' - ' + promocao.horario4;
     }
     return horarios;
  };

  $scope.irParaDestino = function(src) {
      window.open(src,  '_system', 'location=no');
  };

  $scope.trustSrc = function(src) {
    return $sce.trustAsResourceUrl(src);
  };

  $scope.encodeUTF8 = function(s) {
    return unescape(encodeURIComponent(s));
  };

  $scope.decodeUTF8 = function(s) {
    return decodeURIComponent(escape(s));
  };

  $scope.recuperarBackground = function(index) {
    if (index % 2 == 0) {
      return "bg-grey";
    }
  };

  $scope.recuperarCapa = function() {
    var retorno = "";
    if ($scope.estabelecimento.imagens) {
      if ($scope.estabelecimento.imagens.length > 0) {
        $scope.estabelecimento.imagens.forEach(function(x, y) {
          if (x.ordem == 1) {
            retorno = $scope.urlImagens + x.id + '.' + x.extensao;
          }
        });
        return retorno;
      } else {
        return "";
      }
    } else {
      return "";
    }
  };

  $scope.recuperarLogo = function() {
    var retorno = "";
    if ($scope.estabelecimento.imagens.length > 0) {
      $scope.estabelecimento.imagens.forEach(function(x, y) {
        if (x.ordem == 2) {
          retorno = $scope.urlImagens + x.id + '.' + x.extensao;
        }
      });
      return retorno;
    } else {
      return "";
    }
  };

  $scope.mostrarMapa = function() {
    $scope.mostrandoMapa = !$scope.mostrandoMapa;
  };

  $scope.inicializarMapa = function() {
    var endereco = $scope.estabelecimento.endereco + ", " + $scope.estabelecimento.numero + " - " + $scope.estabelecimento.bairro + ", " + $scope.estabelecimento.cidade;
    return "https://maps.google.com/maps?&amp;q=" + encodeURIComponent(endereco) + "&amp;output=embed";
  };

  $scope.obterDistanciaEstabelecimento = function() {
    if (window.cordova) {
      var options = {timeout: 10000, enableHighAccuracy: true};
      $cordovaGeolocation.getCurrentPosition(options).then(function(position) {
        var myLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        var myDestination = new google.maps.LatLng($scope.estabelecimento.latitude, $scope.estabelecimento.longitude);
        $scope.estabelecimento.distancia = (google.maps.geometry.spherical.computeDistanceBetween(myLocation, myDestination) / 1000).toFixed(2) + "km";
        $scope.$apply()
        }, function(error){
          console.log("Could not get location");
      });
    }
  };

})