angular.module('ciklus').controller('EstabelecimentosCtrl', function($scope, $stateParams, EstabelecimentoService, $state, $ionicModal, $sce, $cordovaGeolocation, SessionService) {
  $scope.imagem = "";
  $scope.urlImagens = imagens;
  $scope.orderOptionVar = "";
  $scope.modalDia = {};
  $scope.estabelecimentoCategoria = "Estabelecimentos";
  $scope.diaAtual;
  $scope.localizacao = {};
  $scope.posicaoAtual = {};
  $scope.retorno = [];
  $scope.cidade = localStorage.getItem('cidade');

  $scope.$on('$ionicView.enter', function(event, data) {
    if(window.cordova) {
      window.ga.trackView('Estabelecimentos');
    }
    
    if($scope.estabelecimentos == undefined){ //Cleiton - If inserido para que não fique carregando quando clicar em Voltar depois que entra dentro de um estabelecimento. Antes ficava carregando toda vez q voltava.
      $scope.carregarEstabelecimentos();
      $scope.orderOption(1);
    }

  });

  $scope.fecharModalDia = function() {
    $scope.modalDia.hide();
  };

  $scope.fecharModalCidade = function() {
    $scope.modalCidade.hide();
  };

  if (window.cordova) {
    var options = {timeout: 10000, enableHighAccuracy: true};
    $cordovaGeolocation.getCurrentPosition(options).then(function(position) {
      $scope.posicaoAtual = position;
    });
  }

  $scope.obterDistanciaEstabelecimento = function(estabelecimento) {
    if (window.cordova && $scope.posicaoAtual && estabelecimento.latitude && estabelecimento.longitude) {
      var myLocation = new google.maps.LatLng($scope.posicaoAtual.coords.latitude, $scope.posicaoAtual.coords.longitude);
      var myDestination = new google.maps.LatLng(estabelecimento.latitude, estabelecimento.longitude);
      // estabelecimento.distancia = (google.maps.geometry.spherical.computeDistanceBetween(myLocation, myDestination) / 1000).toFixed(2);
      // return ' - ' + (google.maps.geometry.spherical.computeDistanceBetween(myLocation, myDestination) / 1000).toFixed(2) + " km";
      var dist = (google.maps.geometry.spherical.computeDistanceBetween(myLocation, myDestination) / 1000).toFixed(2);
      estabelecimento.distancia = parseFloat(dist);
      return ' - ' + dist + " km";
    }
  };

  $scope.carregarEstabelecimentos = function() {
    $scope.estabelecimentos = [];
    $scope.estFiltrados = [];
    if ($stateParams.categoria) {
      $scope.estabelecimentoCategoria = $stateParams.categoria.nome || $stateParams.categoria.categoria;
      EstabelecimentoService.buscarPorCategoria($stateParams.categoria.id).then(function(data) {
        if (data.length > 0){
          $scope.estabelecimentos = $scope.estabelecimentos.concat(data);
          $scope.estFiltrados = $scope.estabelecimentos.slice(0);
        }
      });
    }else{
      EstabelecimentoService.buscarTodos().then(function(data) {
        if (data.length > 0){
          $scope.estabelecimentos = $scope.estabelecimentos.concat(data);
          $scope.estFiltrados = $scope.estabelecimentos.slice(0);
        }
      });
    }
  };

  $scope.fecharTeclado = function() {
    if (window.cordova) {
      window.cordova.plugins.Keyboard.close();
    }
  };

  $scope.trocarDia = function() {
    $ionicModal.fromTemplateUrl('templates/modal-trocar-dia.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modalDia = modal;
      $scope.modalDia.show();
    });
  };

  $scope.trocarCidade = function() {
    EstabelecimentoService.buscarCidades().then(function(cidades) {
      $scope.cidades = cidades.rows;
      $ionicModal.fromTemplateUrl('templates/modal-trocar-cidade.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.modalCidade = modal;
        $scope.modalCidade.show();
      });
    });
  };

  $scope.trocarCidadePara = function(cidade) {
    SessionService.persist('cidade', cidade);
    $scope.cidade = cidade;
    $scope.modalCidade.hide();
  };

  $scope.buscarNomeDia = function() {
    switch($scope.diaAtual) {
      case 0:
        return "Domingo";
        break;
      case 1:
        return "Segunda-feira";
        break;
      case 2:
        return "Terça-feira";
        break;
      case 3:
        return "Quarta-feira";
        break;
      case 4:
        return "Quinta-feira";
        break;
      case 5:
        return "Sexta-feira";
        break;
      case 6:
        return "Sábado";
        break;
      default:
        return "";
        break;
    }
  };

  $scope.trocarDiaPara = function(dia) {
    $scope.diaAtual = dia;
    $scope.estFiltrados = [];
    $scope.estabelecimentos.forEach(function(x) {
      var push = false;
      x.promocoes.forEach(function(y) {
        switch (dia) {
          case 0:
            if (y.domingo) push = true;
            break;
          case 1:
            if (y.segundaFeira) push = true;
            break;
          case 2:
            if (y.tercaFeira) push = true;
            break
          case 3:
            if (y.quartaFeira) push = true;
            break
          case 4:
            if (y.quintaFeira) push = true;
            break
          case 5:
            if (y.sextaFeira) push = true;
            break
          case 6:
            if (y.sabado) push = true;
            break
        }
      });
      if (push) $scope.estFiltrados.push(x);
    });
    $scope.modalDia.hide();
  };

  $scope.irParaEstabelecimento = function(value) {
    if ($state.current.name == "tab.estabelecimentosagenda")
      $state.go('tab.estabelecimentoagenda', { id:value });
    else
      $state.go('tab.estabelecimento', { id: value });
  };

  $scope.formatarDias = function(promocoes) {
    var dias = [];
    var seg, ter, qua, qui, sex, sab, dom = false;
    if (promocoes.length > 0) {
      promocoes.forEach(function(promocao) {
        if (promocao.segundaFeira && !seg) {
          if (!seg) {
            dias.push('Seg');
            seg = true;
          }
        }
        if (promocao.tercaFeira) {
          if (!ter)
            dias.push('Ter');
            ter = true;
        }
        if (promocao.quartaFeira) {
          if (!qua)
            dias.push('Qua');
          qua = true;
        }
        if (promocao.quintaFeira) {
          if (!qui)
            dias.push('Qui');
          qui = true;
        }
        if (promocao.sextaFeira) {
          if (!sex)
            dias.push('Sex');
          sex = true;
        }
        if (promocao.sabado) {
          if (!sab)
            dias.push('Sab');
          sab = true;
        }
        if (promocao.domingo) {
          if (!dom)
            dias.push('Dom');
          dom = true;
        }
      });
      return dias.join(', ');
    } else {
      return "";
    }
  };

  $scope.calcularDesconto = function(promocoes) {
    if (promocoes.length > 0) {
      var promocao = 0;
      promocoes.forEach(function(x) {
        if (x.desconto > promocao)
          promocao = x.desconto;
      });
      return promocao;
    } else {
      return "0";
    }
  };

  $scope.calcularSocioMais = function(promocoes) {
    if (promocoes.length > 0) {
      var socioMais = 0;
      promocoes.forEach(function(x) {
        if (x.desconto > socioMais)
          socioMais = x.socioMais;
      });
      return socioMais;
    } else {
      return "0";
    }
  };

  $scope.recuperarFiltroAtivo = function(option) {
    var opt = "";
    switch (option) {
      case 0:
        opt = 'nome';
        break;
      case 1:
        opt = 'distancia';
        break;
    }

    if ($scope.orderOptionVar == opt)
      return "bg-pink white";
  };

  $scope.getOrder = function() {
    return $scope.orderOptionVar;
  };

  $scope.orderOption = function(option) {
    switch (option) {
      case 0:
        $scope.orderOptionVar = 'nome';
        break;
      case 1:
        $scope.orderOptionVar = 'distancia';
        break;
    }
  };

  $scope.trustSrc = function(src) {
    return $sce.trustAsResourceUrl(src);
  };

  $scope.recuperarImagem = function(est) {
    var retorno = "";
    if (est.imagens.length > 0) {
      est.imagens.forEach(function(x, y) {
        if (x.ordem == 1) {
          retorno = $scope.urlImagens + x.id + '.' + x.extensao;
        }
      });
    }
    return retorno;
  };

  $scope.limparFiltros = function() {
    $scope.orderOptionVar = null;
    $scope.diaAtual = null;
    $scope.carregarEstabelecimentos();
  };

  $scope.pullToRefresh = function() {
    // $ionicLoading.show();
    $scope.carregarEstabelecimentos();
    $scope.orderOption(1);
    $scope.$broadcast('scroll.refreshComplete');
    // $ionicLoading.hide(); 
  }

})