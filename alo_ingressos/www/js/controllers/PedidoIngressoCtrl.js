angular.module('ciklus').controller('PedidoIngressoCtrl', function($scope, $state, $stateParams, $ionicPopup, AloService, $ionicLoading, $cordovaScreenshot, ionImgCacheSrv) {
  
	$scope.ingresso = $stateParams.ingresso;
	$scope.pedido 	= $stateParams.pedido;
  $scope.imagem   = {
    url: ""
  };

  //QRCode com cache
  link = "https://api.qrserver.com/v1/create-qr-code/?size=140x140&data=" + $scope.ingresso.barcode; 
  ionImgCacheSrv.checkCacheStatus(link).then(function(path) {
    // already cached
    $scope.QRCode = path;
  }).catch(function(err) {
    // not there, need to cache the image
    ionImgCacheSrv.add(link)
    .then(function(path) {
      $scope.QRCode = path;
    })
    .catch(function(err) {
      console.error(err);
    });
  });

  // BarCode com cache
  link = "http://bwipjs-api.metafloor.com/?bcid=code128&text=" + $scope.ingresso.barcode + "&includetext&scaleX=2&scaleY=1";
  ionImgCacheSrv.checkCacheStatus(link).then(function(path) {
    // already cached
    $scope.BarCode = path;
  }).catch(function(err) {
    // not there, need to cache the image
    ionImgCacheSrv.add(link)
    .then(function(path) {
      $scope.BarCode = path;
    })
    .catch(function(err) {
      console.error(err);
    });
  });


  $scope.imageLoaded = function(object) {
    object.loaded = true;
  }

	
	$scope.salvar = function(nome, documento) {

		if (nome == undefined | documento == undefined){
      $ionicPopup.alert({
        title: 'Ops',
        template: 'Por favor, preencha os campos Nome e Documento (CPF ou RG).'
      });
      return false;
    }
    if (nome.length == 0 | documento.length == 0){
      $ionicPopup.alert({
        title: 'Ops',
        template: 'Por favor, preencha os campos Nome e Documento (CPF ou RG).'
      });
      return false;
    }

		var confirmPopup = $ionicPopup.confirm({
    		title: 'Salvar',
    		template: 'Tem certeza que deseja registrar o Nome e o Documento neste Ingresso?<br><br>Estas informações não poderão ser alteradas.'
  	});
  
  	confirmPopup.then(function(res) {
  		if(res) {
  			$ionicLoading.show();
          	AloService.addHolder($scope.ingresso.barcode, nome, documento.toString()).then(function(result){
              if(result == 401){
                $ionicPopup.alert({
                  title: 'Problemas na conexão',
                  template: 'Os dados não foram registrados no ingresso. </br></br>Para realizar esta ação você precisa estar conectado.'
                });
              }else{
	          		$scope.ingresso.customer = {
		      			'name'		  : nome,
		      			'document'	: documento }
              }
              $ionicLoading.hide();
          	});
        };
   	});
	}

  $scope.irParaDestino = function(src) {
   		window.open(src,  '_system', 'location=no');
  	};

  $scope.screenShot = function(){
  	
    var x = $cordovaScreenshot.capture('ingresso', 'png', 100);
    $ionicPopup.alert({
      title: 'Ingresso salvo',
      template: 'Seu ingresso foi salvo na sua fototeca.'
    });

  	// $cordovaScreenshot.capture('ingresso', 'png', 100).then(function(result) {
   // 		console.log('ok');
   // 		$ionicPopup.alert({
   //      	title: 'Ingresso salvo',
   //      	template: 'Seu ingresso foi salvo na sua fototeca.'
   //    	});

  	// }, function(error) {
  	// 	console.error(error);
  	// 	$ionicPopup.alert({
   //      	title: 'Erro',
   //      	template: 'Seu ingresso não foi salvo - ' + error
   //    	});
    		
  	// });

  };                                      

})