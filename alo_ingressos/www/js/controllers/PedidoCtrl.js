angular.module('ciklus').controller('PedidoCtrl', function($scope, SessionService, AloService, $stateParams, $state) {
  
	$scope.pedido 			= $stateParams.pedido;
	$scope.dadosCarregados 	= false;

	$scope.$on('$ionicView.enter', function(event, data) {
		if(window.cordova) {
		  window.ga.trackView('Pedido');
		}

		if($scope.ingressos == undefined){

			if(navigator.onLine){
				AloService.buscarIngressos($scope.pedido.orderNumber).then(function(result){
					$scope.ingressos = result;
					SessionService.persist('ingressos' + $scope.pedido.orderNumber, result);
					$scope.dadosCarregados 	= true;
				});
			}else{
				var ingressos = SessionService.get('ingressos' + $scope.pedido.orderNumber);
				$scope.ingressos = JSON.parse(ingressos);
				// console.log($scope.ingressos);
				$scope.dadosCarregados 	= true;
			}
		}
	});
	

  	$scope.irParaIngresso = function(ingresso) {
    	$state.go('tab.ingresso', { ingresso: ingresso, pedido: $scope.pedido });
  	};

  	$scope.retornarLinkQrCode = function(){
    	barcode = $scope.pedido.barcode;
    	link = "https://api.qrserver.com/v1/create-qr-code/?size=200x200&data=";
    	return link + barcode;
  	};

})