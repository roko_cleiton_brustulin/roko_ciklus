angular.module('ciklus').controller('SonhoCtrl', function($scope, CategoriaService, $state, $ionicHistory, EstabelecimentoService, $ionicModal, SessionService, $ionicPopup) {
  
  $scope.meta = 0;

  $scope.calcular = function(sonho) {

    if(sonho.quanto <= 0){
      $ionicPopup.show({
        title: 'Valor incorreta',
        template: 'O valor informada deve ser maior que zero.',
        buttons: [
          { text: 'OK' }
        ]
      });
    };

    var dateObj = new Date();
    var mes = dateObj.getUTCMonth();
    var dia = dateObj.getUTCDate();
    var ano = dateObj.getUTCFullYear();  
    var hoje = new Date(ano, mes, dia,  0, 0);
    var milisec_diff = sonho.quando - hoje;
    var days = Math.floor(milisec_diff / 1000 / 60 / (60 * 24));
    
    if(days < 0){
      $ionicPopup.show({
        title: 'Data incorreta',
        template: 'A data informada deve ser maior que a data atual.',
        buttons: [
          { text: 'OK' }
        ]
      });
    }else{
      $scope.meta = sonho.quanto / days;
    }

  };

})