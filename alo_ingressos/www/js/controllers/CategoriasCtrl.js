angular.module('ciklus').controller('CategoriasCtrl', function($scope, CategoriaService, $state, $ionicHistory, EstabelecimentoService, $ionicModal, SessionService, $ionicPopup, $ionicSideMenuDelegate) {
  $scope.$on('$ionicView.enter', function() {
    if(window.cordova) {
      window.ga.trackView('Categorias');
    }
  });

  $scope.trocarCidade = function() {  
    EstabelecimentoService.buscarCidadesEventos().then(function(cidades) {
      $scope.cidades = cidades.rows;
      $ionicModal.fromTemplateUrl('templates/modal-trocar-cidade.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.modalCidade = modal;
        $scope.modalCidade.show();
      });
    });
  };

  $scope.fecharModalCidade = function() {
    if($scope.cidade){
      $scope.modalCidade.hide();
    }else{
      $ionicPopup.show({
        title: 'Cidade!',
        template: 'Por favor escolha uma cidade para continuar.',
        buttons: [
          { text: 'OK' }
        ]
      }); 
    }
  };

  $scope.trocarCidadePara = function(cidade) {
    SessionService.persist('cidade', cidade);
    $scope.cidade = cidade;
    $scope.modalCidade.hide();
  };

  // FILTRO CIDADE PADRÃO
  var cidade = localStorage.getItem('cidade');
  if (cidade) {
      $scope.cidade = cidade;
  }else{
    //LocalStorage está vazio então abre dialogbox para escolha da cidade
    $scope.trocarCidade();
  }
  // FIM FILTRO CIDADE PADRAO


  CategoriaService.resumoCategorias().then(function(data) {
    $scope.categorias = data;
  });

  $scope.irParaEstabelecimentos = function(categoria) {
    $ionicHistory.clearCache().then(function(){
      $state.go('tab.estabelecimentos', { categoria: categoria });
    });
  };

  $scope.irParaEventos = function() {
    $state.go('tab.eventos');
  };
})