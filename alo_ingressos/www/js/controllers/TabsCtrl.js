angular.module('ciklus').controller('TabsCtrl', function($scope, $state, $ionicHistory, SessionService, NotificacaoService, varNotQtde) {
  
  $scope.platform = ionic.Platform.platform();

    if (SessionService.get('socio')) {
      $scope.user = JSON.parse(SessionService.get('socio'));

      NotificacaoService.buscarQtde($scope.user.id).then(function(result){
      if (result[0].qtde == 0){
        varNotQtde.qtde = '';  
      } else {
        varNotQtde.qtde = result[0].qtde;
      }

    });
      
  } else {
    $scope.user = null;
    varNotQtde.qtde = '';
  }

  

  $scope.retornaQtde = function(){
    return varNotQtde.qtde;
  }

  $scope.nl2br = function(str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
  }

  $scope.irParaAgenda = function() {
    $state.go('tab.agenda');
  };

  $scope.irParaCategorias = function() {
    $state.go('tab.categorias');
  };

  $scope.irParaEventos = function() {
    $state.go('tab.eventos');
  };

  $scope.irParaNovidades = function() {
    $state.go('tab.novidades');
  };

  $scope.irParaSonho = function() {
    $state.go('tab.sonho');
  };

  $scope.irParaPerfil = function() {
    $state.go('menu.tab.account');
  };

  $scope.irParaPedidos = function() {
    $state.go('tab.pedidos');
  };

  $scope.irParaDestino = function(src) {
    window.open(src,  '_system', 'location=no');
  };

})