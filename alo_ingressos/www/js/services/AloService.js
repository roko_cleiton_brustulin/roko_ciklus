angular.module('ciklus.services').service('AloService',  ['$http', 'Base64', 'token', 'ionImgCacheSrv', function($http, Base64, token, ionImgCacheSrv) {
  return {

    login: function(email, cpf){
      cpf = cpf.replace(/[\.-]/g, "");
      username = 'alo.sga';
      password = 'iniciar';
      var authdata = Base64.encode(username + ':' + password);

      var req = {  
        method: 'POST',  
        url: restAlo + 'login',  
        headers: {  
             'Content-Type': 'application/json',
             'Authorization': 'Basic ' + authdata
        },  
        data: {
          'login': email,
          'password':cpf
        }
       };
       return $http(req).then(function(response){
        token.Alo = response.data.token;
        return response.data;
      }, function(err) {
        return 401; //Usuário não encontrado
      });
    },

  	buscarPedidos: function(email, cpf) {
      var req = {  
        method: 'GET',
        url: restAlo + 'order',  
        headers: {  
             'Content-Type': 'application/json',
             'Authorization': token.Alo
        }
      };
      return $http(req).then(function(response){
        return response.data;
      });
    },


    buscarIngressos: function(orderNumber) {
        var req = {  
          method: 'GET',  
          url: restAlo + 'ticketsale/' + orderNumber,  
          headers: {  
               'Content-Type': 'application/json',
               'Authorization': token.Alo
          }
        };
        return $http(req).then(function(response){
          return response.data;
        });
    },

    addHolder: function(barcode, nome, documento) {

      var req = {  
        method: 'POST',  
        url: restAlo + 'customer/' + barcode,
        headers: {  
             'Content-Type': 'application/json',
             'Authorization': token.Alo
        },
        data: {
          'name': nome,
          'document':documento
        }
      };
      return $http(req).then(function(response){
        return response.data;
      }, function(err) {
        return 401; //Usuário não encontrado
      });
    },
    
  }

}]);