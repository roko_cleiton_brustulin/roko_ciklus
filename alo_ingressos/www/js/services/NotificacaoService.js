angular.module('ciklus.services').service('NotificacaoService', ['$http', function($http) {
  return {
    buscar: function(data_criacao) {
       return $http.get(rest + 'notificacao/find', { params: { where: {"empresa": { 'like': '%' + empresa + '%' }, "createdAt": {'>=': data_criacao} }, sort: 'createdAt DESC' } }).then(function(response) {
        return response.data;
      });
    },

    marcarLido: function(notificacao, socios) {
      return $http.put(rest + 'notificacao/update/' + notificacao, { socioLido: socios }).then(function(response) {
          return response.data;
      });
    },

    buscarQtde: function(socioId) {
      return $http.get(rest + 'notificacaoQtde/find', { params: {socio: socioId} }).then(function(response) {
        return response.data;
      });
    },

    zerarQtde: function(socioId) {
        return $http.get(rest + 'notificacaoQtde?socio=' + socioId).then(function(response) {
          if(response.data[0].qtde != 0){
            return $http.put(rest + 'notificacaoQtde/update/' + response.data[0].id, { qtde: 0 }).then(function(response2) {
              return true;
           });
          };
          return true;
      });
    }
  }
}]);