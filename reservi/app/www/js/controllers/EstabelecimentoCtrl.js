angular.module('ciklus').controller('EstabelecimentoCtrl', function($scope, $stateParams, EstabelecimentoService, $sce, $cordovaGeolocation, $ionicLoading, $ionicModal, $ionicPopup, CheckinService, SessionService, $state) {
  $scope.estabelecimento  = { };
  $scope.urlImagens       = imagens;
  $scope.mostrandoMapa    = false;
  $scope.dadosCarregados  = false;
  $scope.estabelecimento  = $stateParams.estabelecimento;

  if (SessionService.get('socio')) {
    $scope.socio = JSON.parse(SessionService.get('socio'));
  } else {
    $scope.socio = null;
  }

  $ionicLoading.show();

  $scope.$on('$ionicView.enter', function() {
    if(window.cordova) {
      window.ga.trackView('Estabelecimento');
    }
  });

  $scope.slideOptions = {
    loop: true,
    effect: 'slide',
    speed: 2000,
    slidesPerView: 3,
    paginationClickable: true,
    showNavButtons: false,
    autoplay: 2000,
    centeredSlides: true
  }

  if($scope.estabelecimento == null){
    EstabelecimentoService.buscarPorId($stateParams.id).then(function(estabelecimento) {
      $scope.mostrandoMapa = false;
      $scope.estabelecimento = estabelecimento;
      $scope.estabelecimento.descricao = $scope.estabelecimento.descricao.replace(/(?:\r\n|\r|\n)/g, '<br />');
      $scope.obterDistanciaEstabelecimento();
      $scope.dadosCarregados = true;
      $ionicLoading.hide();
    });
  }else{
    $scope.mostrandoMapa = false;
    $scope.estabelecimento.descricao = $scope.estabelecimento.descricao.replace(/(?:\r\n|\r|\n)/g, '<br />');
    $scope.dadosCarregados = true;
    $ionicLoading.hide();  
  }


  if($scope.estabelecimento == null){
    id = $stateParams.id;
  }else{
    id = $scope.estabelecimento.id;
  }


  EstabelecimentoService.buscarProdutos(id).then(function(produtos){
    $scope.produtos = produtos;
  });


  CheckinService.pesquisar(id, $scope.socio.id).then(function(result){
    if(result.length > 0){
      $scope.checkin = true;
      $scope.checkin_reg = result[0];
    }else{
      $scope.checkin = false;
    }
  });


  $scope.recuperarHorarios1 = function(promocao) {
     var horarios = "";
     if (promocao.horario1 && promocao.horario2) {
       horarios = promocao.horario1 + ' - ' + promocao.horario2;
     }
    return horarios;
  };

  $scope.recuperarHorarios2 = function(promocao) {
    var horarios = "";
    if (promocao.horario3 && promocao.horario4) {
      if (horarios != "") {
        horarios += " & ";
      }
      horarios = promocao.horario1 + ' - ' + promocao.horario2;
      horarios = promocao.horario3 + ' - ' + promocao.horario4;
     }
     return horarios;
  };

  $scope.irParaDestino = function(src) {
      window.open(src,  '_system', 'location=no');
  };

  $scope.trustSrc = function(src) {
    return $sce.trustAsResourceUrl(src);
  };

  $scope.encodeUTF8 = function(s) {
    return unescape(encodeURIComponent(s));
  };

  $scope.decodeUTF8 = function(s) {
    return decodeURIComponent(escape(s));
  };

  $scope.recuperarBackground = function(index) {
    if (index % 2 == 0) {
      return "bg-grey";
    }
  };

  $scope.recuperarCapa = function() {
    var retorno = "";
    if ($scope.estabelecimento.imagens) {
      if ($scope.estabelecimento.imagens.length > 0) {
        $scope.estabelecimento.imagens.forEach(function(x, y) {
          if (x.ordem == 1) {
            retorno = $scope.urlImagens + x.id + '.' + x.extensao;
          }
        });
        return retorno;
      } else {
        return "";
      }
    } else {
      return "";
    }
  };

  $scope.recuperarLogo = function() {
    var retorno = "";
    if ($scope.estabelecimento.imagens.length > 0) {
      $scope.estabelecimento.imagens.forEach(function(x, y) {
        if (x.ordem == 2) {
          retorno = $scope.urlImagens + x.id + '.' + x.extensao;
        }
      });
      return retorno;
    } else {
      return "";
    }
  };


  $scope.recuperarImagem = function(produto) {
    var retorno = "";
    if (produto.foto.id > 0) {
      retorno = $scope.urlImagens + produto.foto.id + '.' + produto.foto.extensao;
        return retorno;
    } else {
      return "";
    }
  };


  $scope.mostrarMapa = function() {
    $scope.mostrandoMapa = !$scope.mostrandoMapa;
  };

  $scope.inicializarMapa = function() {
    var endereco = $scope.estabelecimento.endereco + ", " + $scope.estabelecimento.numero + " - " + $scope.estabelecimento.bairro + ", " + $scope.estabelecimento.cidade;
    return "https://maps.google.com/maps?&amp;q=" + encodeURIComponent(endereco) + "&amp;output=embed";
  };

  $scope.obterDistanciaEstabelecimento = function() {
    if (window.cordova) {
      var options = {timeout: 10000, enableHighAccuracy: true};
      $cordovaGeolocation.getCurrentPosition(options).then(function(position) {
        var myLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        var myDestination = new google.maps.LatLng($scope.estabelecimento.latitude, $scope.estabelecimento.longitude);
        $scope.estabelecimento.distancia = (google.maps.geometry.spherical.computeDistanceBetween(myLocation, myDestination) / 1000).toFixed(2) + "km";
        $scope.$apply()
        }, function(error){
          console.log("Could not get location");
      });
    }
  };

  $scope.abrirModalProduto = function(produto) {
    $scope.produto = produto;
    console.log(produto);
    $ionicModal.fromTemplateUrl('templates/estabelecimento-produto.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modalProduto = modal;
      $scope.modalProduto.show();
    });
  };

  $scope.fecharModalProduto = function() {
    $scope.modalProduto.hide();
  };


  $scope.solicitarCheckin = function(promocao){
    $scope.data = new Date();
    var weekdays = new Array("Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado");
    var day = $scope.data.getDay();
    $scope.weekday = weekdays[day];

    var month = new Array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");
    var mes = $scope.data.getMonth();
    $scope.mes = month[mes];

    $scope.promocao = promocao;
    $ionicModal.fromTemplateUrl('templates/modal-checkin-solicitar.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modalCheckin = modal;
      $scope.modalCheckin.show();
    });
  };

  $scope.realizarCheckin = function(){

    var confirmPopup = $ionicPopup.confirm({
      title: 'Confirmação',
      template: 'Você já está no local e deseja utilizar o desconto agora?',
      buttons: [
              {
                text: '<b>Não</b>',
                type: 'button-negative',
                onTap: function(e) {
                  $scope.modalCheckin.hide();
                }
              },
              {
                text: '<b>Sim</b>',
                type: 'button-positive',
                onTap: function(e) {
                  $ionicLoading.show();

                  CheckinService.realizar($scope.estabelecimento.id, $scope.socio.id).then(function(result){

                    $scope.checkin_reg = result;
                    $scope.checkin = true;

                    $ionicLoading.hide();
                    $scope.modalCheckin.hide();

                    $scope.mostrarCheckin($scope.promocao);
                    
                    $ionicPopup.show({
                      title: 'Pronto',
                      template: 'Apresente a confirmação para quem te atender e receba o seu desconto!',
                      buttons: [
                        {
                          text: '<b>OK</b>',
                          type: 'button-positive',
                          onTap: function(e) {
                            
                          }
                        }
                      ]
                    });
                  });
                }
              }
            ]
    });
  };

  $scope.mostrarCheckin = function(promocao){

    $scope.data = new Date($scope.checkin_reg.data);
    var weekdays = new Array("Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado");
    var day = $scope.data.getDay();
    $scope.weekday = weekdays[day];

    var month = new Array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");
    var mes = $scope.data.getMonth();
    $scope.mes = month[mes];

    $scope.promocao = promocao;

    $ionicModal.fromTemplateUrl('templates/modal-checkin-feito.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modalCheckin = modal;
      $scope.modalCheckin.show();
    });
  };

  $scope.fecharModalCheckin = function() {
    $scope.modalCheckin.hide();
  };
  

  $scope.voltar = function() {
    $state.go('tab.estabelecimentos', { categoria: $scope.estabelecimento.categoriaSelecionada });
  };

})