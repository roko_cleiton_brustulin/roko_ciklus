angular.module('ciklus').controller('EventoCtrl', function($scope, EstabelecimentoService, $stateParams) {

  $scope.evento = $stateParams.evento;

  $scope.formatarData = function(data) {
    var d = data.split('-');
    return d[2].substring(0, 2) + '/' + d[1] + '/' + d[0];
  };

  $scope.recuperarImagem = function(evento) {
    if (evento.foto) {
      return imagens + evento.foto.id + '.' + evento.foto.extensao;
    }
  };

  $scope.trustSrc = function(src) {
    return $sce.trustAsResourceUrl(src);
  };

  $scope.irParaDestino = function(src) {
      window.open(src,  '_system', 'location=no');
  };
  
})