angular.module('ciklus').controller('CategoriasCtrl', function($scope, SocioService, CategoriaService, $state, $ionicHistory, EstabelecimentoService, $ionicModal, SessionService, $ionicPopup, $ionicSideMenuDelegate, $ionicLoading) {

  $scope.checkItems = { };

  $ionicLoading.show();

  $scope.$on('$ionicView.enter', function() {
    if(window.cordova) {
      window.ga.trackView('Categorias');
    }
  });

  if (SessionService.get('socio')) {
    $scope.socio = JSON.parse(SessionService.get('socio'));
    empresa = $scope.socio.empresa;
  } else {
    $scope.socio = null;
  }


  // Cidade Fixa - Curitiba
  SessionService.persist('cidade', 'Curitiba');
  $scope.cidade = 'Curitiba';


  // $scope.trocarCidade = function() {  
  //   EstabelecimentoService.buscarCidades().then(function(cidades) {
  //     $scope.cidades = cidades.rows;
  //     $ionicModal.fromTemplateUrl('templates/modal-trocar-cidade.html', {
  //       scope: $scope,
  //       animation: 'slide-in-up'
  //     }).then(function(modal) {
  //       $scope.modalCidade = modal;
  //       $scope.modalCidade.show();
  //     });
  //   });
  // };

  // $scope.fecharModalCidade = function() {
  //   if($scope.cidade){
  //     $scope.modalCidade.hide();
  //   }else{
  //     $ionicPopup.show({
  //       title: 'Cidade!',
  //       template: 'Por favor escolha uma cidade para continuar.',
  //       buttons: [
  //         { text: 'OK' }
  //       ]
  //     }); 
  //   }
  // };

  // $scope.trocarCidadePara = function(cidade) {
  //   SessionService.persist('cidade', cidade);
  //   $scope.cidade = cidade;
  //   $scope.modalCidade.hide();
  // };


  // FILTRO CIDADE PADRÃO
  var cidade = localStorage.getItem('cidade');
  if (cidade) {
      $scope.cidade = cidade;
  }else{
    // LocalStorage está vazio então abre dialogbox para escolha da cidade
    // $scope.trocarCidade();

    SessionService.persist('cidade', 'Curitiba');
    $scope.cidade = 'Curitiba';

  }
  // FIM FILTRO CIDADE PADRAO


  CategoriaService.resumoCategorias().then(function(data) {
    $scope.categorias = data;
    $ionicLoading.hide();
  });



  $scope.irParaEstabelecimentos = function(categoria) {

    $ionicLoading.show();

    $scope.categoriaSelecionada = categoria;

    //Cidade Fixa - Curitiba
    SessionService.persist('cidade', 'Curitiba');
    $scope.cidade = 'Curitiba';

    var cidade = localStorage.getItem('cidade');
    
    if (!cidade){

      $ionicLoading.hide();
      // $scope.trocarCidade();      

    }else{

      if(categoria != undefined){

        // BUSCA PRIMEIRO OS PERFIS DO ASSOCIADO
        // CASO NÃO TENHA NENHUM PERFIL ENTÃO ABRE MODAL
        SocioService.buscarPerfisCategoria($scope.socio.id, categoria.id).then(function(result){
          
          if(result.length == 0){

            CategoriaService.buscarPerfisCategoria(categoria.id).then(function(res){

              if(res.length > 0){
                $scope.perfis = res;
        
                $ionicLoading.hide();
                $ionicModal.fromTemplateUrl('templates/modal-perfil-categoria.html', {
                  scope: $scope,
                  animation: 'slide-in-up'
                }).then(function(modal) {
                  $scope.modalPerfilCategoria = modal;
                  $scope.modalPerfilCategoria.show();
                });

              }else{
                $ionicLoading.hide();
                $ionicHistory.clearCache().then(function(){
                  $state.go('tab.estabelecimentos', { categoria: categoria });
                });
              }
            }); 
          }else{
            $ionicLoading.hide();
            $ionicHistory.clearCache().then(function(){
              $state.go('tab.estabelecimentos', { categoria: categoria });
            });
          }
        }); 

      }else{
        $ionicLoading.hide();
        $state.go('tab.estabelecimentos', { categoria: '' });
      }

    }

  };

  $scope.irParaEventos = function() {
    $state.go('tab.eventos');
  };

  $scope.fecharModalPerfilCategoria = function(checkItems) {
    $scope.checkItems = checkItems;
  
    perfis = "";
    for(i in $scope.checkItems) {
        if($scope.checkItems[i] == true) {
            perfis += i + ";";
        }
    };

    if(perfis == ""){
      
      $ionicPopup.show({
        title: 'Perfil / Estilo!',
        template: 'Por favor escolha pelo menos um dos perfis/estilos da lista.',
        buttons: [
          { text: 'OK' }
        ]
      }); 

    }else{
      
      dados = {
        socio:      $scope.socio.id,
        categoria:  $scope.categoriaSelecionada.id,
        perfil:     perfis
      }

      SocioService.salvarPerfisCategoria(dados).then(function(result){
        $scope.checkItems = {};
        $scope.modalPerfilCategoria.hide();
        $ionicHistory.clearCache().then(function(){
          $state.go('tab.estabelecimentos', { categoria: $scope.categoriaSelecionada });
        });
      });
    }    
  };

})