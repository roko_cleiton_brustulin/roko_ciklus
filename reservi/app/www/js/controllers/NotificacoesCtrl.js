angular.module('ciklus').controller('NotificacoesCtrl', function($scope, NotificacaoService, $ionicLoading, $state, SessionService, SocioService, $sce, varNotQtde, $timeout) {

  $scope.notificacoes     = [];
  $scope.dadoscarregados  = false;
  
  if (SessionService.get('socio')) {
    $scope.socio = JSON.parse(SessionService.get('socio'));
  } else {
    $scope.socio = null;
  }

  //Zera badge do Tab Novidades
  varNotQtde.qtde = '';

  //Zera q quantidade de notificação não lidas para o sócio
  NotificacaoService.zerarQtde($scope.socio.id).then(function(result){
    // console.log("qtde zerada");
  });

  $scope.contador = [];

  $scope.$on('$ionicView.enter', function() {
    if ($scope.notificacoes.length == 0) {
      $ionicLoading.show();      
      NotificacaoService.buscar($scope.socio.createdAt).then(function(result) {
        $scope.notificacoes = result;
        $scope.notificacoes.forEach(function(x, item, index) {
          if(x.socio != null){
            var arr = x.socio.split(';').filter(String);
            var indice = arr.indexOf($scope.socio.id.toString());
            if(indice == -1){
                $scope.contador.splice(0,0,item); //guarda variável dos itens que não devem ser mostrados
            }
          }
        });

        // excluir os itens que não devem ser mostrados. Foi preciso colocar fora do forEach pela ordenação
        for (var i = 0; i < $scope.contador.length; i++) {
            $scope.notificacoes.splice($scope.contador[i], 1);
        }

        $scope.dadoscarregados  = true;
        $ionicLoading.hide();

      });
    }

    if(window.cordova) {
      window.ga.trackView('Notificacoes');
    }

  });


  $scope.pullToRefresh = function() {
    $ionicLoading.show();
    $scope.notificacoes  = [];
    
    NotificacaoService.buscar($scope.socio.createdAt).then(function(result) {
      $scope.notificacoes = result;
      $scope.notificacoes.forEach(function(x, item, index) {
        if(x.socio != null){
          var arr = x.socio.split(';').filter(String);
          var indice = arr.indexOf($scope.socio.id.toString());
          if(indice == -1){
            $scope.contador.splice(0,0,item); //guarda variável dos itens que não devem ser mostrados
          }
        }
      });

      // excluir os itens que não devem ser mostrados. Foi preciso colocar fora do forEach pela ordenação
      for (var i = 0; i < $scope.contador.length; i++) {
        $scope.notificacoes.splice($scope.contador[i], 1);
      }

      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
    });
  }


  $scope.retornaFormato = function(dataPost){
    var s = 1000;
    var m = s * 60;
    var h = m * 60;
    var d = h * 24;
    var mes = d * 30;

    var dat = new Date(dataPost);
    var dat = Date.now() - dat.getTime();

    if (dat > mes)
      return '' + (dat / mes).toFixed(0) + ' meses atrás'
    if (dat > d)
      return '' + (dat / d).toFixed(0) + ' dias atrás'
    if (dat > h)
        return '' + (dat / h).toFixed(0) + ' horas atrás';
    if (dat > m)
      return '' + (dat / m).toFixed(0) + ' minutos atrás';
    if (dat > s)
      return '' + (dat / s).toFixed(0) + ' segundos atrás';    
    return '' + (dat).toFixed(0) + ' ms atrás';
  };


  $scope.irLink = function(notificacao) {

    //Verifica se o sócio já clicou no link, aí não precisa atualizar novamente, só vai pro link
    if (notificacao.socioLido) {

      var arr = notificacao.socioLido.split(';').filter(String);
      var index = arr.indexOf($scope.socio.id.toString());
      if(index >= 0){
        // $state.go(notificacao.link);
      } else {
        notificacao.socioLido = notificacao.socioLido + $scope.socio.id + ';';
        NotificacaoService.marcarLido(notificacao.id, notificacao.socioLido).then(function(res) {});
      }
    } else {

      notificacao.socioLido = $scope.socio.id + ';';
      NotificacaoService.marcarLido(notificacao.id, notificacao.socioLido).then(function(res) {});

    };
  
    //Direciona a página
    if(notificacao.link != null & notificacao.link != ''){
      if(notificacao.parametro != null & notificacao.parametro != ''){
        $state.go(notificacao.link, { id:notificacao.parametro });
      }else{
        $state.go(notificacao.link);
      };
    };

  };

  $scope.verificaLida = function(notificacao){
    var style = {};

    if (notificacao.socioLido) {
      var arr = notificacao.socioLido.split(';').filter(String);
      var index = arr.indexOf($scope.socio.id.toString());
      if(index >= 0){
        style.fontWeight = 'normal';
        // style.background = 'white'
      } else {
        style.fontWeight = 'bold';
        // style.background = '#fafafa'
      }
    } else {
      style.fontWeight = 'bold';
      // style.background = '#fafafa'
    }

    return style;
  };

})