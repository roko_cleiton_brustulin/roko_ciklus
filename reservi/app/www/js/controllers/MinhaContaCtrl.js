angular.module('ciklus').controller('MinhaContaCtrl', function($scope, $ionicPlatform, SessionService, $ionicPopup, SocioService, TransacaoService, $state, IuguService, $ionicModal, $ionicLoading, EstabelecimentoService, PlanoService, $ionicSideMenuDelegate, md5, EmailService, ionImgCacheSrv, $filter, CategoriaService) {
 
  $scope.urlImagens = imagens;
  $scope.texto = "";

  $scope.openMenu = function () {
    $ionicSideMenuDelegate.toggleLeft();
  }


  $scope.modalCadastro = {};
  $scope.aba = 0;
  $scope.preferencias = true;
  if (SessionService.get('socio')) {
    $scope.socio = JSON.parse(SessionService.get('socio'));
    empresa = $scope.socio.empresa;
  } else {
    $scope.socio = null;
  }
  
  $scope.transacoes = [];
  $scope.somaTransacoes = 0;
  $scope.busca = {};
  $scope.modalFaturas = {};
  $scope.faturas = [];
  $scope.comentario = {};
  $scope.cartoes = [];
  $scope.cartao = {};
  $scope.dadosCarregados = false;
  $scope.socioLogado = false;
  $scope.checkItems = { }
  $scope.estiloUsuario = "";
  $scope.estilo = {outro: ''};

  CategoriaService.buscarPerfisCategoria(null).then(function(result){
    $scope.perfis = result;

    $scope.retornaEstiloUsuario($scope.socio.estilo);
    
  });


  $scope.mostrarAjuda = function(indice){

    if(indice == 1){
        titulo = 'Saldo em Créditos';
        texto = 'Acompanhe seu saldo para utilizar os benefícios do clube';
    }else if(indice == 2){
        // titulo = 'Reserva Ideal';
        // texto = 'A reserva financeira é um fundo de emergência mínimo que deve possuir pelo menos o equivalente para cobrir seis meses do seu custo familiar.<br><br>É um senso comum que qualquer um está sujeito a imprevistos, seja com um problema de saúde, um dano material ou a perda de um negócio. Ninguém sabe a hora que vai precisar de um dinheiro extra. <br><br>Daí é que vem aquela conhecida frase:<br>“É melhor prevenir do que remediar”.';
    }

    $ionicPopup.show({
        title: titulo,
        template: texto,
        scope: $scope,
        buttons: [
            {
                text: 'Entendi',
                type: 'button-positive',
                onTap: function(e) {
                
                }
            }
        ]
    });
  };

  $scope.irParaEstilo = function(){

    $scope.checkItems[0] = $scope.socio.estilo;

    $ionicModal.fromTemplateUrl('templates/modal-estilo.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modalEstilo = modal;
      $scope.modalEstilo.show();
    });    
  };

  $scope.fecharModalEstilo = function(opcao) {

    $scope.socio.estilo = opcao;
    if($scope.socio.estilo != '7'){
      $scope.socio.estiloOutro = '';
    }

    $scope.retornaEstiloUsuario($scope.socio.estilo);

    $scope.modalEstilo.hide();
    
  };

  $scope.retornaEstiloUsuario = function(estilo){
    $scope.estiloUsuario = "";
    var arr = estilo.split(';').filter(Number);
    for(x in $scope.perfis) {
      for(y in arr){
        if($scope.perfis[x].id == arr[y]){
          $scope.checkItems[arr[y]] = true;
          if($scope.estiloUsuario == ""){
            $scope.estiloUsuario = $scope.perfis[x].titulo;
          }else{
            $scope.estiloUsuario += ', ' + $scope.perfis[x].titulo;
          }
        }
      }
    }
  };

  $scope.abrirInput = function(){

    $ionicPopup.show({
      template: '<input type="text" ng-model="estilo.outro">',
      title: 'Digite o seu objetivo',
      scope: $scope,
      buttons: [
        {
          text: '<b>Salvar</b>',
          type: 'button-positive',
          onTap: function(e) {
            $scope.socio.estiloOutro = $scope.estilo.outro;
          }
        }
      ]
    });
  };

  $scope.retornarLinkQrCode = function(){
    //23/12/2019 - Alterado de CPF para Código
    // cpf = $scope.socio.cpf;
    cpf = $scope.socio.codigo;
    
    link = "https://api.qrserver.com/v1/create-qr-code/?size=200x200&data=";
    return link + cpf;
  };

  $scope.irCadastro = function(){ 
    $state.go('landing');
  };

  $scope.irLogin = function(){
    $state.go('landing');
  };  

  $scope.pullToRefresh = function() {
    TransacaoService.buscarPorSocio($scope.socio.id).then(function(results) {
      $scope.transacoes = [];
      $scope.somaTransacoes = 0;
      $scope.transacoes = results;
      $scope.transacoes.forEach(function (val) {
        $scope.somaTransacoes += val.valorDesconto;
      });
      $scope.$broadcast('scroll.refreshComplete');
    });
  };

  $scope.recuperarSocioIugu = function() {
  };

  $scope.recuperarCartoes = function() {
    if ($scope.socio.iugu) {
      IuguService.buscarCartoes($scope.socio.iugu.customer.id).then(function(resultado) {
        $scope.cartoes = [];
        $scope.cartoes = resultado;
      }, function(err) {
        $scope.cartoes = [];
      });
    } else {
      $scope.cartoes = [];
    }
  };

  $scope.editarNota = function(nota, index) {
    $scope.comentario.texto = nota.observacao;
    $ionicPopup.show({
      title: 'Avalie sua experiência',
      template: "<input type='text' placeholder='Deixe seu comentário (opcional) e depois clique na nota desejada...' ng-model='comentario.texto' />",
      scope: $scope,
      buttons: [
        {
          text: '<i class="ion-ios-star-outline"></i> Ruim',
          onTap: function(e) {
            $ionicLoading.show();
            EstabelecimentoService.avaliarTransacao(nota.id, 1, $scope.comentario.texto).then(function(response) {
              $ionicLoading.hide();
              nota.nota = 1;
              nota.observacao = $scope.comentario.texto;
              $scope.comentario.texto = "";
            });
          }
        },
        {
          text: '<i class="ion-ios-star-half"></i> <br/>Boa',
          onTap: function(e) {
            $ionicLoading.show();
            EstabelecimentoService.avaliarTransacao(nota.id, 2, $scope.comentario.texto).then(function(response) {
              $ionicLoading.hide();
              nota.nota = 2;
              nota.observacao = $scope.comentario.texto;
              $scope.comentario.texto = "";
            });
          }
        },
        {
          text: '<i class="ion-ios-star"></i> Ótima',
          onTap: function(e) {
            $ionicLoading.show();
            EstabelecimentoService.avaliarTransacao(nota.id, 3, $scope.comentario.texto).then(function(response) {
              $ionicLoading.hide();
              nota.nota = 3;
              nota.observacao = $scope.comentario.texto;
              $scope.comentario.texto = "";
            });
          }
        }
      ]
    });
  };

  $scope.excluirCartao = function(cartao, index) {
    $ionicLoading.show();
    IuguService.removerCartao($scope.socio.idIugu, cartao.id).then(function(resultado) {
      $scope.cartoes.splice(index, 1);
      $ionicLoading.hide();
      $ionicPopup.show({
        template: 'Cartão removido',
        buttons: [{ text: 'Ok' }]
      });
    });
  };

  $scope.recuperarPlano = function() {
    if ($scope.socio.idIugu) {
      if($scope.iugu){
        if($scope.iugu.status == 'canceled'){
          return "Cancelado";
        }else{
          return $scope.iugu.plan.name;
        }
      } else {
        return "-";
      }
    } else if($scope.socio.ativoAte == null){
      return "Ingresso";
    }else{
      return "Convite";
    }
  };


  $scope.$on('$ionicView.enter', function() {

    if(window.cordova) {
      window.ga.trackView('MinhaConta');
    }

    $ionicLoading.show();

    var socio = SessionService.get('socio');
    if(!socio){

      $scope.socioLogado      = false;
      $scope.dadosCarregados  = true;
      $scope.aba = 0
      $ionicLoading.hide();
      
    } else {
      
      $scope.socio = JSON.parse(socio);
      SocioService.buscar($scope.socio.id).then(function(socio) {
        $scope.socio = socio;
        SessionService.persist('socio', $scope.socio);
        $scope.socioLogado = true;
        TransacaoService.buscarPorSocio($scope.socio.id).then(function(results) {
          $scope.transacoes = [];
          $scope.somaTransacoes = 0;
          $scope.transacoes = results;
          $scope.transacoes.forEach(function (val) {
            $scope.somaTransacoes += val.valorDesconto;
          });
        });
        if ($scope.socio.idIugu) {
          IuguService.buscarAssinatura($scope.socio.idIugu).then(function(result) {
            $scope.iugu       = result;
            $scope.socio.iugu = result;
            $scope.recuperarCartoes();
            $scope.recuperarExpiracao();
            $scope.recuperarPlano();
            $scope.dadosCarregados = true;
            $ionicLoading.hide();
          }, function(err) {
            $scope.dadosCarregados = true;
            $ionicLoading.hide();
          });
        } else {
          $scope.recuperarExpiracao();
          $scope.dadosCarregados = true;
          $ionicLoading.hide();
        }
      }, function(err) {
        $ionicLoading.hide();
      });
    }
  });

  $scope.recuperarExpiracao = function() {
    if ($scope.socio.iugu) {
      validade = $scope.socio.iugu.current_period_end;
    } else {
      validade = $scope.socio.ativoAte;
    }
    if(validade == null){
      $scope.status = false;
      return "";
    }else{
      var hoje = new Date().toISOString();
      if (validade.substring(0, 10) >= hoje.substring(0,10)){
        $scope.status = true;
      }else{
        $scope.status = false;
      }
      return $scope.formatarData(validade);
    }
  };

  $scope.formatarDataFront = function(data) {
    var d = data.split('-');
    return d[2] + '/' + d[1] + '/' + d[0];
  };

  $scope.cadastrarCartao = function() {
    if ($scope.cartao.numero.toString().length == 16 && ($scope.cartao.cvv.toString().length == 3 || $scope.cartao.cvv.toString().length == 4)
      && $scope.cartao.nome.length > 3 && $scope.cartao.mes.length == 2 && $scope.cartao.ano.length == 2) {
      $scope.cartao.numero = $scope.cartao.numero.toString();
      $scope.cartao.cvv = $scope.cartao.cvv.toString();
      $ionicLoading.show();
      if ($scope.assinarPlano) {
        IuguService.associarContaPagamento($scope.socio, $scope.cartao, $scope.assinarPlano.idPlataforma).then(function(pagamento) {
          var resultado = pagamento;
          if (resultado) {
            if (resultado.current_transaction.status == "paid") {
              $scope.socio.idIugu = resultado.id;
              $ionicLoading.hide();
              SocioService.atualizar($scope.socio).then(function(response) {
                $ionicPopup.show({
                  title: 'Sucesso',
                  template: 'Assinatura realizada com sucesso',
                  buttons: [
                    { 
                      text: 'OK', 
                      onTap: function() {
                        IuguService.buscarAssinatura($scope.socio.idIugu).then(function(result) {
                          $scope.socio.iugu = result;
                          // $scope.recuperarSocioIugu();
                          $scope.recuperarCartoes();
                        });
                        $scope.cartao = { numero: '', cvv: '', nome: '', mes: '', ano: '' };
                        $scope.modalCartao.hide();
                      }
                    }
                  ]
                });
              });
            } else {
              $ionicLoading.hide();
              $ionicPopup.show({
                title: 'Erro ao realizar pagamento',
                template: 'Por favor, verifique os dados preenchidos e tente novamente.',
                buttons: [
                  { text: 'OK' }
                ]
              });
            }
          } else {
            $ionicLoading.hide();
            $ionicPopup.show({
              title: 'Erro ao realizar pagamento',
              template: 'Por favor, verifique os dados preenchidos e tente novamente.',
              buttons: [
                { text: 'OK' }
              ]
            });
          }
        }, function(err) {
          $ionicLoading.hide();
          $ionicPopup.show({
            title: 'Erro ao realizar pagamento',
            template: 'Por favor, verifique os dados preenchidos e tente novamente.',
            buttons: [
              { text: 'OK' }
            ]
          });
        });
      } else {
        IuguService.criarCartao($scope.cartao.nome, $scope.cartao.numero.toString(), $scope.cartao.cvv.toString(), $scope.cartao.mes, $scope.cartao.ano, $scope.socio.iugu.customer.id).then(function(cartao) {
          IuguService.alterarCartaoAssinatura($scope.socio.idIugu, cartao.id, $scope.socio.iugu.customer.id).then(function(res) {
            IuguService.buscarAssinatura($scope.socio.idIugu).then(function(result) {
              $scope.socio.iugu = result;
              // $scope.recuperarSocioIugu();
              $scope.recuperarCartoes();
            });
            $ionicLoading.hide();
            $ionicPopup.show({
              title: 'Cartão salvo',
              template: "Seu cartão foi alterado com sucesso!",
              scope: $scope,
              buttons: [
                {
                  text: 'Ok',
                  onTap: function() {
                    $scope.cartao = { numero: '', cvv: '', nome: '', mes: '', ano: '' };
                    $scope.modalCartao.hide();
                  }
                }
              ]
            });
          }, function(err) {
            $ionicLoading.hide();
            $ionicPopup.show({
                title: 'Dados inválidos',
                template: "Por favor, verifique os dados do cartão.",
                scope: $scope,
                buttons: [
                  {
                    text: 'Ok'
                  }
                ]
              });
          });
        });      
      }
    } else {
      $ionicLoading.hide();
      $ionicPopup.show({
        title: 'Dados inválidos',
        template: "Por favor, verifique os dados do cartão.",
        scope: $scope,
        buttons: [
          {
            text: 'Ok'
          }
        ]
      });
    }
  };

  $scope.avaliarTransacao = function(transacao) {
    $ionicPopup.show({
      title: 'Avalie sua experiência',
      template: "<input type='text' placeholder='Deixe seu comentário (opcional) e depois clique na nota desejada...' ng-model='comentario.texto' />",
      scope: $scope,
      buttons: [
        {
          text: '<i class="ion-ios-star-outline"></i> Ruim',
          onTap: function(e) {
            $ionicLoading.show(); 
            EstabelecimentoService.avaliarTransacao(transacao.id, 1, $scope.comentario.texto).then(function(response) {
              $ionicLoading.hide();
              transacao.nota = 1;
              transacao.observacao = $scope.comentario.texto;
              $scope.comentario.texto = "";
            });
          }
        },
        {
          text: '<i class="ion-ios-star-half"></i> <br/>Boa',
          onTap: function(e) {
            $ionicLoading.show();
            EstabelecimentoService.avaliarTransacao(transacao.id, 2, $scope.comentario.texto).then(function(response) {
              $ionicLoading.hide();
              transacao.nota = 2;
              transacao.observacao = $scope.comentario.texto;
              $scope.comentario.texto = "";
            });
          }
        },
        {
          text: '<i class="ion-ios-star"></i> Ótima',
          onTap: function(e) {
            $ionicLoading.show();
            EstabelecimentoService.avaliarTransacao(transacao.id, 3, $scope.comentario.texto).then(function(response) {
              $ionicLoading.hide();
              transacao.nota = 3;
              transacao.observacao = $scope.comentario.texto;
              $scope.comentario.texto = "";
            });
          }
        }
      ]
    });
  };

  $scope.buscarIconeNota = function(nota) {
    var classe = "";
    switch (nota) {
      case 1:
        classe = "ion-ios-star-outline";
        break;
      case 2:
        classe = "ion-ios-star-half";
        break;
      case 3:
        classe = "ion-ios-star";
        break;
    }
    return classe;
  };

  $scope.formatarPreco = function(preco) {
    preco = preco.toString();
    if (preco.length > 1) {
      var precoFormatado = preco.substr(0, preco.length - 2) + ','
                          + preco.substr(preco.length - 2, preco.length - 1);
      return precoFormatado;
    } else {
      return "";
    }
  };

  $scope.formatarData = function(data) {
    if (data)
      return data.substring(8, 10) + '/' + data.substring(5, 7) + '/' + data.substring(0, 4);
    else
      return "";
  };

  $scope.logout = function() {
    var confirmPopup = $ionicPopup.confirm({
      title: 'Logout',
      template: 'Deseja sair da sua conta?'
    });
    
    confirmPopup.then(function(res) {
       if(res) {
          $scope.socio = null;
          // SessionService.destroy('socio');
          SessionService.clear();
          ionImgCacheSrv.clearCache();
          $scope.socioLogado = false;
          $state.go('landing');
        }
     });

  };

  $scope.formatarPreco = function(preco) {
    preco = preco.toString();
    if (preco.length > 1) {
      var precoFormatado = preco.substr(0, preco.length - 2) + ','
                          + preco.substr(preco.length - 2, preco.length - 1);
      return precoFormatado;
    } else {
      return "";
    }
  };

  $scope.formatarData = function(data) {
    if (data)
      return data.substring(8, 10) + '/' + data.substring(5, 7) + '/' + data.substring(0, 4);
    else
      return "";
  };

  $scope.showPreferencias = function() {
    $scope.preferencias = !$scope.preferencias;
  };

  $scope.mudarAba = function(aba) {
    $scope.aba = aba;
  };

  $scope.alterarCartao = function() {
    $ionicModal.fromTemplateUrl('templates/modal-cartao.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modalCartao = modal;
      $scope.modalCartao.show();
    });
  };

  $scope.fecharCartao = function() {
    $scope.modalCartao.hide();
  };

  $scope.alterarPlano = function() {
    $ionicLoading.show();
    
    PlanoService.buscarPlanos().then(function(planos) {
      $scope.planos = planos;
      $ionicModal.fromTemplateUrl('templates/modal-planos.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.modalPlanos = modal;
        $scope.modalPlanos.show();
        $ionicLoading.hide();
      });
    });    
  };

  $scope.fecharPlanos = function() {
    $scope.modalPlanos.hide();
  };

  $scope.mudarPlano = function(plano) {

    if ($scope.iugu) {
      $ionicPopup.show({
        title: 'Deseja realmente alterar o seu plano?',
        template: 'A alteração será realizada à partir da próxima cobrança.',
        scope: $scope,
        buttons: [
          {
            text: 'Sim, alterar',
            type: 'button-positive',
            onTap: function(e) {
              $ionicLoading.show();
              if ($scope.iugu.status != "canceled") {
                IuguService.alterarAssinatura($scope.socio.idIugu, plano.idPlataforma).then(function(resultado) {
                  IuguService.buscarAssinatura($scope.socio.idIugu).then(function(result) {
                    $ionicLoading.hide();
                    $ionicPopup.show({
                      title: 'Sucesso',
                      template: 'Seu plano foi alterado com sucesso.',
                      scope: $scope,
                      buttons: [
                        {
                          text: 'Ok!',
                        }
                      ]
                    });
                    $scope.socio.iugu = result;
                  });
                  $scope.modalPlanos.hide();
                }, function(err) {
                  console.log(err);
                });
              } else {
                var cartao = $scope.iugu.card;

                IuguService.associarContaPagamento($scope.socio, cartao, plano.idPlataforma).then(function(pagamento) {
                  var resultado = pagamento;
                  if (resultado) {
                    if (resultado.current_transaction.status == "paid") {
                      $scope.socio.idIugu = resultado.id;
                      $ionicLoading.hide();
                      SocioService.atualizar($scope.socio).then(function(response) {
                        $ionicPopup.show({
                          title: 'Sucesso',
                          template: 'Assinatura realizada com sucesso',
                          buttons: [
                            { 
                              text: 'OK', 
                              onTap: function() {
                                IuguService.buscarAssinatura($scope.socio.idIugu).then(function(result) {
                                  $scope.socio.iugu = result;
                                  // $scope.recuperarSocioIugu();
                                  $scope.recuperarCartoes();
                                });
                                $scope.cartao = { numero: '', cvv: '', nome: '', mes: '', ano: '' };
                                $scope.modalPlanos.hide();
                              }
                            }
                          ]
                        });
                      });
                    } else {
                      $ionicLoading.hide();
                      $ionicPopup.show({
                        title: 'Erro ao realizar pagamento',
                        template: 'Por favor, verifique os dados preenchidos e tente novamente.',
                        buttons: [
                          { text: 'OK' }
                        ]
                      });
                    }
                  } else {
                    $ionicLoading.hide();
                    $ionicPopup.show({
                      title: 'Erro ao realizar pagamento',
                      template: 'Por favor, verifique os dados preenchidos e tente novamente.',
                      buttons: [
                        { text: 'OK' }
                      ]
                    });
                  }
                }, function(err) {
                  $ionicLoading.hide();
                  $ionicPopup.show({
                    title: 'Erro ao realizar pagamento',
                    template: 'Por favor, verifique os dados preenchidos e tente novamente.',
                    buttons: [
                      { text: 'OK' }
                    ]
                  });
                });
              }
            }
          },
          {
            text: 'Cancelar',
            onTap: function(e) {
              $scope.modalPlanos.hide();
            }
          }
        ]
      });
    } else {
      $scope.assinarPlano = plano;
      $ionicModal.fromTemplateUrl('templates/modal-cartao.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.modalCartao = modal;
        $scope.modalPlanos.hide();
        $scope.modalCartao.show();
      });
    }
  };

  $scope.cancelarPlano = function(plano) {
    if ($scope.iugu) {
      $ionicPopup.show({
        title: 'Deseja realmente cancelar a sua assinatura?',
        template: 'Fazendo o cancelamento as próximas cobranças não serão efetuadas e você ficará impossibitado de economizar pela Ciklus.</br></br>Tem certeza que não quer mais economizar?',
        scope: $scope,
        buttons: [
          {
            text: 'Sim, cancelar',
            type: 'button-positive',
            onTap: function(e) {
              IuguService.cancelarAssinatura($scope.iugu.id).then(function(response) {
                IuguService.buscarAssinatura($scope.socio.idIugu).then(function(result) {
                  $scope.iugu = result;
                  $scope.socio.iugu = result;
                  // $scope.recuperarSocioIugu();
                  $scope.recuperarCartoes();
                  $scope.recuperarExpiracao();
                  $scope.recuperarPlano();
                  $scope.dadosCarregados = true;
                  $ionicLoading.hide();
                }, function(err) {
                  $ionicLoading.hide();
                  $scope.dadosCarregados = true;
                });
                $ionicPopup.show({
                  title: 'Pronto',
                  template: 'Sua assinatura foi cancelada com sucesso.',
                   buttons: [
                    { text: 'Ok',
                      onTap: function(e){
                        $scope.modalPlanos.hide();
                      }
                    }
                  ]
                });
              });
            }
          },
          {
            text: 'Cancelar',
            onTap: function(e) {
              $scope.modalPlanos.hide();
            }
          }
        ]
     });
    }
  }

  $scope.salvar = function() {
    // if ($scope.socio.nome && $scope.socio.cep && $scope.socio.dataNasc && $scope.socio.telefone && $scope.socio.email) {
    if ($scope.socio.nome) {

      // email_antigo = JSON.parse(SessionService.get('socio')).email;
      // if ($scope.socio.email != email_antigo){
      //   var confirmPopup = $ionicPopup.confirm({
      //     title: 'Alteração de e-mail!',
      //     template: 'Você alterou o seu e-mail para ' + $scope.socio.email + '<br><br>Você precisará realizar a validação deste e-mail.'
      //   });
      //   confirmPopup.then(function(res) {
      //     if(res) {

      //       $ionicLoading.show();

      //       EmailService.enviar_email_validacao($scope.socio.nome, $scope.socio.email, $scope.socio.id).then(function(result){

      //         $scope.socio.emailValido = false;
      //         SocioService.atualizar($scope.socio).then(function(result) {
              
      //           SessionService.persist('socio', result);

      //           $ionicLoading.hide();
                
      //           $ionicPopup.show({
      //             title: 'Aguardando Validação',
      //             template: 'Foi enviado um e-mail para ' + $scope.socio.email + '.<br><br>Acesse a sua caixa de e-mail e clique no link para validação.',
      //             buttons: [
      //               {
      //                 text: '<b>OK</b>',
      //                 type: 'button-positive',
      //                 onTap: function(e) {
      //                   $scope.aguardandoValidacao();
      //                 }
      //               }
      //             ]
      //           });
      //         });
      //       });
      //     }else{
      //       return false;
      //     }
      //   });
      // }else{
        $ionicLoading.show();
        SocioService.atualizar($scope.socio).then(function(result) {
          $ionicLoading.hide();
          SessionService.persist('socio', result);
          $ionicPopup.show({
            title: 'Atualizado!',
            template: 'Seus dados foram atualizados com sucesso.',
            buttons: [
              { text: 'OK' }
            ]
          });
        });
      // }
    } else {
      $ionicPopup.show({
        template: 'Ops, preencha todos os dados',
        buttons: [{
          text: 'Ok'
        }]
      });
    }
  };


  $scope.aguardandoValidacao  = function(){

    SocioService.buscar($scope.socio.id).then(function(socio) {
      SessionService.persist('socio', socio);
      if (socio.emailValido == false){
        $ionicPopup.show({
          title: 'E-mail não confirmado',
          template: 'O seu e-mail ainda não foi confirmado.<br><br>Acesse a sua caixa de e-mail e clique no link para validação antes de continuar.',
          buttons: [
            { text: 'Continuar',
              onTap: function(e){
                $scope.aguardandoValidacao();
              }
            }
          ]
        });  
      }else{
        $ionicPopup.show({
          title: 'Parabéns',
          template: 'O seu e-mail foi confirmado.<br><br>Pode navegar a vontade pelo nosso aplicativo.',
          buttons: [
            { text: 'OK',
              onTap: function(e){
                e.preventDefault();
              }
            }
          ]
        });  
      }
    });
  }

  $scope.irFaturas = function() {
    $scope.faturas = [];
    $scope.cartoes = [];
    if ($scope.socio.iugu) {
      $ionicLoading.show();
      IuguService.buscarFaturas($scope.socio.idIugu).then(function(results) {
        if (results.length > 0) {
          IuguService.buscarCartoes(results[0].customer.id).then(function(resultado) {
            $scope.cartoes = resultado;
          });
        }
        $ionicLoading.hide();
        $scope.faturas = results;
      });
    };
    $state.go("menu.tab.faturas");
  };

  $scope.irPerfil = function(){
    $state.go('menu.tab.perfil');
  }

  $scope.irCartao = function(){
    $state.go('menu.tab.account');
  }

  $scope.irTransacoes = function(){
    $state.go('menu.tab.transacoes');
  }

  $scope.irSugestao = function(){
    $scope.texto = "";
    $state.go('menu.tab.sugestao');
  }

  $scope.irSenha = function(){
    $state.go('menu.tab.senha');
  }

  $scope.irInstrucoes = function(){
    $state.go('menu.tab.instrucoes');
  }

  $scope.salvarSenha = function(senha, senha_nova, senha_confirma) {

    if (senha == undefined | senha_nova == undefined | senha_confirma == undefined){
      $ionicPopup.alert({
        title: 'Ops',
        template: 'Por favor, preencha todos os campos.'
      });
      return false;
    }

    if (senha.length == 0 | senha_nova.length == 0 | senha_confirma == 0){
      $ionicPopup.alert({
        title: 'Ops',
        template: 'Por favor, preencha todos os campos.'
      });
      return false;
    };

    if (senha_nova.length < 4 | senha_confirma < 4){
      $ionicPopup.alert({
        title: 'Ops',
        template: 'A nova senha deverá ter mais do que 4 caracteres.'
      });
      return false;
    };

    if (senha_nova != senha_confirma){
      $ionicPopup.alert({
        title: 'Ops',
        template: 'A confirmação da senha não está igual a senha informada.'
      });
      return false;
    };

    senha_hash = md5.createHash(senha);
    if (senha_hash != $scope.socio.senha){
      $ionicPopup.alert({
        title: 'Ops',
        template: 'A senha atual não confere com a sua senha.'
      });
      return false;
    };

    $ionicLoading.show();
    SocioService.alterarSenha($scope.socio.id, senha_nova).then(function() {
      $ionicLoading.hide();
      $ionicPopup.alert({
        title: 'Sucesso',
        template: 'Sua senha foi alterada.'
      });
    });

  };


  $scope.enviarSugestao = function(texto){

    if(texto == undefined){
      $ionicPopup.alert({
        title: 'Ops',
        template: 'Digite a sua dúvida, sugestão ou crítica no campo e clique em Enviar.'
      });
      return false;
    }
    if(texto.length == 0 ){
      $ionicPopup.alert({
        title: 'Ops',
        template: 'Digite a sua dúvida, sugestão ou crítica no campo e clique em Enviar.'
      });
      return false;
    }

    $ionicLoading.show();
    EmailService.enviar_sugestao($scope.socio.nome, $scope.socio.email, "Dúvida/Sugestão - via APP", texto).then(function(data) {
      $ionicLoading.hide();
      $ionicPopup.show({
        title: 'Obrigado!',
        template: 'Sua dúvida/sugestão/crítica foi recebida por nós.<br><br>Em breve responderemos no seu e-mail!<br><br>Obrigado ',
        buttons: [
          { text: 'Ok',
           onTap: function(e) {
            
           }
          }
        ]
      });
    })

  };

  $scope.fecharBemVindo = function(){
    $state.go('tab.categorias');
  };

  $scope.recuperarLogoEmpresa = function() {
    
    retorno = $scope.urlImagens + 'logo_empresa_' + empresa + '.png';
    
    return retorno;

  };


});