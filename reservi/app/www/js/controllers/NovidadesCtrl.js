angular.module('ciklus').controller('NovidadesCtrl', function($scope, NovidadeService, NotificacaoService, $ionicLoading, EstabelecimentoService, $state, SessionService, SocioService, $sce, varNotQtde, $ionicModal) {
  $scope.novidades    = [];
  $scope.notificacoes = [];
  $scope.url          = "http://cikluslive.com.br/admin/uploads/";
  $scope.urlImagens   = imagens;
  $scope.cidade       = localStorage.getItem('cidade');
  $scope.offline      = false;

  if (SessionService.get('socio')) {
    $scope.socio = JSON.parse(SessionService.get('socio'));
  } else {
    $scope.socio = null;
  }

  $scope.$on('$ionicView.enter', function() {
    $scope.offline = false;
    if ($scope.novidades.length == 0) {
      $ionicLoading.show();
      NovidadeService.buscarNovidades().then(function(result) {
        $scope.novidades = result;
        result.forEach(function(x) {
          EstabelecimentoService.buscarPorId(x.estabelecimento.id).then(function(res) {
            x.estabelecimento = res;
          });
        });
        $ionicLoading.hide();
      }, function(err) {
        if (navigator.onLine) {
          // não possui nenhuma novidade
        }else{
          // Offline
          $scope.offline = true;
        }
        $ionicLoading.hide();
      });
    };

    //Buscar qtde de notificações que o usuário possui não lidas
    NotificacaoService.buscarQtde($scope.socio.id).then(function(result){
      if(result.length == 0){
        $scope.notificacaoQtde = 0;
      } else {
        $scope.notificacaoQtde  = result[0].qtde;  
      }
    });


    if(window.cordova) {
      window.ga.trackView('Novidades');
    }

  });

  $scope.recuperarLogo = function(estabelecimento) {
    var retorno = "";
    if (estabelecimento.imagens) {
      if (estabelecimento.imagens.length > 0) {
        estabelecimento.imagens.forEach(function(x, y) {
          if (x.ordem == 2) {
            retorno = $scope.urlImagens + x.id + '.' + x.extensao;
          }
        });
        return retorno;
        } else {
        return "";
      }
    } else {
      return "";
    }
  };

  $scope.curtiuNovidade = function(novidade) {
    if (novidade.curtidas) {
      var arr = novidade.curtidas.split(';').filter(String);
      var index = arr.indexOf($scope.socio.id.toString());
      if (index >= 0) {
        return true; 
      } else {
        return false;
      }
    } else {
      return false;
    }
  };

  $scope.curtir = function(novidade) {
    if (novidade.curtidas) {
      novidade.curtidas = novidade.curtidas + $scope.socio.id + ';';
    } else {
      novidade.curtidas = $scope.socio.id + ';';
    }
    NovidadeService.curtir(novidade.id, novidade.curtidas).then(function(res) {
      // console.log(res);
    });
  };

  $scope.descurtir = function(novidade) {
    var arr = novidade.curtidas.split(';').filter(String);
    var index = arr.indexOf($scope.socio.id.toString());
    if (index >= 0) {
      arr.splice(index, 1);
      novidade.curtidas = arr.join(';');
      if (novidade.curtidas != "") 
        novidade.curtidas += ";";
      NovidadeService.curtir(novidade.id, novidade.curtidas).then(function(res) {
        // console.log(res);
      });
    }
  };

  $scope.trustSrc = function(src) {
    return $sce.trustAsResourceUrl(src);
  };

  $scope.irParaLink = function(link, parametro) {

    if(parametro != null & parametro != ''){

      $state.go(link, { id:parametro });

    }else if(link != null & link != ''){

      if(link.substring(0, 3) == 'tab'){
        $state.go(link);
      }else{
        window.open(link,  '_system', 'location=no');  
      }
    }

  };

  $scope.retornaFormato = function(dataPost){
    var s = 1000;
    var m = s * 60;
    var h = m * 60;
    var d = h * 24;
    var mes = d * 30;

    var dat = new Date(dataPost);
    var dat = Date.now() - dat.getTime();

    if (dat > mes)
      return '' + (dat / mes).toFixed(0) + ' meses atrás'
    if (dat > d)
      return '' + (dat / d).toFixed(0) + ' dias atrás'
    if (dat > h)
        return '' + (dat / h).toFixed(0) + ' horas atrás';
    if (dat > m)
      return '' + (dat / m).toFixed(0) + ' minutos atrás';
    if (dat > s)
      return '' + (dat / s).toFixed(0) + ' segundos atrás';    
    return '' + (dat).toFixed(0) + ' ms atrás';
  };

  $scope.pullToRefresh = function() {
    $scope.offline    = false;
    $scope.novidades  = [];
    
    NovidadeService.buscarNovidades().then(function(result) {
      $scope.novidades = result;
      result.forEach(function(x) {
        EstabelecimentoService.buscarPorId(x.estabelecimento.id).then(function(res) {
          x.estabelecimento = res;
        });
      });
      $scope.$broadcast('scroll.refreshComplete');
    }, function(err) {
      if (navigator.onLine) {
        // não possui nenhuma novidade
      }else{
        // Offline
        $scope.offline = true;
      }
      $scope.$broadcast('scroll.refreshComplete');
    });
  }

  $scope.irNotificacoes = function(){
    $scope.badgeCount = 0;
    $state.go('tab.notificacoes');
  }

  $scope.trocarCidade = function() {  
    NovidadeService.buscarCidades().then(function(cidades) {
      $scope.cidades = cidades.rows;
      $ionicModal.fromTemplateUrl('templates/modal-trocar-cidade.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.modalCidade = modal;
        $scope.modalCidade.show();
      });
    });
  };

  $scope.fecharModalCidade = function() {
    $scope.modalCidade.hide();
  };

  $scope.trocarCidadePara = function(cidade) {
    SessionService.persist('cidade', cidade);
    $scope.cidade = cidade;
    $scope.modalCidade.hide();
  };

  $scope.myFilter = function (item) {
    return item.cidade === $scope.cidade || item.cidade === null; 
  };

})