angular.module('ciklus.services').service('GastosService',  ['$http', function($http) {
    return {
      buscar: function(socioId, mes, ano) {
        // console.log(mes + '-' + ano)
        return $http.get(rest + 'gastos/find', { params: { socio: socioId, mes: mes, ano:ano} }).then(function(response) {
          return response.data;
        });
      },

      buscar_total: function(socioId, mes, ano) {
        total = 0;  
        return $http.get(rest + 'gastos/find', { params: {where: { "mes": { "<=" : mes }, "ano" : ano, "socio":socioId  } } }).then(function(response) {
          
          return $http.get(rest + 'gastos/find', { params: {where: {"ano" : {"<" : ano}, "socio":socioId  } } }).then(function(response_2) {

              // console.log(response.data)
              // console.log(response_2.data)
          
              response.data.forEach(function(gasto){
                total += (gasto.receita_1 + gasto.receita_2 + gasto.receita_3 + gasto.receita_outros - gasto.desconto_1) - (gasto.despesa_essencial_1 + gasto.despesa_essencial_2 + gasto.despesa_essencial_3 + gasto.despesa_essencial_4 + gasto.despesa_essencial_5 + gasto.despesa_essencial_6 + gasto.despesa_essencial_7 + gasto.despesa_essencial_8 + gasto.despesa_essencial_9 + gasto.despesa_essencial_10 + gasto.despesa_essencial_outros + gasto.despesa_nao_1 + gasto.despesa_nao_2 + gasto.despesa_nao_3 + gasto.despesa_nao_4 + gasto.despesa_nao_5 + gasto.despesa_nao_6 + gasto.despesa_nao_7 + gasto.despesa_nao_8 + gasto.despesa_nao_8 + gasto.despesa_nao_10 + gasto.despesa_nao_11 + gasto.despesa_nao_outros + gasto.despesa_divida_1 + gasto.despesa_divida_2 + gasto.despesa_divida_3 + gasto.despesa_divida_4 + gasto.despesa_divida_5);
              });
              
              response_2.data.forEach(function(gasto){
                total += (gasto.receita_1 + gasto.receita_2 + gasto.receita_3 + gasto.receita_outros - gasto.desconto_1) - (gasto.despesa_essencial_1 + gasto.despesa_essencial_2 + gasto.despesa_essencial_3 + gasto.despesa_essencial_4 + gasto.despesa_essencial_5 + gasto.despesa_essencial_6 + gasto.despesa_essencial_7 + gasto.despesa_essencial_8 + gasto.despesa_essencial_9 + gasto.despesa_essencial_10 + gasto.despesa_essencial_outros + gasto.despesa_nao_1 + gasto.despesa_nao_2 + gasto.despesa_nao_3 + gasto.despesa_nao_4 + gasto.despesa_nao_5 + gasto.despesa_nao_6 + gasto.despesa_nao_7 + gasto.despesa_nao_8 + gasto.despesa_nao_8 + gasto.despesa_nao_10 + gasto.despesa_nao_11 + gasto.despesa_nao_outros + gasto.despesa_divida_1 + gasto.despesa_divida_2 + gasto.despesa_divida_3 + gasto.despesa_divida_4 + gasto.despesa_divida_5);
              });
              
            // console.log(total);
            return total;
          });
        });
      },


      salvar: function(dados) {
        if(dados.id){
          // console.log('atualizando:');
          // console.log(dados);
          return $http.put(rest + 'gastos/update/' + dados.id, dados).then(function(response) {
            return response.data;
          });  
        }else{
          delete dados.id;
          // console.log('salvando:');
          // console.log(dados);
          return $http.post(rest + 'gastos/create/', dados).then(function(response) {
            return response.data;
          });
        }
        
      },

      
    }
  }]);