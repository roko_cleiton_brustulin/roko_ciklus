angular.module('ciklus.services').service('CategoriaService',  ['$http', function($http) {
  return {
    resumoCategorias: function() {
      return $http.get(rest + 'categoria/find', { params: {where: {categoriaPai: null, status: 1, "empresa": { 'like': '%' + empresa + '%' }}} }).then(function(response) {
        return response.data;
      });
    },

    buscarPerfisCategoria: function(categoria) {
      return $http.get(rest + 'perfil/find', { params: { where: {"empresa": { 'like': '%' + empresa + '%' } , "categoria": categoria} }} ).then(function(response) {
        return response.data;
      });
    }

  }
}]);