angular.module('ciklus.services').service('SocioService',  ['$http', 'md5', function($http, md5) {
  return {
   login: function(cpf, senha) {
      senha = md5.createHash(senha);
      return $http.get(rest + 'socio/find', { params: {where: { cpf: cpf, senha: senha }} }).then(function(response) {
        return response.data;
      });
    },

    validarVoucher: function(voucher) {
      return $http.get(rest + 'reservi_cupom/find', { params: { codigo: voucher, sort: 'utilizado ASC'} }).then(function(result) {
        return result.data;
      });
    },

    atualizar: function(socio) {
      //para evitar que resete a senha do usuário ao atualizar
      delete socio.senha;
      return $http.post(rest + 'socio/update/' + socio.id, socio).then(function(result) {
        return result.data;
      });
    },

    buscarCEP: function(cep) {
      return $http.get("https://viacep.com.br/ws/" + cep + "/json/").then(function(result) {
        return result.data;
      }, function(err) {
        return "erro";
      });
    },

    atualizarOnesignal: function(socio) {
      if (window.cordova) {
        return window.plugins.OneSignal.getIds(function(ids) {
          if (ids) {
            var idPush = ids.userId + ';' + ids.pushToken;
            return $http.post(rest + 'socio/update/' + socio, { onesignalId: idPush }).then(function(response) {
              return response.data;
            });
          } else {
            return true;
          };
        });
      } else {
        return true;
      }
    },

    alterarSenha: function(id, senha) {
      senha = md5.createHash(senha);
      return $http.post(rest + 'socio/update/' + id, { senha: senha }).then(function(result) {
        return true;
      });
    },

    atualizarCodigo: function(id) {
      min = Math.ceil(10);
      max = Math.floor(100);
      cod1 = Math.floor(Math.random() * (max - min)) + min;
      cod2 = Math.floor(Math.random() * (max - min)) + min;
      var codigoSocio = cod1.toString() + id.toString() + cod2.toString();
      return $http.post(rest + 'socio/update/' + id, { codigo: codigoSocio }).then(function(result) {
        return true;
      });
    },

    cadastrarAuxiliar: function(dados) {
      delete dados.id;
      return $http.post(rest + 'socioauxiliar/create/', dados).then(function(results) {
        return results.data;
      });
    },

    cadastrarAuxiliarCEP: function(dados) {
      id = dados.id;
      return $http.post(rest + 'socioauxiliar/update/' + id, dados).then(function(results) {
        return results.data;
      });
    },

    excluirAuxiliar: function(cpf) {
      return $http.delete(rest + 'socioauxiliar/find', {params:{cpf:cpf}}).then(function(auxiliar) {
        id = auxiliar.data[0].id
        return $http.delete(rest + 'socioauxiliar/' + id).then(function(results) {
          return results.data;
        });
      });
    },

    cadastrar: function(dados) {
      return $http.post(rest + 'socio/create', dados).then(function(result) {
        return result.data;
      });
    },

    atualizarVoucher: function(id, utilizado) {
      return $http.post(rest + 'reservi_cupom/update/' + id, { utilizado: utilizado }).then(function(response) {
        return response.data;
      });
    },

    verificarCPF: function(cpf) {
      return $http.get(rest + 'socio/find', { params: { cpf: cpf, empresa:empresa } }).then(function(results) {
        if (results.data.length > 0) {
          return false;
        } else {
          return true;
        }
      });
    },

    verificarEmail: function(email) {
      return $http.get(rest + 'socio/find', { params: { email: email, empresa:empresa } }).then(function(results) {
        if (results.data.length > 0) {
          return false;
        } else {
          return true;
        }
      });
    },

    buscar: function(id) {
      return $http.get(rest + 'socio/' + id).then(function(res) {
        return res.data;
      });
    },

    gerarSenha: function(email) {
      return $http.post('http://cikluslive.com.br/adminCE/action/ajax/criar-senha-app.php?email=' + email).then(function(res) {
        return res.data;
      });
    }, 

    buscarPerfisCategoria: function(id, categoria) {
      return $http.get(rest + 'socioperfil/find', { params: { "socio":id , "categoria": categoria} } ).then(function(response) {
        return response.data;
      });
    },

    salvarPerfisCategoria: function(dados) {
      return $http.post(rest + 'socioperfil/create', dados).then(function(result) {
        return result.data;
      });
    }
  }
}]);