angular.module('ciklus.services').service('IuguService', ['$http', function($http) {
  return {
    // buscarUsuarioIugu: function(idIugu) {
      // return $http.get(restIugu + 'customers/' + idIugu, { params: { "api_token": apiTokenIugu } }).then(function(result) {
      //   return result.data;
      // });
    // },

    buscarAssinatura: function(idIugu) {
      return pagarme.client.connect({ api_key: apiPagarMe })
        .then(client => client.subscriptions.find({ id: idIugu }))
        .then(subscription => subscription);
    },

    buscarCartoes: function(idUsuario) {
      return pagarme.client.connect({ api_key: apiPagarMe })
                            .then(client => client.cards.all({customer_id: idUsuario}))
                            .then(cards => cards);
    },

    buscarPlanos: function() {
      return pagarme.client.connect({ api_key: apiPagarMe })
        .then(client => client.plans.all({ count: 10, page: 1 }))
        .then(plans => plans);
    },

    buscarFaturas: function(socioIuguId) {
      return pagarme.client.connect({ api_key: apiPagarMe })
                            .then(client => client.subscriptions.findTransactions({ id: socioIuguId }))
                            .then(subscription => subscription);
    },

    cancelarAssinatura: function(idAssinatura) {
      return pagarme.client.connect({ api_key: apiPagarMe })
              .then(client => client.subscriptions.cancel({ id: idAssinatura }))
              .then(subscription => subscription);
    },

    alterarAssinatura: function(idAssinatura, plano) {
      return pagarme.client.connect({ api_key: apiPagarMe })
                            .then(client => client.subscriptions.update({ id: idAssinatura, plan_id: plano }))
                            .then(subscription => subscription);
    },

    removerCartao: function(idIugu, idCartao) {
      return $http.post(rest + 'socio/removerCartao', { api_key: apiTokenIugu, cartaoId: idCartao, socioId: idIugu }).then(function(response) {
        return response.data;
      });
    },

    criarCartao: function(nome, numero, cvv, mes, ano, socioIugu) {
      var card = {} 
      card.card_holder_name = nome;
      card.card_expiration_date = mes + '/' + ano;
      card.card_number = numero;
      card.card_cvv = cvv;
      var cardValidations = pagarme.validate({card: card});
      return pagarme.client.connect({ api_key: apiPagarMe })
                    .then(client => client.cards.create({
                      card_number: card.card_number,
                      card_holder_name: card.card_holder_name,
                      card_expiration_date: mes.toString() + ano.toString(),
                      card_cvv: cvv,
                      customer_id: socioIugu
                    }))
                    .then(card => card);
    },

    associarContaPagamento: function(socioAuxiliar, cartao, plano) {
      
      var cpf = socioAuxiliar.cpf.replace(/[^\d]+/g,'');
      
      telefone  = '';
      ddd       = '';
      var tel   = socioAuxiliar.telefone;
      var x     = tel.length
      if(x == 11){
        telefone = tel.substr(2, 9);
        ddd = tel.substr(0, 2);
      };
      if(x == 10){
        telefone = tel.substr(2, 8);
        ddd = tel.substr(0, 2);
      };
      if(x <= 9){
        telefone = tel;
        ddd = '41';
      };

      var card = {};

      if (cartao.id) {
        return pagarme.client.connect({ api_key: apiPagarMe })
          .then(client => client.subscriptions.create({
              plan_id: plano,
              card_id: cartao.id,
              customer: {
                email:            socioAuxiliar.email,
                name:             socioAuxiliar.nome,
                document_number:  cpf,
                address: {
                  street:         socioAuxiliar.rua,
                  street_number:  socioAuxiliar.numero,
                  neighborhood:   socioAuxiliar.bairro,
                  zipcode:        socioAuxiliar.cep
                },
                phone: {
                  ddd:      ddd,
                  number:   telefone
                }
              }
            }).then(result => result)
          );
      } else {
        card.card_holder_name     = cartao.nome;
        card.card_expiration_date = cartao.mes + '/' + cartao.ano;
        card.card_number          = cartao.numero;
        card.card_cvv             = cartao.cvv;
        return pagarme.client.connect({ api_key: apiPagarMe })
          .then(client => client.subscriptions.create({
              plan_id:                plano,
              card_number:            card.card_number,
              card_holder_name:       card.card_holder_name,
              card_expiration_date:   cartao.mes.toString() + cartao.ano.toString(),
              card_cvv:               card.card_cvv,
              customer: {
                email:            socioAuxiliar.email,
                name:             socioAuxiliar.nome,
                document_number:  cpf,
                address: {
                  street:         socioAuxiliar.rua,
                  street_number:  socioAuxiliar.numero,
                  neighborhood:   socioAuxiliar.bairro,
                  zipcode:        socioAuxiliar.cep
                },
                phone: {
                  ddd:      ddd,
                  number:   telefone
                }
              }
            })
          // .then(result => console.log(result), failure => console.log(failure))
          .then(result => result)
          // .catch(error => console.log(error))
          );
      }
    },

    alterarCartaoAssinatura: function(idAssinatura, cartao, socioIugu) {
      return pagarme.client.connect({ api_key: apiPagarMe })
                              .then(client => client.subscriptions.update({ 
                                id: idAssinatura, 
                                card_id: cartao,
                                payment_method: 'credit_card'
                              })).then(subscription => subscription); 
    }
  }
}]);