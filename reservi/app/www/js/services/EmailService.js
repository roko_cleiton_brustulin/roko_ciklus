angular.module('ciklus.services').service('EmailService',  ['$http', function($http) {
  return {
    enviar: function(nome, email, mensagem) {
      return $http.get('http://cikluslive.com.br/adminFTP/action/ajax/enviar-email', { params: {nome: nome, email: email, mensagem: mensagem} }).then(function(response) {
        return response.data;
      });
    },

    enviar_sugestao: function(nome, email, subject, mensagem) {
      return $http.get('http://www.cikluslive.com.br/adminFPT/action/ajax/enviar-email-geral.php', { params: {nome: nome, email: email, subject:subject, mensagem: mensagem} }).then(function(response) {
        // console.log(response.data);
        return response.data;
      });
    },

    enviar_email_validacao: function(nome, email, id) {
      return $http.get('http://cikluslive.com.br/adminFTP/action/ajax/enviar-email-validacao', { params: {nome: nome, email: email, id:id} }).then(function(response) {
        return response.data;
      });
    }
    
  }
}]);