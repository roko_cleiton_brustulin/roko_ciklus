angular.module('ciklus.services').service('EstabelecimentoService',  ['$http', function($http) {
  return {
    buscarPorCategoria: function(categoria) {
      return $http.get(rest + 'estabelecimento/buscarPorCategoria', { params: {categoria: categoria, empresa: empresa} }).then(function(response) {

        estabelecimentos = response.data;
        estabelecimentos.forEach(function(estabelecimento){

          perfis = estabelecimento.perfil;
          if(perfis == null){
            return '';
          }else{
            var arr = perfis.split(';').filter(Number);

            arr.forEach(function(perfil) {

              return $http.get(rest + 'perfil/' + perfil).then(function(response2) {
                if(estabelecimento.perfilDescricao){
                  estabelecimento.perfilDescricao = estabelecimento.perfilDescricao + '/' + response2.data.titulo;
                }else{
                  estabelecimento.perfilDescricao = response2.data.titulo;
                }
                
              });
            
            });
          };
        });

        return response.data;

      });
    },

    buscarTodos: function() {
      return $http.get(rest + 'estabelecimento/buscarTodosApp', { params: {empresa:empresa} }).then(function(response) {

        estabelecimentos = response.data;
        estabelecimentos.forEach(function(estabelecimento){

          perfis = estabelecimento.perfil;
          if(perfis == null){
            return '';
          }else{
            var arr = perfis.split(';').filter(Number);

            arr.forEach(function(perfil) {

              return $http.get(rest + 'perfil/' + perfil).then(function(response2) {
                if(estabelecimento.perfilDescricao){
                  estabelecimento.perfilDescricao = estabelecimento.perfilDescricao + '/' + response2.data.titulo;
                }else{
                  estabelecimento.perfilDescricao = response2.data.titulo;
                }
                
              });
            
            });
          };
        });

        return response.data;
      });
    },

    buscarCidades: function() {
      return $http.get(rest + 'estabelecimento/buscarCidades', { params: {empresa:empresa} }).then(function(response) {
        return response.data;
      });
    },

    buscarCidadesEventos: function() {
      return $http.get(rest + 'evento/buscarCidades', { params: {empresa:empresa} }).then(function(response) {
        return response.data;
      });
    },

    buscarEventos: function() {
      var dateObj = new Date();
      var mes = dateObj.getUTCMonth() + 1; //mes from 1-12
      var dia = dateObj.getUTCDate();
      var ano = dateObj.getUTCFullYear();
      hoje = ano + "/" + mes + "/" + dia;
      return $http.get(rest + 'evento/find', { params: {where: { "data": { ">=" : hoje }, "empresa": { 'like': '%' + empresa + '%' } } } }).then(function(response) {
        return response.data;
      });
    },

    buscarCapa: function(id, ordem) {
      return $http.get(rest + 'estabelecimentofoto/find', { params: { where: { ordem: ordem, estabelecimento: id } } }).then(function(response) {
        return response.data;
      });
    },

    buscarAgenda: function(dia) {
      var diaSemana = "";
      var promocoes = [];
      switch (dia) {
        case 0:
          diaSemana = "domingo";
          break;
        case 1:
          diaSemana = "segundaFeira";
          break;
        case 2:
          diaSemana = "tercaFeira";
          break;
        case 3:
          diaSemana = "quartaFeira";
          break;
        case 4:
          diaSemana = "quintaFeira";
          break;
        case 5:
          diaSemana = "sextaFeira";
          break;
        case 6:
          diaSemana = "sabado";
          break;
      }

      return $http.get(rest + 'estabelecimentoCategoria/buscarAgenda', { params: { dia: diaSemana, empresa: empresa } }).then(function(results) {
        return results.data;
      });
    },

    avaliarTransacao: function(id, nota, comentario) {
      return $http.put(rest + 'transacao/update/' + id, { nota: nota, observacao: comentario }).then(function(response) {
        return response.data;
      });
    },

    buscarPorId: function(id) {
      return $http.get(rest + 'estabelecimento/' + id).then(function(response) {
        return response.data;
      });
    },

    buscarProdutos: function(id) {
      return $http.get(rest + 'produto/find', {params: {estabelecimento: id}} ).then(function(response) {
        return response.data;
      });
    },

    buscarPerfilPorId: function(perfil) {
      return $http.get(rest + 'perfil/' + perfil).then(function(response) {
        return response.data;
      });
    }

  }
}]);