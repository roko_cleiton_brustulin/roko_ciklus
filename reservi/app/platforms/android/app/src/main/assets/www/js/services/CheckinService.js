angular.module('ciklus.services').service('CheckinService',  ['$http', function($http) {
  return {
    realizar: function(estabelecimento, socio) {
      var hoje = new Date().toISOString();
      return $http.put(rest + 'checkin/create', { estabelecimento: estabelecimento, socio:socio, data:hoje, status:'0' }).then(function(response) {
          return response.data;
      });
    },

    pesquisar: function(estabelecimento, socio) {
      var hoje = new Date().toISOString();
      return $http.get(rest + 'checkin/find', {params: { estabelecimento: estabelecimento, socio:socio, status:'0' }}) .then(function(response) {
          return response.data;
      });
    }
  }
}]);