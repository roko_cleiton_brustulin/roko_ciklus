angular.module('ciklus.services').service('NovidadeService', ['$http', function($http) {
  return {
    buscarNovidades: function() {
      return $http.get(rest + 'novidade/find', { params: { where: {"empresa": { 'like': '%' + empresa + '%' } } , sort: 'createdAt DESC' } } ).then(function(response) {
        return response.data;
      });
    },

    curtir: function(novidade, socios) {
      return $http.put(rest + 'novidade/update/' + novidade, { curtidas: socios })
        .then(function(response) {
          return response.data;
      });
    },

    buscarCidades: function() {
      return $http.get(rest + 'novidade/buscarCidades', { params: {empresa:empresa} }).then(function(response) {
        return response.data;
      });
    },
  }
}]);