angular.module('ciklus').controller('TabsCtrl', function($scope, $state, $ionicHistory, SessionService, NotificacaoService, varNotQtde) {
  
  $scope.platform = ionic.Platform.platform();

    if (SessionService.get('socio')) {
      $scope.user = JSON.parse(SessionService.get('socio'));

      NotificacaoService.buscarQtde($scope.user.id).then(function(result){
      if (result[0].qtde == 0){
        varNotQtde.qtde = '';  
      } else {
        varNotQtde.qtde = result[0].qtde;
      }

    });
      
  } else {
    $scope.user = null;
    varNotQtde.qtde = '';
  }

  

  $scope.retornaQtde = function(){
    return varNotQtde.qtde;
  }

  $scope.nl2br = function(str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
  }


  $scope.irParaCategorias = function() {
    $state.go('tab.categorias');
  };

  $scope.irParaNovidades = function() {
    $state.go('tab.novidades');
  };

  $scope.irParaPerfil = function() {
    $state.go('menu.tab.account');
  };

  $scope.irParaEconomias = function() {
    $state.go('tab.transacoes');
  };

  $scope.irParaPlanejamento = function() {
    $state.go('tab.planejamento');
  };

  $scope.irParaDestino = function(src) {
    window.open(src,  '_system', 'location=no');
  };

})