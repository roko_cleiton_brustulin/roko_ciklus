angular.module('ciklus', [
  'ionic', 
  'ui.utils.masks', 
  'ciklus.controllers', 
  'ciklus.services', 
  'ciklus.directives', 
  'angular-md5', 
  'ngCookies', 
  'ngSanitize', 
  'ngCordova', 
  'ionicImgCache',
  'ksSwiper',
  'idf.br-filters',
  'chart.js'])

.value('varNotQtde', {
  qtde: ''
})

.value('token', {
  Alo: ''
})

.run(function(SessionService, $state, $cordovaGoogleAnalytics, $ionicHistory, $timeout, $rootScope, SocioService, NotificacaoService, $ionicPlatform, $ionicPopup, ionicImgCache) {

  $rootScope.$on('$stateChangeSuccess', function (e, url) {
    if (!$ionicHistory.currentView()) {
      return
    }

    function getCurrentStateId () {
      if ($state && $state.current && $state.current.name) {
        return buildIdFromCurrentState($state.current.name)
      }

      // if something goes wrong make sure its got a unique stateId
      return ionic.Utils.nextUid()
    }

    function buildIdFromCurrentState (id) {
      if ($state.params) {
        for (var key in $state.params) {
          if ($state.params.hasOwnProperty(key) && $state.params[key]) {
            id += '_' + key + '=' + $state.params[key]
          }
        }
      }
      return id
    }

    $timeout(function () {
      var currentView = $ionicHistory.currentView();
      currentView.stateId = getCurrentStateId();
      currentView.stateName = $state.current.name;
      currentView.stateParams = angular.copy($state.params);
    });
  });


  ionic.Platform.ready(function() {

      direcionamento = "";
      
    // console.log(ionic.Platform.platform());
    // console.log(ionic.Platform.version());
    // console.log("Online: " + navigator.onLine);
    
    if (window.StatusBar) {
      StatusBar.styleLightContent();
    }

    // cordova.getAppVersion(function(version) {
    //   console.log('versão: ' + version);
    // })

    //rotina utilizada para mostrar mensagem de permissão de utilização da localização do usuário
    //mostre corretamente no IOs
    navigator.geolocation.getCurrentPosition(onSuccess, onError);
    function onSuccess(position) {
    // your callback here 
    };
    function onError(error) { 
    // your callback here
    }
    
    SessionService.persist('cidade', 'Curitiba');
    
    if (localStorage.getItem('socio')) {
      var socio = JSON.parse(localStorage.getItem('socio'));
      if (socio.id) {
        SocioService.atualizarOnesignal(socio.id);
        // if (socio.emailValido == false){
        if(navigator.onLine){
          SocioService.buscar(socio.id).then(function(socio) {
            SessionService.persist('socio', socio);
            empresa = socio.empresa;
            // if (socio.emailValido == false){
            //   $state.go('landing');
            //   $ionicPopup.show({
            //     title: 'E-mail não confirmado',
            //     template: 'O seu e-mail ainda não foi confirmado.<br><br>Acesse a sua caixa de e-mail e clique no link para validação antes de efetuar o login.',
            //     buttons: [
            //       { text: 'OK' }
            //     ]
            //   });  
            // }else{
              // $state.go('tab.categorias');
              // $state.go('tab.transacoes');
              if(direcionamento == ""){
                $state.go('tab.categorias');
              }
            // }
          });
        }else{
          $state.go('landing');
          $ionicPopup.show({
            title: 'Sem conexão',
            template: 'Não foi possível atualizar sua informações.<br><br>Você precisa estar conectado.',
            buttons: [
              { text: 'OK' }
            ]
          });
        }
      }else{
        $state.go('landing');
      }
    }else{
      $state.go('landing');
    }


    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }


    var notificationOpenedCallback = function(jsonData) {
      //alert("Notification opened:\n" + JSON.stringify(jsonData));
      direcionamento = jsonData.notification.payload.additionalData.direcionamento;
      if(direcionamento == 'estabelecimentos'){
        $state.go('tab.categorias');
      }else if(direcionamento == 'estabelecimento'){
        id = jsonData.notification.payload.additionalData.id;
        $state.go('tab.' + direcionamento, { id: id });
      }else if(direcionamento == 'novidades'){
        $state.go('tab.' + direcionamento);
      }   
      //console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
    };

    if (window.cordova) {
      window.plugins.OneSignal
        .startInit("a6c12734-8f2e-46fc-800e-6253d7378d9e")
        .inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.None)
        .handleNotificationOpened(notificationOpenedCallback)
        .endInit();

        if(typeof window.ga !== undefined) {
          window.ga.startTrackerWithId("UA-178303967-1");
        }
    }

  });
})


// .config(['ChartJsProvider', function (ChartJsProvider) {
//   // Configure all charts
//   ChartJsProvider.setOptions({
//     chartColors: ['#FF5252', '#FF5252', '#FF5252' ],
//     responsive: false
//   });
//   // Configure all line charts
//   ChartJsProvider.setOptions('line', {
//     showLines: false
//   });
// }])

// .config(['$sceDelegateProvider', function($sceDelegateProvider) {
//      $sceDelegateProvider.resourceUrlWhitelist(['self', "/^https?:\/\/(cdn\.)?iugu.com/"]);
//  }])cordova plugin add cordova-plugin-google-analytics

.config(['$ionicConfigProvider', function($ionicConfigProvider) {
  $ionicConfigProvider.tabs.position('bottom');
}])

.config(['$httpProvider', function ($httpProvider) {
  //Reset headers to avoid OPTIONS request (aka preflight)
  $httpProvider.defaults.headers.common = {};
  $httpProvider.defaults.headers.post = {};
  $httpProvider.defaults.headers.delete = {};
  $httpProvider.defaults.headers.put = {};
  $httpProvider.defaults.headers.put["Content-Type"] = "text/plain";
  $httpProvider.defaults.headers.patch = {};
}])

.config(['$compileProvider', function($compileProvider){
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(geo|mailto|tel|maps):/);
}])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  .state('landing', {
    url: '/landing',
    templateUrl: 'templates/landing.html',
    controller: 'LandingCtrl'
  })

  .state('menu', {
    url: '/menu/',
    templateUrl: 'templates/tab-account-menu.html',
    controller: 'MinhaContaCtrl'
  })

  .state('tab', {
    url: '/tab',
    abstract: true,
    controller: 'TabsCtrl',
    templateUrl: 'templates/tabs.html'
  })

  .state('menu.tab', {
    url: '/tab',
    views:{
      'menuContent': {
      controller: 'TabsCtrl',
      templateUrl: 'templates/tabs.html'
      }
    }
  })

  .state('tab.categorias', {
    url: '/categorias',
    views: {
      'tab-categorias': {
        templateUrl: 'templates/tab-categorias.html',
        controller: 'CategoriasCtrl'
      }
    }
  })

  .state('menu.tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'MinhaContaCtrl'
      }
    }
  })

  .state('tab.estabelecimentos', {
    url: '/estabelecimentos',
    views: {
      'tab-categorias': {
        templateUrl: 'templates/estabelecimentos.html',
        controller: 'EstabelecimentosCtrl'
      }
    },
    params: {
      categoria: null,
      agendaDia: null
    }
  })

  .state('tab.estabelecimentosagenda', {
    url: '/estabelecimentos',
    views: {
      'tab-agenda': {
        templateUrl: 'templates/estabelecimentos.html',
        controller: 'EstabelecimentosCtrl'
      }
    },
    cache: false,
    params: {
      categoria: null,
      agendaDia: null
    }
  })

  .state('tab.notificacoes', {
    url: '/notificacoes',
    views: {
      'tab-novidades': {
        templateUrl: 'templates/notificacoes.html',
        controller: 'NotificacoesCtrl'
      }
    }
  })

  .state('tab.novidades', {
    url: '/novidades',
    views: {
      'tab-novidades': {
        templateUrl: 'templates/novidades.html',
        controller: 'NovidadesCtrl'
      }
    }
  })

  .state('tab.eventos', {
    url: '/eventos',
    views: {
      // 'tab-eventos': {
        'tab-categorias': {
        templateUrl: 'templates/eventos.html',
        controller: 'EventosCtrl'
      }
    }
  })

  .state('tab.evento', {
    url: '/eventos/:id',
    views: {
      'tab-categorias': {
        templateUrl: 'templates/evento.html',
        controller: 'EventoCtrl'
      }
    },
    cache: false,
    params: {
      evento: null
    }
  })

  .state('tab.estabelecimentoagenda', {
    url: '/estabelecimentos/:id',
    views: {
      'tab-agenda': {
        templateUrl: 'templates/estabelecimento.html',
        controller: 'EstabelecimentoCtrl'
      }
    }
  })

  .state('tab.estabelecimento', {
    url: '/estabelecimentos/:id',
    views: {
      'tab-categorias': {
        templateUrl: 'templates/estabelecimento.html',
        controller: 'EstabelecimentoCtrl'
      }
    },
    cache: false,
    params: {
      estabelecimento: null
    }
  })

  // .state('tab.sonho', {
  //   url: '/sonho',
  //   views: {
  //     'tab-sonho': {
  //       templateUrl: 'templates/sonho.html',
  //       controller: 'SonhoCtrl'
  //     }
  //   }
  // })

  // .state('tab.pedidos', {
  //   url: '/pedidos',
  //   views: {
  //     'tab-pedidos': {
  //       templateUrl: 'templates/pedidos.html',
  //       controller: 'PedidosCtrl'
  //     }
  //   }
  // })

  // .state('tab.pedido', {
  //   url: '/pedidos',
  //   views: {
  //     'tab-pedidos': {
  //       templateUrl: 'templates/pedido.html',
  //       controller: 'PedidoCtrl'
  //     }
  //   },
  //   cache: false,
  //   params: {
  //     pedido: null
  //   }
  // })

  // .state('tab.ingresso', {
  //   url: '/pedidos',
  //   views: {
  //     'tab-pedidos': {
  //       templateUrl: 'templates/pedido-ingresso.html',
  //       controller: 'PedidoIngressoCtrl'
  //     }
  //   },
  //   cache: false,
  //   params: {
  //     ingresso: null,
  //     pedido: null
  //   }
  // })

  .state('menu.tab.perfil', {
    url: '/perfil',
    views: {
      'tab-account':{
        templateUrl: 'templates/tab-account-perfil.html',
      }
    }
  })

  .state('menu.tab.faturas', {
    url: '/faturas',
    views: {
      'tab-account':{
        templateUrl: 'templates/tab-account-faturas.html',
      }
    }
  })

  .state('tab.transacoes', {
    url: '/transacoes',
    views: {
      'tab-transacoes': {
        templateUrl: 'templates/transacoes.html',
        controller: 'MinhaContaCtrl'
      }
    }
  })

  .state('tab.planejamento', {
    url: '/planejamento',
    views: {
      'tab-planejamento': {
        templateUrl: 'templates/planejamento.html',
        controller: 'PlanejamentoCtrl'
      }
    }
  })

  .state('menu.tab.senha', {
    url: '/senha',
    views: {
      'tab-account':{
        templateUrl: 'templates/tab-account-senha.html',
      }
    }
  })

  .state('menu.tab.sugestao', {
    url: '/sugestao',
    views: {
      'tab-account':{
        templateUrl: 'templates/tab-account-sugestao.html',
      }
    }
  })

  .state('menu.tab.instrucoes', {
    url: '/instrucoes',
    views: {
      'tab-account':{
        templateUrl: 'templates/modal-bem-vindo.html',
      }
    }
  })

  ;

  // if none of the above states are matched, use this as the fallback
  // $urlRouterProvider.otherwise('/landing');

});
