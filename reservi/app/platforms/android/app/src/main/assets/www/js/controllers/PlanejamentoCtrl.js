angular.module('ciklus').controller('PlanejamentoCtrl', function($scope, $ionicLoading, $state, SessionService, SocioService, GastosService,  $sce, $ionicModal, $ionicPopup) {
   
    if (SessionService.get('socio')) {
        $scope.socio = JSON.parse(SessionService.get('socio'));
    } else {
        $scope.socio = null;
    }

    $scope.teste = '';

    $scope.gastos = {passo:1};
    $scope.total_reserva_atual  = '';
    $scope.total_divida_atual   = '';
    $scope.valor = {divida:'', totalRenda:''};
    $scope.valor = {receita_1: '', receita_2: '', receita_3: '', receita_outros: '', desconto_1: ''};
    $scope.valor = {despesa_essencial_1: 0, despesa_essencial_2: 0, despesa_essencial_3: '', despesa_essencial_4: '', despesa_essencial_5: '', despesa_essencial_6: '', despesa_essencial_7: '', despesa_essencial_8: '', despesa_essencial_9: '', despesa_essencial_10: '', despesa_essencial_outros: ''};
    $scope.valor = {despesa_nao_1: '', despesa_nao_2: '', despesa_nao_3: '', despesa_nao_4: '', despesa_nao_5: '', despesa_nao_6: '', despesa_nao_7: '', despesa_nao_8: '', despesa_nao_9: '', despesa_nao_10: '', despesa_nao_11: '', despesa_nao_outros: ''};
    $scope.valor = {despesa_divida_1:'',despesa_divida_2:'',despesa_divida_3:'',despesa_divida_4:'', despesa_divida_5:''};
    $scope.valor = {divida_1:'',divida_2:'',divida_3:'',divida_4:'', divida_5:''};
    $scope.valor = {total_reserva:'', total_divida:''};


    $scope.resetaVariareis = function(){
        $scope.valor.receita_1 = 0
        $scope.valor.receita_2 = 0
        $scope.valor.receita_3 = 0
        $scope.valor.receita_4 = 0
        $scope.valor.receita_5 = 0
        $scope.valor.receita_outros = 0
        $scope.valor.desconto_1 = 0
        $scope.valor.despesa_essencial_1 = 0
        $scope.valor.despesa_essencial_2 = 0
        $scope.valor.despesa_essencial_3 = 0
        $scope.valor.despesa_essencial_4 = 0
        $scope.valor.despesa_essencial_5 = 0
        $scope.valor.despesa_essencial_6 = 0
        $scope.valor.despesa_essencial_7 = 0
        $scope.valor.despesa_essencial_8 = 0
        $scope.valor.despesa_essencial_9 = 0
        $scope.valor.despesa_essencial_10 = 0
        $scope.valor.despesa_essencial_outros = 0
        $scope.valor.despesa_nao_1 = 0
        $scope.valor.despesa_nao_2 = 0
        $scope.valor.despesa_nao_3 = 0
        $scope.valor.despesa_nao_4 = 0
        $scope.valor.despesa_nao_5 = 0
        $scope.valor.despesa_nao_6 = 0
        $scope.valor.despesa_nao_7 = 0
        $scope.valor.despesa_nao_8 = 0
        $scope.valor.despesa_nao_9 = 0
        $scope.valor.despesa_nao_10 = 0
        $scope.valor.despesa_nao_11 = 0
        $scope.valor.despesa_nao_outros = 0
        $scope.valor.despesa_divida_1 = 0
        $scope.valor.despesa_divida_2 = 0
        $scope.valor.despesa_divida_3 = 0
        $scope.valor.despesa_divida_4 = 0
        $scope.valor.despesa_divida_5 = 0
        $scope.valor.divida_1 = 0
        $scope.valor.divida_2 = 0
        $scope.valor.divida_3 = 0
        $scope.valor.divida_4 = 0
        $scope.valor.divida_5 = 0
        // $scope.valor.total_reserva = 0;
        // $scope.valor.total_divida = 0;
        $scope.reserva_atual = 0;
        $scope.divida_atual = 0;
    }


    $scope.validaCampos = function(){
        if($scope.valor.receita_1 == "") $scope.valor.receita_1 = 0;
        if($scope.valor.receita_2 == "") $scope.valor.receita_2 = 0;
        if($scope.valor.receita_3 == "") $scope.valor.receita_3 = 0 ;
        if($scope.valor.receita_4 == "") $scope.valor.receita_4 = 0;
        if($scope.valor.receita_5 == "")  $scope.valor.receita_5 = 0;
        if($scope.valor.receita_outros == "") $scope.valor.receita_outros = 0;
        if($scope.valor.desconto_1 == "")$scope.valor.desconto_1 = 0;
        if($scope.valor.despesa_essencial_1 == "") $scope.valor.despesa_essencial_1 = 0;
        if($scope.valor.despesa_essencial_2 == "")  $scope.valor.despesa_essencial_2 = 0;
        if($scope.valor.despesa_essencial_3 == "") $scope.valor.despesa_essencial_3 = 0;
        if($scope.valor.despesa_essencial_4 == "") $scope.valor.despesa_essencial_4 = 0;
        if($scope.valor.despesa_essencial_5 == "") $scope.valor.despesa_essencial_5 = 0;
        if($scope.valor.despesa_essencial_6 == "") $scope.valor.despesa_essencial_6 = 0;
        if($scope.valor.despesa_essencial_7 == "")  $scope.valor.despesa_essencial_7 = 0;
        if($scope.valor.despesa_essencial_8 == "")  $scope.valor.despesa_essencial_8 = 0;
        if($scope.valor.despesa_essencial_9 == "")  $scope.valor.despesa_essencial_9 = 0;
        if($scope.valor.despesa_essencial_10 == "")  $scope.valor.despesa_essencial_10 = 0;
        if($scope.valor.despesa_essencial_outros == "") $scope.valor.despesa_essencial_outros = 0;
        if($scope.valor.despesa_nao_1 == "") $scope.valor.despesa_nao_1 = 0;
        if($scope.valor.despesa_nao_2 == "") $scope.valor.despesa_nao_2 = 0;
        if($scope.valor.despesa_nao_3 == "") $scope.valor.despesa_nao_3 = 0;
        if($scope.valor.despesa_nao_4 == "") $scope.valor.despesa_nao_4 = 0;
        if($scope.valor.despesa_nao_5 == "") $scope.valor.despesa_nao_5 = 0;
        if($scope.valor.despesa_nao_6 == "") $scope.valor.despesa_nao_6 = 0;
        if($scope.valor.despesa_nao_7 == "") $scope.valor.despesa_nao_7 = 0;
        if($scope.valor.despesa_nao_8 == "") $scope.valor.despesa_nao_8 = 0;
        if($scope.valor.despesa_nao_9 == "") $scope.valor.despesa_nao_9 = 0;
        if($scope.valor.despesa_nao_10 == "") $scope.valor.despesa_nao_10 = 0;
        if($scope.valor.despesa_nao_11 == "") $scope.valor.despesa_nao_11 = 0;
        if($scope.valor.despesa_nao_outros == "")$scope.valor.despesa_nao_outros = 0; 
        if($scope.valor.despesa_divida_1 == "") $scope.valor.despesa_divida_1 = 0;
        if($scope.valor.despesa_divida_2 == "") $scope.valor.despesa_divida_2 = 0;
        if($scope.valor.despesa_divida_3 == "") $scope.valor.despesa_divida_3 = 0;
        if($scope.valor.despesa_divida_4 == "") $scope.valor.despesa_divida_4 = 0;
        if($scope.valor.despesa_divida_5 == "") $scope.valor.despesa_divida_5 = 0;
        if($scope.valor.divida_1 == "") $scope.valor.divida_1 = 0;
        if($scope.valor.divida_2 == "") $scope.valor.divida_2 = 0;
        if($scope.valor.divida_3 == "") $scope.valor.divida_3 = 0;
        if($scope.valor.divida_4 == "") $scope.valor.divida_4 = 0;
        if($scope.valor.divida_5 == "") $scope.valor.divida_5 = 0;
    }


    $scope.calculaReservaMes = function(){

        GastosService.buscar_total($scope.socio.id, $scope.mes + 1, $scope.ano).then(function (response_total) {

            $scope.reserva_ate_mes = response_total;

            $scope.reserva_atual = ($scope.valor.receita_1 + $scope.valor.receita_2 + $scope.valor.receita_3 + $scope.valor.receita_outros - $scope.valor.desconto_1) - ($scope.valor.despesa_essencial_1 + $scope.valor.despesa_essencial_2 + $scope.valor.despesa_essencial_3 + $scope.valor.despesa_essencial_4 + $scope.valor.despesa_essencial_5 + $scope.valor.despesa_essencial_6 + $scope.valor.despesa_essencial_7 + $scope.valor.despesa_essencial_8 + $scope.valor.despesa_essencial_9 + $scope.valor.despesa_essencial_10 + $scope.valor.despesa_essencial_outros + $scope.valor.despesa_nao_1 + $scope.valor.despesa_nao_2 + $scope.valor.despesa_nao_3 + $scope.valor.despesa_nao_4 + $scope.valor.despesa_nao_5 + $scope.valor.despesa_nao_6 + $scope.valor.despesa_nao_7 + $scope.valor.despesa_nao_8 + $scope.valor.despesa_nao_8 + $scope.valor.despesa_nao_10 + $scope.valor.despesa_nao_11 + $scope.valor.despesa_nao_outros + $scope.valor.despesa_divida_1 + $scope.valor.despesa_divida_2 + $scope.valor.despesa_divida_3 + $scope.valor.despesa_divida_4 + $scope.valor.despesa_divida_5);
            
            if($scope.total_reserva_atual == ''){
                $scope.total_reserva_atual = $scope.reserva_ate_mes;
            }
        });
    };


    $scope.buscarGastos = function(indice){
        
        GastosService.buscar($scope.socio.id, $scope.mes + 1, $scope.ano).then(function (response) {
            if(response.length){

                //Guarda variáveis DIVIDA para mostrar nos meses posteriores caso não tenha nenhum valor gravado
                divida_1 = $scope.valor.divida_1 - $scope.valor.despesa_divida_1; 
                divida_2 = $scope.valor.divida_2 - $scope.valor.despesa_divida_2;
                divida_3 = $scope.valor.divida_3 - $scope.valor.despesa_divida_3;
                divida_4 = $scope.valor.divida_4 - $scope.valor.despesa_divida_4;
                divida_5 = $scope.valor.divida_5 - $scope.valor.despesa_divida_5;

                $scope.valor = response[0];
                delete $scope.valor.createdAt;
                delete $scope.valor.updatedAt;

                if($scope.valor.divida_1 == 0) $scope.valor.divida_1 = divida_1;
                if($scope.valor.divida_2 == 0) $scope.valor.divida_2 = divida_2;
                if($scope.valor.divida_3 == 0) $scope.valor.divida_3 = divida_3;
                if($scope.valor.divida_4 == 0) $scope.valor.divida_4 = divida_4;
                if($scope.valor.divida_5 == 0) $scope.valor.divida_5 = divida_5;
                

            }else{

                $scope.valor.id = '';

                //Busca gastos do mês anterior. Caso não tenha nenhum registro então não faz a pergunta pois não tem o que atualizar
                GastosService.buscar($scope.socio.id, $scope.mes, $scope.ano).then(function (response) {
                    if(response.length){
                
                        //Pergunta se quer atualizar com os valores do mês anterior
                        if($scope.data_hoje <= $scope.data_mes){
                            var confirmPopup = $ionicPopup.confirm({
                                title: 'Atualizar gastos',
                                template: 'Deseja atualizar os gastos desse mês com os valores informados no mês passado?',
                                buttons: [
                                {
                                    text: '<b>Não</b>',
                                    type: 'button-negative',
                                    onTap: function(e) {
                                        
                                        //Guarda variáveis DIVIDA para mostrar nos meses posteriores caso não tenha nenhum valor gravado
                                        divida_1 = $scope.valor.divida_1 - $scope.valor.despesa_divida_1; 
                                        divida_2 = $scope.valor.divida_2 - $scope.valor.despesa_divida_2;
                                        divida_3 = $scope.valor.divida_3 - $scope.valor.despesa_divida_3;
                                        divida_4 = $scope.valor.divida_4 - $scope.valor.despesa_divida_4;
                                        divida_5 = $scope.valor.divida_5 - $scope.valor.despesa_divida_5;
                        
                                        $scope.resetaVariareis();

                                        //Seta variáveis DIVIDA do mês anterior
                                        $scope.valor.divida_1 = divida_1; 
                                        $scope.valor.divida_2 = divida_2; 
                                        $scope.valor.divida_3 = divida_3; 
                                        $scope.valor.divida_4 = divida_4; 
                                        $scope.valor.divida_5 = divida_5; 

                                    }
                                },
                                {
                                    text: '<b>Sim</b>',
                                    type: 'button-positive',
                                    onTap: function(e) {

                                        // $scope.valor = valor_bkp;
                                        $scope.valor.divida_1 = $scope.valor.divida_1 - $scope.valor.despesa_divida_1; 
                                        $scope.valor.divida_2 = $scope.valor.divida_2 - $scope.valor.despesa_divida_2; 
                                        $scope.valor.divida_3 = $scope.valor.divida_3 - $scope.valor.despesa_divida_3; 
                                        $scope.valor.divida_4 = $scope.valor.divida_4 - $scope.valor.despesa_divida_4; 
                                        $scope.valor.divida_5 = $scope.valor.divida_5 - $scope.valor.despesa_divida_5;  

                                        $scope.gravarGastos();

                                        $ionicPopup.show({
                                        title: 'Pronto',
                                        template: 'Valores atualizados!',
                                        buttons: [
                                            {
                                            text: '<b>OK</b>',
                                            type: 'button-positive',
                                            onTap: function(e) {
                                            
                                            }
                                            }
                                        ]
                                        });
                            
                                    }
                                }
                                ]
                            });
                        }

                    //Caso não tenha registro no mês anterior
                    }else{

                        //Guarda variáveis DIVIDA para mostrar nos meses posteriores caso não tenha nenhum valor gravado
                        divida_1 = $scope.valor.divida_1 - $scope.valor.despesa_divida_1; 
                        divida_2 = $scope.valor.divida_2 - $scope.valor.despesa_divida_2;
                        divida_3 = $scope.valor.divida_3 - $scope.valor.despesa_divida_3;
                        divida_4 = $scope.valor.divida_4 - $scope.valor.despesa_divida_4;
                        divida_5 = $scope.valor.divida_5 - $scope.valor.despesa_divida_5;
        
                        $scope.resetaVariareis();

                        //Seta variáveis DIVIDA do mês anterior
                        $scope.valor.divida_1 = divida_1; 
                        $scope.valor.divida_2 = divida_2; 
                        $scope.valor.divida_3 = divida_3; 
                        $scope.valor.divida_4 = divida_4; 
                        $scope.valor.divida_5 = divida_5;

                    }
                })

            }           
        });




        $scope.calculaReservaMes();
        
    }


    $scope.data_hoje = new Date();
    $scope.data_hoje.setDate(1);
    $scope.data_hoje.setHours(12);
    $scope.data_mes = new Date();
    $scope.data_mes.setDate(1);
    $scope.data_mes.setHours(12);
    $scope.mes = $scope.data_mes.getMonth();
    $scope.ano = $scope.data_mes.getFullYear();
    $scope.meses_ext = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
    $scope.mes_txt = $scope.meses_ext[$scope.mes];
    $scope.reserva_ate_mes_txt = 'Atual';
    $scope.resetaVariareis();
    $scope.buscarGastos();
   
    




    $scope.trocarMes = function(indice) {
        $scope.data_mes.setMonth($scope.mes + indice);
        $scope.mes = $scope.data_mes.getMonth();
        $scope.ano = $scope.data_mes.getFullYear();
        $scope.mes_txt = $scope.meses_ext[$scope.mes];
        
        if($scope.data_hoje > $scope.data_mes){
            $scope.reserva_ate_mes_txt = 'Anterior'; 
        }else if($scope.data_hoje < $scope.data_mes){
            $scope.reserva_ate_mes_txt = 'Previsão'; 
        }else{
            $scope.reserva_ate_mes_txt = 'Atual';
        }

        $scope.buscarGastos(indice);

    }



    $scope.calculaReservaIdeal = function(){

        //Calculado a partir do mês atual (hoje)
        //Busca os últimos 3 meses (caso tenha) e faz uma média das despesas essenciais * 6

        data_hoje = new Date();
        mes_hoje = data_hoje.getMonth();
        ano_hoje = data_hoje.getFullYear();
        valor = 0;
        dividir_por = 0;
        GastosService.buscar($scope.socio.id, mes_hoje + 1, ano_hoje).then(function (response) {
            if(response.length){
                
                dados = response[0];
                tmp = dados.despesa_essencial_1 + dados.despesa_essencial_2 + dados.despesa_essencial_3 + dados.despesa_essencial_4 + dados.despesa_essencial_5 + dados.despesa_essencial_6 + dados.despesa_essencial_7 + dados.despesa_essencial_8 + dados.despesa_essencial_9 + dados.despesa_essencial_10 + dados.despesa_essencial_outros;
                if(tmp != 0){
                    valor += tmp;
                    dividir_por += 1;
                }
                

            }        
            
            GastosService.buscar($scope.socio.id, mes_hoje + 0, ano_hoje).then(function (response) {
                if(response.length){
                    
                    dados = response[0];
                    tmp = dados.despesa_essencial_1 + dados.despesa_essencial_2 + dados.despesa_essencial_3 + dados.despesa_essencial_4 + dados.despesa_essencial_5 + dados.despesa_essencial_6 + dados.despesa_essencial_7 + dados.despesa_essencial_8 + dados.despesa_essencial_9 + dados.despesa_essencial_10 + dados.despesa_essencial_outros;
                    if(tmp != 0){
                        valor += tmp;
                        dividir_por += 1;
                    }
    
                }        
                
                GastosService.buscar($scope.socio.id, mes_hoje - 1, ano_hoje).then(function (response) {
                    if(response.length){
                        
                        dados = response[0];
                        tmp = dados.despesa_essencial_1 + dados.despesa_essencial_2 + dados.despesa_essencial_3 + dados.despesa_essencial_4 + dados.despesa_essencial_5 + dados.despesa_essencial_6 + dados.despesa_essencial_7 + dados.despesa_essencial_8 + dados.despesa_essencial_9 + dados.despesa_essencial_10 + dados.despesa_essencial_outros;
                        if(tmp != 0){
                            valor += tmp;
                            dividir_por += 1;
                        }
        
                    }
                    
                    if(valor == 0){
                        $scope.reserva_ideal = null;
                    }else{
                        $scope.reserva_ideal = ((valor) * 6) / dividir_por;   
                    }

                });

            });

        });

                    

    }



    $scope.calculaReservaIdeal();

    
    // Cores
    vermelho = "rgb(237,28,36)";
    amarelo  = "rgb(255,201,14)";
    verde    = "rgb(34,177,76)";


    $ionicModal.fromTemplateUrl('templates/modal-gastos-lancamento.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.modalGastos = modal;
    });

    $ionicModal.fromTemplateUrl('templates/modal-gastos-analise.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.modalAnalise = modal;
    });




    $scope.abrirSlides = function() {
        
        $ionicModal.fromTemplateUrl('templates/slides-reserva-financeira.html', {
            scope: $scope,
            animation: 'slide-in-up'
          }).then(function(modal) {
            $scope.modalSlides = modal;
            $scope.modalSlides.show();
        });
  
    };

    $scope.fecharSlides = function(){
        $scope.modalSlides.hide();

        $ionicPopup.show({
            title: 'Primeiros passos!',
            template: 'Preciso que você preencha todos os campos para que eu possa começar a analisar suas finanças, vamos lá?',
            buttons: [
              { text: 'Começar agora',
              type: 'button-positive',
                onTap: function(e) {
                  
                }
              }
            ]
          });
    }

    // A primeira vez que acessar a aba irá aparecer os Slides explicativos
    if(localStorage.getItem('firstTimeGastos') != 'TRUE'){
        SessionService.persist('firstTimeGastos', 'TRUE');
        $scope.abrirSlides();
    };
    
    $scope.abrirModalGastos = function(indice) {
        $scope.gastos.passo = indice;
        $scope.modalGastos.show();
    }

    $scope.fecharModalGastos = function(){
        $scope.modalGastos.hide();
    }

    // $scope.editarPoupanca = function() {
    //     $scope.valor.poupanca = $scope.totalPoupanca;
    //     $ionicPopup.show({
    //         title: 'Digite o valor total que você tem guardado:',
    //         template: "<input type='tel' placeholder='R$ 1.000,00' ng-model='valor.poupanca' ui-money-mask/>",
    //         scope: $scope,
    //         buttons: [
    //             {
    //                 text: 'Registrar',
    //                 type: 'button-positive',
    //                 onTap: function(e) {
    //                     $scope.totalPoupanca = $scope.valor.poupanca;
    //                 }
    //             }
    //         ]
    //     });
    // };

    $scope.mostrarAjuda = function(indice){

        if(indice == 1){
            if($scope.reserva_ate_mes_txt == "Atual"){
                titulo = 'Reserva Atual';
                texto = 'É simples e rápido começar uma reserva financeira. <br><br>Aqui você visualiza a soma do que economiza mês a mês para alcançar sua reserva ideal.';
            }else if($scope.reserva_ate_mes_txt == "Previsão"){
                titulo = 'Previsão da Reserva';
                texto = 'Este valor é uma previsão caso continue a manter o ritmo de economia para alcançar sua reserva ideal.';
            }else if($scope.reserva_ate_mes_txt == "Anterior"){
                titulo = 'Reserva Anterior';
                texto = 'Este valor mostra a sua reserva como estava nos meses anteriores para você ter um acompanhamento.';
            }
        }else if(indice == 2){
            titulo = 'Reserva Ideal';
            texto = 'A reserva financeira é um fundo de emergência mínimo que deve possuir pelo menos o equivalente para cobrir seis meses do seu custo familiar.<br><br>É um senso comum que qualquer um está sujeito a imprevistos. Seja com um problema de saúde, um dano material ou a perda de um negócio, ninguém sabe a hora que vai precisar de um dinheiro extra.<br><br>Esse fundo deve possuir o equivalente a pelo menos seis meses do seu custo familiar. É o que calculamos para você.';
        }

        $ionicPopup.show({
            title: titulo,
            template: texto,
            scope: $scope,
            buttons: [
                {
                    text: 'Entendi',
                    type: 'button-positive',
                    onTap: function(e) {
                    
                    }
                }
            ]
        });
    };

    // $scope.editarDivida = function() {
    //     $scope.valor.divida = $scope.totalDivida;
    //     $ionicPopup.show({
    //         title: 'Digite o valor total que você tem de dívida:',
    //         template: "<input type='tel' placeholder='R$ 1.000,00' ng-model='valor.divida' ui-money-mask/>",
    //         scope: $scope,
    //         buttons: [
    //             {
    //                 text: 'Registrar',
    //                 type: 'button-positive',
    //                 onTap: function(e) {
    //                    $scope.totalDivida = $scope.valor.divida;
    //                 }
    //             }
    //         ]
    //     });
    // };


    $scope.gravarGastos = function(){
        $scope.valor.socio  = $scope.socio.id;
        $scope.valor.mes    = $scope.mes + 1;
        $scope.valor.ano    = $scope.ano;
        $scope.validaCampos();
        GastosService.salvar($scope.valor).then(function (response) {
            $scope.valor.id = response.id;
            $scope.calculaReservaIdeal();
            $scope.calculaReservaMes();
            $scope.modalGastos.hide();
        });

    }


    $scope.salvar = function(){
        
        receita             = $scope.valor.receita_1 + $scope.valor.receita_2 + $scope.valor.receita_3 + $scope.valor.receita_outros - $scope.valor.desconto_1;
        despesas_essencias  = $scope.valor.despesa_essencial_1 + $scope.valor.despesa_essencial_2 + $scope.valor.despesa_essencial_3 + $scope.valor.despesa_essencial_4 + $scope.valor.despesa_essencial_5 + $scope.valor.despesa_essencial_6 + $scope.valor.despesa_essencial_7 + $scope.valor.despesa_essencial_8 + $scope.valor.despesa_essencial_9 + $scope.valor.despesa_essencial_10 + $scope.valor.despesa_essencial_outros;
        despesas_nao        = $scope.valor.despesa_nao_1 + $scope.valor.despesa_nao_2 + $scope.valor.despesa_nao_3 + $scope.valor.despesa_nao_4 + $scope.valor.despesa_nao_5 + $scope.valor.despesa_nao_6 + $scope.valor.despesa_nao_7 + $scope.valor.despesa_nao_8 + $scope.valor.despesa_nao_8 + $scope.valor.despesa_nao_10 + $scope.valor.despesa_nao_11 + $scope.valor.despesa_nao_outros;
        
        dividas             = $scope.valor.despesa_divida_1 + $scope.valor.despesa_divida_2 + $scope.valor.despesa_divida_3 + $scope.valor.despesa_divida_4 + $scope.valor.despesa_divida_5;
        total_dividas       = ($scope.valor.divida_1 + $scope.valor.divida_2 + $scope.valor.divida_3 + $scope.valor.divida_4 + $scope.valor.divida_5) - (dividas);

        // Atualiza Totais
        total = receita - (despesas_essencias + despesas_nao + dividas);
        $scope.reserva_ate_mes          += total - $scope.reserva_atual;
        $scope.valor.total_reserva      += total - $scope.reserva_atual;
        $scope.total_reserva_atual      += total - $scope.reserva_atual;
        $scope.valor.total_divida       -= dividas - $scope.divida_atual;
        $scope.reserva_atual            = total;
        $scope.divida_atual             = dividas;


        $scope.gravarGastos();
        


        if(receita == undefined){
            receita = 0; 
        }
        if(despesas_essencias == undefined){
            despesas_essencias = 0; 
        }
        if(despesas_nao == undefined){
            despesas_nao = 0; 
        }
        if(dividas == undefined){
            dividas = 0; 
        }

        percentual_1 = (despesas_essencias / receita) * 100;
        percentual_2 = (despesas_nao / receita) * 100;
        percentual_3 = ((receita - (despesas_essencias + despesas_nao)) / receita) * 100; 
        //O percentual 3 (Indicador 20) será sempre o 50 menos o 30. Independente se tem ou não dívida.
        //Isto será tratado nas análise e não no gráfico 50/30/20

        // if(dividas == 0){
        //     // Nao tem divida, então o 20% é sobre o que sobrou
        //     percentual_3 = ((receita - (despesas_essencias + despesas_nao)) / receita) * 100;
        // }else{
        //     // Se está pagando dívida, então o 20% é pra pagar dívida
        //     percentual_3 = (dividas / receita) * 100;
        // }

        cor_1 = verde;
        if(percentual_1 <= 45){
            cor_1 = verde;
        }else if(percentual_1 > 45 & percentual_1 <= 55){
            cor_1 = amarelo;
        }else if(percentual_1 > 55){
            cor_1 = vermelho;
        }

        cor_2 = verde;
        if(percentual_2 <= 25){
            cor_2 = verde;
        }else if(percentual_2 > 25 & percentual_2 <= 35){
            cor_2 = amarelo;
        }else if(percentual_2 > 35){
            cor_2 = vermelho;
        }

        cor_3 = '';
        if(percentual_3 > 0 & percentual_3 <= 15){
            cor_3 = vermelho;
        }else if(percentual_3 > 15 & percentual_3 <= 25){
            cor_3 = amarelo;
        }else if(percentual_3 > 25){
            cor_3 = verde;
        }
        
        if(dividas == 0){
            $scope.labels = ["Essenciais", "Variáveis", "Reserva"];
        }else{
            $scope.labels = ["Essenciais", "Variáveis", "Dívidas"];
        }
        $scope.data = [
            // [50, 30, 20],
            [percentual_1, percentual_2, percentual_3]
        ];
        // $scope.options = {
        //     scales: {
        //       yAxes: [
        //         {
        //           id: 'y-axis-1',
        //           type: 'linear',
        //           display: true,
        //           position: 'left',
        //           beginAtZero: true
        //         },
        //         // {
        //         //   id: 'y-axis-2',
        //         //   type: 'linear',
        //         //   display: true,
        //         //   position: 'right'
        //         // }
        //       ]
        //     }
        //   };
        $scope.datasetOverride = [
            // {
            //     label: "Ideial",
            //     borderWidth: 1,
            //     type: 'bar',
            //     data: [50, 30, 20],
            //     backgroundColor: [
            //         'rgba(235, 235, 224, 0.2)',
            //         'rgba(235, 235, 224, 0.2)',
            //         'rgba(235, 235, 224, 0.2)'
            //     ],
            // },
            {
                label: "Realizado",
                borderWidth: 2,
                type: 'bar',
                data: [percentual_1, percentual_2, percentual_3],
                backgroundColor: [cor_1, cor_2, cor_3],
                // options: {
                //     scales: {
                //         yAxes: [{
                //             ticks: {
                //                 beginAtZero: true
                //             }
                //         }]
                //     }
                // }
            }
        ];
        // $scope.colors = [
        //     {
        //       backgroundColor: "rgba(159,204,0, 0.2)",
        //       pointBackgroundColor: "rgba(159,204,0, 1)",
        //       pointHoverBackgroundColor: "rgba(159,204,0, 0.8)",
        //       borderColor: "rgba(159,204,0, 1)",
        //       pointBorderColor: '#fff',
        //       pointHoverBorderColor: "rgba(159,204,0, 1)"
        //     },"rgba(250,109,33,0.5)","#9a9a9a","rgb(233,177,69)"
        //   ];
        //   $scope.labels = ["Green", "Peach", "Grey", "Orange"];
        //   $scope.data = [300, 500, 100, 150];


        // 1.	Análise dos Gastos Essenciais (50)
        $scope.analise_1_texto = "";
        if(cor_1 == verde){
            $scope.analise_1_texto = "Excelente o seu controle com os gastos essênciais. Parabéns!"
        }else if(cor_1 == amarelo){
            $scope.analise_1_texto = "Cautela com os gastos essenciais, pois está próximo do limite ideal de 50% da sua renda."
        }else if(cor_1 == vermelho){
            $scope.analise_1_texto = "Atenção, muito cuidado! Os sinais mostram que você está com os gastos essênciais acima do ideal de 50% da sua renda."
        }

        // 2.	Análise dos Gastos Variáveis (30)
        $scope.analise_2_texto = "";
        if(cor_2 == verde){
            $scope.analise_2_texto = "Muito bom! Pelo o que estou vendo, você está com os gastos variáveis dentro do orçamento ideal de 30% da sua renda."
        }else if(cor_2 == amarelo){
            $scope.analise_2_texto = "Opa, cuidado! Você está com os gastos variáveis dentro do limite indicado de 30% da sua renda."
        }else if(cor_2 == vermelho){
            $scope.analise_2_texto = "Fique de olho com o seu 'estilo de vida'. Seus gastos variáveis estão excedendo o ideal de 30% da sua renda."
        }


        // // 2.	Analisar se os gastos 50 - 30 estão equilibrados;
        // $scope.analise_2_texto = "";
        // if(cor_1 == verde && cor_2 == verde){
        //     $scope.analise_2_texto = "Excelente! Tudo mostra que os seus gastos essenciais e variáveis estão dentro do limite. O ideal é ter até 50% em gastos essenciais e até 30% em gastos variáveis."
        // }else if(cor_1 == verde && cor_2 == amarelo){
        //     $scope.analise_2_texto = "Muito bom! Pelo o que estou vendo, você está com os gastos essenciais e variáveis dentro do limite, só cuidado com os gastos Variáveis pois está próximo do limite ideal. O ideal é ter até 50% em gastos essenciais e até 30% em gastos variáveis."
        // }else if(cor_1 == amarelo && cor_2 == verde){
        //     $scope.analise_2_texto = "Muito bom! Pelo o que estou vendo, você está com os gastos essenciais e variáveis dentro do limite, só cuidado com os gastos essenciais pois está próximo do limite ideal. O ideal é ter até 50% em gastos essenciais e até 30% em gastos variáveis."
        // }else if(cor_1 == amarelo && cor_2 == amarelo){
        //     $scope.analise_2_texto = "Alerta, parece que você está com os gastos essenciais e variáveis dentro do limite porém ambos estão perto do ideal. O ideal para gastos essenciais é 50% e para gastos variáveis é 30%."
        // }else if(cor_1 == vermelho && cor_2 == vermelho){
        //     $scope.analise_2_texto = "Atenção, muito cuidado! Os sinais mostram que você está com os gastos essenciais e variáveis acima do ideal. O ideal para gastos essenciais é 50% e para gastos variáveis é 30%."
        // }else if(cor_1 == vermelho){
        //     $scope.analise_2_texto = "Atenção, muito cuidado! Os sinais mostram que você está com os gastos essenciais acima do ideal. O ideal para gastos essenciais é 50% e para gastos variáveis é 30%."
        // }else if(cor_2 == vermelho){
        //     $scope.analise_2_texto = "Atenção, muito cuidado! Os sinais mostram que você está com os gastos variáveis acima do ideal. O ideal para gastos essenciais é 50% e para gastos variáveis é 30%."
        // }
        
        // 3.	Se tem dívidas, se sim, é os 20%, se não, os 20% vai para a reserva financeira
        $scope.analise_3_texto = "";
        if(cor_3 == verde || cor_3 == amarelo){
            if( total_dividas > 0 && dividas > 0){
                $scope.analise_3_texto = "Parabéns, você está no caminho certo, com o valor que sobra no final do mês você está pagando suas dívidas. Este é o 1o passo - quitar suas dívidas"
            }else if( total_dividas > 0 && dividas <= 0){
                $scope.analise_3_texto = "Atenção, como você possui um valor pendente em dívídas, o ideal é você sempre pagar primeiro as suas dívidas para depois começar a criar uma reserva.";
            }else if( total_dividas <= 0){
                $scope.analise_3_texto = "Sensacional! Pelo jeito as suas finanças estão controladas e já sobra dinheiro para a sua reserva finaceira. Não pule essa etapa, é sempre melhor prevenir do que remediar. A sua reserva financeira ideal é R$ " + $scope.reserva_ideal + ".";
                if($scope.reserva_ate_mes >= $scope.reserva_ideal){
                    $scope.analise_3_texto += "Parabéns, você já atingiu sua reserva financeira!!";
                }else{
                    $scope.analise_3_textoo += "Ainda faltam R$ " + ($scope.reserva_ideal - $scope.reserva_ate_mes) + " para atingir. Continue que você está no caminho.";
                }
            }
        }else if(cor_3 == vermelho){
            if( total_dividas > 0 && dividas > 0){
                $scope.analise_3_texto = "Atenção, você precisa equilibrar os gastos essenciais e variáveis para que você consiga pagar um valor maior das suas dívidas"
            }else if( total_dividas > 0 && dividas <= 0){
                $scope.analise_3_texto = "Atenção, como você possui um valor pendente em dívídas, o ideal é você sempre pagar primeiro as suas dívidas para depois começar a criar uma reserva.";
            }else if( total_dividas <= 0){
                $scope.analise_3_texto = "Atenção, você precisa equilibrar os gastos essenciais e variáveis para que você guardar mais no final de cada mês para atingir mais rápido sua reserva ideal."
            }
        }else if(cor_3 == ''){
            if(total_dividas > 0){
                $scope.analise_3_texto = "Atenção, você não está conseguindo pagar suas dívidas pois não está sobrando nada no final do mês."
            }else{
                $scope.analise_3_texto = "Atenção, você não está conseguindo aumentar sua reserva pois não está sobrando nada no final do mês."
            }
        }


        // Análise do Percentual
        // percentual_1
        // 1.	Se a soma da receita está maior ou menor que os gastos;
        if(total >= 0){
            $ionicPopup.show({
                title: 'SALDO POSITIVO',
                template: 'Maravilha, parece que o seu controle financeiro está positivo. Você está gastando menos do que recebe.',
                buttons: [
                    {   text: 'Ver análise',
                        type: 'button-green',
                        onTap: function(e) {
                            $scope.modalAnalise.show();
                        }
                    }
                ]
            });
        }else{
            $ionicPopup.alert({
                title: 'SALDO NEGATIVO',
                template: 'Fiquei preocupada com você, parece que está gastando mais do que recebe. Mas calma! Vou te ajudar a sair dessa.',
                buttons: [
                    {   text: 'Ver análise',
                        type: 'button-assertive',
                        onTap: function(e) {
                            $scope.modalAnalise.show();
                        }
                    }
                ]
            });
        }




    };
    

    $scope.fecharModalAnalise = function() {
        $scope.modalAnalise.hide();
    };

  
})