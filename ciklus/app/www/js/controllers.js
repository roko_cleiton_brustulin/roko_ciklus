var imagens = 'http://admin.cikluslive.com.br/uploads/';

String.prototype.replaceAll = function(search, replacement) {
  var target = this;
  return target.replace(new RegExp(search, 'g'), replacement);
};

String.prototype.padZero= function(len, c){
    var s= this, c= c || '0';
    while(s.length< len) s= c+ s;
    return s;
}

angular.module('ciklus.controllers', [])

.filter('trusted', ['$sce', function ($sce) {
  return function(url) {
      return $sce.trustAsResourceUrl(url);
  };
}])

.controller('DashCtrl', function($scope) {})

.controller('TabsCtrl', function($scope, $state, $ionicHistory, SessionService, NotificacaoService, varNotQtde) {
  
  $scope.platform = ionic.Platform.platform();

    if (SessionService.get('socio')) {
      $scope.user = JSON.parse(SessionService.get('socio'));

      NotificacaoService.buscarQtde($scope.user.id).then(function(result){
      if (result[0].qtde == 0){
        varNotQtde.qtde = '';  
      } else {
        varNotQtde.qtde = result[0].qtde;
      }

    });
      
  } else {
    $scope.user = null;
    varNotQtde.qtde = '';
  }

  

  $scope.retornaQtde = function(){
    return varNotQtde.qtde;
  }

  $scope.nl2br = function(str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
  }

  $scope.irParaAgenda = function() {
    $state.go('tab.agenda');
  };

  $scope.irParaCategorias = function() {
    $state.go('tab.categorias');
  };

  $scope.irParaEventos = function() {
    $state.go('tab.eventos');
  };

  $scope.irParaNovidades = function() {
    // $scope.badgeCount = 0;
    $state.go('tab.novidades');
  };

})

.controller('EventoCtrl', function($scope, EstabelecimentoService, $stateParams) {

  $scope.evento = $stateParams.evento;

  $scope.formatarData = function(data) {
    var d = data.split('-');
    return d[2].substring(0, 2) + '/' + d[1] + '/' + d[0];
  };

  $scope.recuperarImagem = function(evento) {
    if (evento.foto) {
      return imagens + evento.foto.id + '.' + evento.foto.extensao;
    }
  };

  $scope.trustSrc = function(src) {
    return $sce.trustAsResourceUrl(src);
  };
})

.controller('EventosCtrl', function($scope, EstabelecimentoService, $state) {
  $scope.eventos = [];
  $scope.orderOptionVar = "data";
  $scope.semEventos = false;

  EstabelecimentoService.buscarEventos().then(function (response) {
    $scope.eventos = response;
    if ($scope.eventos.length == 0) {
      $scope.semEventos = true;
    }
  });

  $scope.formatarData = function(data) {
    var d = data.split('-');
    return d[2].substring(0, 2) + '/' + d[1] + '/' + d[0];
  };

  $scope.recuperarImagem = function(evento) {
    if (evento.foto) {
      return imagens + evento.foto.id + '.' + evento.foto.extensao;
    }
  };

  $scope.irParaEvento = function(evento) {
    console.log(evento);
    $state.go('tab.evento', { evento: evento });
  };

  $scope.getOrder = function() {
    return $scope.orderOptionVar;
  };

  $scope.orderOption = function(option) {
    switch (option) {
      case 0:
        $scope.orderOptionVar = 'titulo';
        break;
      case 1:
        $scope.orderOptionVar = 'data';
        break;
    }
  };

  $scope.recuperarFiltroAtivo = function(option) {
    var opt = "";
    switch (option) {
      case 0:
        opt = 'data';
        break;
      case 1:
        opt = 'titulo';
        break;
    }

    if ($scope.orderOptionVar == opt)
      return "bg-pink white";
  };

})

.controller('AgendaCtrl', function($scope, SessionService, EstabelecimentoService, SocioService, $ionicModal, $state, $sce, $cordovaGeolocation, NotificacaoService) {

  if (SessionService.get('socio')) {
    $scope.user = JSON.parse(SessionService.get('socio'));
  } else {
    $scope.user = null;
  }
  $scope.modalDia = {};
  $scope.agenda = [];
  $scope.diaAtual = new Date().getDay();
  $scope.urlImagens = imagens;
  $scope.localizacao = {};
  $scope.posicaoAtual = {};
  $scope.dadosCarregados = false;
  $scope.validadoOnesignal = false;

  $scope.$on('$ionicView.enter', function() {
    if(window.cordova) {
      window.ga.trackView('Agenda');
    }
  });

  if (window.cordova) {
    var options = {timeout: 10000, enableHighAccuracy: true};
    $cordovaGeolocation.getCurrentPosition(options).then(function(position) {
      $scope.posicaoAtual = position;
    });
  }

  $scope.obterDistanciaEstabelecimento = function(promocao) {
    if (window.cordova && $scope.posicaoAtual && promocao.latitude && promocao.longitude) {
      var myLocation = new google.maps.LatLng($scope.posicaoAtual.coords.latitude, $scope.posicaoAtual.coords.longitude);
      var myDestination = new google.maps.LatLng(promocao.latitude, promocao.longitude);
      promocao.distancia = (google.maps.geometry.spherical.computeDistanceBetween(myLocation, myDestination) / 1000).toFixed(2);
      return '- ' + (google.maps.geometry.spherical.computeDistanceBetween(myLocation, myDestination) / 1000).toFixed(2) + "km";
    }
  };

  $scope.trustSrc = function(src) {
    return $sce.trustAsResourceUrl(src);
  };

  $scope.fecharModalDia = function() {
    $scope.modalDia.hide();
  };

  $scope.obterLocalizacao = function() {
    if (window.cordova) {
      var options = {timeout: 10000, enableHighAccuracy: true};
      $cordovaGeolocation.getCurrentPosition(options).then(function(position) {
        $scope.localizacao = position;
        $scope.agenda.forEach(function(x) {
          x.forEach(function(y) {
            y.distancia = $scope.obterDistanciaEstabelecimento($scope.localizacao, y);
          });
        });
      }, function(error){
        console.log("Could not get location");
      });
    }
  };

  $scope.atualizarAgenda = function(dia) {
    if (dia == null) {
      dia = $scope.diaAtual;
    }
    $scope.dadosCarregados = false;

    EstabelecimentoService.buscarAgenda(dia).then(function(data) {
      $scope.agenda = data;
      $scope.dadosCarregados = true;
    });
  };

  $scope.irParaCategoria = function(id) {
    $state.go('tab.estabelecimentosagenda', { categoria: id });
  };

  $scope.atualizarAgenda(null);

  $scope.buscarNomeDia = function() {
    switch($scope.diaAtual) {
      case 0:
        return "Domingo";
        break;
      case 1:
        return "Segunda-feira";
        break;
      case 2:
        return "Terça-feira";
        break;
      case 3:
        return "Quarta-feira";
        break;
      case 4:
        return "Quinta-feira";
        break;
      case 5:
        return "Sexta-feira";
        break;
      case 6:
        return "Sábado";
        break;
      default:
        return "";
        break;
    }
  };

  $scope.classeDia = function(dia) {
    if ($scope.diaAtual == dia)
      return "pink";
  };

  $scope.trocarDiaPara = function(dia) {
    $scope.diaAtual = dia;
    $scope.atualizarAgenda(dia);
    $scope.modalDia.hide();
  };

  $scope.formatarDias = function(promocao) {
    var dias = [];
    if (promocao.segundaFeira)
      dias.push('Seg');
    if (promocao.tercaFeira)
      dias.push('Ter');
    if (promocao.quartaFeira)
      dias.push('Qua');
    if (promocao.quintaFeira)
      dias.push('Qui');
    if (promocao.sextaFeira)
      dias.push('Sex');
    if (promocao.sabado)
      dias.push('Sab');
    if (promocao.tercaFeira)
      dias.push('Dom');

    return dias.join(', ');
  };

  $scope.trocarDia = function() {
    $scope.modalDia.show();
  };

  $scope.irParaEstabelecimento = function(promocao) {
    $state.go('tab.estabelecimentoagenda', { id: promocao.id });
  };

  $ionicModal.fromTemplateUrl('templates/modal-trocar-dia.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modalDia = modal;
  });

})

.controller('EstabelecimentosCtrl', function($scope, $stateParams, EstabelecimentoService, $state, $ionicModal, $sce, $cordovaGeolocation) {
  $scope.imagem = "";
  $scope.urlImagens = imagens;
  $scope.orderOptionVar = "";
  $scope.modalDia = {};
  $scope.bairros = [];
  $scope.estabelecimentoCategoria = "Estabelecimentos";
  $scope.diaAtual;
  $scope.localizacao = {};
  $scope.posicaoAtual = {};

  $scope.$on('$ionicView.enter', function(event, data) {
    if(window.cordova) {
      window.ga.trackView('Estabelecimentos');
    }
    
    if($scope.estabelecimentos == undefined){ //Cleiton - If inserido para que não fique carregando quando clicar em Voltar depois que entra dentro de um estabelecimento. Antes ficava carregando toda vez q voltava.
      $scope.carregarEstabelecimentos();
      $scope.orderOption(1);
    }
  });

  $scope.fecharModalDia = function() {
    $scope.modalDia.hide();
  };

  if (window.cordova) {
    var options = {timeout: 10000, enableHighAccuracy: true};
    $cordovaGeolocation.getCurrentPosition(options).then(function(position) {
      $scope.posicaoAtual = position;
    });
  }

  $scope.obterDistanciaEstabelecimento = function(estabelecimento) {
    if (window.cordova && $scope.posicaoAtual && estabelecimento.latitude && estabelecimento.longitude) {
      var myLocation = new google.maps.LatLng($scope.posicaoAtual.coords.latitude, $scope.posicaoAtual.coords.longitude);
      var myDestination = new google.maps.LatLng(estabelecimento.latitude, estabelecimento.longitude);
      // estabelecimento.distancia = (google.maps.geometry.spherical.computeDistanceBetween(myLocation, myDestination) / 1000).toFixed(2);
      // return ' - ' + (google.maps.geometry.spherical.computeDistanceBetween(myLocation, myDestination) / 1000).toFixed(2) + " km";
      var dist = (google.maps.geometry.spherical.computeDistanceBetween(myLocation, myDestination) / 1000).toFixed(2);
      estabelecimento.distancia = parseFloat(dist);
      return ' - ' + dist + " km";
    }
  };

  $scope.carregarEstabelecimentos = function() {
    $scope.estabelecimentos = [];
    $scope.estFiltrados = [];
    if ($stateParams.categoria) {
      $scope.estabelecimentoCategoria = $stateParams.categoria.nome || $stateParams.categoria.categoria;
      switch($stateParams.categoria.id) {
        case 21:
          $scope.imagem = "img/restaurant.png";
          break;
        default:
          $scope.imagem = "img/restaurant.png";
          break;
      }
      EstabelecimentoService.buscarPorCategoria($stateParams.categoria.id).then(function(data) {
        if (data.length > 0)
          $scope.estabelecimentos = $scope.estabelecimentos.concat(data);
          $scope.estFiltrados = $scope.estabelecimentos.slice(0);
      });
    }else{
      EstabelecimentoService.buscarTodos().then(function(data) {
        if (data.length > 0)
          $scope.estabelecimentos = $scope.estabelecimentos.concat(data);
          $scope.estFiltrados = $scope.estabelecimentos.slice(0);
      });
    }
  };

  $scope.fecharTeclado = function() {
    if (window.cordova) {
      window.cordova.plugins.Keyboard.close();
    }
  };

  $scope.trocarDia = function() {
    $ionicModal.fromTemplateUrl('templates/modal-trocar-dia.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modalDia = modal;
      $scope.modalDia.show();
    });
  };

  $scope.buscarNomeDia = function() {
    switch($scope.diaAtual) {
      case 0:
        return "Domingo";
        break;
      case 1:
        return "Segunda-feira";
        break;
      case 2:
        return "Terça-feira";
        break;
      case 3:
        return "Quarta-feira";
        break;
      case 4:
        return "Quinta-feira";
        break;
      case 5:
        return "Sexta-feira";
        break;
      case 6:
        return "Sábado";
        break;
      default:
        return "";
        break;
    }
  };

  $scope.trocarDiaPara = function(dia) {
    $scope.diaAtual = dia;
    $scope.estFiltrados = [];
    $scope.estabelecimentos.forEach(function(x) {
      var push = false;
      x.promocoes.forEach(function(y) {
        switch (dia) {
          case 0:
            if (y.domingo) push = true;
            break;
          case 1:
            if (y.segundaFeira) push = true;
            break;
          case 2:
            if (y.tercaFeira) push = true;
            break
          case 3:
            if (y.quartaFeira) push = true;
            break
          case 4:
            if (y.quintaFeira) push = true;
            break
          case 5:
            if (y.sextaFeira) push = true;
            break
          case 6:
            if (y.sabado) push = true;
            break
        }
      });
      if (push) $scope.estFiltrados.push(x);
    });
    $scope.modalDia.hide();
  };

  $scope.irParaEstabelecimento = function(value) {
    if ($state.current.name == "tab.estabelecimentosagenda")
      $state.go('tab.estabelecimentoagenda', { id:value });
    else
      $state.go('tab.estabelecimento', { id: value });
  };

  $scope.formatarDias = function(promocoes) {
    var dias = [];
    var seg, ter, qua, qui, sex, sab, dom = false;
    if (promocoes.length > 0) {
      promocoes.forEach(function(promocao) {
        if (promocao.segundaFeira && !seg) {
          if (!seg) {
            dias.push('Seg');
            seg = true;
          }
        }
        if (promocao.tercaFeira) {
          if (!ter)
            dias.push('Ter');
            ter = true;
        }
        if (promocao.quartaFeira) {
          if (!qua)
            dias.push('Qua');
          qua = true;
        }
        if (promocao.quintaFeira) {
          if (!qui)
            dias.push('Qui');
          qui = true;
        }
        if (promocao.sextaFeira) {
          if (!sex)
            dias.push('Sex');
          sex = true;
        }
        if (promocao.sabado) {
          if (!sab)
            dias.push('Sab');
          sab = true;
        }
        if (promocao.domingo) {
          if (!dom)
            dias.push('Dom');
          dom = true;
        }
      });
      return dias.join(', ');
    } else {
      return "";
    }
  };

  $scope.calcularDesconto = function(promocoes) {
    if (promocoes.length > 0) {
      var promocao = 0;
      promocoes.forEach(function(x) {
        if (x.desconto > promocao)
          promocao = x.desconto;
      });
      return promocao;
    } else {
      return "0";
    }
  };

  $scope.calcularSocioMais = function(promocoes) {
    if (promocoes.length > 0) {
      var socioMais = 0;
      promocoes.forEach(function(x) {
        if (x.desconto > socioMais)
          socioMais = x.socioMais;
      });
      return socioMais;
    } else {
      return "0";
    }
  };

  $scope.recuperarFiltroAtivo = function(option) {
    var opt = "";
    switch (option) {
      case 0:
        opt = 'nome';
        break;
      case 1:
        opt = 'distancia';
        break;
    }

    if ($scope.orderOptionVar == opt)
      return "bg-pink white";
  };

  $scope.getOrder = function() {
    return $scope.orderOptionVar;
  };

  $scope.orderOption = function(option) {
    switch (option) {
      case 0:
        $scope.orderOptionVar = 'nome';
        break;
      case 1:
        $scope.orderOptionVar = 'distancia';
        break;
    }
  };

  $scope.trustSrc = function(src) {
    return $sce.trustAsResourceUrl(src);
  };

  $scope.recuperarImagem = function(est) {
    var retorno = "";
    if (est.imagens.length > 0) {
      est.imagens.forEach(function(x, y) {
        if (x.ordem == 1) {
          retorno = $scope.urlImagens + x.id + '.' + x.extensao;
        }
      });
    }
    return retorno;
  };

  $scope.limparFiltros = function() {
    $scope.orderOptionVar = null;
    $scope.diaAtual = null;
    $scope.carregarEstabelecimentos();
  };
})

.controller('EstabelecimentoCtrl', function($scope, $stateParams, EstabelecimentoService, $sce, $cordovaGeolocation, $ionicLoading) {
  $scope.estabelecimento = { };
  $scope.urlImagens = imagens;
  $scope.mostrandoMapa = false;
  $scope.dadosCarregados = false;

  // $ionicLoading.show({
  //   template: 'Carregando...'
  // });
  $ionicLoading.show();

  $scope.$on('$ionicView.enter', function() {
    if(window.cordova) {
      window.ga.trackView('Estabelecimento');
    }
  });

  EstabelecimentoService.buscarPorId($stateParams.id).then(function(estabelecimento) {
    $scope.mostrandoMapa = false;
    $scope.estabelecimento = estabelecimento;
    $scope.estabelecimento.descricao = $scope.estabelecimento.descricao.replace(/(?:\r\n|\r|\n)/g, '<br />');
    $scope.obterDistanciaEstabelecimento();
    $scope.dadosCarregados = true;
    $ionicLoading.hide();
  });

  $scope.recuperarHorarios1 = function(promocao) {
     var horarios = "";
     if (promocao.horario1 && promocao.horario2) {
       horarios = promocao.horario1 + ' - ' + promocao.horario2;
     }
    return horarios;
  };

  $scope.recuperarHorarios2 = function(promocao) {
    var horarios = "";
    if (promocao.horario3 && promocao.horario4) {
      if (horarios != "") {
        horarios += " & ";
      }
      horarios = promocao.horario1 + ' - ' + promocao.horario2;
      horarios = promocao.horario3 + ' - ' + promocao.horario4;
     }
     return horarios;
  };

  $scope.irParaDestino = function(src) {
      window.open(src,  '_system', 'location=no');
  };

  $scope.trustSrc = function(src) {
    return $sce.trustAsResourceUrl(src);
  };

  $scope.encodeUTF8 = function(s) {
    return unescape(encodeURIComponent(s));
  };

  $scope.decodeUTF8 = function(s) {
    return decodeURIComponent(escape(s));
  };

  $scope.recuperarBackground = function(index) {
    if (index % 2 == 0) {
      return "bg-grey";
    }
  };

  $scope.recuperarCapa = function() {
    var retorno = "";
    if ($scope.estabelecimento.imagens) {
      if ($scope.estabelecimento.imagens.length > 0) {
        $scope.estabelecimento.imagens.forEach(function(x, y) {
          if (x.ordem == 1) {
            retorno = $scope.urlImagens + x.id + '.' + x.extensao;
          }
        });
        return retorno;
      } else {
        return "";
      }
    } else {
      return "";
    }
  };

  $scope.recuperarLogo = function() {
    var retorno = "";
    if ($scope.estabelecimento.imagens.length > 0) {
      $scope.estabelecimento.imagens.forEach(function(x, y) {
        if (x.ordem == 2) {
          retorno = $scope.urlImagens + x.id + '.' + x.extensao;
        }
      });
      return retorno;
    } else {
      return "";
    }
  };

  $scope.mostrarMapa = function() {
    $scope.mostrandoMapa = !$scope.mostrandoMapa;
  };

  $scope.inicializarMapa = function() {
    var endereco = $scope.estabelecimento.endereco + ", " + $scope.estabelecimento.numero + " - " + $scope.estabelecimento.bairro + ", " + $scope.estabelecimento.cidade;
    return "https://maps.google.com/maps?&amp;q=" + encodeURIComponent(endereco) + "&amp;output=embed";
  };

  $scope.obterDistanciaEstabelecimento = function() {
    if (window.cordova) {
      var options = {timeout: 10000, enableHighAccuracy: true};
      $cordovaGeolocation.getCurrentPosition(options).then(function(position) {
        var myLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        var myDestination = new google.maps.LatLng($scope.estabelecimento.latitude, $scope.estabelecimento.longitude);
        $scope.estabelecimento.distancia = (google.maps.geometry.spherical.computeDistanceBetween(myLocation, myDestination) / 1000).toFixed(2) + "km";
        $scope.$apply()
        }, function(error){
          console.log("Could not get location");
      });
    }
  };

})

.controller('LandingCtrl', function($scope, $ionicModal, $state, SocioService, $ionicPopup, SessionService, IuguService, $ionicLoading, EmailService) {
  $scope.modalCadastro = {};
  $scope.modalBemVindo = {};
  $scope.novaSenha = { email: '', cpf: '' };
  $scope.cadastro = { passo: 1, senha: '' };
  $scope.login = { cpf: '', senha: '' };
  $scope.planos = [];
  $scope.emailInvalido = false;
  $scope.convite = {nome: '', email: ''};

  $scope.$on('$ionicView.beforeEnter', function() {
    if (SessionService.get('socio')) {
      var socio = JSON.parse(SessionService.get('socio'));
      if (socio.id) {
        $state.go('tab.agenda');
      }
    }
  });

  $scope.$on('$ionicView.enter', function() {
    if(window.cordova) {
      setTimeout(function() {
        window.ga.trackView('Landing');
      }, 1500);
    }
  });

  $scope.verBeneficios = function() {
    $state.go('tab.agenda');
  };

  $scope.validarVoucher = function() {
     $ionicLoading.show();
    SocioService.validarVoucher($scope.cadastro.voucher).then(function(result) {
      if (result.length > 0) {
        result = result[0];

        $scope.voucherFrequencia = result.plano.frequencia;
        if (result.utilizado) {
          $ionicPopup.show({
            title: 'Convite já utilizado',
            template: 'O código que você digitou já foi utilizado.',
            buttons: [
              { text: 'OK' }
            ]
          });
          $ionicLoading.hide();
          return;
        } else {
          var expiracao = result.expiracao.split('-');
          var expiracaoObj = new Date(expiracao[0], expiracao[1], expiracao[2].substring(0, 2));
          var hoje = new Date();
          if (expiracaoObj.valueOf() < hoje.valueOf()) {
            $ionicPopup.show({
              title: 'Convite expirado!',
              template: 'O código que você digitou está expirado.',
              buttons: [
                { text: 'OK' }
              ]
            });
            $ionicLoading.hide();
            return;
          } else {
              $ionicLoading.hide();
              $ionicPopup.show({
              title: 'Parabéns!',
              template: 'O código que você digitou é válido e dá direito a ' + result.plano.frequencia + ' dias grátis.<br><br>Continue preenchendo os seus dados para ativar sua conta agora mesmo.',
              buttons: [
                { text: 'OK',
                  onTap: function(e) {
                    $scope.voucherValido = true;
                    $scope.voucherObj = result;
                    $scope.cadastro.passo = 2;
                    $scope.modalCadastro.show();
                  }
                }
              ]
            });
          }
        }
      } else {
        $ionicLoading.hide();
        $ionicPopup.show({
          title: 'Convite inválido',
          template: 'Ops, parece que o código que você digitou é inválido.',
          buttons: [
            { text: 'OK' }
          ]
        });
      }
    });
  };

  $scope.esqueceuSenha = function() {
    $ionicModal.fromTemplateUrl('templates/modal-esqueceu-senha.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modalEsqueceuSenha = modal;
      $scope.modalEsqueceuSenha.show();
    });
  };

  $scope.fecharEsqueceuSenha = function() {
    $scope.modalEsqueceuSenha.hide();
  };

  $scope.gerarNovaSenha = function() {
    var cpf = $scope.formatarCpf($scope.novaSenha.cpf);
    if (cpf.length == 14) {
      if ($scope.validarEmail($scope.novaSenha.email)) {
        $ionicLoading.show();
        SocioService.gerarSenha(cpf, $scope.novaSenha.email).then(function(res) {
          if (res) {
            $ionicLoading.hide();
            $ionicPopup.show({
              title: 'Pronto!',
              subTitle: 'Uma nova senha foi gerada e enviada para seu e-mail.',
              scope: $scope,
              buttons: [
                { text: 'Ok',
                onTap: function(e) {
                  $scope.modalEsqueceuSenha.hide();
                }
              }
              ]
            });
          } else {
            $ionicLoading.hide();
            $ionicPopup.show({
              title: 'Atenção!',
              subTitle: 'Dados inválidos, por favor, verifique.',
              scope: $scope,
              buttons: [
                { text: 'Ok' }
              ]
            });
          }
        });
      } else {
        $ionicPopup.show({
          title: 'Atenção!',
          subTitle: 'Dados inválidos, por favor, verifique.',
          scope: $scope,
          buttons: [
            { text: 'Ok' }
          ]
        });  
      }
    } else {
      $ionicPopup.show({
        title: 'Atenção!',
        subTitle: 'Dados inválidos, por favor, verifique.',
        scope: $scope,
        buttons: [
          { text: 'Ok' }
        ]
      });
    }
  };

  $scope.convite = function(){
    if ($scope.convite.nome && $scope.convite.email) {
      $ionicLoading.show();
      EmailService.enviar($scope.convite.nome, $scope.convite.email, 'Solicitacao de convite via app').then(function(data) {
        $ionicLoading.hide();
        $ionicPopup.show({
          title: 'Obrigado!',
          template: 'Seu  nome já está na nossa lista de espera.<br><br>Agora é só aguardar!<br><br>Obrigado pelo interesse na Ciklus live - Clube da economia',
          buttons: [
            { text: 'Ok',
             onTap: function(e) {
              $scope.modalCadastro.hide();
             }
            }
          ]
        });
      })
    } else {
      $ionicPopup.show({
        title: 'Ops',
        template: 'Preencha seu Nome Completo e e-mail.',
        buttons: [
          { text: 'OK' }
        ]
      });
    }
  }

  $scope.validarEmail = function(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (re.test(email)) {
      return true;
    } else {
      $ionicPopup.show({
        title: 'Atenção!',
        subTitle: 'E-mail inválido, por favor, verifique.',
        scope: $scope,
        buttons: [
          { text: 'Ok' }
        ]
      });
      return false;
    }
  };

  $scope.formatarCpf = function(cpf) {
    cpf = cpf.toString();
    while (cpf.length < 11) {
      cpf = "0" + cpf;
    }
    cpf = cpf.slice(0, 3) + '.' + cpf.slice(3, 6) + '.' + cpf.slice(6, 9) + '-' + cpf.slice(9, 11);
    return cpf;
  };

  $scope.login = function() {
    if ($scope.login.cpf && $scope.login.senha) {
      $ionicLoading.show();
      SocioService.login($scope.formatarCpf($scope.login.cpf), $scope.login.senha).then(function(data) {
        $ionicLoading.hide();
        if (data.length > 0) {
          SocioService.atualizarOnesignal(data[0].id);
          SessionService.persist('socio', data[0]);
          $state.go('tab.agenda');
          $scope.modalCadastro.hide();
        } else {
          $ionicPopup.show({
            title: 'Login inválido',
            template: 'CPF e/ou senha inválidos, tente novamente.',
            buttons: [
              { text: 'OK' }
            ]
          });
        }
      });
    } else {
      $ionicPopup.show({
        title: 'Ops',
        template: 'Preencha seu CPF e senha.',
        buttons: [
          { text: 'OK' }
        ]
      });
    }
  };

  $scope.validarCPF = function(cpf) {
    cpf = cpf.replace(/[^\d]+/g,'');
    if(cpf == '') return false;
    // Elimina CPFs invalidos conhecidos
    if (cpf.length != 11 ||
        cpf == "00000000000" ||
        cpf == "11111111111" ||
        cpf == "22222222222" ||
        cpf == "33333333333" ||
        cpf == "44444444444" ||
        cpf == "55555555555" ||
        cpf == "66666666666" ||
        cpf == "77777777777" ||
        cpf == "88888888888" ||
        cpf == "99999999999")
            return false;
    // Valida 1o digito
    add = 0;
    for (i=0; i < 9; i ++)
        add += parseInt(cpf.charAt(i)) * (10 - i);
        rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;
        if (rev != parseInt(cpf.charAt(9)))
            return false;
    // Valida 2o digito
    add = 0;
    for (i = 0; i < 10; i ++)
        add += parseInt(cpf.charAt(i)) * (11 - i);
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)
        rev = 0;
    if (rev != parseInt(cpf.charAt(10)))
        return false;
    return true;
  };

  $scope.cadastrarAuxiliar = function() {
    if ($scope.cadastro.dados.cpf && $scope.cadastro.dados.cep) {
      $scope.cadastro.dados.cpf = $scope.cadastro.dados.cpf.toString();
      $scope.cadastro.dados.cep = $scope.cadastro.dados.cep.toString();

      if ($scope.cadastro.dados.cpf.length < 11) {
        $scope.cadastro.dados.cpf = $scope.cadastro.dados.cpf.padZero(11, '0');
      }
    }
    if ($scope.validarEmail($scope.cadastro.dados.email)) {
      if ($scope.cadastro.dados.nome.length > 1
        && $scope.cadastro.dados.cpf.length == 11
        && $scope.cadastro.dados.cep.length == 8
        && ($scope.cadastro.dados.sexo == 'mas' || $scope.cadastro.dados.sexo == 'fem')
        && $scope.cadastro.dados.termo){
        $ionicLoading.show();
        var cpf = $scope.cadastro.dados.cpf;
        $scope.cadastro.dados.cpf = cpf.substring(0, 3) + '.' + cpf.substring(3, 6) + '.' + cpf.substring(6, 9) + '-' + cpf.substring(9, 11);
        if ($scope.validarCPF($scope.cadastro.dados.cpf)) {
          SocioService.buscarCEP($scope.cadastro.dados.cep).then(function(result) {
            if (result.localidade) {
              SocioService.verificarCPF($scope.cadastro.dados.cpf).then(function(resultado) {
                if (resultado) {
                  // se tiver um voucher, entra no fluxo sem cartão de crédito.
                  if ($scope.voucherValido) {
                    var hoje =  new Date();
                    hoje.setDate(hoje.getDate() + $scope.voucherFrequencia);
                    var mes = hoje.getMonth() + 1;
                    var dia = hoje.getDate();
                    var ano = hoje.getFullYear();

                    if (mes.length < 2) mes = '0' + mes;
                    if (dia.length < 2) dia = '0' + dia;
                    ativoAte = [ano, mes, dia].join('-');

                    $scope.cadastro.dados.ativoAte = ativoAte;

                    SocioService.cadastrar($scope.cadastro.dados).then(function(response) {
                      if (response.id > 0) {
                        var socio = response;

                        // seta voucher como utilizado.
                        SocioService.atualizarVoucher($scope.voucherObj.id, true).then(function(response) {
                          socio.cupom = $scope.voucherObj.id;
                          SocioService.atualizar(socio).then(function(d) {
                           // console.log(d);
                          });
                        });

                        // SocioService.atualizarOnesignal(socio.id);

                        $ionicLoading.hide();
                        $ionicPopup.show({
                          template: '<input type="password" ng-model="cadastro.senha">',
                          title: 'Crie uma senha',
                          scope: $scope,
                          buttons: [
                            {
                              text: '<b>Salvar</b>',
                              type: 'button-positive',
                              onTap: function(e) {
                                if ($scope.cadastro.senha.length > 3) {
                                  SocioService.atualizarCodigo(socio.id);
                                  SocioService.alterarSenha(socio.id, $scope.cadastro.senha).then(function(response) {
                                    SessionService.persist('socio', socio);
                                    SocioService.atualizarOnesignal(socio.id);
                                    $scope.cadastro = { passo: 1, senha: '' };
                                    $scope.voucherValido = false;
                                    $scope.modalCadastro.hide();
                                    $scope.modalBemVindo.show();
                                  });
                                } else {
                                  e.preventDefault();
                                  $ionicPopup.show({
                                    template: 'Sua senha deve ter pelo menos 4 caracteres.',
                                    buttons: [
                                      {
                                        text: '<b>Salvar</b>',
                                        type: 'button-positive'
                                      }
                                    ]
                                  });
                                }
                              }
                            }
                          ]
                        });
                      }
                    });
                  } else {
                    // sem voucher, registra na tabela auxiliar e vai para o cartão de crédito.
                    SocioService.cadastrarAuxiliar($scope.cadastro.dados).then(function(socioAux) {
                      $scope.cadastro.dados.id = socioAux.id;
                      $scope.cadastro.passo = 3;
                      $ionicLoading.hide();
                    });
                  }
                } else {
                  $scope.cadastro.dados.cpf = '';
                  $ionicLoading.hide();
                  $ionicPopup.show({
                    title: 'CPF encontrado',
                    template: 'Esse CPF já está cadastrado na Ciklus.',
                    buttons: [
                      { text: 'OK' }
                    ]
                  });
                }
              });
            } else {
              $ionicPopup.show({
                template:'CEP inválido!',
                buttons: [{
                  text: 'Ok'
                }]
              });
              $scope.cadastro.dados.cpf = $scope.cadastro.dados.cpf.replace(/[^\d]+/g,'');
              $ionicLoading.hide();
              return;
            }
          });
        } else {
          $ionicLoading.hide();
          $ionicPopup.show({
            title: 'CPF inválido',
            template: 'Por favor, verifique seu CPF.',
            buttons: [
              { text: 'OK' }
            ]
          });
        }
      } else {
        $ionicPopup.show({
          title: 'Dados inválidos',
          template: 'Por favor, verifique os dados preenchidos.',
          buttons: [
            { text: 'OK' }
          ]
        });
      }
    }
  };

  $scope.realizarPagamento = function() {
    $scope.cadastro.dados.cartao.numero = $scope.cadastro.dados.cartao.numero.toString();
    $scope.cadastro.dados.cartao.cvv = $scope.cadastro.dados.cartao.cvv.toString();
    if ($scope.cadastro.dados.cartao.nome.length > 3 && $scope.cadastro.dados.cartao.numero.length == 16
      && ($scope.cadastro.dados.cartao.cvv.length == 3 || $scope.cadastro.dados.cartao.cvv.length == 4)
      && $scope.cadastro.dados.cartao.mes.length == 2 && $scope.cadastro.dados.cartao.ano.length == 2) {
      var cartao = $scope.cadastro.dados.cartao;
      $ionicLoading.show();
      IuguService.associarContaPagamento($scope.cadastro.dados, cartao, $scope.cadastro.plano.id).then(function(pagamento) {
        var resultado = pagamento;
        if (resultado) {
          if (resultado.current_transaction.status == "paid") {
            $scope.cadastro.dados.idIugu = resultado.id;
            delete $scope.cadastro.dados.id;
            SocioService.cadastrar($scope.cadastro.dados).then(function(socioRes) {
              resultado.socioCiklus = socioRes;
              if (resultado.socioCiklus.id > 0) {
                $ionicLoading.hide();
                SocioService.atualizarOnesignal(resultado.socioCiklus.id);
                $ionicPopup.show({
                  template: '<input type="password" ng-model="cadastro.senha" placeholder="Crie sua senha">',
                  title: 'Crie uma senha',
                  scope: $scope,
                  buttons: [
                    {
                      text: '<b>Salvar</b>',
                      type: 'button-positive',
                      onTap: function(e) {
                        $ionicLoading.show();

                        SocioService.alterarSenha(resultado.socioCiklus.id, $scope.cadastro.senha).then(function(response) {
                          SessionService.persist('socio', resultado.socioCiklus);
                          SocioService.atualizarOnesignal(resultado.socioCiklus.id);
                          $ionicLoading.hide();
                          $scope.modalCadastro.hide();
                          $scope.modalBemVindo.show();
                          $scope.voucherValido = false;
                          $scope.cadastro = { passo: 1, senha: '' };
                        });
                      }
                    }
                  ]
                });
              }
            });
          } else {
            $ionicLoading.hide();
            $ionicPopup.show({
              title: 'Erro ao realizar pagamento',
              template: 'A transação não foi bem sucedida.',
              buttons: [
                { text: 'OK' }
              ]
            });  
          }
        } else {
          $ionicLoading.hide();
          $ionicPopup.show({
            title: 'Erro ao realizar pagamento',
            template: 'Por favor, verifique os dados preenchidos e tente novamente.',
            buttons: [
              { text: 'OK' }
            ]
          });
        }
      }, function(err) {
        $ionicLoading.hide();
        $ionicPopup.show({
          title: 'Erro ao realizar pagamento',
          template: 'Por favor, verifique os dados preenchidos e tente novamente.',
          buttons: [
            { text: 'OK' }
          ]
        });
      });
    } else {
      $ionicLoading.hide();
      $ionicPopup.show({
        title: 'Dados inválidos',
        template: 'Por favor, verifique os dados preenchidos e tente novamente.',
        buttons: [
          { text: 'OK' }
        ]
      });
    }
  };


  $scope.fecharBemVindo = function() {
    $state.go('tab.agenda');
    $scope.modalBemVindo.hide();
  };

  $scope.formatarPreco = function(preco) {
    preco = preco.toString();
    if (preco.length > 1) {
      var precoFormatado = preco.substr(0, preco.length - 2) + ','
                          + preco.substr(preco.length - 2, preco.length - 1);
      return precoFormatado;
    } else {
      return "";
    }
  };

  $scope.criarConta = function() {
    $ionicLoading.show();
    IuguService.buscarPlanos().then(function(response) {
      $scope.planos = response;
      $scope.modalCadastro.show();
      $ionicLoading.hide();
    });    
  };

  $scope.preecherSolicitarVoucher = function() {
    $scope.modalCadastro.show();
    $scope.cadastro.passo = 4;
  };

  $scope.irLogin = function() {
    $scope.modalCadastro.show();
    $scope.cadastro.passo = 5;
  };

  $scope.selecionarPlano = function(plano) {
    $scope.cadastro.plano = plano;
    $scope.cadastro.passo = 2;
  };

  $scope.irParaHome = function() {
    $state.go('tab.agenda', {});
    $scope.modalCadastro.hide();
  };

  $ionicModal.fromTemplateUrl('templates/cadastro.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modalCadastro = modal;
  });

  $ionicModal.fromTemplateUrl('templates/modal-bem-vindo.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modalBemVindo = modal;
  });

  $scope.fecharCadastro = function() {
    if ($scope.cadastro.passo == 2){
      $scope.modalCadastro.hide();
    }else if($scope.cadastro.passo == 4){
      $scope.modalCadastro.hide();
    }else if($scope.cadastro.passo == 5){
      $scope.modalCadastro.hide();
    }else{
      if ($scope.cadastro.passo > 1) {
        $scope.cadastro.passo = $scope.cadastro.passo - 1;
      } else {
        $scope.modalCadastro.hide();
      }
    }
  };

})

.controller('CategoriasCtrl', function($scope, CategoriaService, $state, $ionicHistory) {
  $scope.$on('$ionicView.enter', function() {
    if(window.cordova) {
      window.ga.trackView('Categorias');
    }
  });

  CategoriaService.resumoCategorias().then(function(data) {
    $scope.categorias = data;
  });

  $scope.irParaEstabelecimentos = function(categoria) {
    $ionicHistory.clearCache().then(function(){
      $state.go('tab.estabelecimentos', { categoria: categoria });;
    });
  };

  $scope.irParaEventos = function() {
    $state.go('tab.eventos');
  };
})


.controller('NotificacoesCtrl', function($scope, NotificacaoService, $ionicLoading, $state, SessionService, SocioService, $sce, varNotQtde, $timeout) {

  $scope.notificacoes  = [];
  
  if (SessionService.get('socio')) {
    $scope.socio = JSON.parse(SessionService.get('socio'));
  } else {
    $scope.socio = null;
  }

  //Zera badge do Tab Novidades
  varNotQtde.qtde = '';

  //Zera q quantidade de notificação não lidas para o sócio
  NotificacaoService.zerarQtde($scope.socio.id).then(function(result){
    // console.log("qtde zerada");
  });

  $scope.contador = [];

  $scope.$on('$ionicView.enter', function() {
    if ($scope.notificacoes.length == 0) {
      $ionicLoading.show();
      NotificacaoService.buscar().then(function(result) {
        $scope.notificacoes = result;
        $scope.notificacoes.forEach(function(x, item, index) {
          if(x.socio != null){
            var arr = x.socio.split(';').filter(String);
            var indice = arr.indexOf($scope.socio.id.toString());
            if(indice == -1){
                $scope.contador.splice(0,0,item); //guarda variável dos itens que não devem ser mostrados
            }
          }
        });

        // excluir os itens que não devem ser mostrados. Foi preciso colocar fora do forEach pela ordenação
        for (var i = 0; i < $scope.contador.length; i++) {
            $scope.notificacoes.splice($scope.contador[i], 1);
        }

        $ionicLoading.hide();

      });
    }

    if(window.cordova) {
      window.ga.trackView('Notificacoes');
    }

  });


  $scope.pullToRefresh = function() {
    $ionicLoading.show();
    $scope.notificacoes  = [];
    
    NotificacaoService.buscar().then(function(result) {
      $scope.notificacoes = result;
      $scope.notificacoes.forEach(function(x, item, index) {
        if(x.socio != null){
          var arr = x.socio.split(';').filter(String);
          var index = arr.indexOf($scope.socio.id.toString());
          $timeout(function () {
            $scope.notificacoes.splice(index, 1);
          });
        }
      });
      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
    });
  }

  $scope.retornaFormato = function(dataPost){
    var s = 1000;
    var m = s * 60;
    var h = m * 60;
    var d = h * 24;
    var mes = d * 30;

    var dat = new Date(dataPost);
    var dat = Date.now() - dat.getTime();

    if (dat > mes)
      return '' + (dat / mes).toFixed(0) + ' meses atrás'
    if (dat > d)
      return '' + (dat / d).toFixed(0) + ' dias atrás'
    if (dat > h)
        return '' + (dat / h).toFixed(0) + ' horas atrás';
    if (dat > m)
      return '' + (dat / m).toFixed(0) + ' minutos atrás';
    if (dat > s)
      return '' + (dat / s).toFixed(0) + ' segundos atrás';    
    return '' + (dat).toFixed(0) + ' ms atrás';
  };


  $scope.irLink = function(notificacao) {

    //Verifica se o sócio já clicou no link, aí não precisa atualizar novamente, só vai pro link
    if (notificacao.socioLido) {

      var arr = notificacao.socioLido.split(';').filter(String);
      var index = arr.indexOf($scope.socio.id.toString());
      if(index >= 0){
        // $state.go(notificacao.link);
      } else {
        notificacao.socioLido = notificacao.socioLido + $scope.socio.id + ';';
        NotificacaoService.marcarLido(notificacao.id, notificacao.socioLido).then(function(res) {});
      }
    } else {

      notificacao.socioLido = $scope.socio.id + ';';
      NotificacaoService.marcarLido(notificacao.id, notificacao.socioLido).then(function(res) {});

    };
  
    //Direciona a página
    if(notificacao.link != null & notificacao.link != ''){
      if(notificacao.parametro != null & notificacao.parametro != ''){
        $state.go(notificacao.link, { id:notificacao.parametro });
      }else{
        $state.go(notificacao.link);
      };
    };

  };

  $scope.verificaLida = function(notificacao){
    var style = {};

    if (notificacao.socioLido) {
      var arr = notificacao.socioLido.split(';').filter(String);
      var index = arr.indexOf($scope.socio.id.toString());
      if(index >= 0){
        style.fontWeight = 'normal';
        // style.background = 'white'
      } else {
        style.fontWeight = 'bold';
        // style.background = '#fafafa'
      }
    } else {
      style.fontWeight = 'bold';
      // style.background = '#fafafa'
    }

    return style;
  };

})


.controller('NovidadesCtrl', function($scope, NovidadeService, NotificacaoService, $ionicLoading, EstabelecimentoService, $state, SessionService, SocioService, $sce, varNotQtde) {
  $scope.novidades  = [];
  $scope.notificacoes = [];
  $scope.url        = "http://cikluslive.com.br/uploads/";
  $scope.urlImagens = imagens;

  if (SessionService.get('socio')) {
    $scope.socio = JSON.parse(SessionService.get('socio'));
  } else {
    $scope.socio = null;
  }

  $scope.$on('$ionicView.enter', function() {
    if ($scope.novidades.length == 0) {
      $ionicLoading.show();
      NovidadeService.buscarNovidades().then(function(result) {
        $scope.novidades = result;
        result.forEach(function(x) {
          EstabelecimentoService.buscarPorId(x.estabelecimento.id).then(function(res) {
            x.estabelecimento = res;
          });
        });
        $ionicLoading.hide();
      });
    };

    //Buscar qtde de notificações que o usuário possui não lidas
    NotificacaoService.buscarQtde($scope.socio.id).then(function(result){
      if(result.length == 0){
        $scope.notificacaoQtde = 0;
      } else {
        $scope.notificacaoQtde  = result[0].qtde;  
      }
    });


    if(window.cordova) {
      window.ga.trackView('Novidades');
    }

  });

  $scope.recuperarLogo = function(estabelecimento) {
    var retorno = "";
    if (estabelecimento.imagens) {
      if (estabelecimento.imagens.length > 0) {
        estabelecimento.imagens.forEach(function(x, y) {
          if (x.ordem == 2) {
            retorno = $scope.urlImagens + x.id + '.' + x.extensao;
          }
        });
        return retorno;
        } else {
        return "";
      }
    } else {
      return "";
    }
  };

  $scope.curtiuNovidade = function(novidade) {
    if (novidade.curtidas) {
      var arr = novidade.curtidas.split(';').filter(String);
      var index = arr.indexOf($scope.socio.id.toString());
      if (index >= 0) {
        return true; 
      } else {
        return false;
      }
    } else {
      return false;
    }
  };

  $scope.curtir = function(novidade) {
    if (novidade.curtidas) {
      novidade.curtidas = novidade.curtidas + $scope.socio.id + ';';
    } else {
      novidade.curtidas = $scope.socio.id + ';';
    }
    NovidadeService.curtir(novidade.id, novidade.curtidas).then(function(res) {
      // console.log(res);
    });
  };

  $scope.descurtir = function(novidade) {
    var arr = novidade.curtidas.split(';').filter(String);
    var index = arr.indexOf($scope.socio.id.toString());
    if (index >= 0) {
      arr.splice(index, 1);
      novidade.curtidas = arr.join(';');
      if (novidade.curtidas != "") 
        novidade.curtidas += ";";
      NovidadeService.curtir(novidade.id, novidade.curtidas).then(function(res) {
        // console.log(res);
      });
    }
  };

  $scope.trustSrc = function(src) {
    return $sce.trustAsResourceUrl(src);
  };

  $scope.irParaLink = function(link, parametro) {
    if(link != null & link != ''){
      if(parametro != null & parametro != ''){
        $state.go(link, { id:parametro });
      }else{
        $state.go(link);
      }
    }
  };

  $scope.retornaFormato = function(dataPost){
    var s = 1000;
    var m = s * 60;
    var h = m * 60;
    var d = h * 24;
    var mes = d * 30;

    var dat = new Date(dataPost);
    var dat = Date.now() - dat.getTime();

    if (dat > mes)
      return '' + (dat / mes).toFixed(0) + ' meses atrás'
    if (dat > d)
      return '' + (dat / d).toFixed(0) + ' dias atrás'
    if (dat > h)
        return '' + (dat / h).toFixed(0) + ' horas atrás';
    if (dat > m)
      return '' + (dat / m).toFixed(0) + ' minutos atrás';
    if (dat > s)
      return '' + (dat / s).toFixed(0) + ' segundos atrás';    
    return '' + (dat).toFixed(0) + ' ms atrás';
  };

  $scope.pullToRefresh = function() {

    $ionicLoading.show();

    $scope.novidades  = [];
    
    NovidadeService.buscarNovidades().then(function(result) {
      $scope.novidades = result;
      result.forEach(function(x) {
        EstabelecimentoService.buscarPorId(x.estabelecimento.id).then(function(res) {
          x.estabelecimento = res;
        });
      });
      $scope.$broadcast('scroll.refreshComplete');
      $ionicLoading.hide();
    });
  }

  $scope.irNotificacoes = function(){
    $scope.badgeCount = 0;
    $state.go('tab.notificacoes');
  }

  // $scope.qtdeNaoLida = function(){
  //   total = 0;
  //   $scope.notificacoes.forEach(function(x) {
      
  //     if (x.socioLido) {
  //       var arr = x.socioLido.split(';').filter(String);
  //       var index = arr.indexOf($scope.socio.id.toString());
  //       if (index >= 0) {
  //         soma = 0;
  //       } else {
  //         soma = 1;
  //       }
  //     } else {
  //       soma = 1;
  //     }

  //     total += soma;

  //   });

  //   return total;

  // };

})

.controller('MinhaContaCtrl', function($scope, $ionicPlatform, SessionService, $ionicPopup, SocioService, TransacaoService, $state, IuguService, $ionicModal, $ionicLoading, EstabelecimentoService) {
 
  $scope.modalCadastro = {};
  $scope.aba = 0;
  $scope.preferencias = true;
  if (SessionService.get('socio')) {
    $scope.socio = JSON.parse(SessionService.get('socio'));
  } else {
    $scope.socio = null;
  }
  $scope.transacoes = [];
  $scope.somaTransacoes = 0;
  $scope.busca = {};
  $scope.modalFaturas = {};
  $scope.faturas = [];
  $scope.comentario = {};
  $scope.cartoes = [];
  $scope.cartao = {};
  $scope.dadosCarregados = false;
  $scope.socioLogado = false;

  $scope.pullToRefresh = function() {
    TransacaoService.buscarPorSocio($scope.socio.id).then(function(results) {
      $scope.transacoes = [];
      $scope.somaTransacoes = 0;
      $scope.transacoes = results;
      $scope.transacoes.forEach(function (val) {
        $scope.somaTransacoes += val.valorDesconto;
      });
      $scope.$broadcast('scroll.refreshComplete');
    });
  };

  $scope.recuperarSocioIugu = function() {
  };

  $scope.recuperarCartoes = function() {
    if ($scope.socio.iugu) {
      IuguService.buscarCartoes($scope.socio.iugu.customer.id).then(function(resultado) {
        $scope.cartoes = [];
        $scope.cartoes = resultado;
      }, function(err) {
        $scope.cartoes = [];
      });
    } else {
      $scope.cartoes = [];
    }
  };

  $scope.editarNota = function(nota, index) {
    $scope.comentario.texto = nota.observacao;
    $ionicPopup.show({
      title: 'Avalie sua experiência',
      template: "<input type='text' placeholder='Deixe seu comentário (opcional) e depois clique na nota desejada...' ng-model='comentario.texto' />",
      scope: $scope,
      buttons: [
        {
          text: '<i class="ion-ios-star-outline"></i> Ruim',
          onTap: function(e) {
            $ionicLoading.show();
            EstabelecimentoService.avaliarTransacao(nota.id, 1, $scope.comentario.texto).then(function(response) {
              $ionicLoading.hide();
              nota.nota = 1;
              nota.observacao = $scope.comentario.texto;
              $scope.comentario.texto = "";
            });
          }
        },
        {
          text: '<i class="ion-ios-star-half"></i> <br/>Boa',
          onTap: function(e) {
            $ionicLoading.show();
            EstabelecimentoService.avaliarTransacao(nota.id, 2, $scope.comentario.texto).then(function(response) {
              $ionicLoading.hide();
              nota.nota = 2;
              nota.observacao = $scope.comentario.texto;
              $scope.comentario.texto = "";
            });
          }
        },
        {
          text: '<i class="ion-ios-star"></i> Ótima',
          onTap: function(e) {
            $ionicLoading.show();
            EstabelecimentoService.avaliarTransacao(nota.id, 3, $scope.comentario.texto).then(function(response) {
              $ionicLoading.hide();
              nota.nota = 3;
              nota.observacao = $scope.comentario.texto;
              $scope.comentario.texto = "";
            });
          }
        }
      ]
    });
  };

  $scope.excluirCartao = function(cartao, index) {
    $ionicLoading.show();
    IuguService.removerCartao($scope.socio.idIugu, cartao.id).then(function(resultado) {
      $scope.cartoes.splice(index, 1);
      $ionicLoading.hide();
      $ionicPopup.show({
        template: 'Cartão removido',
        buttons: [{ text: 'Ok' }]
      });
    });
  };

  $scope.recuperarPlano = function() {
    if ($scope.socio.iugu) {
      if($scope.iugu.status == 'canceled'){
        return 'Cancelado'
      }else{
        return $scope.socio.iugu.plan.name;
      }
    } else {
      return "Convite";
    }
  };


  $scope.$on('$ionicView.enter', function() {
    if(window.cordova) {
      window.ga.trackView('MinhaConta');
    }

    if($scope.socio == null){
      $ionicPopup.show({
        title: 'Acesso exclusivo',
        template: 'Faça já o seu login ou cadastre-se agora mesmo para ter acesso a este menu exclusivo Ciklus Live! <br><br> Você será direcionado para a tela de início. <br><br> Aproveite agora mesmo.',
        buttons: [{ text: 'OK' }]
      }).then(function(){
        $state.go('landing');
      });
    } else {
      $ionicLoading.show();
      var socio = SessionService.get('socio');
      if (!$scope.socio) {
        if (SessionService.get('socio')) {
          $scope.socio = JSON.parse(SessionService.get('socio'));
          SocioService.buscar($scope.socio.id).then(function(socio) {
            $scope.socio = socio;
            SessionService.persist('socio', $scope.socio);
            $scope.socioLogado = true;

            TransacaoService.buscarPorSocio($scope.socio.id).then(function(results) {
              $scope.transacoes = [];
              $scope.somaTransacoes = 0;
              $scope.transacoes = results;
              $scope.transacoes.forEach(function (val) {
                $scope.somaTransacoes += val.valorDesconto;
              });
            });

            if ($scope.socio.idIugu) {
              IuguService.buscarAssinatura($scope.socio.idIugu).then(function(result) {
                $scope.iugu = result;
                console.log($scope.iugu);
                $scope.socio.iugu = result;
                // $scope.recuperarSocioIugu();
                $scope.recuperarCartoes();
                $scope.recuperarExpiracao();
                $scope.recuperarPlano();
                $scope.dadosCarregados = true;
                $ionicLoading.hide();
              }, function(err) {
                $scope.dadosCarregados = true;
                $ionicLoading.hide();
              });
            } else {
              $scope.dadosCarregados = true;
              $ionicLoading.hide();
            }
          });
        } else {
          $scope.socioLogado = false;
          $ionicLoading.hide();
        }
      } else {
        $scope.socioLogado = true;
        TransacaoService.buscarPorSocio($scope.socio.id).then(function(results) {
          $scope.transacoes = [];
          $scope.somaTransacoes = 0;
          $scope.transacoes = results;
          $scope.transacoes.forEach(function (val) {
            $scope.somaTransacoes += val.valorDesconto;
          });
        });

        if (!$scope.dadosCarregados) {
          SocioService.buscar($scope.socio.id).then(function(socio) {
           $scope.socio = socio;
           SessionService.persist('socio', $scope.socio);
            if ($scope.socio.idIugu) {
              IuguService.buscarAssinatura($scope.socio.idIugu).then(function(result) {
                $scope.iugu = result;
                $scope.socio.iugu = result;
                // $scope.recuperarSocioIugu();
                $scope.recuperarCartoes();
                $scope.recuperarExpiracao();
                $scope.recuperarPlano();
                $scope.dadosCarregados = true;
                $ionicLoading.hide();
              }, function(err) {
                $ionicLoading.hide();
                $scope.dadosCarregados = true;
              });
            } else {
              $scope.recuperarCartoes();
              $scope.recuperarExpiracao();
              $scope.recuperarPlano();
              $scope.dadosCarregados = true;
              $ionicLoading.hide();
            }
          });       
        } else {
           $ionicLoading.hide();
        }  



        // SocioService.buscar($scope.socio.id).then(function(socio) {
        //  $scope.socio = socio;
        //  SessionService.persist('socio', $scope.socio);
        //   if ($scope.socio.idIugu) {
        //     IuguService.buscarAssinatura($scope.socio.idIugu).then(function(result) {
        //       $scope.iugu = result;
        //       $scope.socio.iugu = result;
        //       // $scope.recuperarSocioIugu();
        //       $scope.recuperarCartoes();
        //       $scope.recuperarExpiracao();
        //       $scope.recuperarPlano();
        //       $scope.dadosCarregados = true;
        //       $ionicLoading.hide();
        //     }, function(err) {
        //       $ionicLoading.hide();
        //       $scope.dadosCarregados = true;
        //     });
        //   } else {
        //     $scope.recuperarCartoes();
        //     $scope.recuperarExpiracao();
        //     $scope.recuperarPlano();
        //     $scope.dadosCarregados = true;
        //     $ionicLoading.hide();
        //   }
        // });

      }
    }
  });

  $scope.recuperarExpiracao = function() {
    if ($scope.socio.iugu) {
      return $scope.formatarData($scope.socio.iugu.current_period_end);
    } else {
      return $scope.formatarData($scope.socio.ativoAte);
    }
  };

  $scope.formatarDataFront = function(data) {
    var d = data.split('-');
    return d[2] + '/' + d[1] + '/' + d[0];
  };

  $scope.cadastrarCartao = function() {
    if ($scope.cartao.numero.toString().length == 16 && ($scope.cartao.cvv.toString().length == 3 || $scope.cartao.cvv.toString().length == 4)
      && $scope.cartao.nome.length > 3 && $scope.cartao.mes.length == 2 && $scope.cartao.ano.length == 2) {
      $scope.cartao.numero = $scope.cartao.numero.toString();
      $scope.cartao.cvv = $scope.cartao.cvv.toString();
      $ionicLoading.show();
      if ($scope.assinarPlano) {
        IuguService.associarContaPagamento($scope.socio, $scope.cartao, $scope.assinarPlano.id).then(function(pagamento) {
          var resultado = pagamento;
          if (resultado) {
            if (resultado.current_transaction.status == "paid") {
              $scope.socio.idIugu = resultado.id;
              $ionicLoading.hide();
              SocioService.atualizar($scope.socio).then(function(response) {
                $ionicPopup.show({
                  title: 'Sucesso',
                  template: 'Assinatura realizada com sucesso',
                  buttons: [
                    { 
                      text: 'OK', 
                      onTap: function() {
                        IuguService.buscarAssinatura($scope.socio.idIugu).then(function(result) {
                          $scope.socio.iugu = result;
                          // $scope.recuperarSocioIugu();
                          $scope.recuperarCartoes();
                        });
                        $scope.cartao = { numero: '', cvv: '', nome: '', mes: '', ano: '' };
                        $scope.modalCartao.hide();
                      }
                    }
                  ]
                });
              });
            } else {
              $ionicLoading.hide();
              $ionicPopup.show({
                title: 'Erro ao realizar pagamento',
                template: 'Por favor, verifique os dados preenchidos e tente novamente.',
                buttons: [
                  { text: 'OK' }
                ]
              });
            }
          } else {
            $ionicLoading.hide();
            $ionicPopup.show({
              title: 'Erro ao realizar pagamento',
              template: 'Por favor, verifique os dados preenchidos e tente novamente.',
              buttons: [
                { text: 'OK' }
              ]
            });
          }
        }, function(err) {
          $ionicLoading.hide();
          $ionicPopup.show({
            title: 'Erro ao realizar pagamento',
            template: 'Por favor, verifique os dados preenchidos e tente novamente.',
            buttons: [
              { text: 'OK' }
            ]
          });
        });
      } else {
        IuguService.criarCartao($scope.cartao.nome, $scope.cartao.numero.toString(), $scope.cartao.cvv.toString(), $scope.cartao.mes, $scope.cartao.ano, $scope.socio.iugu.customer.id).then(function(cartao) {
          IuguService.alterarCartaoAssinatura($scope.socio.idIugu, cartao.id, $scope.socio.iugu.customer.id).then(function(res) {
            IuguService.buscarAssinatura($scope.socio.idIugu).then(function(result) {
              $scope.socio.iugu = result;
              // $scope.recuperarSocioIugu();
              $scope.recuperarCartoes();
            });
            $ionicLoading.hide();
            $ionicPopup.show({
              title: 'Cartão salvo',
              template: "Seu cartão foi alterado com sucesso!",
              scope: $scope,
              buttons: [
                {
                  text: 'Ok',
                  onTap: function() {
                    $scope.cartao = { numero: '', cvv: '', nome: '', mes: '', ano: '' };
                    $scope.modalCartao.hide();
                  }
                }
              ]
            });
          }, function(err) {
            $ionicLoading.hide();
            $ionicPopup.show({
                title: 'Dados inválidos',
                template: "Por favor, verifique os dados do cartão.",
                scope: $scope,
                buttons: [
                  {
                    text: 'Ok'
                  }
                ]
              });
          });
        });      
      }
    } else {
      $ionicLoading.hide();
      $ionicPopup.show({
        title: 'Dados inválidos',
        template: "Por favor, verifique os dados do cartão.",
        scope: $scope,
        buttons: [
          {
            text: 'Ok'
          }
        ]
      });
    }
  };

  $scope.avaliarTransacao = function(transacao) {
    $ionicPopup.show({
      title: 'Avalie sua experiência',
      template: "<input type='text' placeholder='Deixe seu comentário (opcional) e depois clique na nota desejada...' ng-model='comentario.texto' />",
      scope: $scope,
      buttons: [
        {
          text: '<i class="ion-ios-star-outline"></i> Ruim',
          onTap: function(e) {
            $ionicLoading.show(); 
            EstabelecimentoService.avaliarTransacao(transacao.id, 1, $scope.comentario.texto).then(function(response) {
              $ionicLoading.hide();
              transacao.nota = 1;
              transacao.observacao = $scope.comentario.texto;
              $scope.comentario.texto = "";
            });
          }
        },
        {
          text: '<i class="ion-ios-star-half"></i> <br/>Boa',
          onTap: function(e) {
            $ionicLoading.show();
            EstabelecimentoService.avaliarTransacao(transacao.id, 2, $scope.comentario.texto).then(function(response) {
              $ionicLoading.hide();
              transacao.nota = 2;
              transacao.observacao = $scope.comentario.texto;
              $scope.comentario.texto = "";
            });
          }
        },
        {
          text: '<i class="ion-ios-star"></i> Ótima',
          onTap: function(e) {
            $ionicLoading.show();
            EstabelecimentoService.avaliarTransacao(transacao.id, 3, $scope.comentario.texto).then(function(response) {
              $ionicLoading.hide();
              transacao.nota = 3;
              transacao.observacao = $scope.comentario.texto;
              $scope.comentario.texto = "";
            });
          }
        }
      ]
    });
  };

  $scope.buscarIconeNota = function(nota) {
    var classe = "";
    switch (nota) {
      case 1:
        classe = "ion-ios-star-outline";
        break;
      case 2:
        classe = "ion-ios-star-half";
        break;
      case 3:
        classe = "ion-ios-star";
        break;
    }
    return classe;
  };

  $scope.formatarPreco = function(preco) {
    preco = preco.toString();
    if (preco.length > 1) {
      var precoFormatado = preco.substr(0, preco.length - 2) + ','
                          + preco.substr(preco.length - 2, preco.length - 1);
      return precoFormatado;
    } else {
      return "";
    }
  };

  $scope.formatarData = function(data) {    
    if (data)
      return data.substring(8, 10) + '/' + data.substring(5, 7) + '/' + data.substring(0, 4);
    else
      return "";
  };

  $scope.verFaturas = function() {
    if (!$scope.socio.iugu) {
      $ionicModal.fromTemplateUrl('templates/modal-faturas.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(modal) {
          $scope.modalFaturas = modal;
          $scope.modalFaturas.show();
        });
    } else {
      $ionicLoading.show();
      IuguService.buscarFaturas($scope.socio.idIugu).then(function(results) {
        if (results.length > 0) {
          IuguService.buscarCartoes(results[0].customer.id).then(function(resultado) {
            $scope.cartoes = resultado;
          });
        }
        $ionicLoading.hide();
        $scope.faturas = results;
        $ionicModal.fromTemplateUrl('templates/modal-faturas.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(modal) {
          $scope.modalFaturas = modal;
          $scope.modalFaturas.show();
        });
      });
    }
  };

  $scope.fecharFaturas = function() {
    $scope.modalFaturas.hide();
  };

  $scope.alterarSenha = function() {
    $scope.socio.senha = "";
    $ionicPopup.show({
      template: '<input type="password" ng-model="socio.senha">',
      title: 'Digite sua nova senha',
      scope: $scope,
      buttons: [
        {
          text: '<b>Salvar</b>',
          type: 'button-positive',
          onTap: function(e) {
            if (!$scope.socio.senha.length > 3) {
              $ionicPopup.alert({
                title: 'Ops',
                template: 'Por favor, digite a nova senha (maior que 4 caracteres).'
              });
              e.preventDefault();
            } else {
              $ionicLoading.show();
              SocioService.alterarSenha($scope.socio.id, $scope.socio.senha).then(function() {
                $ionicLoading.hide();
                $ionicPopup.alert({
                  title: 'Sucesso',
                  template: 'Sua senha foi alterada.'
                });
              });
            }
          }
        },
        {
          text: '<b>Cancelar</b>',
          type: 'button-negative',
          onTap: function(e) {
            return true;
          }
        }
      ]
    });
  };

  $scope.logout = function() {
    var confirmPopup = $ionicPopup.confirm({
      title: 'Logout',
      template: 'Deseja sair da sua conta?'
    });
    
    confirmPopup.then(function(res) {
       if(res) {
          $scope.socio = null;
          SessionService.destroy('socio');
          $scope.socioLogado = false;
          $state.go('landing');
        }
     });

  };

  $scope.formatarPreco = function(preco) {
    preco = preco.toString();
    if (preco.length > 1) {
      var precoFormatado = preco.substr(0, preco.length - 2) + ','
                          + preco.substr(preco.length - 2, preco.length - 1);
      return precoFormatado;
    } else {
      return "";
    }
  };

  $scope.formatarData = function(data) {
    if (data)
      return data.substring(8, 10) + '/' + data.substring(5, 7) + '/' + data.substring(0, 4);
    else
      return "";
  };

  $scope.showPreferencias = function() {
    $scope.preferencias = !$scope.preferencias;
  };

  $scope.mudarAba = function(aba) {
    $scope.aba = aba;
  };

  $scope.buscarClassePorAba = function(aba) {
    if ($scope.aba == aba) {
      return { "background-color":"#d60b52", "color": "#fff" };
    } else {
      return { };
    }
  };

  $scope.alterarCartao = function() {
    $ionicModal.fromTemplateUrl('templates/modal-cartao.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modalCartao = modal;
      $scope.modalCartao.show();
    });
  };

  $scope.fecharCartao = function() {
    $scope.modalCartao.hide();
  };

  $scope.alterarPlano = function() {
    $ionicLoading.show();
    IuguService.buscarPlanos().then(function(planos) {
      $scope.planos = planos;
      $ionicModal.fromTemplateUrl('templates/modal-planos.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.modalPlanos = modal;
        $scope.modalPlanos.show();
        $ionicLoading.hide();
      });
    });    
  };

  $scope.fecharPlanos = function() {
    $scope.modalPlanos.hide();
  };

  $scope.mudarPlano = function(plano) {
    if ($scope.iugu) {
      $ionicPopup.show({
        title: 'Deseja realmente alterar o seu plano?',
        template: 'A alteração será realizada à partir da próxima cobrança.',
        scope: $scope,
        buttons: [
          {
            text: 'Sim, alterar',
            type: 'button-positive',
            onTap: function(e) {
              $ionicLoading.show();
              if ($scope.iugu.status != "canceled") {
                IuguService.alterarAssinatura($scope.socio.idIugu, plano.id).then(function(resultado) {
                  IuguService.buscarAssinatura($scope.socio.idIugu).then(function(result) {
                    $ionicLoading.hide();
                    $ionicPopup.show({
                      title: 'Sucesso',
                      template: 'Seu plano foi alterado com sucesso.',
                      scope: $scope,
                      buttons: [
                        {
                          text: 'Ok!',
                        }
                      ]
                    });
                    $scope.socio.iugu = result;
                  });
                  $scope.modalPlanos.hide();
                }, function(err) {
                  console.log(err);
                });
              } else {
                var cartao = $scope.iugu.card;

                IuguService.associarContaPagamento($scope.socio, cartao, plano.id).then(function(pagamento) {
                  var resultado = pagamento;
                  if (resultado) {
                    if (resultado.current_transaction.status == "paid") {
                      $scope.socio.idIugu = resultado.id;
                      $ionicLoading.hide();
                      SocioService.atualizar($scope.socio).then(function(response) {
                        $ionicPopup.show({
                          title: 'Sucesso',
                          template: 'Assinatura realizada com sucesso',
                          buttons: [
                            { 
                              text: 'OK', 
                              onTap: function() {
                                IuguService.buscarAssinatura($scope.socio.idIugu).then(function(result) {
                                  $scope.socio.iugu = result;
                                  // $scope.recuperarSocioIugu();
                                  $scope.recuperarCartoes();
                                });
                                $scope.cartao = { numero: '', cvv: '', nome: '', mes: '', ano: '' };
                                $scope.modalPlanos.hide();
                              }
                            }
                          ]
                        });
                      });
                    } else {
                      $ionicLoading.hide();
                      $ionicPopup.show({
                        title: 'Erro ao realizar pagamento',
                        template: 'Por favor, verifique os dados preenchidos e tente novamente.',
                        buttons: [
                          { text: 'OK' }
                        ]
                      });
                    }
                  } else {
                    $ionicLoading.hide();
                    $ionicPopup.show({
                      title: 'Erro ao realizar pagamento',
                      template: 'Por favor, verifique os dados preenchidos e tente novamente.',
                      buttons: [
                        { text: 'OK' }
                      ]
                    });
                  }
                }, function(err) {
                  $ionicLoading.hide();
                  $ionicPopup.show({
                    title: 'Erro ao realizar pagamento',
                    template: 'Por favor, verifique os dados preenchidos e tente novamente.',
                    buttons: [
                      { text: 'OK' }
                    ]
                  });
                });
              }
            }
          },
          {
            text: 'Cancelar',
            onTap: function(e) {
              $scope.modalPlanos.hide();
            }
          }
        ]
      });
    } else {
      $scope.assinarPlano = plano;
      $ionicModal.fromTemplateUrl('templates/modal-cartao.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.modalCartao = modal;
        $scope.modalPlanos.hide();
        $scope.modalCartao.show();
      });
    }
  };

  $scope.cancelarPlano = function(plano) {
    if ($scope.iugu) {
      $ionicPopup.show({
        title: 'Deseja realmente cancelar a sua assinatura?',
        template: 'Fazendo o cancelamento as próximas cobranças não serão efetuadas e você ficará impossibitado de economizar pela Ciklus.</br></br>Tem certeza que não quer mais economizar?',
        scope: $scope,
        buttons: [
          {
            text: 'Sim, cancelar',
            type: 'button-positive',
            onTap: function(e) {
              IuguService.cancelarAssinatura($scope.iugu.id).then(function(response) {
                IuguService.buscarAssinatura($scope.socio.idIugu).then(function(result) {
                  $scope.iugu = result;
                  $scope.socio.iugu = result;
                  // $scope.recuperarSocioIugu();
                  $scope.recuperarCartoes();
                  $scope.recuperarExpiracao();
                  $scope.recuperarPlano();
                  $scope.dadosCarregados = true;
                  $ionicLoading.hide();
                }, function(err) {
                  $ionicLoading.hide();
                  $scope.dadosCarregados = true;
                });
                $ionicPopup.show({
                  title: 'Pronto',
                  template: 'Sua assinatura foi cancelada com sucesso.',
                   buttons: [
                    { text: 'Ok',
                      onTap: function(e){
                        $scope.modalPlanos.hide();
                      }
                    }
                  ]
                });
              });
            }
          },
          {
            text: 'Cancelar',
            onTap: function(e) {
              $scope.modalPlanos.hide();
            }
          }
        ]
     });
    }
  }

  $scope.salvar = function() {
    if ($scope.socio.nome && $scope.socio.cep && $scope.socio.dataNasc && $scope.socio.telefone && $scope.socio.email) {
      $ionicLoading.show();
      SocioService.atualizar($scope.socio).then(function(result) {
        $ionicLoading.hide();
        SessionService.persist('socio', result);
        $ionicPopup.show({
          title: 'Atualizado!',
          template: 'Seus dados foram atualizados com sucesso.',
          buttons: [
            { text: 'OK' }
          ]
        });
      });
    } else {
      $ionicPopup.show({
        template: 'Ops, preencha todos os dados',
        buttons: [{
          text: 'Ok'
        }]
      });
    }
  };

});
