angular.module('ciklus', ['ionic', 'ciklus.controllers', 'ciklus.services', 'ciklus.directives', 'angular-md5', 'ngCookies', 'ngSanitize', 'ngCordova'])

.value('varNotQtde', {
  qtde: ''
})

.run(function($ionicPlatform, SessionService, $state, $cordovaGoogleAnalytics, $ionicHistory, $timeout, $rootScope, SocioService, NotificacaoService) {

  $rootScope.$on('$stateChangeSuccess', function (e, url) {
    if (!$ionicHistory.currentView()) {
      return
    }

    function getCurrentStateId () {
      if ($state && $state.current && $state.current.name) {
        return buildIdFromCurrentState($state.current.name)
      }

      // if something goes wrong make sure its got a unique stateId
      return ionic.Utils.nextUid()
    }

    function buildIdFromCurrentState (id) {
      if ($state.params) {
        for (var key in $state.params) {
          if ($state.params.hasOwnProperty(key) && $state.params[key]) {
            id += '_' + key + '=' + $state.params[key]
          }
        }
      }
      return id
    }

    $timeout(function () {
      var currentView = $ionicHistory.currentView();
      currentView.stateId = getCurrentStateId();
      currentView.stateName = $state.current.name;
      currentView.stateParams = angular.copy($state.params);
    });
  });

  $ionicPlatform.ready(function() {
    if (localStorage.getItem('socio')) {
      var socio = JSON.parse(localStorage.getItem('socio'));
      if (socio.id) {
        SocioService.atualizarOnesignal(socio.id);
        $state.go('tab.agenda');
      }
    }
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }

    var notificationOpenedCallback = function(jsonData) {
      console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
    };

    if (window.cordova) {
      window.plugins.OneSignal
        .startInit("f8b8d4f7-d32d-4abc-92d1-d3a707f9a760")
        .inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.None)
        .handleNotificationOpened(notificationOpenedCallback)
        .endInit();

        if(typeof window.ga !== undefined) {
          window.ga.startTrackerWithId("UA-48248255-2");
        }
    }

    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

// .config(['$sceDelegateProvider', function($sceDelegateProvider) {
//      $sceDelegateProvider.resourceUrlWhitelist(['self', "/^https?:\/\/(cdn\.)?iugu.com/"]);
//  }])cordova plugin add cordova-plugin-google-analytics

.config(['$ionicConfigProvider', function($ionicConfigProvider) {
  $ionicConfigProvider.tabs.position('bottom');
}])

.config(['$httpProvider', function ($httpProvider) {
  //Reset headers to avoid OPTIONS request (aka preflight)
  $httpProvider.defaults.headers.common = {};
  $httpProvider.defaults.headers.post = {};
  $httpProvider.defaults.headers.delete = {};
  $httpProvider.defaults.headers.put = {};
  $httpProvider.defaults.headers.put["Content-Type"] = "text/plain";
  $httpProvider.defaults.headers.patch = {};
}])

.config(['$compileProvider', function($compileProvider){
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(geo|mailto|tel|maps):/);
}])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  .state('landing', {
    url: '/landing',
    templateUrl: 'templates/landing.html',
    controller: 'LandingCtrl'
  })

  // setup an abstract state for the tabs directive
  .state('tab', {
    url: '/tab',
    abstract: true,
    controller: 'TabsCtrl',
    templateUrl: 'templates/tabs.html'
  })

  .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'DashCtrl'
      }
    }
  })

  .state('tab.categorias', {
    url: '/categorias',
    views: {
      'tab-categorias': {
        templateUrl: 'templates/tab-categorias.html',
        controller: 'CategoriasCtrl'
      }
    }
  })

  .state('tab.agenda', {
    url: '/agenda',
    views: {
      'tab-agenda': {
        templateUrl: 'templates/tab-agenda.html',
        controller: 'AgendaCtrl'
      }
    }
  })

  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'MinhaContaCtrl'
      }
    }
  })

  .state('tab.estabelecimentos', {
    url: '/estabelecimentos',
    views: {
      'tab-categorias': {
        templateUrl: 'templates/estabelecimentos.html',
        controller: 'EstabelecimentosCtrl'
      }
    },
    params: {
      categoria: null,
      agendaDia: null
    }
  })

  .state('tab.estabelecimentosagenda', {
    url: '/estabelecimentos',
    views: {
      'tab-agenda': {
        templateUrl: 'templates/estabelecimentos.html',
        controller: 'EstabelecimentosCtrl'
      }
    },
    cache: false,
    params: {
      categoria: null,
      agendaDia: null
    }
  })

  .state('tab.notificacoes', {
    url: '/notificacoes',
    views: {
      'tab-novidades': {
        templateUrl: 'templates/notificacoes.html',
        controller: 'NotificacoesCtrl'
      }
    }
  })

  .state('tab.novidades', {
    url: '/novidades',
    views: {
      'tab-novidades': {
        templateUrl: 'templates/novidades.html',
        controller: 'NovidadesCtrl'
      }
    }
  })

  .state('tab.eventos', {
    url: '/eventos',
    views: {
      'tab-eventos': {
        templateUrl: 'templates/eventos.html',
        controller: 'EventosCtrl'
      }
    }
  })

  .state('tab.evento', {
    url: '/eventos/:id',
    views: {
      'tab-eventos': {
        templateUrl: 'templates/evento.html',
        controller: 'EventoCtrl'
      }
    },
    cache: false,
    params: {
      evento: null
    }
  })

  .state('tab.estabelecimentoagenda', {
    url: '/estabelecimentos/:id',
    views: {
      'tab-agenda': {
        templateUrl: 'templates/estabelecimento.html',
        controller: 'EstabelecimentoCtrl'
      }
    }
  })

  .state('tab.estabelecimento', {
    url: '/estabelecimentos/:id',
    views: {
      'tab-categorias': {
        templateUrl: 'templates/estabelecimento.html',
        controller: 'EstabelecimentoCtrl'
      }
    }
  })

  // .state('tab.estabelecimentonovidades', {
  //   url: '/estabelecimentos/:id',
  //   views: {
  //     'tab-novidades': {
  //       templateUrl: 'templates/estabelecimento.html',
  //       controller: 'EstabelecimentoCtrl'
  //     }
  //   }
  // })
  ;

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/landing');

});
