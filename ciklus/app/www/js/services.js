var rest = "http://ciklus-server.westus2.cloudapp.azure.com/";
// var rest = "http://localhost/";

var restIugu = "https://api.iugu.com/v1/";
var apiTokenIugu = "a23ab6ccd33c84395ac7feb10d974ce7";
var accountIdIugu = "6D4A89C10C004CBFB4D3157889EF50A0";

// var apiPagarMe = "ak_test_V0Z4orzhzFDMOD6Hf2vmKz8JdRVHrO"; //Ambiente de Teste
var apiPagarMe = "ak_live_8TI9GTYHQTbqEnXuoUu5tneP2VqA6O"; //Ambiente de Produção

angular.module('ciklus.services', [])

.service('IuguService', ['$http', function($http) {
  return {
    buscarUsuarioIugu: function(idIugu) {
      // return $http.get(restIugu + 'customers/' + idIugu, { params: { "api_token": apiTokenIugu } }).then(function(result) {
      //   return result.data;
      // });
    },

    buscarAssinatura: function(idIugu) {
      return pagarme.client.connect({ api_key: apiPagarMe })
        .then(client => client.subscriptions.find({ id: idIugu }))
        .then(subscription => subscription);
    },

    buscarCartoes: function(idUsuario) {
      return pagarme.client.connect({ api_key: apiPagarMe })
                            .then(client => client.cards.all({customer_id: idUsuario}))
                            .then(cards => cards);
    },

    buscarPlanos: function() {
      return pagarme.client.connect({ api_key: apiPagarMe })
        .then(client => client.plans.all({ count: 10, page: 1 }))
        .then(plans => plans);
    },

    buscarFaturas: function(socioIuguId) {
      return pagarme.client.connect({ api_key: apiPagarMe })
                            .then(client => client.subscriptions.findTransactions({ id: socioIuguId }))
                            .then(subscription => subscription);
    },

    cancelarAssinatura: function(idAssinatura) {
      return pagarme.client.connect({ api_key: apiPagarMe })
              .then(client => client.subscriptions.cancel({ id: idAssinatura }))
              .then(subscription => subscription);
    },

    alterarAssinatura: function(idAssinatura, plano) {
      return pagarme.client.connect({ api_key: apiPagarMe })
                            .then(client => client.subscriptions.update({ id: idAssinatura, plan_id: plano }))
                            .then(subscription => subscription);
    },

    removerCartao: function(idIugu, idCartao) {
      return $http.post(rest + 'socio/removerCartao', { api_key: apiTokenIugu, cartaoId: idCartao, socioId: idIugu }).then(function(response) {
        return response.data;
      });
    },

    criarCartao: function(nome, numero, cvv, mes, ano, socioIugu) {
      var card = {} 
      card.card_holder_name = nome;
      card.card_expiration_date = mes + '/' + ano;
      card.card_number = numero;
      card.card_cvv = cvv;
      var cardValidations = pagarme.validate({card: card});
      return pagarme.client.connect({ api_key: apiPagarMe })
                    .then(client => client.cards.create({
                      card_number: card.card_number,
                      card_holder_name: card.card_holder_name,
                      card_expiration_date: mes.toString() + ano.toString(),
                      card_cvv: cvv,
                      customer_id: socioIugu
                    }))
                    .then(card => card);
    },

    associarContaPagamento: function(socioAuxiliar, cartao, plano) {
      var card = {};
      if (cartao.id) {
        return pagarme.client.connect({ api_key: apiPagarMe })
          .then(client => client.subscriptions.create({
              plan_id: plano,
              card_id: cartao.id,
              customer: {
                email: socioAuxiliar.email,
                name: socioAuxiliar.nome,
                document_number: socioAuxiliar.cpf
              }
            }).then(result => result)
          );
      } else {
        card.card_holder_name = cartao.nome;
        card.card_expiration_date = cartao.mes + '/' + cartao.ano;
        card.card_number = cartao.numero;
        card.card_cvv = cartao.cvv;
        return pagarme.client.connect({ api_key: apiPagarMe })
          .then(client => client.subscriptions.create({
              plan_id: plano,
              card_number: card.card_number,
              card_holder_name: card.card_holder_name,
              card_expiration_date: cartao.mes.toString() + cartao.ano.toString(),
              card_cvv: card.card_cvv,
              customer: {
                email: socioAuxiliar.email,
                name: socioAuxiliar.nome,
                document_number: socioAuxiliar.cpf
              }
            }).then(result => result)
          );
      }
    },

    alterarCartaoAssinatura: function(idAssinatura, cartao, socioIugu) {
      return pagarme.client.connect({ api_key: apiPagarMe })
                              .then(client => client.subscriptions.update({ 
                                id: idAssinatura, 
                                card_id: cartao,
                                payment_method: 'credit_card'
                              })).then(subscription => subscription); 
    }
  }
}])

.service('SessionService', ['$cookieStore', function ($cookieStore) {
  var localStoreAvailable = typeof (Storage) !== "undefined";
  this.store = function (name, details) {
    if (localStoreAvailable) {
      if (angular.isUndefined(details)) {
        details = null;
      } else if (angular.isObject(details) || angular.isArray(details) || angular.isNumber(+details || details)) {
        details = angular.toJson(details);
      };
      sessionStorage.setItem(name, details);
    } else {
      $cookieStore.put(name, details);
    };
  };

  this.persist = function(name, details) {
    if (localStoreAvailable) {
      if (angular.isUndefined(details)) {
        details = null;
      } else if (angular.isObject(details) || angular.isArray(details) || angular.isNumber(+details || details)) {
        details = angular.toJson(details);
      };
      localStorage.setItem(name, details);
    } else {
      $cookieStore.put(name, details);
    }
  };

  this.get = function (name) {
    if (localStoreAvailable) {
      return localStorage.getItem(name);
    } else {
      return $cookieStore.get(name);
    }
    return "";
  };

  this.destroy = function (name) {
    if (localStoreAvailable) {
      localStorage.setItem(name, "");
      sessionStorage.setItem(name, "");
    } else {
      $cookieStore.remove(name);
    };
  };

  this.clear = function() {
    localStorage.clear();
  };

  return this;
}])


.service('EmailService',  ['$http', function($http) {
  return {
    enviar: function(nome, email, mensagem) {
      return $http.get('http://cikluslive.com.br/action/ajax/enviar-email', { params: {nome: nome, email: email, mensagem: mensagem} }).then(function(response) {
        return response.data;
      });
    }
  }
}])


.service('CategoriaService',  ['$http', function($http) {
  return {
    resumoCategorias: function() {
      return $http.get(rest + 'categoria/find', { params: {where: {categoriaPai: null, status: 1}} }).then(function(response) {
        return response.data;
      });
    }
  }
}])

.service('TransacaoService',  ['$http', function($http) {
  return {
    buscarPorSocio: function(socioId) {
      return $http.get(rest + 'transacao/buscarPorSocio', { params: { socio: socioId } }).then(function(response) {
        return response.data;
      });
    }
  }
}])

.service('SocioService',  ['$http', 'md5', function($http, md5) {
  return {
   login: function(cpf, senha) {
      senha = md5.createHash(senha);
      return $http.get(rest + 'socio/find', { params: {where: { cpf: cpf, senha: senha }} }).then(function(response) {
        return response.data;
      });
    },

    validarVoucher: function(voucher) {
      return $http.get(rest + 'cupom/find', { params: { codigo: voucher, sort: 'utilizado ASC' } }).then(function(result) {
        return result.data;
      });
    },

    atualizar: function(socio) {
      //para evitar que resete a senha do usuário ao atualizar
      delete socio.senha;
      return $http.post(rest + 'socio/update/' + socio.id, socio).then(function(result) {
        return result.data;
      });
    },

    buscarCEP: function(cep) {
      return $http.get("https://viacep.com.br/ws/" + cep + "/json/").then(function(result) {
        return result.data;
      });
    },

    atualizarOnesignal: function(socio) {
      if (window.cordova) {
        return window.plugins.OneSignal.getIds(function(ids) {
          if (ids) {
            var idPush = ids.userId + ';' + ids.pushToken;
            return $http.post(rest + 'socio/update/' + socio, { onesignalId: idPush }).then(function(response) {
              return response.data;
            });
          } else {
            return true;
          };
        });
      } else {
        return true;
      }
    },

    alterarSenha: function(id, senha) {
      senha = md5.createHash(senha);
      return $http.post(rest + 'socio/update/' + id, { senha: senha }).then(function(result) {
        return true;
      });
    },

    atualizarCodigo: function(id) {
      min = Math.ceil(10);
      max = Math.floor(100);
      cod1 = Math.floor(Math.random() * (max - min)) + min;
      cod2 = Math.floor(Math.random() * (max - min)) + min;
      var codigoSocio = cod1.toString() + id.toString() + cod2.toString();
      return $http.post(rest + 'socio/update/' + id, { codigo: codigoSocio }).then(function(result) {
        return true;
      });
    },

    cadastrarAuxiliar: function(dados) {
      delete dados.id;
      return $http.post(rest + 'socioauxiliar/create/', dados).then(function(results) {
        return results.data;
      });
    },

    cadastrar: function(dados) {
      return $http.post(rest + 'socio/create', dados).then(function(result) {
        return result.data;
      });
    },

    atualizarVoucher: function(id, utilizado) {
      return $http.post(rest + 'cupom/update/' + id, { utilizado: utilizado }).then(function(response) {
        return response.data;
      });
    },

    verificarCPF: function(cpf) {
      return $http.get(rest + 'socio/find', { params: { cpf: cpf } }).then(function(results) {
        if (results.data.length > 0) {
          return false;
        } else {
          return true;
        }
      });
    },

    buscar: function(id) {
      return $http.get(rest + 'socio/' + id).then(function(res) {
        return res.data;
      });
    },

    gerarSenha: function(cpf, email) {
      return $http.post('http://cikluslive.com.br/action/ajax/criar-senha-app.php?cpf=' + cpf + '&email=' + email).then(function(res) {
        return res.data;
      });
    }
  }
}])

.service('NovidadeService', ['$http', function($http) {
  return {
    buscarNovidades: function() {
      return $http.get(rest + 'novidade/find', { params: { sort: 'createdAt DESC' } }).then(function(response) {
        return response.data;
      });
    },

    curtir: function(novidade, socios) {
      return $http.put(rest + 'novidade/update/' + novidade, { curtidas: socios })
        .then(function(response) {
          return response.data;
      });
    }
  }
}])

.service('NotificacaoService', ['$http', function($http) {
  return {
    buscar: function() {
      return $http.get(rest + 'notificacao/find', { params: { sort: 'createdAt DESC' } }).then(function(response) {
        return response.data;
      });
    },

    marcarLido: function(notificacao, socios) {
      return $http.put(rest + 'notificacao/update/' + notificacao, { socioLido: socios }).then(function(response) {
          return response.data;
      });
    },

    buscarQtde: function(socioId) {
      return $http.get(rest + 'notificacaoQtde/find', { params: {socio: socioId} }).then(function(response) {
        return response.data;
      });
    },

    zerarQtde: function(socioId) {
        return $http.get(rest + 'notificacaoQtde?socio=' + socioId).then(function(response) {
          if(response.data[0].qtde != 0){
            return $http.put(rest + 'notificacaoQtde/update/' + response.data[0].id, { qtde: 0 }).then(function(response2) {
              return true;
           });
          };
          return true;
      });
    }
  }
}])

.service('EstabelecimentoService',  ['$http', function($http) {
  return {
    buscarPorCategoria: function(categoria) {
      return $http.get(rest + 'estabelecimento/buscarPorCategoria', { params: {categoria: categoria} }).then(function(response) {
        return response.data;
      });
    },

    buscarTodos: function() {
      return $http.get(rest + 'estabelecimento/buscarTodosApp').then(function(response) {
        return response.data;
      });
    },

    buscarEventos: function() {
      var dateObj = new Date();
      var mes = dateObj.getUTCMonth() + 1; //mes from 1-12
      var dia = dateObj.getUTCDate();
      var ano = dateObj.getUTCFullYear();
      hoje = ano + "/" + mes + "/" + dia;
      return $http.get(rest + 'evento/find', { params: { where: { "data": { ">=" : hoje } } } }).then(function(response) {
        return response.data;
      });
    },

    buscarCapa: function(id, ordem) {
      return $http.get(rest + 'estabelecimentofoto/find', { params: { where: { ordem: ordem, estabelecimento: id } } }).then(function(response) {
        return response.data;
      });
    },

    buscarAgenda: function(dia) {
      var diaSemana = "";
      var promocoes = [];
      switch (dia) {
        case 0:
          diaSemana = "domingo";
          break;
        case 1:
          diaSemana = "segundaFeira";
          break;
        case 2:
          diaSemana = "tercaFeira";
          break;
        case 3:
          diaSemana = "quartaFeira";
          break;
        case 4:
          diaSemana = "quintaFeira";
          break;
        case 5:
          diaSemana = "sextaFeira";
          break;
        case 6:
          diaSemana = "sabado";
          break;
      }

      return $http.get(rest + 'estabelecimentoCategoria/buscarAgenda', { params: { dia: diaSemana } }).then(function(results) {
        return results.data;
      });
    },

    avaliarTransacao: function(id, nota, comentario) {
      return $http.put(rest + 'transacao/update/' + id, { nota: nota, observacao: comentario }).then(function(response) {
        return response.data;
      });
    },

    buscarPorId: function(id) {
      return $http.get(rest + 'estabelecimento/' + id).then(function(response) {
        return response.data;
      });
    }
  }
}]);
