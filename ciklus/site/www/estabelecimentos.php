<?php
include("includes/settings.php");
include("action/listar-estabelecimentos.php");
$scripts = "<script src='jsapp/estabelecimentos.js'></script>";
include("widgets/header.php"); ?>
<div class="col-lg-12 titulo-topo conainter-fluid">
	<div class="overlay-topo">
	<span class="text-center titulo-pagina overlay-titulo">ESTABELECIMENTOS</span>
	</div>
</div>
<div class="container">
	<div class="col-md-3" style="margin-bottom: 30px;">
		<div class="row">
			<div class="panel">
				<div class="panel panel-default estabelecimentos-filtros">
				  <div class="panel-heading estabelecimentos-filtros-header"><span class="text-center estabelecimentos-filtro">Filtros</span></div>
				  <div class="panel-body">
				  	<div class="row">
				  		<div class="col-sm-12">
				  			<h6 class="text-center estabelecimentos-filtro-titulo">Por nome</h6>
				  		</div>
				  		<!--<div class="col-sm-9" style="margin-right:0px; padding-right:0px;">
				  			<input type="text" class="form-control" id="buscar-nome" placeholder="Nome do restaurante..." />
							</div>-->
						<div class="col-sm-12">
						<div class="input-group">
								<input type="text" class="form-control" id="buscar-nome" placeholder="Ex: Bar do João">
								<div class="input-group-btn">
								<button class="btn btn-default btn-block botao-buscar" id="buscar-por-nome">Buscar</button>
								</div>
						</div>
				  		</div>
				  		<!--<div class="col-sm-3" style="margin-left:0px; padding-left:0px;">
					  		<button class="btn btn-default btn-block" id="buscar-por-nome">Ir</button>
					  	</div>-->
				  		<div class="col-sm-12">
				  			<h6 class="text-center estabelecimentos-filtro-titulo">Categorias</h6>
				  		</div>
				  		<div class="col-sm-12">
				  			<a href="#" class="add-filtro-categoria list-group-item filtro-link" ciklus-categoria="" <?php if (@!$_GET['categoria']) : echo "style='font-weight:bold'"; endif; ?>>Todas</a>
				  			<?php 	$count = 0;
				  				  	foreach ($categorias as $categoria) : 
				  				  		$count++;
				  				  		if ($count < 11) {	?>
				  							<a href="javascript:void(0);" class="add-filtro-categoria  list-group-item filtro-link" ciklus-categoria="<?php echo $categoria->id; ?>" <?php if (@$_GET['categoria'] == $categoria->id) : echo "style='font-weight:bold;'"; endif; ?>><?php echo $categoria->nome; ?></a>
				  							<?php 	if (@$_GET['categoria'] == $categoria->id) : 
				  										if (count($subcategorias) > 0) {
				  											echo "<ul style='list-style: none;padding: 0;text-align: center;'>";
				  											foreach ($subcategorias as $s) : ?>
				  												<li><a style="color:#666" href="javascript:void(0);" class="add-filtro-subcategoria" ciklus-categoria="<?php echo $s->id; ?>" <?php if (@$_GET['subcategoria'] == $s->id) : echo "style='font-weight:bold;'"; endif; ?>><i class="glyphicon glyphicon-chevron-right"></i> <?php echo $s->nome; ?></a></li>
				  									<?php	endforeach;
				  											echo "</ul>";
				  										}
				  									endif; ?>
			  				<?php 		} else { 			?>
			  								<a href="javascript:void(0);" class="add-filtro-categoria categoria-hide" style="display:none; <?php if (@$_GET['categoria'] == $categoria->id) : echo "font-weight:bold;"; endif; ?>" ciklus-categoria="<?php echo $categoria->id; ?>"><?php echo $categoria->nome; ?></a>
							<?php		}
				  					endforeach;
				  					
				  					if ($count > 10) { ?>
				  						<a href="#" style="color:#000;" id="ver-tudo-categoria">+ Ver todos</a>
				  			<?php 	} ?>
						</div>
				  		<div class="col-sm-12">
				  			<h6 class="text-center estabelecimentos-filtro-titulo">Dias da semana</h6>
				  		</div>
				  		<div class="col-sm-12 list-group">
				  			<div class="">
								<a href="#" ciklus-dia="8" class="filtro-dia list-group-item filtro-link" /><span <?php if (@$_GET['dia'] == 8 || !@$_GET['dia']) : echo "style='font-weight:bold;'"; endif; ?>>Todos</span></a>
							</div>
							<div class="">
								<a href="#" ciklus-dia="1" class="filtro-dia list-group-item filtro-link" /> <span <?php if (@$_GET['dia'] == 1) : echo "style='font-weight:bold;'"; endif; ?>>Segunda-feira</span></a>
							</div>
							<div class="">
								<a href="#" ciklus-dia="2" class="filtro-dia list-group-item filtro-link" /> <span <?php if (@$_GET['dia'] == 2) : echo "style='font-weight:bold;'"; endif; ?>>Terça-feira</span></a>
							</div>
							<div class="">
								<a href="#" ciklus-dia="3" class="filtro-dia list-group-item filtro-link" /> <span <?php if (@$_GET['dia'] == 3) : echo "style='font-weight:bold;'"; endif; ?>>Quarta-feira</span></a>
							</div>
							<div class="">
								<a href="#" ciklus-dia="4" class="filtro-dia list-group-item filtro-link" /> <span <?php if (@$_GET['dia'] == 4) : echo "style='font-weight:bold;'"; endif; ?>>Quinta-feira</span></a>
							</div> 
							<div class="">
								<a href="#" ciklus-dia="5" class="filtro-dia list-group-item filtro-link" /> <span <?php if (@$_GET['dia'] == 5) : echo "style='font-weight:bold;'"; endif; ?>>Sexta-feira</span></a>
							</div>
							<div class="">
								<a href="#" ciklus-dia="6" class="filtro-dia list-group-item filtro-link" /> <span <?php if (@$_GET['dia'] == 6) : echo "style='font-weight:bold;'"; endif; ?>>Sábado</span></a>
							</div>
							<div class="">
								<a href="#" ciklus-dia="7" class="filtro-dia list-group-item filtro-link" /> <span <?php if (@$_GET['dia'] == 7) : echo "style='font-weight:bold;'"; endif; ?>>Domingo</span></a>
							</div>
				  		</div>
				  		<div class="col-sm-12">
				  			<h6 class="text-center estabelecimentos-filtro-titulo">Bairros</h6>
				  		</div>
				  		<div class="col-sm-12 list-group">
							<a href="#" class="add-filtro-bairro list-group-item" <?php if (@!$_GET['bairro']) : echo "style='font-weight:bold'"; endif; ?>  ciklus-bairro="">Todos</a>
				  			<?php 	$count2 = 0;
				  				  	foreach ($bairros as $bairro) : 
				  				  		$count2++;
				  				  		$bairro = $bairro->bairro;
				  				  		if (trim($bairro) != "") {
				  				  			if ($count2 < 11) {	?>
				  							<a href="javascript:void(0)" class="add-filtro-bairro list-group-item filtro-link" ciklus-bairro="<?php echo $bairro; ?>"<?php if (@$_GET['bairro'] == $bairro) : echo "style='font-weight:bold;'"; endif; ?>><?php echo $bairro; ?></a>
				  				<?php 		} else { 			?>
				  								<a href="javascript:void(0)" class="add-filtro-bairro bairro-hide list-group-item filtro-link" style="<?php if (@$_GET['bairro'] == $bairro) : echo "font-weight:bold;"; endif; ?>" ciklus-bairro="<?php echo $bairro; ?>"><?php echo $bairro; ?></a>
								<?php		}
				  				  		}
				  					endforeach;
				  					
				  					if ($count2 > 10) { ?>
				  						<!-- <a href="javascript:void(0);" style="color:#000;" id="ver-tudo-bairro">+ Ver todos</a> -->
				  			<?php 	} ?>
						</div>
						<!-- <div class="col-sm-12">
				  			<h6 class="text-center">Horário</h6>
				  		</div>
				  		<div class="col-sm-12">
							<div class="">
								<input type="checkbox" style="margin-right:10px;" checked /><span>Manhã (06:00 - 11:00)</span>
							</div>
							<div class="">
								<input type="checkbox" style="margin-right:10px;" checked /><span>Tarde (11:00 - 18:00)</span>
							</div>
							<div class="">
								<input type="checkbox" style="margin-right:10px;" checked /><span>Noite (18:00 - 06:00)</span>
							</div>
						</div> -->
				  	</div>
				  </div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-9" id="estabelecimentos-resultado">
		<?php 
		if (count($estabelecimentos) == 0) {
			echo "<h3 class='text-center'>Nenhum resultado encontrado.</h3>";
		}

		foreach ($estabelecimentos as $estabelecimento) :
			$infos = recuperarInfos($estabelecimento->promocoes);
			if (@$_GET['bairro']) {
				if ($estabelecimento->bairro != $_GET['bairro']) {
					continue;
				}
			}
		?>
		<div class="row">
			<div class="col-xs-12" style="margin-bottom: 20px;">
				<div class="row estabelecimentos-secao">
					<a class="col-xs-12 col-md-5" href="estabelecimento?id=<?php echo $estabelecimento->id; ?>" style="background:url('<?php echo $urlImagens . recuperarFotoMontada($estabelecimento->imagens, 1); ?>');min-height: 220px;max-height: 260px;height: 100%;background-position:50% 50%;background-size:cover;}">
					</a>
					<div class="col-xs-12 col-md-7 estabelecimentos-box">
						<span class="estabelecimentos-nome"><a href="estabelecimento?id=<?php echo $estabelecimento->id; ?>"><?php echo $estabelecimento->nome; ?></a>
						<br>
						<br>
						<?php if ($estabelecimento->categoria) : ?></span><span class="estabelecimentos-categoria"><i class="fa fa-bookmark"></i> <?php echo $estabelecimento->categoria->nome; ?><?php endif; ?></span>
						<span>
							<span>
							<?php if ($infos["socios"] > 0) { ?>
								<span class="estabelecimentos-socio"><i class="glyphicon glyphicon-user"></i> + <?php echo $infos["socios"]; ?></span>
							<?php } else { ?>
								<span class="estabelecimentos-socio"><i class="glyphicon glyphicon-user"></i></span>
							</span>		
							<span>
							<?php } 
							  if ($infos["minDesconto"] == $infos["maxDesconto"]) { ?>
									<span class="estabelecimentos-desconto"><span class="oferta-tag-texto">Economize</span><br><span class="oferta-tag-desconto"> 	<?php echo $infos["maxDesconto"] . "%"; ?></span>
									</span>
									<?php } else { ?>
									<span class="estabelecimentos-desconto">
									<span class="oferta-tag-texto">Economize</span><br><span class="oferta-tag-desconto"> até <?php echo $infos["maxDesconto"] . "%"; ?></span>
									</span>
								<?php }	?>
							</span>
						</span>	
						<br><br>
						<div class="estabelecimentos-informacoes"><span class="estabelecimentos-endereco"><i class="fa fa-map-marker"></i> <?php echo $estabelecimento->endereco . ', ' . @$estabelecimento->numero; ?></span>
						<br />
						<span class="estabelecimentos-telefone"><i class="fa fa-phone"></i> <?php echo $estabelecimento->telefone; ?></span></div><br>
						<?php echo recuperarDias($estabelecimento->promocoes);?>
						<br>
						<!-- <a href="estabelecimento.php?id=<?php echo $estabelecimento->id; ?>" class="ver-horarios" ciklus-estabelecimento="<?php echo $estabelecimento->id; ?>">Ver dias e horários</a><br /><br /> --><br>
						<a class="estabelecimentos-botao" href="estabelecimento?id=<?php echo $estabelecimento->id; ?>"><i class='glyphicon glyphicon-arrow-right'></i>  Mais Informações</a>
					</div>
				</div>
			</div>
		</div>
		<?php endforeach; ?>
	</div>
</div>

<?php include("widgets/footer.php"); ?>