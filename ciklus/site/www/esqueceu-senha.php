<?php 
include("includes/settings.php");
$scripts = "<script src='jsapp/esqueceu-senha.js'></script>";
include("widgets/header.php");
?>
<div class="container">
	<div class="row text-center" style="margin-top: 100px;margin-bottom:20px;">
		<div class="col-xs-12">
			<h3>Esqueceu sua senha?</h3>
			<h4>Digite seu CPF e e-mail para criar uma nova senha.</h4>
		</div>
	</div>
	<div class="row text-center" id="nova-senha-1">
		<div class="col-lg-offset-3 col-lg-6 col-xs-12">
			<div class="form-group">
				<label>CPF</label>
				<input type="text" class="form-control" id="cpf" placeholder="Seu CPF" />
			</div>
			<div class="form-group">
				<label>E-mail</label>
				<input type="email" class="form-control" id="email" placeholder="Seu e-mail cadastrado" />
			</div>
			<div class="form-group">
				<button class="btn btn-primary" id="criar-senha">Criar nova senha</button>
				<input type="hidden" id="hdn_socio" />
			</div>
		</div>
	</div>
	<div class="row text-center" id="nova-senha-2" style="display:none;">
		<div class="col-lg-offset-3 col-lg-6 col-xs-12">
			<div class="form-group">
				<label>Nova senha</label>
				<input type="password" class="form-control" id="nova-senha" placeholder="Sua nova senha" maxlength="15" />
			</div>
			<div class="form-group">
				<button class="btn btn-primary btn-large" id="salvar-senha">Salvar senha</button>
			</div>
		</div>
	</div>
</div>

<?php
include("widgets/footer.php");