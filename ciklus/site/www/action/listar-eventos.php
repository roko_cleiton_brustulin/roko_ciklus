<?php
$eventos = array();

$sql = "SELECT e.*, m.extensao FROM evento e INNER JOIN estabelecimentofoto m ON m.id = e.foto WHERE data >= NOW() - INTERVAL '1 day' ORDER BY data ASC";
$res = json_decode(api('estabelecimento/fullSearch?where='.urlencode($sql)));
if ($res->rows[0]->json_agg) {
	$eventos = $res->rows[0]->json_agg;
}