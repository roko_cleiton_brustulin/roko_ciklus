<?php
$estabelecimento = $_COOKIE['estabelecimento-ciklus'];
$estabelecimento = json_decode(api('estabelecimento/'.$estabelecimento));

$promocoes = array();
$transacoes = array();
if (@$estabelecimento) {
	$params = array("estabelecimento" => $estabelecimento->id, "status" => 1);
	$promocoes = json_decode(api('promocao/find', $params));	

	$params2 = array("estabelecimento" => $estabelecimento->id, "sort" => "createdAt DESC");
	$transacoes = json_decode(api('transacao/find', $params2));
} else {
	header('Location: index.php');
}
