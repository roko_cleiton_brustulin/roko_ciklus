<?php
$usuario = json_decode($_COOKIE['usuario-ciklus']);
$arr = array("socio" => $usuario->id, "sort" => 'createdAt DESC');
$transacoes = json_decode(api('transacao/find', $arr));
$user = $usuario;
$usuario = json_decode(api('socio/'.$usuario->id));
$plano = "";
$transactions = array();
$subscriptions = null;
$faturas = null;
$card = null;
$plans = $pagarMe->plan()->getList(1, 10);
$planos = array();
foreach ($plans as $plano) {
	$p = new stdClass();
	$p->nome = $plano->getName();
	$p->id = $plano->getId();
	$p->valor = $plano->getAmount();
	array_push($planos, $p);
}

if ($usuario->idIugu) {
	try {
		$subscription = @$pagarMe->subscription()->get($usuario->idIugu);
		if ($subscription) {
			$plan = $subscription->getPlan();
			$card = $subscription->getCard();
			$validade = $subscription->getCurrentPeriodEnd()->format('d/m/Y');
			$transactions = $pagarMe->subscription()->transactions($subscription);			
			$plano = $plan->getName();
		} else {
			$subscriptions = array();
			$faturas = array();
			$subscription = "Expirado";
			$plano = "Expirado";
			@$validade = explode('-', $user->ativoAte);
		}
	} catch (Exception $e) {
		$subscriptions = array();
		$faturas = array();
		$subscription = "Expirado";
		$plano = "Expirado";
		@$validade = explode('-', $user->ativoAte);	
	}
} else {
	$subscriptions = array();
	$faturas = array();
	$subscription = "Voucher";
	$plano = "Voucher";
	$validade = explode('-', $user->ativoAte);
}