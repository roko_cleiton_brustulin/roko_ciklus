<?php
include("../includes/settings.php");
$planos = Iugu_Plan::search()->results();
$planosRetorno = array();
foreach($planos as $plano) {
	$p = new stdClass;
	$p->id = $plano->id;
	$p->nome = $plano->name;
	$p->preco = $plano->prices[0]->value_cents;
	array_push($planosRetorno, $p);
}
echo json_encode($planosRetorno);
