<?php

$estabelecimentos = array();
$subcategorias = array();
$filtro = array();

if (@$_GET['categoria']) {
	if (@$_GET['subcategoria']) {
		$filtro = array("categoria" => $_GET['subcategoria']);
	} else {
		$filtro = array("categoria" => $_GET['categoria']);	
	}
	$categoria = array('status' => 1, 'categoriaPai' => $_GET['categoria']);
	$subcategorias = json_decode(api('categoria/find', $categoria));
}

if (@$_GET['nome']) {
	$filtro['nome'] = $_GET['nome'];
}

$estabelecimentos = json_decode(api('estabelecimento/buscar', $filtro));

function cmp($a, $b)
{
    return strcmp($a->nome, $b->nome);
}

usort($estabelecimentos, "cmp");

$categorias = json_decode(api('categoria/buscarAtivas'));

$sqlBairros = "SELECT DISTINCT(bairro) FROM estabelecimento WHERE status = 'a'";
$res = json_decode(api('estabelecimento/fullSearch?where='.urlencode($sqlBairros)));
$bairros = $res->rows[0]->json_agg;
sort($bairros);

$estab2 = $estabelecimentos;
$estab3 = array();
if (@$_GET['dia']) {
	$dia = $_GET['dia'];
	if ($dia != 8) {
		foreach ($estab2 as $e) {
			$valido = false;
			foreach ($e->promocoes as $p) {
				switch ($dia) {
					case 7:
						if ($p->domingo == true)
							$valido = true;
						break;
					case 1:
						if ($p->segundaFeira == true)
							$valido = true;
						break;
					case 2:
						if ($p->tercaFeira == true)
							$valido = true;
						break;
					case 3:
						if ($p->quartaFeira == true)
							$valido = true;
						break;
					case 4:
						if ($p->quintaFeira == true)
							$valido = true;
						break;
					case 5:
						if ($p->sextaFeira == true)
							$valido = true;
						break;
					case 6:
						if ($p->sabado == true)
							$valido = true;
						break;
				}
			}
			if ($valido) {
				array_push($estab3, $e);
			}
		}
		$estabelecimentos = $estab3;
	}
}