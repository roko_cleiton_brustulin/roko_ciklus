<?php
include('../../includes/settings.php');
$estabelecimento = $_COOKIE['estabelecimento-ciklus'];

if (@$_FILES['foto']) {
	$ext = pathinfo($_FILES['foto']['name'], PATHINFO_EXTENSION);
	$nome = $_FILES['foto']['name'];
	
	$dados = array(	"nome" => $nome, 
					"extensao" => $ext);
	$imagem = json_decode(api('estabelecimentofoto/create', $dados));

	$uploaddir = "../../uploads/";
	$uploadfile = $uploaddir . basename($imagem->id . "." . $ext);

	if (move_uploaded_file($_FILES['foto']['tmp_name'], $uploadfile)) {
		$dadosNovidade = array("titulo" => $_POST['titulo'], 
								"texto" => $_POST['desc'], 
								"imagem" => $imagem->id, 
								"estabelecimento" => $estabelecimento,
								"link" => $_POST['link'],
								"parametro" => $_POST['parametro']);
		$novidade = json_decode(api('novidade/create', $dadosNovidade));
		echo true;
	} else {
		echo false;
	}
} else {
	echo false;
}
