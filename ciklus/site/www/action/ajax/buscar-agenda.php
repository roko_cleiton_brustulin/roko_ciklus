<?php
include('../../includes/settings.php');

$dia = $_POST['agendaDia'];

switch ($dia) {
	case 0:
		$dia = "domingo";
		break;
	case 1:
		$dia = "segundaFeira";
		break;
	case 2:
		$dia = "tercaFeira";
		break;
	case 3:
		$dia = "quartaFeira";
		break;
	case 4:
		$dia = "quintaFeira";
		break;
	case 5:
		$dia = "sextaFeira";
		break;
	case 6:
		$dia = "sabado";
		break;
}

$sql2 = "SELECT * FROM categoria WHERE \"categoriaPai\" IS NULL AND status = 1";
$res = json_decode(api('estabelecimento/fullSearch?where='.urlencode($sql2)));

$cats = $res->rows[0]->json_agg;


foreach ($cats as $cat) {
	$resFinal['categorias'][trim($cat->nome)] = $cat;
}

foreach ($cats as $cat) {
	$sql = "SELECT e.id, titulo, politica, desconto, \"socioMais\", horario1, horario2, horario3, horario4, e.nome, ef.id as fotoid, ef.extensao as fotoextensao, e.categoria, e.destaque
		FROM promocao p
		INNER JOIN estabelecimento e ON p.estabelecimento = e.id 
		INNER JOIN estabelecimentofoto ef ON e.id = ef.estabelecimento
		INNER JOIN categoria c ON e.categoria = c.id
		WHERE (e.categoria IN (".$cat->id.") OR c.\"categoriaPai\" IN (".$cat->id.")) AND \"".$dia."\" = 't' AND c.status = 1 AND p.status = 1 AND (validade > NOW() OR validade IS NULL) AND e.status = 'a' AND p.destaque IN ('f', 'r')
		AND ef.ordem = 1
		ORDER BY p.destaque ASC, random()
		LIMIT 3";
	$res = json_decode(api('estabelecimento/fullSearch?where='.urlencode($sql)));
	$resFinal['agenda'][trim($cat->nome)] = $res->rows[0]->json_agg;
}

echo json_encode($resFinal);