<?php
include('../../includes/settings.php');
$nome = "";
$sobrenome = "";

if (strpos(trim($_POST['nome']), ' ') > 0) {
	$nome = explode(' ', trim($_POST['nome']), 2);
	$sobrenome = $nome[1];
	$nome = $nome[0];
} else {
	$nome = $_POST['nome'];
	$sobrenome = "";
}

 
$card = $pagarMe->card()->create(
    $_POST['numero'],
    $_POST['nome'],
    $_POST['mes'] . $_POST['ano']
);

if ($card->getId()) {
	$socio = json_decode($_COOKIE['usuario-ciklus']);
	$socio = json_decode(api('socio/'.$socio->id));
	if ($socio->idIugu) {
		$assinatura = $pagarMe->subscription()->get($socio->idIugu);
		$assinatura->setPaymentMethod('credit_card');
		$assinatura->setCard($card);
		$assinatura = $pagarMe->subscription()->update($assinatura);
		echo true;
	} else {
		echo $card->getId();
	}	
} else {
	echo false;
}