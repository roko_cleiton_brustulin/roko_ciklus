<?php
include('../../includes/settings.php');

$socio = $_POST['id'];

$params = array("ativoAte" => date('Y-m-d', strtotime("+30 days")));

$res = json_decode(api('socio/update/'.$socio, $params));

echo json_encode($res);