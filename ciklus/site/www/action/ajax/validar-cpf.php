<?php
include('../../includes/settings.php');

// Busca o sócio por ID na base Ciklus
$cliente = null;

if (strlen($_POST['cpf']) == 14) {
	$cliente = json_decode(api('socio/find?cpf='.$_POST['cpf']));
} else {
	$cliente = json_decode(api('socio/find?codigo='.$_POST['codigo']));
}

if (count($cliente) > 0) {
	$idIugu = $cliente[0]->idIugu;
	if ($idIugu) {
		try {
			$subscriptions = $pagarMe->subscription()->get($cliente[0]->idIugu);
			if (@$subscriptions->getStatus() == "paid") {
				echo "true";
			} else { 
				echo "false";
			}
		} catch (Exception $e) {
			echo "false";
		}
	} else {
		$data = $cliente[0]->ativoAte;
		if ($data) {
			$data = date("Y-m-d", strtotime($data));
			if ($data > date("Y-m-d")) {
				echo "true";
			} else {
				echo "false";
			}
		} else {
			echo "false";
		}
	}
} else {
	echo "invalido";
}