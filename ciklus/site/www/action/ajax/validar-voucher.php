<?php
include('../../includes/settings.php');

$voucher = $_POST['voucher'];

$cupom = json_decode(api('cupom/find?codigo='.$voucher.'&sort=utilizado'));

if (count($cupom) > 0) {
	if (count($cupom) > 1) {
		echo json_encode($cupom[0]);
	} else {
		foreach ($cupom as $c) {
			if (!$c->utilizado) {
				echo json_encode($c);
				return;
			}
		}
	}

} else {
	echo false;
}
