<?php
include('../../includes/settings.php');

// Busca o sócio por ID na base Ciklus
$cliente = json_decode(api('socioauxiliar/find/'.$_POST['socio']));
if ($cliente) {
	$card = $pagarMe->card()->create(
	    $_POST['numero'],
	    $_POST['nome'] . ' ' . $_POST['sobrenome'],
	    $_POST['mes'] . $_POST['ano']
  	);
	$plan = $pagarMe->plan()->get($_POST['plano']);

	$customer = new \PagarMe\Sdk\Customer\Customer(
			[
			'name' => $cliente->nome,
			'email' => $cliente->email,
			'document_number' => $cliente->cpf
			]
	);

	$subscription = $pagarMe->subscription()->createCardSubscription(
					$plan,
					$card,
					$customer
	);

	if (@$subscription->getStatus() != "paid") {
		echo json_decode($subscription->getStatus());
	} else {
		$clienteFinal = array(
					'nome' => $cliente->nome, 
				 	'email' => $cliente->email, 
				 	'cpf' => $cliente->cpf,
				 	'telefone' => $cliente->telefone,
				 	'dataNasc' => $cliente->dataNasc,
				 	'sexo' => $cliente->sexo,
				 	'cep' => $cliente->cep,
				 	'rua' => $cliente->rua,
				 	'numero' => $cliente->numero,
				 	'bairro' => $cliente->bairro,
				 	'cidade' => $cliente->cidade,
				 	'uf' => $cliente->uf,
				 	'idIugu' => $subscription->getId()
				);
		$clienteFinal = json_decode(api('socio/create', $clienteFinal));
		$retorno = array("cliente" => $clienteFinal, "resultado" => $subscription->getStatus());
		echo json_encode($retorno);
	}
}