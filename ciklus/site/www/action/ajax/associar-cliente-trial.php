<?php
include('../../includes/settings.php');

$usuario = json_decode($_COOKIE['usuario-ciklus']);
$plano = $_POST['plano'];
$card = $_POST['cartao'];

$usuario = json_decode(api('socio/'.$usuario->id));
$plan = $pagarMe->plan()->get($plano);
$card = $pagarMe->card()->get($card);

$customer = new \PagarMe\Sdk\Customer\Customer(
		[
		'name' => $usuario->nome,
		'email' => $usuario->email,
		'document_number' => $usuario->cpf
		]
);

$subscription = $pagarMe->subscription()->createCardSubscription(
				$plan,
				$card,
				$customer
);

$iuguId = array('idIugu' => $subscription->getId());
$usuario = json_decode(api('socio/update/'.$usuario->id, $iuguId));

echo @$subscription->getStatus();