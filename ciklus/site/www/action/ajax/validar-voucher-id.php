<?php
include('../../includes/settings.php');

$voucher = $_POST['voucher'];

$cupom = json_decode(api('cupom/'.$voucher));

if ($cupom) {
	echo json_encode($cupom);
} else {
	echo false;
}
