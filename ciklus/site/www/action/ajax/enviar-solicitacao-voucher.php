<?php
include('../../includes/settings.php');
include('../../includes/PHPMailer/PHPMailerAutoload.php');


//**** Esta rotina é utilizada pelo site quando um visitante solicita um convite *******//

$nome 	= $_POST['nome'];
$email 	= $_POST['email'];
$msg 	= $_POST['msg'];

$mail = new PHPMailer(true);
try {
	$mail->isSMTP();                                      	// Set mailer to use SMTP
	$mail->Host = 'smtp.kinghost.net';  					// Specify main and backup SMTP servers
	$mail->SMTPAuth = true;                               	// Enable SMTP authentication
	$mail->Username = 'naoresponda@cikluslive.com.br';     	// SMTP username
	$mail->Password = 'rokociklus0897';                    	// SMTP password
	$mail->Port = 587;
	$mail->CharSet = 'UTF-8';

	$mail->setFrom('contato@ciklus.com.br', 'Ciklus');
	$mail->addReplyTo('contato@ciklus.com.br', 'Ciklus');
	$mail->isHTML(true);
	

	//E-mail que a Ciklus receberá
	$mail->addAddress('contato@ciklus.com.br', 'Ciklus');
	$mail->addAddress('cleiton@rokoit.com.br', 'Cleiton');
	$mail->Subject = 'Solicitação de convite';
	$mail->Body    .= '<strong>Nome:</strong> ' . $nome . '<br \>';
	$mail->Body    .= '<strong>E-mail:</strong> ' . $email . '<br \>';
	$mail->Body    .= '<strong>Mensagem:</strong> ' . $msg . '<br \>';
	$mail->send();


	//E-mail que o visitante receberá
	$mail->ClearAddresses();
	$mail->addAddress($email, $nome);
	list($primeironome, $sobrenome) = explode(' ', $nome, 2);
	$mail->Subject 	= 'Seu pedido foi confirmado';
	$body 	= "<html>\n"; 
    $body 	.= "<body style=\"font-family:'Helvetica Neue', Helvetica, Arial, sans-serif; font-size:16px;\">\n"; 
  	$body 	.= '<strong>Olá ' . $primeironome . ', </strong><br><br>';
	$body 	.= 'Você solicitou um convite para entrar no clube da economia Ciklus Live, certo?<br><br>';
	$body 	.= 'Recebemos seus dados e logo você receberá o seu convite com o código. <br><br>';
	$body 	.= 'Estamos liberando os códigos manualmente, por isso não existe um prazo.<br><br>';
	$body 	.= 'Em breve você receberá mais informações. Aguarde. <br><br>';
	$body 	.= 'Muito obrigado pelo seu interesse! <br><br>';
	$body 	.= 'Abraços, <br>Equipe Ciklus live <br><br>';
	$body 	.= "<img width=\"30%\" src=\"http://cikluslive.com.br/assets/images/logo.png\">"; 
    $body 	.= "</body>\n"; 
    $body 	.= "</html>\n"; 
	$mail->Body 	= $body;
	$mail->send();


} catch (phpmailerException $e) {
  echo $e->errorMessage(); //Pretty error messages from PHPMailer
} catch (Exception $e) {
  echo $e->getMessage(); //Boring error messages from anything else!
}