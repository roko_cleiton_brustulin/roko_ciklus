<?php
include('../../includes/settings.php');

$socio = $_POST['socio'];
if (strlen($socio) == 14) {
	$socio = json_decode(api('socio/find?cpf='.$socio));
} else {
	$socio = json_decode(api('socio/find?codigo='.$socio));
}
$valor = $_POST['valor'];
$estabelecimento = $_COOKIE['estabelecimento-ciklus'];
$promocao = json_decode(api('promocao/'.$_POST['promocao']));

$data = array("socio" => $socio[0]->id, "valor" => $valor, "estabelecimento" => $estabelecimento, 
				"promocao" => $promocao->id, "desconto" => $promocao->desconto, "valorDesconto" => $valor * ($promocao->desconto * 0.01));
$res = json_decode(api('transacao/create', $data));

echo json_encode($res);