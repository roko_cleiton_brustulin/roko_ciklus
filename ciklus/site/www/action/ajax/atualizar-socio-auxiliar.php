<?php
include('../../includes/settings.php');

$cep = $_POST['cep'];
$rua = $_POST['rua'];
$numero = $_POST['numero'];
$bairro = $_POST['bairro'];
$cidade = $_POST['cidade'];
$estado = $_POST['estado'];
$socio = $_POST['socio'];

$data = array(
			"cep" => $cep,
			"rua" => $rua,
			"numero" => $numero,
			"bairro" => $bairro,
			"cidade" => $cidade,
			"uf" => $estado
		);

echo api('socioauxiliar/update/'.$socio, $data);