<?php
$estabelecimento = "";
$scripts = "";
$metaTags = true;
$metaTitulo = "";
$metaDescricao = "";
if (isset($_GET['id'])) {
	$estabelecimento = json_decode(api('estabelecimento/'.$_GET['id']));
	$fotos = json_decode(api('estabelecimentofoto/find?estabelecimento='.$_GET['id']));
	$capa = recuperarFoto($fotos, 1);
	$logo = recuperarFoto($fotos, 2);
	$metaTitulo = $estabelecimento->metaTitulo;
	$metaDescricao = $estabelecimento->metaDescricao;
	
	if ($estabelecimento == NULL) {
		$scripts = "<script>alert('O estabelecimento que você está tentando acessar não existe.');window.history.back();</script>";
	}
} else {
	$scripts = "<script>alert('O estabelecimento que você está tentando acessar não existe.');window.history.back();</script>";
}