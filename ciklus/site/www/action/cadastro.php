<?php

// $planos = Iugu_Plan::search()->results();
$plans = $pagarMe->plan()->getList(1, 10);
$planos = array();
foreach ($plans as $plano) {
	$p = new stdClass();
	$p->nome = $plano->getName();
	$p->id = $plano->getId();
	$p->valor = $plano->getAmount();
	array_push($planos, $p);
}

$usuario = json_decode(@$_COOKIE['usuario-ciklus']);
if ($usuario) {
	header('Location: minha-conta.php');
}