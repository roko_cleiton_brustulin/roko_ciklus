<?php

$evento = null;
if (isset($_GET['id'])) {
	$evento = json_decode(api('evento/'.$_GET['id']));
	$data = explode('-', $evento->data);
	$data = substr($data[2], 0, 2) . '/' . $data[1] . '/' . $data[0];
} else {
	header('Location: index.php');
}