<?php
$sql = "SELECT c.nome, c.id, cp.nome AS \"nomePai\" 
		FROM categoria c 
		LEFT JOIN categoria cp ON c.\"categoriaPai\" = cp.id 
		WHERE c.status = 1 ORDER BY \"nomePai\"";
$query = json_decode(api('estabelecimento/fullSearch?where='.urlencode($sql)));

$categorias = array();
if (isset($query->rows)) {
	$categorias = $query->rows[0]->json_agg;
}