<?php
include("includes/settings.php");
include("action/cadastro.php");
$scripts = '<script type="text/javascript" src="jsapp/cadastro.js"></script>';
include("widgets/header.php"); 
?>
<div class="col-lg-12 titulo-topo container-fluid">
	<div class="overlay-topo">
		<span class="text-center titulo-pagina overlay-titulo" style="text-transform:uppercase;">Painel do Estabelecimento</span>
	</div>
</div>
<div class="container">	
	
	<div class="col-md-offset-2 col-md-8 text-center conteudo">

		<div class="row">
	  		<div class="col-xs-12 text-center">
	  			<h4>Digite o login e senha do seu estabelecimento</h4>
	  		</div>
	  		<div class="col-xs-12 col-lg-offset-3 col-lg-6 col-lg-offset-3 text-center" style="margin-top:10px;">
	  			<div class="form-group">
	  				<label>Login</label>
	  				<input type="text" class="form-control text-center" id="login-estabelecimento" />
	  			</div>
	  			<div class="form-group">
	  				<label>Senha</label>
	  				<input type="password" class="form-control text-center" id="senha-estabelecimento" />
	  			</div>
	  			<div class="form-group">
	  				<button id="btn-login-estabelecimento" class="btn btn-primary">Acessar</button>
	  				<br><br><br>
	  			</div>
	  		</div>
	  	</div>
	</div>
</div>

<?php 
include("widgets/footer.php");