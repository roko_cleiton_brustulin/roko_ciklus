<?php 
include("includes/settings.php");
include("action/confirmar-conta.php");
$scripts = "<script src='jsapp/confirmar-conta.js'></script>";
include("widgets/header.php");
?>
<div class="container">
	<div class="row text-center" style="margin-top: 100px;margin-bottom:20px;">
		<div class="col-xs-12">
			<h3><?php echo $usuario->nome; ?>, bem-vindo a Ciklus!</h3>
		</div>
	</div>
	<div class="row" style="margin-bottom: 30px !important;">
		<div class="col-lg-offset-3 col-lg-6 col-xs-12 col-md-12">
			<h4 class="text-center">Confirme seus dados e crie uma senha para a sua conta</h4>
			<br />
			<div class="form-group text-center col-xs-12">
				<label>Login</label>
				<input type="text" class="form-control text-center" value="<?php echo @$usuario->cpf; ?>" readonly />
			</div>
			<div class="form-group text-center col-xs-12">
				<label>Confirme seu e-mail *</label>
				<input type="email" class="form-control text-center" id="email" value="<?php echo @$usuario->email; ?>" />
			</div>
			<div class="form-group text-center col-xs-12">
				<label>Crie uma senha *</label>
				<input type="password" class="form-control text-center" id="senha" />
			</div>
			<div class="form-group text-center col-xs-12">
				<label>Confirme sua senha *</label>
				<input type="password" class="form-control text-center" id="confirmar-senha" />
			</div>
			<div class="form-group text-center col-xs-12">
				<button class="btn btn-primary" id="confirmar-conta">Confirmar</button>
			</div>
		</div>
	</div>
</div>

<?php
include("widgets/footer.php");