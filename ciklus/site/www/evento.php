<?php
include("includes/settings.php");
include("action/evento.php");
include("widgets/header.php"); ?>
<<div class="container" style="padding-bottom:0px;">
	<div class="row">
		<div class="col-lg-12" id="<?php echo $evento->id; ?>" style="background-color: #f1f1f1;margin-bottom:20px; border-radius: 10px; padding: 20px; margin-top: 50px;">
			<div class="row">
				<div class="col-lg-12">
					<img class="eventos-imagem" src="<?php echo $urlImagens . $evento->foto->id . '.' . $evento->foto->extensao; ?>"/>
					<span class="discount-tag">
						<span class="oferta-tag-texto">Economize</span>
						<br>
						<!-- <span class="oferta-tag-desconto"><?php echo $evento->desconto; ?>%</span> -->
						<span class="oferta-tag-desconto">
							<?php if($evento->desconto == 0){
								echo '$';
							}else{
								echo $evento->desconto . '%';	
							} ?>
						</span>
					</span>
				</div>
				<div class="col-lg-12">
					<div class="eventos-borda">
						<div class="eventos-overlay">
							<span class="text-center eventos-titulo"><i class="fa fa-bullhorn"></i> <?php echo $evento->titulo; ?></span>
							<span class="text-center eventos-data"><i class="fa fa-calendar"></i> <?php echo $data; ?></span><br><br>
							<span class="text-center eventos-descricao"><?php echo nl2br($evento->politica); ?></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include("widgets/footer.php"); ?>
