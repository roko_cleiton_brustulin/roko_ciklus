<?php

function recuperarInfos($promocoes) {
	$infos = array();
	$minDesconto = 0;
	$maxDesconto = 0;
	$socios = 0;
	foreach ($promocoes as $promo) {
		if ($promo->socioMais > $socios) {
			$socios = $promo->socioMais;
		}

		if ($minDesconto > $promo->desconto || $minDesconto == 0) {
			$minDesconto = $promo->desconto;
		}

		if ($maxDesconto < $promo->desconto) {
			$maxDesconto = $promo->desconto;
		}
	} 
	$infos["minDesconto"] = $minDesconto;
	$infos["maxDesconto"] = $maxDesconto;
	$infos["socios"] = $socios;

	return $infos;
}

function recuperarDias($promocoes) {
	$dias = array();
	foreach ($promocoes as $p) {
		if ($p->segundaFeira == 't') {
			array_push($dias, 'SEG');
		}
		if ($p->tercaFeira == 't') {
			array_push($dias, 'TER');
		}
		if ($p->quartaFeira == 't') {
			array_push($dias, 'QUA');
		}
		if ($p->quintaFeira == 't') {
			array_push($dias, 'QUI');
		}
		if ($p->sextaFeira == 't') {
			array_push($dias, 'SEX');
		}
		if ($p->sabado == 't') {
			array_push($dias, 'SAB');
		}
		if ($p->domingo == 't') {
			array_push($dias, 'DOM');
		}
	}
	$diasUnicos = array_unique($dias);

	foreach ($diasUnicos as $dia) {
		echo "<span class='dia-promocao'>$dia</span>";
	}
}

function recuperarFoto($imgArr, $ordem) {
	foreach ($imgArr as $img) {
		if ($img->ordem == $ordem)
			return $img;
	}
	return null;
}

function recuperarFotoMontada($imgArr, $ordem) {
	$img = recuperarFoto($imgArr, $ordem);
	return $img->id . '.' . $img->extensao;
}