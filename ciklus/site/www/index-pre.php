<?php
include("includes/settings.php");
include("action/index.php");
$scripts = "<script src='jsapp/index.js'></script>";
include("widgets/header-pre.php");
?>


<div class="container-fluid home-top">

	<video autoplay loop src="video-promo.mp4" alt="..." type="video/mp4"></video>

	<div class="container">
		<div class="steps text-center">
			<h1 class="titulo-borda">AGUARDE, ESTAMOS CHEGANDO</h1>
			<img src="assets/images/logo.png" style="width:400px;padding-top:10px;padding-bottom:10px;">
		</div>
	</div>
</div>

<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		  ga('create', 'UA-48248255-3', 'auto');
		  ga('send', 'pageview');
		</script>
