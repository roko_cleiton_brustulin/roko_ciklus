<div class="modal fade" id="modal-planos">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-body">
	  	<input type="hidden" id="hdn_plano_selecionado" />
	  	<input type="hidden" id="hdn_payment_id" value="<?php echo $card->getId(); ?>" />
	  	<div class="row">
	  		<div class="col-lg-12 text-right">
	  			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">x</span></button>
	  		</div>
	  	</div>
	  	<div class="row plano-plano">
	  		<div class="col-lg-12 text-center">
	  			<h3>Planos</h3>
	  			<p>Para qual plano você quer migrar?</p>
	  		</div>
	  	</div>
	  	<div class="row plano-plano">
	  		<?php 
	  		foreach ($planos as $p) : ?>
	  		<div class="col-lg-12 text-center" style="margin-bottom: 15px;">
	  			<?php if ($p->nome == $plano) : ?>
	  			<button class="btn btn-default"><?php echo $p->nome; ?> (R$ <?php echo substr($p->valor, 0, 2) . ',' . substr($p->valor, 2, 2); ?>)</button>
	  			<?php else : ?>
	  			<button class="btn btn-primary alterar-plano" ciklus-plano="<?php echo $p->id; ?>"><?php echo $p->nome; ?> (R$ <?php echo substr($p->valor, 0, 2) . ',' . substr($p->valor, 2, 2); ?>)</button>
	  			<?php endif; ?>
	  		</div>
	  		<?php endforeach; ?>
	  	</div>
	  	<div class="row plano-cartao" style="display:none;">
	  		<div class="col-lg-12 text-center">
	  			<h3>Forma de pagamento</h3>
	  			<p>Digite os dados do seu cartão</p>
	  		</div>
	  	</div>
	  	<div class="row plano-cartao" style="display:none;">
	  		<div class="col-lg-12">
	  			<div class="form-group text-center">
	  				<label>Nome (igual escrito no cartão)</label>
		  			<input type="text" class="form-control text-center" id="nome-cartao" maxlength="20" />
		  		</div>
	  		</div>
	  		<div class="col-lg-8">
	  			<div class="form-group text-center">
	  				<label>Número</label>
		  			<input type="text" class="form-control text-center" id="numero-cartao" maxlength="16" />
		  		</div>
	  		</div>
	  		<div class="col-lg-4">
	  			<div class="form-group text-center">
	  				<label>CVV</label>
		  			<input type="text" class="form-control text-center" id="cvv-cartao" maxlength="4" />
		  		</div>
	  		</div>
	  		<div class="col-lg-6">
	  			<div class="form-group text-center">
	  				<label>Mês</label>
		  			<select class="form-control" style="text-align-last:center;" id='mes-cartao'>
	                	<option value="01">01</option>
	                	<option value="02">02</option>
	                	<option value="03">03</option>
	                	<option value="04">04</option>
	                	<option value="05">05</option>
	                	<option value="06">06</option>
	                	<option value="07">07</option>
	                	<option value="08">08</option>
	                	<option value="09">09</option>
	                	<option value="10">10</option>
	                	<option value="11">11</option>
	                	<option value="12">12</option>
	                </select>
		  		</div>
	  		</div>
	  		<div class="col-lg-6">
	  			<div class="form-group text-center">
	  				<label>Ano</label>
		  			<select class="form-control" style="text-align-last:center;" id='ano-cartao'>
	                	<option value="17">17</option>
	                	<option value="18">18</option>
	                	<option value="19">19</option>
	                	<option value="20">20</option>
	                	<option value="21">21</option>
	                	<option value="22">22</option>
	                	<option value="23">23</option>
	                	<option value="24">24</option>
	                	<option value="25">25</option>
	                	<option value="26">26</option>
	                	<option value="27">27</option>
	                	<option value="28">28</option>
	                	<option value="29">29</option>
	                	<option value="30">30</option>
	                </select>
		  		</div>
	  		</div>
	  		<div class="col-lg-12 text-center">
	  			<button class="btn btn-primary" id="voltar-planos">Voltar</button>
	  			<button class="btn btn-primary" id="salvar-cartao-plano">Salvar</button>
	  		</div>
	  	</div>
	  </div>
	</div>
  </div>
</div>