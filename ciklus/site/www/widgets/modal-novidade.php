<div class="modal fade" id="modal-novidade">
  	<div class="modal-dialog">
		<div class="modal-content">
	  		<div class="modal-body">
	  			<div class="row">
	  				<div class="col-lg-12">
	  					<h1>Postar novidade</h1>
	  				</div>
	  			</div>
	  			<form id="form-novidade" name="form-novidade" enctype="multipart/data-type">
		  			<div class="row">
		  				<div class="col-lg-12">
		  					<div class="form-group text-center">
		  						<label>Título</label>
		  						<input type="text" class="form-control" id="titulo-novidade" name="titulo">
		  					</div>
		  				</div>
		  			</div>
		  			<div class="row">
		  				<div class="col-lg-12">
		  					<div class="form-group text-center">
		  						<label>Descrição (até 200 caracteres)</label>
		  						<textarea class="form-control" id="descricao-novidade" name="desc" style="height: 80px"></textarea>
		  					</div>
		  				</div>
		  			</div>
		  			<div class="row">
		  				<div class="col-lg-12">
		  					<div class="form-group text-center">
		  						<label>Selecionar foto</label>
		  						<input type="file" class="form-control" id="foto-novidade" name="foto">
		  					</div>
		  				</div>
		  			</div>
		  			<div class="row">
		  				<div class="col-lg-6">
		  					<div class="form-group text-center">
		  						<label>Link</label>
		  						<input type="text" class="form-control" id="link-novidade" name="link" placeholder="tab.estabelecimento">
		  					</div>
		  				</div>
		  				<div class="col-lg-6">
		  					<div class="form-group text-center">
		  						<label>Parâmetro</label>
		  						<input type="text" class="form-control" id="parametro-novidade" name="parametro" placeholder="código do estabelecimento (Ex: 70)"></input>
		  					</div>
		  				</div>
		  			</div>
		  			<div class="row">
		  				<div class="col-lg-12 text-center">
		  					<button id="btn-postar-novidade" class="btn btn-primary">Postar</button>
		  				</div>
		  			</div>
		  		</div>
		  	</form>
		</div>
	</div>
</div>