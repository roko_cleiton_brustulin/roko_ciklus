<div class="modal fade" id="termos-modal">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-body">
	  	<div class="row">
	  		<div class="col-lg-12 text-right">
	  			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">x</span></button>
	  		</div>
	  	</div>
	  	<div class="row">
	  		<div class="col-lg-12">
	  			<h3>Termos de uso</h3>
	  			

					<div>
					 
					<p  style='text-align:justify'><b style='mso-bidi-font-weight:
					normal'>TERMO DE ADESÃO AO CLUBE DA ECONOMIA CIKLUS<o:p></o:p></b></p>
					 
					<p ><b style='mso-bidi-font-weight:normal'><o:p>&nbsp;</o:p></b></p>
					 
					<p >O presente termo dispõe de condições para Associação ao
					Clube da economia CIKLUS, administrado pela ROKO - OSM Informática Ltda, CNPJ
					00.123.611/001-27, obrigando as partes, mutuamente, a submeterem-se
					expressamente a&#768; todas as cláusulas e condições descritas a seguir:</p>
					 
					<p >Considerando que:<o:p></o:p></p>
					 
					<p class=MsoListParagraphCxSpFirst style='margin-left:54.0pt;mso-add-space:
					auto;text-indent:-36.0pt;mso-list:l0 level1 lfo1'><![if !supportLists]><span
					style='mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin'><span
					style='mso-list:Ignore'>(i)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</span></span></span><![endif]><span class=GramE>o</span> ASSINANTE manifestou
					interesse em submeter sua assinatura ao clube da economia CIKLUS;<span
					style='mso-tab-count:1'>        </span></p>
					 
					<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;mso-add-space:
					auto;text-indent:-36.0pt;mso-list:l0 level1 lfo1'><![if !supportLists]><span
					style='mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin'><span
					style='mso-list:Ignore'>(ii)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</span></span></span><![endif]>(<span class=SpellE>ii</span>) a CIKLUS e&#769;
					uma empresa brasileira que atua no ramo de marketing digital, com
					relacionamento entre estabelecimentos parceiros e consumidores através de um
					clube de economia o qual da&#769; direito a benefícios e vantagens exclusivas
					aos seus sócios assinantes.</p>
					 
					<p class=MsoListParagraphCxSpLast style='margin-left:54.0pt;mso-add-space:auto;
					text-indent:-36.0pt;mso-list:l0 level1 lfo1'><![if !supportLists]><span
					style='mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin'><span
					style='mso-list:Ignore'>(iii)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</span></span></span><![endif]>(<span class=SpellE>iii</span>) poderão assinar
					o clube da economia Ciklus as pessoas físicas, maiores de 18 anos, que possuírem
					CPF, mediante o pagamento da taxa estipulada pelo clube da economia Ciklus.</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p ><b style='mso-bidi-font-weight:normal'>Clausula Primeira –
					do objeto<span style='mso-tab-count:9'>                                                                                                                                 </span><span
					style='mso-tab-count:1'>                </span> <span style='mso-tab-count:
					2'>                              </span><span style='mso-tab-count:5'>                                                                               </span><o:p></o:p></b></p>
					 
					<p >1.1.<span style='mso-spacerun:yes'>  </span>O objeto do
					presente contrato consiste em estabelecer, detalhadamente, os termos e
					condições que alinham regras do uso do cartão digital Ciklus para obtenção de
					vantagens nos estabelecimentos comerciais parceiros.</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p >1.2.<span style='mso-spacerun:yes'>  </span>O presente termo
					de adesão descreve, de maneira clara e concisa, o método para obtenção e uso do
					cartão digital Ciklus pelos ASSINANTES (usuários), cabendo como principal ação
					efetuar o pagamento do valor descrito na clausula terceira, para usufruir da intermediação
					de vantagens que o clube Ciklus oferece.</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p ><b style='mso-bidi-font-weight:normal'>Clausula Segunda -
					Participação e Inscrição ao Programa<o:p></o:p></b></p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p >2.1 O ingresso como ASSINANTE ao clube de vantagens Ciklus
					se dará&#769; pela expressa aceitação virtual do respectivo termo através da
					marcação do campo &quot;Aceito os termos de uso da Ciklus&quot;, manifestando
					assim de modo inequívoco, sua vontade de adesão e ao pagamento da taxa
					administrativa estipulada na clausula terceira.</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p >2.2 O ASSINANTE (usuário) deve acessar e efetuar seu
					cadastro dentro do próprio site (www.cikluslive.com.br) ou aplicativo mobile
					(Apple Store ou Google Play). Isso envolve o preenchimento correto e completo
					de seus dados de registro e contato no formulário próprio disponibilizado, no
					qual autoriza, inclusive, o uso e repasse das informações cadastrais da forma
					como melhor entender o clube da economia Ciklus, nas atividades necessárias ao
					desenvolvimento da concessão de vantagens e descontos.</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p >2.3 O clube da economia Ciklus fornece somente cartões
					digitais via aplicativo a usuários comprovadamente domiciliados no território
					brasileiro (Brasil), ou seja, em caso de aquisição feita fora de território
					nacional, a Ciklus não ira&#769; efetuar qualquer tipo de envio do cartão ou prestação
					de serviços. Assim, a empresa torna-se somente responsável pela disponibilidade
					do cartão digital e prestação de serviços aos usuários que fornecerem seus
					dados corretos e dentro do território brasileiro (Brasil).</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p ><span class=GramE>2.4 Em</span> caso de qualquer discordância
					ou suspeita de fornecimento de dados incorretos ou inverídicos, por via deste
					contrato, a CIKLUS terá&#769; o direito total de solicitar cópia autenticada de
					documentos do ASSINANTE para confirmação de seu domicílio, mediante comprovação
					pessoal, com intuito de conferência da existência domiciliar e/ou pessoal. Em
					caso de não confirmação após solicitação extra de documentos, a Ciklus tem
					pleno poder de recusar o novo membro ou suspende&#770;-lo temporariamente do
					programa de descontos intermediados pelo clube.</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p ><span class=GramE>2.5<span style='mso-spacerun:yes'> 
					</span>O</span> ASSINANTE devera&#769; apresentar o cartão digital pelo
					aplicativo Ciklus vigente ou apresentação do CPF, no estabelecimento comercial
					que almeja as vantagens estabelecidas pelo clube, sendo a única forma de obter
					as vantagens ofertadas.</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p >2.6 O uso do cartão do ASSINANTE só&#769; poderá&#769;
					ocorrer pelo titular ou em conjunto com um acompanhante, não podendo de forma
					alguma ser emprestado ou transferido a terceiros, sejam eles parentes, amigos
					ou qualquer outra instituição. Caso o estabelecimento conveniado, por sua livre
					e espontânea vontade, aceite a concessão de descontos ou vantagens a mais
					pessoas que não apenas ao ASSINANTE e seu convidado, tal fato não
					implicara&#769; em aumento dos valores contratados, tampouco vinculara&#769; a
					empresa Ciklus a permitir tal prática, já&#769; que o contrato previamente
					realizado com o estabelecimento prevê&#770;, tão somente, a utilização do cartão
					por parte do ASSINANTE em conjunto com um acompanhante.</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p ><b style='mso-bidi-font-weight:normal'><o:p>&nbsp;</o:p></b></p>
					 
					<p ><b style='mso-bidi-font-weight:normal'>Clausula Terceira -
					Formas e Tipos de Pagamentos</b></p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p >3.1 após devidamente preenchido o formulário próprio
					fornecido no site/aplicativo, o ASSINANTE devera&#769; efetuar o pagamento dos
					valores devidos, na data em que aderirem ao clube da economia Ciklus, de acordo
					com o plano a ser escolhido. Tendo as seguintes opções:<o:p></o:p></p>
					 
					<p ><span style='mso-tab-count:5'>                                                                               </span><o:p></o:p></p>
					 
					<p >*** Plano Mensal - R$ 9,90 (nove reais e noventa centavos)
					para o plano mensal, compreendendo o período de 1 (um) mês de vantagens
					mediante a apresentação do cartão;</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p ><span class=GramE>3.2 Os</span> pagamentos deverão ser
					efetuados, única e exclusivamente, através das formas de pagamento
					disponibilizadas pela Ciklus, sejam elas através de parceiros ou próprias, (que
					poderão gerar juros de acordo com a opção desejada).</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p ><span class=GramE>3.3 Eventuais</span> encargos como juros e
					cobranças de tarifas extraordinárias pelo uso das formas de pagamento
					escolhidas, serão de responsabilidade exclusivas do ASSINANTE, isentando a
					empresa Ciklus de qualquer responsabilidade.</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p ><span class=GramE>3.4 As</span> eventuais alterações na
					forma de pagamento serão informadas aos usuários por meio de notificação
					antecipada, considerando, desde já&#769;, válidas as notificações feitas através
					de correio eletrônico (e-mail) ou publicação no próprio website da Ciklus. As alterações
					poderão ocorrer mensalmente em virtude de atualização ao preço, mercado ou variação
					de índices de inflação, contudo não afetam aos ASSINANTES que adquiriram seus cartões
					até&#769; a data da modificação e que se encontram no período de validade de
					seus cartões.<o:p></o:p></p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p >3.5 Todos os tributos ligados aos serviços contidos na Clausula
					Terceira estão incluídos no pagamento efetuado, de modo que as competentes
					notas fiscais ou recibos serão fornecidos aos ASSINANTES. As empresas
					parceiras, tais como meios de pagamento, não estão vinculadas ao pagamento
					desses tributos mencionados. Contudo, os descontos oferecidos e adquiridos são
					de opção e responsabilidade do ASSINANTE.</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p ><b style='mso-bidi-font-weight:normal'>Clausula quarta –
					demais considerações</b></p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p >4.1 O ASSINANTE ao clube de vantagens outorga a Ciklus o
					direito de utilizar as informações contidas em seu cadastro para fins
					administrativos, de marketing, envio de SMS e notificações <span class=SpellE>push</span>.</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p >4.2.<span style='mso-spacerun:yes'>  </span>A Ciklus
					compromete-se a disponibilizar em seu site as vantagens delimitadas pelo
					estabelecimento comercial parceiro, reservando-se o direito de alterar as
					ofertas sem aviso prévio aos ASSINANTES sem qualquer ônus.</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p >4.3.<span style='mso-spacerun:yes'>  </span>A Ciklus sugere
					que os ASSINANTES sempre visualizem o site Ciklus antes de se dirigir ao
					estabelecimento comercial parceiro a fim de verificarem se ainda estão
					conveniados como parceiros.</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p >4.4.<span style='mso-spacerun:yes'>  </span>Os descontos
					oferecidos pelos estabelecimentos comerciais são garantidos por contrato. No
					caso do não cumprimento das promoções comunique imediatamente a Central de
					Relacionamentos CIKLUS através de nosso e-mail: comercial@ciklus.com.br e do
					telefone (41) 3234-3000 fornecendo os seguintes dados: Nome do estabelecimento,
					nome do associado, data da ocorrência e o nome do atendente ou vendedor do estabelecimento.</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p >4.5.<span style='mso-spacerun:yes'>  </span>A CIKLUS não será&#769;
					responsável pela recusa de um estabelecimento em aceitar o cartão ou por outro
					motivo que o Assinante venha a ter com os estabelecimentos parceiros.</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p >4.6.<span style='mso-spacerun:yes'>  </span>A tolerância ou transigência,
					quanto ao cumprimento das obrigações contratuais serão consideradas atos de
					mera liberalidade da Ciklus, sem acarretar renúncia, novação ou modificação dos
					termos do contrato, os quais permanecerão validos integralmente, como se nada
					houvesse, para todos os fins de direito e efeitos legais, renunciando as partes
					invoca&#769;-<span class=SpellE>las</span> em seu benefício.</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p >4.7.<span style='mso-spacerun:yes'>  </span>A Ciklus reserva
					todos os direitos intelectuais, industriais e comerciais sobre seu portal, como
					sua operação, design das páginas, imagem e pecas publicitárias, estrutura e
					layout, conteúdo exclusivo, informações, ferramentas de pesquisa, programação, símbolos,
					logomarca, slogans e expressões. Todo o material impresso ou online e&#769; de
					total propriedade da Ciklus, sendo vedado qualquer direito ao ASSINANTE ou visitante
					da utilização deste material, sem autorização oficial de um dos representantes
					legais da Ciklus. Em caso de utilização indevida do nome, site, marca ou dos
					direitos correlatos a&#768; empresa Ciklus, por parte de seus ASSINANTES, a
					mesma se reserva ao direito de cancelar a utilização do cartão de descontos,
					bem como a postular a reparação de eventuais danos morais e patrimoniais
					ocasionados.</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p >4.8.<span style='mso-spacerun:yes'>  </span>Na ocorrência de
					falhas, geradas por problemas técnicos, em razão da falta de acesso a informações
					do site, ou por problemas de outra natureza que ocasionem a ausência de
					funcionamento temporário do mesmo, os ASSINANTES não poderão responsabilizar a
					Ciklus, requerendo qualquer tipo de indenização ou reparação, eis que a empresa
					não garante o uso.</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p >4.09.<span style='mso-spacerun:yes'>  </span>A violação de
					qualquer disposição deste contrato de adesão, em especial, o não pagamento da
					assinatura do cartão, dará&#769; causa a rescisão desse contrato e cancelamento
					da conta do ASSINANTE.</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p >4.10.<span style='mso-spacerun:yes'>  </span>Sem prejuízo
					das modificações decorrentes de mudanças legais ou regulamentares, o Contrato só&#769;
					poderá&#769; ser alterado se, respeitado a Boa Fe&#769; e a função Social de
					contrato, não se onerar injustificadamente o ASSINANTE, nem romper o que aqui
					foi acordado. Sendo assim o mesmo não poderá&#769; ser alterado sem o prévio
					aviso da parte interessada, no acaso o ASSINANTE.</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p >4.11.<span style='mso-spacerun:yes'>  </span>Todos os dados
					informativos contidos e cedidos na ficha do ASSINANTE são confidenciais e de
					uso exclusivo para efeito de cadastro e correspondências a serem encaminhadas
					ao mesmo.</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p ><b style='mso-bidi-font-weight:normal'>Clausula quinta - Aceitação
					e Condição do Contrato e suas Cláusulas</b></p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p >5.1. Sendo pressionado a marcação (check box) na página do formulário
					de cadastro (ou a seleção onde trouxe o ASSINANTE a leitura deste contrato),
					fica definitivamente declarado que o mesmo leu todas as cláusulas e aspectos
					descritos neste contrato e aceitou ser membro do clube diante todas as normas
					aqui descritas. Isso também define que sua intenção em ingressar ao clube da
					economia Ciklus não esteja, de forma alguma, relacionada a algum tipo de
					garantia, manifestação ou declaração aqui não delineada continuo e ininterrupto
					de seu site.</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p ><b style='mso-bidi-font-weight:normal'>Clausula sexta - Renovação
					Automática do Titular<o:p></o:p></b></p>
					 
					<p ><b style='mso-bidi-font-weight:normal'><o:p>&nbsp;</o:p></b></p>
					 
					<p >6.1 O presente termo de adesão estabelece o uso do cartão
					digital em 1 (um) mês, conforme opção selecionada e valor a ser pago a Ciklus.
					O contrato poderá&#769; ser reincidido por ambas as partes por meio de carta
					escrita ou eletrônica, independentemente de qualquer prazo ou de qualquer sorte
					de indenização ou reembolso.<b style='mso-bidi-font-weight:normal'><o:p></o:p></b></p>
					 
					<p ><b style='mso-bidi-font-weight:normal'><o:p>&nbsp;</o:p></b></p>
					 
					<p ><span class=GramE>6.2 Em</span> caso da não manifestação do
					ASSINTANTE, o plano em andamento será&#769; automaticamente renovado mediante a
					cobrança de nova taxa de assinatura no valor vigente ao tempo de renovação.<b
					style='mso-bidi-font-weight:normal'><o:p></o:p></b></p>
					 
					<p ><b style='mso-bidi-font-weight:normal'><o:p>&nbsp;</o:p></b></p>
					 
					<p ><span class=GramE>6.3 Após</span> a renovação ASSINANTE terá&#769;
					5 (cinco) dias para solicitar o cancelamento e reembolso em caso de
					arrependimento ou surpresa sobre a renovação.</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p ><b style='mso-bidi-font-weight:normal'>Clausula sétima –
					Parceiros comerciais<span style='mso-tab-count:1'> </span><o:p></o:p></b></p>
					 
					<p ><b style='mso-bidi-font-weight:normal'><o:p>&nbsp;</o:p></b></p>
					 
					<p >7.1 O ASSINANTE do serviço devera&#769; verificar todos os
					detalhes e condições das vantagens, horários e datas oferecidos pelos
					estabelecimentos, não sendo de responsabilidade da Ciklus divergências dessas
					promoções. Em caso de insatisfação ou falhas junto as promoções desses
					estabelecimentos, o ASSINANTE devera&#769; procurar seus direitos como
					consumidor, junto ao estabelecimento comercial concessor da vantagem, já&#769;
					que a empresa Ciklus apenas confere ao ASSINANTE a possibilidade de obter
					vantagens nos estabelecimentos conveniados, não participando, de qualquer
					forma, na prestação de serviços do estabelecimento concessor das vantagens.<b
					style='mso-bidi-font-weight:normal'><o:p></o:p></b></p>
					 
					<p ><b style='mso-bidi-font-weight:normal'><o:p>&nbsp;</o:p></b></p>
					 
					<p >7.2 A utilização do cartão digital não e&#769; acumulativa
					com outras promoções do estabelecimento, salvo se houver expressa concessão do
					estabelecimento parceiro. O desconto concedido e&#769; valido somente para os preços
					apresentados no site da Ciklus.</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p ><b style='mso-bidi-font-weight:normal'><o:p>&nbsp;</o:p></b></p>
					 
					<p ><b style='mso-bidi-font-weight:normal'>Clausula oitava –
					Disposições gerais<o:p></o:p></b></p>
					 
					<p ><b style='mso-bidi-font-weight:normal'><o:p>&nbsp;</o:p></b></p>
					 
					<p ><span class=GramE>8.1 As</span> partes elegem o Foro Central
					da Comarca da Região Metropolitana de Curitiba, Estado do Paraná&#769;, para
					dirimir quaisquer dúvidas oriundas do presente contrato, renunciando a qualquer
					outro, por mais privilegiado que seja.</p>
					 
					<p ><o:p>&nbsp;</o:p></p>
					 
					<p >Por fim, o ASSINANTE se responsabiliza pelas informações
					prestadas, bem como autoriza a confirmação das mesmas.</p>
					 
					<p ><b style='mso-bidi-font-weight:normal'><o:p>&nbsp;</o:p></b></p>
					 
					</div>



	  		</div>
	  	</div>
	  </div>
	</div>
  </div>
</div>