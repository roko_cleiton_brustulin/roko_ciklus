<?php
$sqlBairros = "SELECT DISTINCT(bairro) FROM estabelecimento WHERE status = 'a'";
$res = json_decode(api('estabelecimento/fullSearch?where='.urlencode($sqlBairros)));
$bairros = $res->rows[0]->json_agg;
sort($bairros);

$estabelecimentoLogado = false;
if (isset($_COOKIE['usuario-ciklus'])) {
	$usuario = json_decode($_COOKIE['usuario-ciklus']);
}
if (isset($_COOKIE['estabelecimento-ciklus'])) {
	$estabelecimentoLogado = true;
}
?>
<!DOCTYPE html>
<html class="no-js before-run" lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
		<link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon" />
		<?php 
			//na tela de estabelecimento essa variável é setada como true pra carregar as meta tags cadastradas para o estabelecimento.
			if (@$metaTags) {
				?>
				<title><?php echo $metaTitulo; ?></title>
				<meta name="description" content="<?php echo utf8_decode($metaDescricao); ?>">
				<?php
			} else {
				?>
				<title>Ciklus live - Clube da economia</title>
				<meta name="description" content="Ciklus Live é um clube da economia que, mais do que dar descontos, ele traz a informação de quanto e onde você pode economizar.">
				<meta name="author" content="Ciklus">
				<?php
			}
		?>
		
		<link href='https://fonts.googleapis.com/css?family=Oxygen:400,300,700' rel='stylesheet' type='text/css'>
	  	<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
	  	<link rel="stylesheet" href="assets/custom/css/custom.css">
	  	<link rel="stylesheet" href="assets/sweetalert/sweetalert.css">
		<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="assets/maskedinput/jquery.mask.plugin.js"></script>
		<script type="text/javascript" src="assets/maskedinput/maskmoney.js"></script>
		<script type="text/javascript" src="assets/sweetalert/sweetalert.min.js"></script>
		<script type="text/javascript" src="assets/cookie/cookie.min.js"></script>
		<script src="https://assets.pagar.me/pagarme-js/3.0/pagarme.min.js"></script>
		<script type="text/javascript" src="jsapp/custom.js"></script>
		<?php echo @$scripts; ?>
		<script type="text/javascript">
		$(document).ready(function() {
			$("#entre-para-o-clube").click(function(){
				window.location = "cadastro.php";
			});
		});
		</script>
  	</head>
  	<body>
  		<nav class="navbar navbar-default navbar-fixed-top" style="min-height:70px !important;">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="/"><img src="assets/images/logo.png" style="width:200px;padding-top:19px;"></a>
		      <div class="dropdown" style="line-height:30px;margin-top:24px;padding-left:50px;float:left;">
		      		<button class="btn btn-default dropdown-toggle" style="" type="button" data-toggle="dropdown"><i class="glyphicon glyphicon-map-marker"></i> CURITIBA
		      		<span class="caret"></span></button>
		      		<ul class="dropdown-menu">
						<!--LISTAGEM DE BAIRROS-->
  					<?php 	$count2 = 0;
  						  	foreach ($bairros as $bairro) : 
		  				  		$count2++;
		  				  		$bairro = $bairro->bairro;
		  				  		if (trim($bairro) != "") {
		  				  			if ($count2 < 11) {	?>
		  								<li>
		  									<a href="estabelecimentos?bairro=<?php echo $bairro; ?>" <?php if (@$_GET['bairro'] == $bairro) : echo "style='font-weight:bold;'"; endif; ?>><?php echo $bairro; ?>
		  									</a>
		  								</li>
	  					<?php 		}
	  				  			}
		  					endforeach;
  					?>
					</ul>
			   </div>
			</div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li <?php if ((strpos($_SERVER['REQUEST_URI'], '/'))  == (strlen($_SERVER['REQUEST_URI']) - 1)) : echo "class='active'"; endif; ?>><a href="/">Início <span class="sr-only">(current)</span></a></li>
		        <li <?php if (strpos($_SERVER['REQUEST_URI'], 'estabelecimentos') > 0) : echo "class='active'"; endif; ?>><a href="estabelecimentos">Estabelecimentos</a></li>
		        <li <?php if (strpos($_SERVER['REQUEST_URI'], 'eventos') > 0) : echo "class='active'"; endif; ?>><a href="eventos">Eventos</a></li>
		        <li <?php if (strpos($_SERVER['REQUEST_URI'], 'seja') > 0) : echo "class='active'"; endif; ?>><a href="seja-parceiro">Seja Parceiro</a></li>
		        <li <?php if (strpos($_SERVER['REQUEST_URI'], 'contato') > 0) : echo "class='active'"; endif; ?>><a href="contato">Contato</a></li>
		      </ul>
		      <!-- <form class="navbar-form navbar-left" role="search">
		        <div class="form-group">
		          <input type="text" class="form-control" placeholder="Procurar ofertas...">
		        </div>
		        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
		      </form> -->
		      <ul class="nav navbar-nav navbar-right" style="padding-right:15px;">
		      	<?php if (@$usuario->id > 0) {
		      		echo '<li><a href="minha-conta" class="menu-user"><i class="glyphicon glyphicon-user"></i> Minha Conta</a></li><li><a class="menu-user" href="#" id="btn-logout"> <i class="glyphicon glyphicon-log-out"></i> Sair</a></li>';
		        } else if (@$estabelecimentoLogado) {
		        	echo '<li><a class="menu-user" href="painel-estabelecimento"><i class="glyphicon glyphicon-home"></i> Painel</a></li><li><a class="menu-user" href="#" id="btn-logout"><i class="glyphicon glyphicon-log-out"></i> Sair</a></li>';
		        } else {
		        	echo '<li style="text-align: center;">
		        		<button id="entre-para-o-clube" class="btn btn-primary" style="line-height:30px;margin-top:18px;">Entre para o clube</button>
		        	</li><li><a href="#login" data-toggle="modal" class="menu-user"><i class="glyphicon glyphicon-log-in"></i> Acessar</a></li>';
		        } ?>
		        	
		      </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>