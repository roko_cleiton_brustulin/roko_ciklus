<footer class="footer">
	<div>
		<div class="text-center">
			<div class="col-md-4 footer-links">
				<h4>Ciklus</h4>
					<a href="estabelecimentos"><i class="glyphicon glyphicon-chevron-right"></i> Estabelecimentos</a><br>
					<a href="eventos"><i class="glyphicon glyphicon-chevron-right"></i> Eventos</a><br>
					<a href="seja-parceiro"><i class="glyphicon glyphicon-chevron-right"></i> Seja Parceiro</a><br>
					<a href="contato"><i class="glyphicon glyphicon-chevron-right"></i> Contato</a><br>
					<a href="minha-conta"><i class="glyphicon glyphicon-chevron-right"></i> Minha Conta</a><br>
					<a href="cadastro"><i class="glyphicon glyphicon-chevron-right"></i> Cadastre-se</a>
			</div>
			<div class="col-md-4 text-center">
				<h4>Newsletter</h4>
				<p>Cadastre seu e-mail para receber nossas novidades:</p>
				<form>
					<div class="form-group text-center">
					<input type="email" class="form-control" id="email-newsletter" placeholder="Email">
					<button class="btn btn-default" id="btn-email-newsletter" style="margin: 10px 0;">Cadastrar</button>
					</div>
				</form>
			</div>
			<div class="col-md-4" style="line-height: 25px;">
				<h4>Fale Conosco</h4>
				<i class="fa fa-phone"></i> (41) 3234-3000<br>
				<i class="fa fa-whatsapp"></i> (41) 9 9863-4869<br>
				<i class="fa fa-envelope"></i> contato@ciklus.com.br<br>
				<i class="fa fa-map-maker"></i> R. XV de Novembro, 1155, Centro, Curitiba-PR<br>
				<h4 style="margin-bottom: 5px;">Siga-nos</h4>
				<div class="icones-midias">
					<a href="http://www.facebook.com.br/ciklusliveclube"><i class="fa fa-facebook"></i></a> 
					<a href="http://www.instagram.com/clube_ciklus"><i class="fa fa-instagram"></i></a> 
					<!-- <a href="#"><i class="fa fa-youtube"></i></a> 
					<a href="#"><i class="fa fa-pinterest"></i></a> -->
				</div>
			</div>
		</div>
	<div class="text-center">
		<span class="v-align text-center" >2017 - Ciklus &copy; - Todos os direitos reservados.</span>
	</div>
</div>
</footer>
	<?php
		include('widgets/modal-login.php');
		include("widgets/modal-termos.php");
		include("widgets/modal-planos.php");
		include("widgets/modal-cartao.php");
	?>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		  ga('create', 'UA-48248255-3', 'auto');
		  ga('send', 'pageview');
		</script>
	</body>
</html>