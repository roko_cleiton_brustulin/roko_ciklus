<div class="modal fade" id="login">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-body">
	  	<div class="row">
	  		<div class="col-lg-12 text-right">
	  			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">x</span></button>
	  		</div>
	  	</div>
	  	<div class="row">
	  		<div class="col-xs-12 text-center login-usuario">
	  			<h4>Digite seu CPF e senha para acessar sua conta</h4>
	  		</div>
	  		<div class="col-xs-12 col-lg-offset-3 col-lg-6 col-lg-offset-3 text-center login-usuario" style="margin-top:10px;">
	  			<div class="form-group">
	  				<label>CPF</label>
	  				<input type="text" class="form-control text-center" id="login-cpf" />
	  			</div>
	  			<div class="form-group">
	  				<label>Senha</label>
	  				<input type="password" class="form-control text-center" id="login-senha" />
	  			</div>
	  			<div class="form-group">
	  				<button id="btn-login" class="btn btn-primary">Acessar</button>
	  			</div>
	  			<div class="form-group">
	  				<a href="esqueceu-senha">Esqueci minha senha</a>
	  			</div>
	  		</div>
	  		<div class="col-xs-12 text-center login-usuario">
  				<a href="cadastro">Ainda não tem Ciklus? Clique aqui e comece agora!</a>
  			</div>
	  	</div>
	  	<br>
	  	<div class="row">
	  		<div class="col-xs-12 text-center login-estabelecimento">
	  			<h4>Digite o login e senha do seu estabelecimento</h4>
	  		</div>
	  		<div class="col-xs-12 col-lg-offset-3 col-lg-6 col-lg-offset-3 text-center login-estabelecimento" style="margin-top:10px;">
	  			<div class="form-group">
	  				<label>Login</label>
	  				<input type="text" class="form-control text-center" id="login-estabelecimento" />
	  			</div>
	  			<div class="form-group">
	  				<label>Senha</label>
	  				<input type="password" class="form-control text-center" id="senha-estabelecimento" />
	  			</div>
	  			<div class="form-group">
	  				<button id="btn-login-estabelecimento" class="btn btn-primary">Acessar</button>
	  			</div>
	  		</div>
	  		<div class="col-xs-12 text-center">
	  			<a href="#" id="acessar-login-usuario">Acessar como Sócio</a>
  				<a href="#" id="acessar-login-estabelecimento">Acessar como Estabelecimento</a>
  			</div>
	  	</div>
	  </div>
	</div>
  </div>
</div>