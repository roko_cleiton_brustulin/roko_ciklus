<!DOCTYPE html>
<html class="no-js before-run" lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
		<?php 
			//na tela de estabelecimento essa variável é setada como true pra carregar as meta tags cadastradas para o estabelecimento.
			if (@$metaTags) {
				?>
				<title><?php echo $metaTitulo; ?></title>
				<meta name="description" content="<?php echo utf8_decode($metaDescricao); ?>">
				<?php
			} else {
				?>
				<title>Ciklus live - Clube da economia</title>
				<?php
			}
		?>
		
		<link href='https://fonts.googleapis.com/css?family=Oxygen:400,300,700' rel='stylesheet' type='text/css'>
	  	<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
	  	<link rel="stylesheet" href="assets/custom/css/custom.css">
	  	<link rel="stylesheet" href="assets/sweetalert/sweetalert.css">
		<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="assets/maskedinput/jquery.mask.plugin.js"></script>
		<script type="text/javascript" src="assets/maskedinput/maskmoney.js"></script>
		<script type="text/javascript" src="assets/sweetalert/sweetalert.min.js"></script>
		<script type="text/javascript" src="assets/cookie/cookie.min.js"></script>
		<script src="https://assets.pagar.me/pagarme-js/3.0/pagarme.min.js"></script>
		<script type="text/javascript" src="jsapp/custom.js"></script>
		<?php echo @$scripts; ?>

  	</head>
  </html>