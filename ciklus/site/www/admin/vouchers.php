<?php 
include('includes/settings.php');
include('action/planos.php');
include('widgets/header.php');
?>

<div class="page">
  	<div class="page-content">
    	<div class="panel">
      		<div class="panel-heading">
        		<h3 class="panel-title">Ações</h3>
      		</div>
      		<div class="panel-body container-fluid">
        		<a href="voucher.php" class="btn btn-success">Gerar Vouchers</a>
            <a href="#" id="baixar" class="btn btn-success">Baixar Relatório de Vouchers</a>
      		</div>
    	</div>
	</div>
</div>

<?php 
$script = "<script type='text/javascript' src='lib/vouchers.js'></script>";
include('widgets/footer.php');
