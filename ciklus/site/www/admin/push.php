<?php 
include('includes/settings.php');
include('action/voucher.php'); 
include('widgets/header.php'); 
?>

  <div class="page">
    <div class="page-content">


      <div class="panel">
        <div class="panel-heading">
          <h3 class="panel-title">Empresa</h3>
        </div>
        <div class="panel-body container-fluid">
          <div class="row row-lg">
            <div class="col-sm-12 col-md-12" style="margin-bottom: 15px;">  
              <select class="form-control" id="empresa">
                  <!-- <option value="01;">Ciklus</option> -->
                  <option value="02;" selected>Alô Ingressos</option>
                </select>
            </div>
          </div>
        </div>
      </div>


      <div class="panel">
        <div class="panel-heading">
          <h3 class="panel-title">Destinatários</h3>
        </div>
        <div class="panel-body container-fluid">
          <div class="row row-lg">
            <div class="col-sm-12 col-md-12" style="margin-bottom: 15px;">  
              <h4 class="example-title">IDs dos Associados (separar sempre por ;)</h4>
              <input type="text" id="ids" class="form-control" placeholder="Em branco irá para todos os associados"/>
            </div>
          </div>
        </div>
      </div>


      <div class="panel">
        <div class="panel-heading">
          <h3 class="panel-title">Parâmetros - Push</h3>
        </div>
        <div class="panel-body container-fluid">
          <div class="row row-lg">

            <div class="col-sm-12 col-md-12" style="margin-bottom: 15px;">  
              <h4 class="example-title">Título (40 caracteres)</h4>
              <input type="text" id="titulo" class="form-control" />
            </div>

            <div class="col-sm-12 col-md-12" style="margin-bottom: 15px;">  
              <h4 class="example-title">Mensagem</h4>
              <textarea type="text" id="mensagem" class="form-control"></textarea>
            </div>

          </div>
        </div>
      </div>

      <div class="panel">
        <div class="panel-heading">
          <h3 class="panel-title">Parâmetros - Histórico Notificação App</h3>
        </div>
        <div class="panel-body container-fluid">
          <div class="row row-lg">

            <div class="col-sm-12 col-md-12" style="margin-bottom: 15px;">  
              <h4 class="example-title">Mensagem</h4>
              <textarea type="text" id="msg_notificacao" class="form-control"></textarea>
            </div>

            <div class="col-sm-12 col-md-12" style="margin-bottom: 15px;">  
              <h4 class="example-title">Link</h4>
              <input type="text" id="link_notificacao" class="form-control" placeholder="tab.estabelecimento"></input>
            </div>

            <div class="col-sm-12 col-md-12" style="margin-bottom: 15px;">  
              <h4 class="example-title">Parâmetro</h4>
              <input type="text" id="parametro_notificacao" class="form-control" placeholder="código do estabelecimento (Ex.: 70)"></input>
            </div>

          </div>
        </div>
      </div>

      <div class="panel">
        <div class="panel-heading">
          <h3 class="panel-title">Ações</h3>
        </div>
        <div class="panel-body container-fluid">
          <button class="btn btn-success" id="btn-salvar" type="submit">Enviar Push</button>
        </div>
      </div>
    </div>
  </div>

<?php 
$script = "<script type='text/javascript' src='lib/push.js'></script>";
include('widgets/footer.php'); ?>