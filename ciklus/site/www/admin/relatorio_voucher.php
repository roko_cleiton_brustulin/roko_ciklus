<?php
include('includes/settings.php');

$relatorio = json_decode(api('cupom/baixar'));

$csv = "";
foreach ($relatorio as $r) { 
	if ($r->utilizado == false || $r->utilizado == '') {
		$r->utilizado = 'N';
	} else {
		$r->utilizado = 'S';
	}

	$r->expiracao = explode('T', $r->expiracao);

	$csv .= $r->plano . ';' . $r->codigo . ';' . $r->expiracao[0] . ';' . $r->utilizado . ';<br />';
}

echo $csv;