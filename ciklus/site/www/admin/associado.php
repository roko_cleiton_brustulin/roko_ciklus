<?php 
include('includes/settings.php');
include('action/associado.php'); 
include('widgets/header.php'); 
?>

<div class="page">
  <div class="page-content">
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">Ações</h3>
      </div>
      <div class="panel-body container-fluid">
        <a class="btn btn-default" href="associados.php">Voltar</a>
        <button class="btn btn-success" id="btn-salvar" type="button">Salvar</button>
        <?php if (@$associado->id > 0) : ?>
        <button class="btn btn-warning" id="btn-desativar" type="button">Desativar</button>
        <?php endif; ?>
      </div>
    </div>

    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">Cadastro do Associado</h3>
      </div>
      <div class="panel-body container-fluid">
        <div class="row row-lg">
          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">  
            <h4 class="example-title">CPF</h4>
            <input type="hidden" id="id" value="<?php echo @$associado->id; ?>" />
            <input type="text" class="form-control" id="cpf" name="cpf" value="<?php echo @$associado->cpf; ?>"  placeholder="CPF do Associado">
          </div>

          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">  
            <h4 class="example-title">Nome Completo</h4>
            <input type="text" class="form-control" name="nome" id="nome" value="<?php echo @$associado->nome; ?>"  placeholder="Nome do Associado">
          </div>

          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">  
            <h4 class="example-title">E-mail</h4>
            <input type="text" class="form-control" name="email" id="email" value="<?php echo @$associado->email; ?>"  placeholder="E-mail do Associado">
          </div>

          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">  
            <h4 class="example-title">Senha</h4>
            <input type="password" class="form-control" name="senha" id="senha" placeholder="Criar/trocar senha">
          </div>          

          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">  
            <h4 class="example-title">Data de Nascimento</h4>
            <input type="text" class="form-control" id="data_nasc" name="data_nasc" value="<?php echo @$associado->dataNasc; ?>"  placeholder="Data de Nascimento">
          </div>

          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">  
            <h4 class="example-title">Sexo</h4>
            <select class="form-control" id="sexo">
              <option>Selecione</option>
              <option value="fem" <?php if (@$associado->sexo == "fem") : echo "selected"; endif; ?>>Feminino</option>
              <option value="mas" <?php if (@$associado->sexo == "mas") : echo "selected"; endif; ?>>Masculino</option>
            </select>
          </div>

          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">  
            <h4 class="example-title">CEP</h4>
            <input type="text" class="form-control" id="cep" name="cep" value="<?php echo @$associado->cep; ?>"  placeholder="CEP do Associado">
          </div>

          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">  
            <h4 class="example-title">Endereço</h4>
            <input type="text" class="form-control" id="endereco" name="endereco" value="<?php echo @$associado->rua; ?>"  placeholder="Endereço">
          </div>

          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">  
            <h4 class="example-title">Número & Complemento</h4>
            <input type="text" class="form-control" name="numero" id="numero" value="<?php echo @$associado->numero; ?>"  placeholder="Número e complemento">
          </div>

          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">  
            <h4 class="example-title">Bairro</h4>
            <input type="text" class="form-control" id="bairro" name="bairro" value="<?php echo @$associado->bairro; ?>"  placeholder="Bairro">
          </div>

          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">  
            <h4 class="example-title">Cidade</h4>
            <input type="text" class="form-control" id="cidade" name="cidade" value="<?php echo @$associado->cidade; ?>"  placeholder="Cidade">
          </div>

          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">  
            <h4 class="example-title">Estado</h4>
            <input type="text" class="form-control" id="estado" name="estado" value="<?php echo @$associado->uf; ?>"  placeholder="Estado">
          </div>

          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">  
            <h4 class="example-title">Telefone</h4>
            <input type="text" class="form-control" id="telefone" name="telefone" value="<?php echo @$associado->telefone; ?>"  placeholder="Telefone">
          </div>

          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">  
            <h4 class="example-title">Celular</h4>
            <input type="text" class="form-control" id="celular" name="celular" value="<?php echo @$associado->celular; ?>"  placeholder="Celular">
          </div>

        </div>
      </div>
    </div>

    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">Cartões do Associado</h3>
      </div>
      <div class="panel-body container-fluid">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="persist">Nº Cartão</th>
              <th data-tablesaw-sortable-col data-tablesaw-priority="3">Tipo</th>
              <th data-tablesaw-sortable-col data-tablesaw-priority="2">Aquisição</th>
              <th data-tablesaw-sortable-col data-tablesaw-priority="2">Origem</th>
              <th data-tablesaw-sortable-col data-tablesaw-priority="2">Vencimento</th>
              <th data-tablesaw-sortable-col data-tablesaw-priority="1">
                <abbr title="Ações">Ações</abbr>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1000 0000 1000 0001</td>
              <td><a href="javascript:void(0)">Ciklus Padrão</a></td>
              <td>30/10/2015</td>
              <td>Site Ciklus</td>
              <td>30/10/2016</td>
              <td><a href="#">Desativar</a></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">Pagamentos do Associado</h3>
      </div>
      <div class="panel-body container-fluid">
        
      </div>
    </div>

    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">Utilização</h3>
      </div>
      <div class="panel-body container-fluid">
        
      </div>
    </div>

  </div>
</div>

<?php
$script = "<script type='text/javascript' src='lib/associado.js'></script>";
include('widgets/footer.php'); ?>