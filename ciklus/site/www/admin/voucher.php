<?php 
include('includes/settings.php');
include('action/voucher.php'); 
include('widgets/header.php'); 
?>

  <div class="page">
    <div class="page-content">

      <div class="panel">
        <div class="panel-heading">
          <h3 class="panel-title">Gerar Vouchers</h3>
        </div>
        <div class="panel-body container-fluid">
          <div class="row row-lg">
            <input type="hidden" value="<?php echo @$plano->id; ?>" id="planoid" />
            <div class="col-sm-12 col-md-12" style="margin-bottom: 15px;">  
              <h4 class="example-title">Plano</h4>
              <select class="form-control" id="plano" name="plano">
                <option>Selecione</option>
                <?php foreach ($planos as $plano) { ?>
                <option value="<?php echo $plano->id ?>" <?php if (@$voucher->plano == $plano->id) : echo "selected"; endif; ?>><?php echo $plano->titulo ?></option>
                <?php } ?>
              </select>
            </div>

            <div class="col-sm-4 col-md-4" style="margin-bottom: 15px;">  
              <h4 class="example-title">Tipo do Código</h4>
              <select class="form-control" id="tipo" name="tipo">
                <option>Selecione</option>
                <option value="1">Fixo</option>
                <option value="2">Aleatório (somente números)</option>
                <option value="3">Aleatório (números e letras)</option>
              </select>
            </div>

            <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">  
              <h4 class="example-title">Quantidade a ser gerada</h4>
              <input type="text" id="qtde" class="form-control" />
            </div>

            <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">  
              <h4 class="example-title">Expiração</h4>
              <input type="text" class="form-control" name="expiracao" id="expiracao" value="<?php echo @$voucher->expiracao; ?>">
            </div>

            <div class="col-sm-12 col-md-12" style="margin-bottom: 15px;">  
              <h4 class="example-title">Prefixo do código</h4>
              <input type="text" id="prefixo" class="form-control" />
            </div>

          </div>
        </div>
      </div>
      <div class="panel">
        <div class="panel-heading">
          <h3 class="panel-title">Ações</h3>
        </div>
        <div class="panel-body container-fluid">
          <button class="btn btn-success" id="btn-salvar" type="submit">Gerar Vouchers</button>
        </div>
      </div>
    </div>
  </div>

<?php 
$script = "<script type='text/javascript' src='lib/voucher.js'></script>";
include('widgets/footer.php'); ?>