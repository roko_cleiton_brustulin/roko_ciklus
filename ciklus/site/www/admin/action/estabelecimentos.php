<?php

$status = @$_GET['status'];

$sql = "SELECT e.nome, c.nome as categoria, e.id, e.bairro, e.cidade
		FROM estabelecimento e 
		LEFT JOIN categoria c ON e.categoria = c.id";

if ($status) {
	$sql .= " WHERE e.status = '$status'";
}

$query = json_decode(api('estabelecimento/fullSearch?where='.urlencode($sql)));

$rows = array();
if (isset($query->rows)) {
	$rows = $query->rows[0]->json_agg;
}