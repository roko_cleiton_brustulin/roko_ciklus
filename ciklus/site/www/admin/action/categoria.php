<?php

$categorias = json_decode(api('categoria/buscarTodas'));
if (@$_GET['id']) {
	$categoria = json_decode(api('categoria/' . $_GET['id']));
	$imagemCategoria = json_decode(api('estabelecimentofoto/find?categoria=' . $categoria->id));
	if (!empty($imagemCategoria)) {
		$imagemCategoria = $imagemCategoria[0];
	}
} else {
	$categoria = null;
}

$uploaddir = "uploads/";

if (@$_POST['id'] > 0) {
	$id = $_POST['id'];
	if (@$_FILES['imagem-upload']) {
		$ext = pathinfo($_FILES['imagem-upload']['name'], PATHINFO_EXTENSION);
		$nome = $_FILES['imagem-upload']['name'];
		
		$dados = array(	"nome" => $nome, 
						"extensao" => $ext,
						//"estabelecimento" => null,
						"categoria" => $id);
		$imagem = json_decode(api('estabelecimentofoto/create', $dados));

		$uploadfile = $uploaddir . basename($imagem->id . "." . $ext);

		if (move_uploaded_file($_FILES['imagem-upload']['tmp_name'], $uploadfile)) {
			echo "Upload realizado com sucesso.";
		} else {
			echo "Erro ao realizar upload do arquivo.";
		}
		header("location: categoria.php?id=$id");
	}
}
