<?php

$status = @$_GET['status'];

$sql = "SELECT c.id, c.nome, cc.nome AS nomepai, cc.id AS idpai, c.status
FROM categoria c
LEFT JOIN categoria cc ON c.\"categoriaPai\" = cc.id";

if ($status != NULL) {
	$sql .= " WHERE c.status = ".$status;
}

$query = json_decode(api('estabelecimento/fullSearch?where='.urlencode($sql)));
$rows = array();
if (isset($query->rows)) {
	$rows = $query->rows[0]->json_agg;
}

if (@$_GET['desativar']) {
	$id = $_GET['id'];
	json_decode(api('categoria/update/'.$id, array('status' => 0)));
	$cats = json_decode(api('categoria/find?categoriaPai='.$id));
	foreach ($cats as $cat) {
		api('categoria/update/'.$cat->id, array('status' => 0));
	}
	header('Location: categorias.php?status=1');
}
if (@$_GET['ativar']) {
	$id = $_GET['id'];
	json_decode(api('categoria/update/'.$id, array('status' => 1)));
	$cats = json_decode(api('categoria/find?categoriaPai='.$id));
	foreach ($cats as $cat) {
		api('categoria/update/'.$cat->id, array('status' => 1));
	}
	header('Location: categorias.php?status=1');
}