<?php
ini_set("post_max_size", "20MB");
ini_set("upload_max_filesize", "20MB");

//$categorias = json_decode(api('categoria/find'));
$sql = "SELECT c.nome, c.id, cp.nome AS \"nomePai\" 
		FROM categoria c 
		LEFT JOIN categoria cp ON c.\"categoriaPai\" = cp.id 
		WHERE c.status = 1 ORDER BY \"nomePai\"";
$query = json_decode(api('estabelecimento/fullSearch?where='.urlencode($sql)));

$categorias = array();
if (isset($query->rows)) {
	$categorias = $query->rows[0]->json_agg;
}

if (@isset($_GET['id'])) {
	$id = $_GET['id'];

	if (@$_FILES['imagem-upload']) {
		$ext = pathinfo($_FILES['imagem-upload']['name'], PATHINFO_EXTENSION);
		$nome = $_FILES['imagem-upload']['name'];
		
		$dados = array(	"nome" => $nome, 
						"extensao" => $ext,
						"estabelecimento" => $id);
		$imagem = json_decode(api('estabelecimentofoto/create', $dados));

		$uploaddir = "uploads/";
		$uploadfile = $uploaddir . basename($imagem->id . "." . $ext);

		if (move_uploaded_file($_FILES['imagem-upload']['tmp_name'], $uploadfile)) {

		} else {
			echo "Erro ao realizar upload do arquivo.";
		}
	}

	$estabelecimento = json_decode(api('estabelecimento/'.$id));
}