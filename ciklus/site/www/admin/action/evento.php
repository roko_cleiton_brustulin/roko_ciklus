<?php

$sql = "SELECT c.nome, c.id, cp.nome AS \"nomePai\" FROM categoria c LEFT JOIN categoria cp ON c.\"categoriaPai\" = cp.id WHERE c.status = 1 ORDER BY \"nomePai\"";
$query = json_decode(api('estabelecimento/fullSearch?where='.urlencode($sql)));
$evento = null;

$rows = null;
if (isset($query->rows)) {
	$categorias = $query->rows[0]->json_agg;
}

if (@$_FILES['imagem-upload']) {
	$ext = pathinfo($_FILES['imagem-upload']['name'], PATHINFO_EXTENSION);
	$nome = $_FILES['imagem-upload']['name'];
	
	$dados = array(	"nome" => $nome, 
					"extensao" => $ext);
	$imagem = json_decode(api('estabelecimentofoto/create', $dados));

	$updateEventoData = array('foto' => $imagem->id);
	api('evento/update/'.$_GET['id'], $updateEventoData);

	$uploaddir = "uploads/";
	$uploadfile = $uploaddir . basename($imagem->id . "." . $ext);

	if (move_uploaded_file($_FILES['imagem-upload']['tmp_name'], $uploadfile)) {
		
	} else {
		echo "Erro ao realizar upload do arquivo.";
	}
}

if (@$_GET['id']) {
	$evento = json_decode(api('evento/'.$_GET['id']));
	$evento->data = explode('-', substr($evento->data, 0, 10));
	$evento->data = $evento->data[2] . '/' . $evento->data[1] . '/' . $evento->data[0];
}