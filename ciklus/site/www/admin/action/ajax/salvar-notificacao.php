<?php
include('../../includes/settings.php');

$IDs = $_POST['ids'];

//Cria Histórico de Notificação
$data = array(	'texto' 	=> $_POST['texto'],
				'link' 		=> $_POST['link'],
				'parametro' => $_POST['parametro'],
				'socio' 	=> $IDs,
				'empresa' 	=> $_POST['empresa']);
var_dump(api('notificacao/create', $data));


//Atualiza qtde de notificações em cada usuário
if($IDs == ''){
	//Busca todos os IDs de todos os associados caso não tenha sido passado nenhum
	$todos = json_decode(api('socio/buscarTodos'));
	$arr_length = count($todos);
	for($i=0; $i < $arr_length; $i++)
	{
		$IDs = $IDs . ';' . $todos[$i]->id;
	}
}

//For para cada sócio somando 1 na quantidade de notificações - Badge
$socios 	= explode(';', $IDs);
$arr_length = count($socios); 
for($i=0; $i < ($arr_length - 1); $i++) 
{ 
	$id = json_decode(api('notificacaoqtde/find?socio='.$socios[$i]));
	if($id[0]->id > 0){
		$data = array('qtde' => $id[0]->qtde + 1);
		var_dump(api('notificacaoqtde/update/'.$id[0]->id , $data));
	}else{
    	$data = array('socio' => $socios[$i], 'qtde' => '1');
    	var_dump(api('notificacaoqtde/create', $data));
    }
}

echo true;