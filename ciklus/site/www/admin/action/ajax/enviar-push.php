<?PHP
include('../../includes/settings.php');

$IDs 		= $_POST['ids'];
$titulo 	= $_POST['titulo'];
$mensagem 	= $_POST['mensagem'];
$empresa 	= $_POST['empresa'];

$OneSignal 	= '';


//Busca os OneSignal de todos os associados caso não tenha sido passado nenhum
if($IDs == ''){
	$todos = json_decode(api('socio/buscarTodosApp/empresa=' . $empresa));
	$arr_length = count($todos);
	for($i=0; $i < $arr_length; $i++)
	{
		if($todos[$i]->onesignalId != ''){
			$temp = explode(';', $todos[$i]->onesignalId);
			if($OneSignal == ''){
				$OneSignal = $temp[0];
			}else{
				$OneSignal = $OneSignal . '","' . $temp[0];
			};
		}
	}

//Busca os OneSignal dos associados que foram passados no campo de envio
}else{
	$socios 	= explode(';', $IDs);
	$arr_length = count($socios); 
	for($i=0; $i < ($arr_length - 1); $i++) 
	{ 
		$socio = json_decode(api('socio/'.$socios[$i]));
		if($socio->onesignalId != ''){
			$temp = explode(';', $socio->onesignalId);
			if($OneSignal == ''){
				$OneSignal = $temp[0];
			}else{
				$OneSignal = $OneSignal . '","' . $temp[0];
			};
		}
	}	
};


$headings = array(
	"en" => $titulo
	);

$content = array(
	"en" => $mensagem
	);

$fields = array(
	// 'app_id' => "f8b8d4f7-d32d-4abc-92d1-d3a707f9a760", // CIKLUS
	'app_id' => "a7783d58-2788-4b59-95a5-638a220badea", // Clube Alô Ingressos
	// 'include_player_ids' => array("6392d91a-b206-4b7b-a620-cd68e32c3a76","76ece62b-bcfe-468c-8a78-839aeaa8c5fa","8e0f21fa-9a5a-4ae7-a9a6-ca1f24294b86"),
	// 'include_player_ids' => array($id),
	'include_player_ids' => array($OneSignal),
	'data' => array("foo" => "bar"),
	'android_accent_color' => "EA1F2D",
	'ios_badgeType' => 'Increase',
	'ios_badgeCount' => 1,
	'headings' => $headings,
	'contents' => $content
);


$fields = json_encode($fields);
$fields = str_replace('\"', '"', $fields);
print("\nJSON sent:\n");
print($fields);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
										   'Authorization: Basic MTkzYjQzZDMtY2M1Zi00YmM4LWI1N2UtN2YyODllNTMxZGFm'));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_HEADER, FALSE);
curl_setopt($ch, CURLOPT_POST, TRUE);
curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

$response = curl_exec($ch);
curl_close($ch);

return $response;

$response = sendMessage();
$return["allresponses"] = $response;
$return = json_encode( $return);

print("\n\nJSON received:\n");
print($return);
print("\n");

echo true;