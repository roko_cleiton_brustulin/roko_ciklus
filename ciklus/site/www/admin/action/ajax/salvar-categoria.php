<?php
include('../../includes/settings.php');

$data = array();

if ($_POST['categoriaPai'] > 0) {
	$data = array('nome' => $_POST['nome'],
			'categoriaPai' => $_POST['categoriaPai'],
			'status' => 1);
} else {
	$data = array('nome' => $_POST['nome'], 'status' => 1);
}

if ($_POST['id'] > 0) {
	echo api('categoria/update/' . $_POST['id'], $data);	
} else {
	echo api('categoria/create', $data);
}