<?php
include('../../includes/settings.php');

$data = array('frequencia' => $_POST['frequencia'],
				  'tipo' => $_POST['tipo'],
				  'ativo' => $_POST['ativo'],
				  'preco' => substr($_POST['preco'], 2),
				  'titulo' => $_POST['titulo']);

if ($_POST['id'] > 0) {
	echo api('plano/update/' + $_POST['id'], $data);
} else {
	echo api('plano/create', $data);
}