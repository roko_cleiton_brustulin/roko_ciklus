<?php
include('../../includes/settings.php');

$dt = explode('/', $_POST['data']);
$dt = $dt[2] . '-' . $dt[1] . '-' . $dt[0];

$data = array('titulo' 		=> $_POST['titulo'],
			  	'politica' 	=> $_POST['politica'],
			  	'data' 		=> $dt,
			  	'desconto' 	=> $_POST['desconto'],
			  	'categoria' => $_POST['categoria'],
			  	'cidade' 	=> $_POST['cidade'],
				'estado' 	=> $_POST['estado'],
				'local' 	=> $_POST['local'],
				'link' 		=> $_POST['link'],
				'empresa' 	=> $_POST['empresa']);

if ($_POST['id'] > 0) {
	echo api('evento/update/'.$_POST['id'], $data);
} else {
	echo api('evento/create', $data);
}