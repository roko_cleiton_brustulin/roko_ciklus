<?php
include('../../includes/settings.php');

$estabelecimento = $_POST['estabelecimento'];
$id 			 = $_POST['id'];
$titulo			 = $_POST['titulo'];
$politica		 = $_POST['politica'];
$socioMais		 = $_POST['socioMais'];
$segunda		 = $_POST['segunda'];
$terca			 = $_POST['terca'];
$quarta			 = $_POST['quarta'];
$quinta			 = $_POST['quinta'];
$sexta			 = $_POST['sexta'];
$sabado			 = $_POST['sabado'];
$domingo		 = $_POST['domingo'];
$horario1		 = $_POST['horario1'];
$horario2		 = $_POST['horario2'];
$horario3		 = $_POST['horario3'];
$horario4		 = $_POST['horario4'];
$desconto		 = $_POST['desconto'];
$validade		 = $_POST['validade'];
$destaque 		 = $_POST['destaque'];
if ($validade) {
	$validade = explode("/", $validade);
	$validade = $validade[2] . "-" . $validade[1] . "-" . $validade[0];
}
$status			 = $_POST['status'];

$data = array(
			'estabelecimento' => $estabelecimento,
			'titulo' => $titulo,
			'politica' => $politica,
			'socioMais' => $socioMais,
			'segundaFeira' => $segunda,
			'tercaFeira' => $terca,
			'quartaFeira' => $quarta,
			'quintaFeira' => $quinta,
			'sextaFeira' => $sexta,
			'sabado' => $sabado,
			'domingo' => $domingo,
			'horario1' => $horario1,
			'horario2' => $horario2,
			'horario3' => $horario3,
			'horario4' => $horario4,
			'desconto' => $desconto,
			'status' => $status,
			'destaque' => $destaque
			);

if ($validade) {
	$data['validade'] = $validade;
}

if ($id > 0) {
	$promocao = json_decode(api('promocao/update/'.$id, $data));
} else {
	$promocao = json_decode(api('promocao/create', $data));
}

echo json_encode($promocao);