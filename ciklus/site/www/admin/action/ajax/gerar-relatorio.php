<?php
include('../../includes/settings.php');

$inicio = explode('/', $_POST['inicio']);
$fim = explode('/', $_POST['fim']);
$inicio = $inicio[2] . '-' . $inicio[1] . '-' . $inicio[0];
$fim = $fim[2] . '-' . $fim[1] . '-' . $fim[0];
$categoria = $_POST['categoria'];

$sqlRelatorio = "SELECT COUNT(*) AS num_transacoes, SUM(valor) AS total_vendas, SUM(\"valorDesconto\") AS total_economizado, AVG(desconto)  AS media_desconto, COUNT(DISTINCT socio) AS socios
FROM transacao t
INNER JOIN estabelecimento e ON t.estabelecimento = e.id
WHERE t.\"createdAt\" BETWEEN '$inicio 02:00' AND '$fim 02:00'";

if ($categoria > 0) {
	$sqlRelatorio .= " AND e.categoria = " . $categoria;
}

$query = json_decode(api('estabelecimento/fullSearch?where='.urlencode($sqlRelatorio)));

$rows = null;
if (isset($query->rows)) {
	$resultado = $query->rows[0]->json_agg;
	echo json_encode($resultado);
} else {
	echo "null";
}