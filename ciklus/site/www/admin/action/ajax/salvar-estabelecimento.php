﻿<?php
include('../../includes/settings.php');

$id 		= $_POST['id'];
$nome 		= $_POST['nome'];
$endereco 	= $_POST['endereco'];
$bairro 	= $_POST['bairro'];
$cidade 	= $_POST['cidade'];
$cep 		= $_POST['cep'];
$telefone 	= $_POST['telefone'];
$email 		= $_POST['email'];
$site 		= $_POST['site'];
$fb 		= $_POST['facebook'];
$descricao 	= $_POST['descricao'];
$categoria 	= $_POST['categoria'];
$urlMaps    = $_POST['url'];
$login 		= $_POST['login'];
$status 	= $_POST['status'];
$senha 	 	= md5($_POST['senha']);
$numero     = $_POST['numero'];
$latitude 	= $_POST['latitude'];
$longitude 	= $_POST['longitude'];
$metaDesc	= $_POST['metaDescricao'];
$metaTit	= $_POST['metaTitulo'];

$nome = str_replace('+', '%2B', $nome);

$srcMaps = $urlMaps;

$data = array(
			"nome" => $nome,
			"endereco" => $endereco,
			"bairro" => $bairro,
			"cidade" => $cidade,
			"cep" => $cep,
			"telefone" => $telefone,
			"email" => $email,
			"site" => $site,
			"facebook" => $fb,
			"descricao" => utf8_encode($descricao),
			"categoria" => $categoria,
			"urlMaps" => $srcMaps,
			"login" => $login,
			"senha" => $senha,
			"status" => $status,
			"numero" => $numero,
			"latitude" => $latitude,
			"longitude" => $longitude,
			"metaDescricao" => utf8_encode($metaDesc),
			"metaTitulo" => htmlspecialchars($metaTit)
		);
if ($id > 0) {
	$estabelecimento = json_decode(api('estabelecimento/update/'.$id, $data));
} else {
	$estabelecimento = json_decode(api('estabelecimento/create', $data));
}

echo json_encode($estabelecimento);