<?php

$sql = "SELECT e.*, c.nome AS categorianome
FROM estabelecimento e
LEFT JOIN categoria c ON c.id = e.categoria";
$query = json_decode(api('estabelecimento/fullSearch?where='.urlencode($sql)));

$rows = null;
if (isset($query->rows)) {
	$rows = $query->rows[0]->json_agg;
}