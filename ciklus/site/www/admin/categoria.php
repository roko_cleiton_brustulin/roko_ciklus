<?php 
include('includes/settings.php');
include('action/categoria.php'); 
include('widgets/header.php'); 
?>

<div class="page">
  <div class="page-content">
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">Ações</h3>
      </div>
      <div class="panel-body container-fluid">
        <a href="categorias.php" class="btn btn-default" id="btn-voltar">Voltar</a>
        <button class="btn btn-success" id="btn-salvar" type="submit">Salvar</button>
      </div>
    </div>
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">Cadastrar Categoria</h3>
      </div>
      <div class="panel-body container-fluid">
        <div class="row row-lg">
          <input type="hidden" value="<?php echo @$categoria->id; ?>" id="categoriaid" />
          <div class="col-sm-6 col-md-6" style="margin-bottom: 15px;">  
            <h4 class="example-title">Categoria Pai</h4>
            <select class="form-control" id="categoriaPai" name="categoriaPai">
              <option>Selecione</option>
              <?php foreach ($categorias as $cat) { ?>
              <option value="<?php echo $cat->id; ?>" <?php if (@$categoria->categoriaPai == $cat->id) : echo "selected"; endif; ?>><?php echo $cat->nome ?></option>
              <?php } ?>
            </select>
          </div>

          <div class="col-sm-6 col-md-6" style="margin-bottom: 15px;">  
            <h4 class="example-title">Nome Categoria</h4>
            <input type="text" id="nome" class="form-control" value="<?php echo @$categoria->nome; ?>" />
          </div>

        </div>
      </div>
    </div>
    <?php if (!(@$categoria->id > 0)) { ?>
    <style>
      .edit-mode {
        display: none;
      }
    </style>
    <?php } ?>
    <div class="panel edit-mode">
      <div class="panel-heading">
        <h3 class="panel-title">Imagem</h3>
      </div>
      <div class="panel-body container-fluid">
        <form action="categoria.php" method="POST" enctype="multipart/form-data">
          <input type="hidden" value="<?php echo @$categoria->id; ?>" name="id" id="categoriaidimagem" />
          <div class="row">
            <div class="col-sm-6" style="margin-bottom: 10px;">
              <input type="file" class="form-control" name="imagem-upload" id="imagem-upload" />
              <input type="hidden" id="imagem-upload-base" />
            </div>
            <div class="col-sm-6" style="margin-bottom: 10px;">
              <input type="submit" class="btn btn-primary" value="Salvar" />
              <!-- <button class="btn btn-primary" id="btn-salvar-imagem" type="button">Salvar</button> -->
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="row" id="imagens" <?php echo !isset($imagemCategoria) ? 'style="display: none;' : null ?>">
      <div class="col-md-12">
          <img src="<?php echo isset($imagemCategoria) ? $uploaddir . $imagemCategoria->id . "." . $imagemCategoria->extensao : null ?>" style="width:50%;">
      </div>
    </div>

  </div>
</div>
<?php 
$script = "<script type='text/javascript' src='lib/categoria.js'></script>";
include('widgets/footer.php'); ?>