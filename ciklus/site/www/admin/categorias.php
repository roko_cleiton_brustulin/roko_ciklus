<?php 
include('includes/settings.php');
include('action/categorias.php');
include('widgets/header.php');
?>

<div class="page">
  	<div class="page-content">
    	<div class="panel">
      		<div class="panel-heading">
        		<h3 class="panel-title">Ações</h3>
      		</div>
      		<div class="panel-body container-fluid">
        		<a class="btn btn-success" href="categoria.php" type="button">Cadastrar Categoria</a><br /><br />
            <h5>Filtros</h5>
            <a class="btn btn-default" href="categorias.php">Todos</a>
            <a class="btn btn-default" href="categorias.php?status=1">Apenas Ativas</a>
            <a class="btn btn-default" href="categorias.php?status=0">Apenas Inativas</a>
      		</div>
    	</div>
    	<!-- Panel Form Elements -->
    	<div class="panel">
	      <div class="panel-heading">
	        <h3 class="panel-title">Lista de Categorias</h3>
	      </div>
	      <div class="panel-body container-fluid">
	      	<table class="table table-hover dataTable table-striped width-full" id="tbl-estabelecimentos">
	            <thead>
	              <tr>
	                <th>Nome</th>
                  <th>Categoria Pai</th>
                  <th>Ação</th>
	              </tr>
	            </thead>
	            <tbody>
            	<?php 
              foreach (@$rows as $row) { ?>
            		<tr>
            			<td><a href="categoria.php?id=<?php echo $row->id; ?>"><?php echo $row->nome; ?></a></td>
                  <td><a href="categoria.php?id=<?php echo $row->idpai; ?>"><?php echo $row->nomepai; ?></a></td>
                  <?php 
                      if ($row->status == 1) : ?>
                  <td><a href="categorias.php?id=<?php echo $row->id; ?>&desativar=true" onclick="confirm('Tem certeza que deseja desativar essa categoria? Todas as categorias filho serão desativadas também.')">Desativar</a></td>
                <?php else: ?>
                  <td><a href="categorias.php?id=<?php echo $row->id; ?>&ativar=true" onclick="confirm('Tem certeza que deseja ativar essa categoria? Todas as categorias filho serão ativadas também.')">Ativar</a></td>
                <?php endif; ?>
            		</tr>
            	<?php } ?>
	            </tbody>
        	</table>
	      </div>
  		</div>
	</div>
</div>

<?php 
$script = "<script type='text/javascript' src='lib/estabelecimentos.js'></script>";
include('widgets/footer.php');
