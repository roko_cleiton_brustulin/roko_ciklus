<?php 
include('includes/settings.php');
include('action/associados.php');
include('widgets/header.php');
?>

<div class="page">
  	<div class="page-content">
    	<div class="panel">
      		<div class="panel-heading">
        		<h3 class="panel-title">Ações</h3>
      		</div>
      		<div class="panel-body container-fluid">
        		<a class="btn btn-success" id="btn-novo" href="associado.php">Cadastrar Associado</a>
            <a class="btn btn-default" id="btn-csv" href="associados_csv.php" target="__blank">Exportar CSV</a>
      		</div>
    	</div>
    	<!-- Panel Form Elements -->
    	<div class="panel">
	      <div class="panel-heading">
	        <h3 class="panel-title">Lista de Associados</h3>
	      </div>
	      <div class="panel-body container-fluid">
	      	<table class="table table-hover dataTable table-striped width-full" id="tbl-associados">
	            <thead>
	              <tr>
	                <th>Nome</th>
	                <th>CPF</th>
	                <th>Telefone</th>
	                <th>E-mail</th>
                  <th>Aquisição</th>
                  <th>Vencimento</th>
                  <th>Status</th>
                  <th>Pagamento</th>
	              </tr>
	            </thead>
	            <tbody>
	            	<?php 
	            	foreach (@$rows as $row) { ?>
            		<tr>
            			<td><a href="associado.php?id=<?php echo $row->id; ?>"><?php echo $row->nome; ?></a></td>
            			<td><a href="associado.php?id=<?php echo $row->id; ?>"><?php echo $row->cpf; ?></a></td>
            			<td><a href="associado.php?id=<?php echo $row->id; ?>"><?php echo $row->celular; ?></a></td>
            			<td><a href="associado.php?id=<?php echo $row->id; ?>"><?php echo $row->email; ?></a></td>
                  <td><a href="associado.php?id=<?php echo $row->id; ?>"><?php echo substr($row->createdAt, 0, 10); ?></a></td>
                  <td></td>
                  <td></td>
                  <td></td>
            		</tr>
            		<?php } ?>
	            </tbody>
        	</table>
	      </div>
  		</div>
	</div>
</div>

<?php 
$script = "<script type='text/javascript' src='lib/associados.js'></script>";
include('widgets/footer.php');
