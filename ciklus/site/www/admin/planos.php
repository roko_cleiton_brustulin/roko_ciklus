<?php 
include('includes/settings.php');
include('action/planos.php');
include('widgets/header.php');
?>

<div class="page">
  	<div class="page-content">
    	<div class="panel">
      		<div class="panel-heading">
        		<h3 class="panel-title">Ações</h3>
      		</div>
      		<div class="panel-body container-fluid">
        		<a href="plano.php" class="btn btn-success">Cadastrar Plano</a>
      		</div>
    	</div>
    	<!-- Panel Form Elements -->
    	<div class="panel">
	      <div class="panel-heading">
	        <h3 class="panel-title">Lista de Planos</h3>
	      </div>
	      <div class="panel-body container-fluid">
	      	<table class="table table-hover dataTable table-striped width-full" id="tbl-planos">
	            <thead>
	              <tr>
	                <th>Título</th>
	                <th>Preço</th>
	                <th>Frequência</th>
	                <th>Ativo</th>
	              </tr>
	            </thead>
	            <tbody>
            	<?php 
              foreach (@$rows as $row) { ?>
            		<tr>
            			<td><a href="plano.php?id=<?php echo $row->id; ?>"><?php echo $row->titulo; ?></a></td>
            			<td><a href="plano.php?id=<?php echo $row->id; ?>"><?php echo $row->preco; ?></a></td>
            			<td><a href="plano.php?id=<?php echo $row->id; ?>"><?php echo $row->frequencia . " dias"; ?></a></td>
            			<td><a href="plano.php?id=<?php echo $row->id; ?>"><?php if ($row->ativo) : echo "Sim"; else : echo "Não"; endif; ?></a></td>
            		</tr>
            	<?php } ?>
	            </tbody>
        	</table>
	      </div>
  		</div>
	</div>
</div>

<?php 
$script = "<script type='text/javascript' src='lib/planos.js'></script>";
include('widgets/footer.php');
