<?php
if (isset($_SERVER['HTTP_COOKIE'])) {
    $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
    foreach($cookies as $cookie) {
        $parts = explode('=', $cookie);
        $name = trim($parts[0]);
        setcookie($name, '', time()-1000);
        setcookie($name, '', time()-1000, '/');
    }
}
?>
<!DOCTYPE html>
<html class="no-js before-run" lang="en">

<!-- Mirrored from getbootstrapadmin.com/remark/html/pages/lockscreen.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 26 Jul 2015 05:48:35 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">

  <title>Ciklus</title>

  <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="http://getbootstrapadmin.com/remark/assets/images/favicon.ico">

  <!-- Stylesheets -->
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/bootstrap-extend.min.css">
  <link rel="stylesheet" href="assets/css/site.min.css">

  <link rel="stylesheet" href="assets/vendor/animsition/animsition.min.css">
  <link rel="stylesheet" href="assets/vendor/asscrollable/asScrollable.min.css">
  <link rel="stylesheet" href="assets/vendor/switchery/switchery.min.css">
  <link rel="stylesheet" href="assets/vendor/intro-js/introjs.min.css">
  <link rel="stylesheet" href="assets/vendor/slidepanel/slidePanel.min.css">
  <link rel="stylesheet" href="assets/vendor/flag-icon-css/flag-icon.min.css">


  <!-- Page -->
  <link rel="stylesheet" href="assets/css/pages/lockscreen.min.css">

  <!-- Fonts -->
  <link rel="stylesheet" href="assets/fonts/web-icons/web-icons.min.css">
  <link rel="stylesheet" href="assets/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>


  <!--[if lt IE 9]>
    <script src="assets/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->

  <!--[if lt IE 10]>
    <script src="assets/vendor/media-match/media.match.min.js"></script>
    <script src="assets/vendor/respond/respond.min.js"></script>
    <![endif]-->

  <!-- Scripts -->
  <script src="assets/vendor/modernizr/modernizr.min.js"></script>
  <script src="assets/vendor/breakpoints/breakpoints.min.js"></script>
  <script>
    Breakpoints();
  </script>
</head>
<body class="page-locked layout-full">
  <style>
  .page-locked:after {
    background-color: rgba(214,11,82,0.5) !important;
  }
  </style>
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->


  <!-- Page -->
  <div class="page animsition vertical-align text-center" data-animsition-in="fade-in"
  data-animsition-out="fade-out" style="">
    <div class="page-content vertical-align-middle">
      <img src="http://cikluslive.com.br/assets/images/logo.png" alt="..." style="width: 400px;">
      <form style="margin-top: 20px;">
        <div class="input-group">
          <input type="password" class="form-control last" id="inputPassword" name="password"
          placeholder="Digite sua senha">
          <span class="input-group-btn">
            <button id="logar" class="btn btn-primary"><i class="icon wb-unlock" aria-hidden="true"></i>
              <span class="sr-only">unLock</span>
            </button>
          </span>
        </div>
      </form>
    </div>
  </div>
  <!-- End Page -->


  <!-- Core  -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/bootstrap.min.js"></script>
  <script src="assets/vendor/animsition/jquery.animsition.min.js"></script>
  <script src="assets/vendor/asscroll/jquery-asScroll.min.js"></script>
  <script src="assets/vendor/mousewheel/jquery.mousewheel.min.js"></script>
  <script src="assets/vendor/asscrollable/jquery.asScrollable.all.min.js"></script>
  <script src="assets/vendor/ashoverscroll/jquery-asHoverScroll.min.js"></script>

  <!-- Plugins -->
  <script src="assets/vendor/switchery/switchery.min.js"></script>
  <script src="assets/vendor/intro-js/intro.min.js"></script>
  <script src="assets/vendor/screenfull/screenfull.js"></script>
  <script src="assets/vendor/slidepanel/jquery-slidePanel.min.js"></script>

  <script src="assets/vendor/jquery-placeholder/jquery.placeholder.min.js"></script>

  <!-- Scripts -->
  <script src="assets/js/core.min.js"></script>
  <script src="assets/js/site.min.js"></script>

  <script src="assets/js/sections/menu.min.js"></script>
  <script src="assets/js/sections/menubar.min.js"></script>
  <script src="assets/js/sections/sidebar.min.js"></script>

  <script src="assets/js/configs/config-colors.min.js"></script>
  <script src="assets/js/configs/config-tour.min.js"></script>

  <script src="assets/js/components/asscrollable.min.js"></script>
  <script src="assets/js/components/animsition.min.js"></script>
  <script src="assets/js/components/slidepanel.min.js"></script>
  <script src="assets/js/components/switchery.min.js"></script>
  <script src="assets/js/components/jquery-placeholder.min.js"></script>

  <script>
    (function(document, window, $) {
      'use strict';

      var Site = window.Site;
      $(document).ready(function() {
        Site.run();
      });

      $("#logar").click(function(e) {
        e.preventDefault();
        var pass = $("#inputPassword").val();
        if (pass.length) {
          $.ajax({
            url: 'action/login.php',
            data: { password: pass },
            type: 'POST',
            success:function (res) {
              if (res == 1) {
                document.cookie = "admin-ciklus=RokoAdmin;";
                window.location="index.php";
              } else {
                alert('Senha inválida.');
              }
            }
          });
        } else {
          alert('Preencha a senha.');
        }

        return false;
      });

    })(document, window, jQuery);
  </script>
</body>
</html>