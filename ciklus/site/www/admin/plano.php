<?php 
include('includes/settings.php');
include('action/plano.php'); 
include('widgets/header.php'); 
?>

  <div class="page">
    <div class="page-content">
      <div class="panel">
        <div class="panel-heading">
          <h3 class="panel-title">Ações</h3>
        </div>
        <div class="panel-body container-fluid">
          <a href="planos.php" class="btn btn-default" id="btn-voltar">Voltar</a>
          <button class="btn btn-success" id="btn-salvar" type="submit">Salvar Plano</button>
        </div>
      </div>
      <div class="panel">
        <div class="panel-heading">
          <h3 class="panel-title">Cadastro do Plano</h3>
        </div>
        <div class="panel-body container-fluid">
          <div class="row row-lg">
            <input type="hidden" value="<?php echo @$plano->id; ?>" id="planoid" />
            <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">  
              <h4 class="example-title">Frequência</h4>
              <select class="form-control" id="frequencia" name="tipo">
                <option>Selecione</option>
                <option value="15" <?php if (@$plano->frequencia == '15') : echo "selected"; endif; ?>>15 dias</option>
                <option value="30" <?php if (@$plano->frequencia == '30') : echo "selected"; endif; ?>>Mensal</option>
                <option value="60" <?php if (@$plano->frequencia == '60') : echo "selected"; endif; ?>>Bimestral</option>
                <option value="90" <?php if (@$plano->frequencia == '90') : echo "selected"; endif; ?>>Trimestral</option>
                <option value="180" <?php if (@$plano->frequencia == '180') : echo "selected"; endif; ?>>Semestral</option>
                <option value="365" <?php if (@$plano->frequencia == '365') : echo "selected"; endif; ?>>Anual</option>
              </select>
            </div>

            <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">  
              <h4 class="example-title">Tipo</h4>
              <select class="form-control" id="tipo" name="tipo">
                <option>Selecione</option>
                <option value="n" <?php if (@$plano->tipo == 'n') : echo "selected"; endif; ?>>Normal</option>
                <option value="p" <?php if (@$plano->tipo == 'p') : echo "selected"; endif; ?>>Promoção</option>
              </select>
            </div>

            <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">  
              <h4 class="example-title">Preço</h4>
              <input type="text" class="form-control" name="preco" id="preco" value="<?php echo @$plano->preco; ?>"  placeholder="Preço">
            </div>

            <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">  
              <h4 class="example-title">Título</h4>
              <input type="text" class="form-control" id="titulo" name="titulo" value="<?php echo @$plano->titulo; ?>"  placeholder="Nome do plano que aparecerá no site">
            </div>

            <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">  
              <h4 class="example-title">Ativo</h4>
              <select class="form-control" id="ativo" name="ativo">
                <option value="true" <?php if (@$plano->ativo == true) : echo "selected"; endif; ?>>Ativo</option>
                <option value="false" <?php if (@$plano->tipo == false) : echo "selected"; endif; ?>>Inativo</option>
              </select>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>

<?php 
$script = "<script type='text/javascript' src='lib/plano.js'></script>";
include('widgets/footer.php'); ?>