<?php 
include('includes/settings.php');
include('action/estabelecimentos.php');
include('widgets/header.php');
?>

<div class="page">
  	<div class="page-content">
    	<div class="panel">
      		<div class="panel-heading">
        		<h3 class="panel-title">Ações</h3>
      		</div>
      		<div class="panel-body container-fluid">
        		<button class="btn btn-success" id="btn-novo" type="button">Cadastrar Estabelecimento</button>
            <a class="btn btn-warning" href="estabelecimentos_csv.php" target="__blank">Exportar CSV</a><br /><br />
            <h5>Filtros</h5>
            <a class="btn btn-default" href="estabelecimentos.php">Todos</a>
            <a class="btn btn-default" href="estabelecimentos.php?status=a">Apenas Ativos</a>
            <a class="btn btn-default" href="estabelecimentos.php?status=i">Apenas Inativos</a>
            <a class="btn btn-default" href="estabelecimentos.php?status=p">Apenas Pré-Cadastro</a>
      		</div>
    	</div>
    	<!-- Panel Form Elements -->
    	<div class="panel">
	      <div class="panel-heading">
	        <h3 class="panel-title">Lista de Estabelecimento</h3>
	      </div>
	      <div class="panel-body container-fluid">
	      	<table class="table table-hover dataTable table-striped width-full" id="tbl-estabelecimentos">
	            <thead>
	              <tr>
	                <th>Nome</th>
	                <th>Categoria</th>
	                <th>Bairro</th>
	                <th>Cidade</th>
	              </tr>
	            </thead>
	            <tbody>
            	<?php 
              if (count($rows) > 0) {
                foreach (@$rows as $row) { ?>
            		<tr>
            			<td><a href="estabelecimento.php?id=<?php echo $row->id; ?>"><?php echo $row->nome; ?></a></td>
            			<td><a href="estabelecimento.php?id=<?php echo $row->id; ?>"><?php echo $row->categoria; ?></a></td>
            			<td><a href="estabelecimento.php?id=<?php echo $row->id; ?>"><?php echo $row->bairro; ?></a></td>
            			<td><a href="estabelecimento.php?id=<?php echo $row->id; ?>"><?php echo $row->cidade; ?></a></td>
            		</tr>
            	<?php }
              } ?>
	            </tbody>
        	</table>
	      </div>
  		</div>
	</div>
</div>

<?php 
$script = "<script type='text/javascript' src='lib/estabelecimentos.js'></script>";
include('widgets/footer.php');
