<?php 
include('includes/settings.php');
include('action/relatorio_consumo.php');
include('widgets/header.php');
?>

<div class="page">
  	<div class="page-content">
    	<div class="panel">
      		<div class="panel-heading">
        		<h3 class="panel-title">Ações</h3>
      		</div>
      		<div class="panel-body container-fluid">
            <div class="row">
              <div class="col-lg-3 col-xs-12">
                <label>Início</label>
                <input type="text" class="form-control" id="inicio" placeholder="__/__/____" />
              </div>
              <div class="col-lg-3 col-xs-12">
                <label>Fim</label>
                <input type="text" class="form-control" id="fim" placeholder="__/__/____" />
              </div>
              <div class="col-lg-3 col-xs-12">
                <label>Categoria</label>
                <select class="form-control" id="categoria">
                  <option value="0">Todas</option>
                  <?php foreach ($categorias as $categoria) : ?>
                  <option value="<?php echo $categoria->id; ?>"><?php echo $categoria->nome; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <div class="col-lg-3 col-xs-12">
                <button class="btn btn-success" id="btn-relatorio" type="button" style="margin-top:25px">Gerar Relatório</button>
              </div>
            </div>
      		</div>
    	</div>
    	<!-- Panel Form Elements -->
    	<div class="panel" id="resultados" style="display:none;">
	      <div class="panel-heading text-center">
	        <h1 class="panel-title">Resultados</h1>
	      </div>
	      <div class="panel-body container-fluid">
          <div class="row">
  	      	<div class="col-lg-2 text-center">
              <h3 id="transacoes"></h3>
              <span>Transações</span>
            </div>
            <div class="col-lg-2 text-center">
              <h3 id="socios"></h3>
              <span>Sócios Únicos</span>
            </div>
            <div class="col-lg-2 text-center">
              <h3 id="mediatransacoes"></h3>
              <span>Transações x Sócios</span>
            </div>
            <div class="col-lg-2 text-center">
              <h3 id="vendas"></h3>
              <span>Vendas (Bruto)</span>
            </div>
            <div class="col-lg-2 text-center">
              <h3 id="economizado"></h3>
              <span>Economizado</span>
            </div>
            <div class="col-lg-2 text-center">
              <h3 id="media"></h3>
              <span>Média de Desconto</span>
            </div>
          </div>
	      </div>
  		</div>
	</div>
</div>

<?php 
$script = "<script type='text/javascript' src='lib/relatorio_consumo.js'></script>";
include('widgets/footer.php');
