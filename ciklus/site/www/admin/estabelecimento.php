﻿<?php 
include('includes/settings.php');
include('action/estabelecimento.php');
include('widgets/header.php');
?>
<div class="page">
  <div class="page-content">
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">Ações</h3>
      </div>
      <div class="panel-body container-fluid">
        <a href="estabelecimentos.php" class="btn btn-default" id="btn-voltar">Voltar</a>
        <button class="btn btn-success" id="btn-salvar" type="button">Salvar Estabelecimento</button>
      </div>
    </div>
    <!-- Panel Form Elements -->
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">Cadastro de Estabelecimento</h3>
      </div>
      <div class="panel-body container-fluid">
        <div class="row row-lg">
          <input type="hidden" id="hdnId" value="<?php echo $estabelecimento->id; ?>" />
          <div class="col-sm-12 col-md-12" style="margin-bottom: 15px;">  
            <h4 class="example-title">Status</h4>
            <select class="form-control" id="status">
              <option value="a" <?php if (@$estabelecimento->status == "a") : echo "selected"; endif; ?>>Ativo</option>
              <option value="p" <?php if (@$estabelecimento->status == "p") : echo "selected"; endif; ?>>Pré-cadastro</option>
              <option value="i" <?php if (@$estabelecimento->status == "i") : echo "selected"; endif; ?>>Inativo</option>
            </select>
          </div>

          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">  
            <h4 class="example-title">Nome</h4>
            <input type="text" class="form-control" id="nome" name="nome" value="<?php echo @$estabelecimento->nome; ?>"  placeholder="Nome do estabelecimento">
          </div>

          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">
            <h4 class="example-title">CEP</h4>
            <input type="text" class="form-control" id="cep" name="cep" value="<?php echo @$estabelecimento->cep; ?>" placeholder="CEP (99999-999)">
          </div>

          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">
            <h4 class="example-title">Endereço</h4>
            <input type="text" class="form-control" id="endereco" name="endereco" value="<?php echo @$estabelecimento->endereco; ?>" placeholder="Endereço">
          </div>

          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">
            <h4 class="example-title">Número e Complemento</h4>
            <input type="text" class="form-control" id="numero" name="numero" value="<?php echo @$estabelecimento->numero; ?>" placeholder="Nº, Complemento">
          </div>

          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">
            <h4 class="example-title">Bairro</h4>
            <input type="text" class="form-control" id="bairro" name="bairro" placeholder="Bairro" value="<?php echo @$estabelecimento->bairro; ?>">
          </div>

          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">
            <h4 class="example-title">Cidade</h4>
            <input type="text" class="form-control" id="cidade" name="cidade" value="<?php echo @$estabelecimento->cidade; ?>"  placeholder="Cidade">
          </div>

          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">
            <h4 class="example-title">Telefone</h4>
            <input type="text" class="form-control" id="telefone" name="telefone" value="<?php echo @$estabelecimento->telefone; ?>" placeholder="Telefone"  pattern="\([0-9]{2}\)[\s][0-9]{4}-[0-9]{4,5}">
          </div>

          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">
            <h4 class="example-title">E-mail do estabelecimento</h4>
            <input type="text" class="form-control" id="email" name="email" placeholder="E-mail" value="<?php echo @$estabelecimento->email; ?>">
          </div>

          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">
            <h4 class="example-title">Site</h4>
            <input type="text" class="form-control" id="site" name="website" placeholder="Site (http://...)" value="<?php echo @$estabelecimento->site; ?>">
          </div>

          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">
            <h4 class="example-title">Facebook</h4>
            <input type="text" class="form-control" id="facebook" name="facebook" placeholder="Endereço Facebook (http://...)" value="<?php echo @$estabelecimento->facebook; ?>">
          </div>

          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">
            <h4 class="example-title">URL Maps</h4>
            <input type="text" class="form-control" id="url-maps" name="url-maps" placeholder="Url do Google Maps" value="<?php echo @$estabelecimento->urlMaps; ?>">
          </div>

          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">
            <h4 class="example-title">Latitude</h4>
            <input type="text" class="form-control" id="latitude" name="url-maps" placeholder="Latitude" value="<?php echo @$estabelecimento->latitude; ?>">
          </div>

          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">
            <h4 class="example-title">Longitude</h4>
            <input type="text" class="form-control" id="longitude" name="url-maps" placeholder="Longitude" value="<?php echo @$estabelecimento->longitude; ?>">
          </div>

          <div class="col-sm-12 col-md-12" style="margin-bottom: 15px;">
            <h4 class="example-title">Descrição</h4>
            <textarea class="form-control" id="descricao" name="descricao" placeholder="Descrição do estabelecimento..." rows="3"><?php echo utf8_decode(@$estabelecimento->descricao); ?></textarea>
          </div>

          <div class="col-sm-6 col-md-4" style="margin-bottom: 15px;">  
            <h4 class="example-title">Meta título</h4>
            <input type="text" class="form-control" id="meta-titulo" name="meta-titulo" value="<?php echo @$estabelecimento->metaTitulo; ?>"  placeholder="Título (Meta tag)">
          </div>

          <div class="col-sm-12 col-md-12" style="margin-bottom: 15px;">
            <h4 class="example-title">Meta Descrição</h4>
            <textarea class="form-control" id="meta-descricao" name="meta-descricao" placeholder="Meta descrição..." rows="3"><?php echo utf8_decode(@$estabelecimento->metaDescricao); ?></textarea>
          </div>
          <div class="col-sm-12 col-md-12">
            <h4 class="example-title">Categoria</h4>
            <div class="form-group">
              <select class="form-control" id="categoria">
                <option value="0">Selecione</option>
                <?php 
                $optGroupCount = 0;
                $optGroup = "";
                foreach ($categorias as $categoria) { 
                  if ($optGroup != $categoria->nomePai) {
                    if ($optGroupCount == 0) {
                      echo "</optgroup>";
                    }
                    $optGroupCount++;
                    echo "<optgroup label='$categoria->nomePai'>";
                    $optGroup = $categoria->nomePai;
                  }
                ?>
                  <option <?php if (@$estabelecimento->categoria->id == $categoria->id) : echo "selected='selected'"; endif; ?> 
                    value="<?php echo $categoria->id; ?>">
                    <?php echo $categoria->nome; ?>
                  </option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="col-sm-6 col-md-4">
            <h4 class="example-title">Login</h4>
            <div class="form-group">
              <input type="text" class="form-control" id="login" name="login" placeholder="Login Estabelecimento" value="<?php echo @$estabelecimento->login; ?>">
            </div>
          </div>
          <div class="col-sm-6 col-md-4">
            <h4 class="example-title">Senha</h4>
            <div class="form-group">
              <input type="password" class="form-control" id="senha" name="senha" placeholder="Senha Estabelecimento" value="<?php echo @$estabelecimento->senha; ?>">
            </div>
          </div>
        </div>
        </div>
      </div>
      <?php if (!(@$estabelecimento->id > 0)) { ?>
      <style>
        .edit-mode {
          display: none;
        }
      </style>
      <?php } ?>
    <!-- End Panel Form Elements -->
      <div class="panel edit-mode">
        <div class="panel-heading">
          <h3 class="panel-title">Imagens</h3>
        </div>
        <div class="panel-body container-fluid">
          <form action="estabelecimento.php?id=<?php echo $id; ?>" method="POST" enctype="multipart/form-data">
            <div class="row">
              <div class="col-sm-6" style="margin-bottom: 10px;">
                <input type="file" class="form-control" name="imagem-upload" id="imagem-upload" />
                <input type="hidden" id="imagem-upload-base" />
              </div>
              <div class="col-sm-6" style="margin-bottom: 10px;">
                <input type="submit" class="btn btn-primary" value="Salvar" />
                <!-- <button class="btn btn-primary" id="btn-salvar-imagem" type="button">Salvar</button> -->
              </div>
            </div>
          </form>
          <br />
          <div class="row" id="imagens">
              <?php foreach ($estabelecimento->imagens as $imagem) { ?>
                <div class="col-md-4" id="img-<?php echo $imagem->id; ?>">
                  <img src="uploads/<?php echo $imagem->id . '.' . $imagem->extensao; ?>" style="width:150px;" />
                  <br />
                  <a href="#" class="excluir-img" id="excluir-link-<?php echo $imagem->id; ?>" imagem="<?php echo $imagem->id; ?>">Excluir Imagem</a>
                  <?php if ($imagem->ordem != 1 && $imagem->ordem != 2) : ?>
                  <br />
                  <a href="javascript:void(0)" class="capa-img" imagem="<?php echo $imagem->id; ?>">Tornar Capa</a>
                  <br />
                  <a href="javascript:void(0)" class="logo-img" imagem="<?php echo $imagem->id; ?>">Tornar Logo</a>
                  <?php else : 
                          if ($imagem->ordem == 1) : echo "<br><strong>Foto capa</strong>"; endif; 
                          if ($imagem->ordem == 2) : echo "<br><strong>Foto logo</strong>"; endif; ?>
                  <?php endif; ?>
                </div>
              <?php } ?>
          </div>
        </div>
      </div>
      <!-- Panel Input Groups -->
      <div class="panel edit-mode">
        <div class="panel-heading">
          <h3 class="panel-title">Promoções</h3>
        </div>
        <div class="panel-body container-fluid">
          <div class="col-sm-6" style="margin-bottom: 10px;">
            <button class="btn btn-default" id="adicionar-promocao" class="btn btn-outline btn-default" type="button">Adicionar Promoção</button>
          </div>
          <div class="col-sm-6 text-right" style="margin-bottom: 10px;">
            <button class="btn btn-default" id="todas-promocoes" class="btn btn-outline btn-default" type="button">Todas</button>
            <button class="btn btn-default" id="apenas-ativas" class="btn btn-outline btn-default" type="button">Ativas</button>
            <button class="btn btn-default" id="apenas-inativas" class="btn btn-outline btn-default" type="button">Inativas</button>
          </div>
          <table class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <th data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="persist">Título</th>
                <th data-tablesaw-sortable-col data-tablesaw-priority="3">Desconto</th>
                <th data-tablesaw-sortable-col data-tablesaw-priority="3">Status</th>
                <th data-tablesaw-sortable-col data-tablesaw-priority="3">Destaque</th>
                <th data-tablesaw-sortable-col data-tablesaw-priority="1">
                  <abbr title="Ações">Ações</abbr>
                </th>
              </tr>
            </thead>
            <tbody id="tbody-promocoes">
              <?php foreach ($estabelecimento->promocoes as $promocao) {
                $status = "Ativa";
                $destaque = "Sem destaque";
                if ($promocao->status == 0) {
                  $status = "Inativa";
                }
                if ($promocao->destaque == 'r') {
                  $destaque = "Randômico";
                } else if ($promocao->destaque == 'f') {
                  $destaque = "Fixo";
                }
                echo "<tr class='" . $status . "' id='tr-promocao-".$promocao->id."'><td><a href='#' ciklus-promocao='" . $promocao->id . "' class='abrir-promocao'>" . $promocao->titulo . "</a></td><td>" . $promocao->desconto . "%</td><td>".$status."</td><td>".$destaque."</td><td><a href='javascript:void(0);' class='excluir-promocao' ciklus-promocao='".$promocao->id."'>Excluir</a></td></tr>";
              } ?>
              <!-- <tr>
                <td><a href="javascript:void(0)">Desconto de 20% em todo o cardápio.</a></td>
                <td>1, 2, 3, 4, 5</td>
                <td>18h - 23h</td>
                <td><a href="#">Excluir</a></td>                
              </tr> -->
            </tbody>
          </table>
        </div>
      </div>
    <!-- End Panel Input Groups -->
    </div>
  </div>
</div>

<div class="modal fade modal-info" id="modal-promocao" aria-hidden="true" aria-labelledby="exampleModalInfo"
                  role="dialog" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title">Cadastro de Promoção</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <input type="hidden" id="promocao-id" value="0" />
          <div class="col-sm-12" style="margin-bottom: 15px;">  
            <h4 class="example-title">Título da Promoção</h4>
            <input type="text" class="form-control" id="promo-titulo" name="nome" value="" placeholder="Título">
          </div>
          <div class="col-sm-6" style="margin-bottom: 15px;">  
            <h4 class="example-title">Validade</h4>
            <input type="text" class="form-control" id="validade" name="validade" value="" placeholder="99/99/9999">
          </div>
          <div class="col-sm-6" style="margin-bottom: 15px;">  
            <h4 class="example-title">Status</h4>
            <select class="form-control" id="status-promocao">
              <option value="1">Ativo</option>
              <option value="0">Inativo</option>
            </select>
          </div>
          <div class="col-sm-12 col-md-12">
            <h4 class="example-title">Destaque</h4>
            <div class="form-group">
              <select class="form-control" id="destaque-promocao">
                <option value="null">Nenhum</option>
                <option value="r">Randômico</option>
                <option value="f">Fixo</option>
              </select>
            </div>
          </div>
          <div class="col-sm-12" style="margin-bottom: 15px;">  
            <h4 class="example-title">Política</h4>
            <textarea class="form-control" id="promo-politica" name="descricao" placeholder="Política da promoção..." rows="3"></textarea>            
          </div>
          <div class="col-sm-12" style="margin-bottom: 15px;">  
            <h4 class="example-title">Desconto (%)</h4>
            <input type="text" class="form-control" id="promo-desconto" name="nome" value="" placeholder="Desconto">
          </div>
          <div class="col-sm-12" style="margin-bottom: 15px;">  
            <h4 class="example-title">Sócio Ciklus + </h4>
            <select class="form-control" id="promo-socio-mais">
              <option value="0">Apenas Sócio</option>
              <option value="1">1 convidado</option>
              <option value="2">2 convidados</option>
              <option value="3">3 convidados</option>
              <option value="4">4 convidados</option>
              <option value="5">5 convidados</option>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6 col-lg-4">
            <!-- Example Checkboxes -->
            <h4 class="example-title">Dias válidos</h4>
            <div>
              <input type="checkbox" id="promo-segunda" />
              <label for="inputChecked">Segunda-feira</label>
            </div>
            <div>
              <input type="checkbox" id="promo-terca" />
              <label for="inputChecked">Terça-feira</label>
            </div>
            <div>
              <input type="checkbox" id="promo-quarta" />
              <label for="inputChecked">Quarta-feira</label>
            </div>
            <div>
              <input type="checkbox" id="promo-quinta" />
              <label for="inputChecked">Quinta-feira</label>
            </div>
            <div>
              <input type="checkbox" id="promo-sexta" />
              <label for="inputChecked">Sexta-feira</label>
            </div>
            <div>
              <input type="checkbox" id="promo-sabado" />
              <label for="inputChecked">Sábado</label>
            </div>
            <div>
              <input type="checkbox" id="promo-domingo" />
              <label for="inputChecked">Domingo</label>
            </div>
            <!-- End Example Checkboxes -->
        </div>
        <div class="col-sm-6 col-lg-4">
          <h4 class="example-title">Horários válidos</h4>
          <div class="row">
            <h5 class="example-title">Período 1</h5>
            <div class="col-sm-6">
              <input type="text" class="form-control" id="promo-horario1" placeholder="__:__" />
            </div>
            <div class="col-sm-6">
              <input type="text" class="form-control" id="promo-horario2" placeholder="__:__" />
            </div>
          </div>
          <br />
          <div class="row">
            <h5 class="example-title">Período 2</h5>
            <div class="col-sm-6">
              <input type="text" class="form-control" id="promo-horario3" placeholder="__:__" />
            </div>
            <div class="col-sm-6">
              <input type="text" class="form-control" id="promo-horario4" placeholder="__:__" />
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="btn-salvar-promocao">Salvar</button>
      </div>
    </div>
  </div>
</div>
<?php 
$script = "<script type='text/javascript' src='lib/estabelecimento.js'></script>";
include('widgets/footer.php'); ?>