<?php 
include('includes/settings.php');
include('action/spots.php');
include('widgets/header.php');
?>

<div class="page">
  	<div class="page-content">
    	<!-- Panel Form Elements -->
    	<div class="panel">
	      <div class="panel-heading">
	        <h3 class="panel-title">Lista de Planos</h3>
	      </div>
	      <div class="panel-body container-fluid">
	      	<table class="table table-hover dataTable table-striped width-full" id="tbl-planos">
	            <thead>
	              <tr>
	                <th>Spot</th>
	              </tr>
	            </thead>
	            <tbody>
            	<?php 
              foreach (@$rows as $row) { ?>
            		<tr>
            			<td><a href="spot.php?id=<?php echo $row->id; ?>"><?php echo $row->spot; ?></a></td>
            		</tr>
            	<?php } ?>
	            </tbody>
        	</table>
	      </div>
  		</div>
	</div>
</div>

<?php 
$script = "<script type='text/javascript' src='lib/estabelecimentos.js'></script>";
include('widgets/footer.php');
