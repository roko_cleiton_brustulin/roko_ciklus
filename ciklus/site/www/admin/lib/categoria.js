$(document).ready(function() {
	$("#btn-salvar").click(function(e) {
		e.preventDefault();
		var id   = $("#categoriaid").val();
		var cPai = $("#categoriaPai").val();
		var nome = $("#nome").val();

		$.ajax({
			url: 'action/ajax/salvar-categoria.php',
			data: { id: id, nome: nome, categoriaPai: cPai },
			type: 'POST',
			success: function(data) {
				console.log(data);
				data = JSON.parse(data);
				$("#categoriaid").val(data.id);
				swal("Eba!", data.nome + " foi salvo com sucesso!", "success");
			}
		});

	});
});