﻿$(document).ready(function() {

	var SPMaskBehavior = function (val) {
		return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	},
	spOptions = {
		onKeyPress: function(val, e, field, options) {
			field.mask(SPMaskBehavior.apply({}, arguments), options);
		}
	};

	$(".Ativa").show();
	$(".Inativa").hide();

	$("#cep").mask("99999-999");
	$("#telefone").mask(SPMaskBehavior, spOptions);
	$("#promo-horario1").mask("99:99");
	$("#promo-horario2").mask("99:99");
	$("#promo-horario3").mask("99:99");
	$("#promo-horario4").mask("99:99");
	//$("#promo-desconto").mask("9?99");
	$('#validade').datepicker({
		format: 'dd/mm/yyyy'
	});

	$(".excluir-img").click(function(e) {
		e.preventDefault();
		if (confirm('Confirma exclusão?')) {
			var id = $(this).attr("imagem");
			$.ajax({
				url: 'action/ajax/excluir-imagem.php',
				data: { id: id },
				type: 'POST',
				success: function(data) {
					$("#img-" + id).hide();
					swal('Foto excluída.');				
				}
			});
		}
	});

	$(".capa-img").click(function(e) {
		e.preventDefault();
		var id = $(this).attr('imagem');
		var est = $("#hdnId").val();
		$.ajax({
			url: 'action/ajax/atualizar-imagem.php',
			data: { tipo: 'capa', id: id, estabelecimento: est },
			type: 'POST',
			success: function(data) {
				window.location = 'estabelecimento.php?id=' + est;
			}
		});
	});

	$(".logo-img").click(function(e) {
		e.preventDefault();
		var id = $(this).attr('imagem');
		var est = $("#hdnId").val();
		$.ajax({
			url: 'action/ajax/atualizar-imagem.php',
			data: { tipo: 'logo', id: id, estabelecimento: est },
			type: 'POST',
			success: function(data) {
				window.location = 'estabelecimento.php?id=' + est;
			}
		});
	});

	$("#adicionar-promocao").click(function() {
		$("#promocao-id").val("0");
		$("#promo-titulo").val('');
		$("#promo-politica").val('');
		$("#promo-socio-mais").val("0");
		$("#promo-horario1").val('');
		$("#promo-horario2").val('');
		$("#promo-horario3").val('');
		$("#promo-horario4").val('');
		$("#validade").val('');
		$("#status-promocao").val(1);
		$("#modal-promocao").modal();
		return false;
	});

	$("#cep").blur(function() {
		if ($("#cep").val()) {
			var cep = $("#cep").val().replace("-", "");
			$.ajax({
				url: 'https://viacep.com.br/ws/' + cep + '/json/ ',
				type: 'GET',
				success: function(data) {
					$("#endereco").val(data.logradouro);
					$("#bairro").val(data.bairro);
					$("#cidade").val(data.localidade);
				}
			});
		}
	});

	$(".excluir-promocao").click(function(){
		if (confirm('Deseja realmente excluir essa promoção?')) {
			var id = $(this).attr('ciklus-promocao');
			$.ajax({
				url: 'action/ajax/excluir-promocao.php',
				data: { promocao: id },
				type: 'POST',
				success: function(data) {
					$("#tr-promocao-"+id).hide();
				}
			});
		}
	});

	function abrirPromocao(id) {
		$.ajax({
			url: 'action/ajax/buscar-promocao.php',
			data: { promocao: id },
			type: 'POST',
			success: function(data) {
				data = $.parseJSON(data);
				$("#promocao-id").val(data['id']);
				$("#promo-titulo").val(data['titulo']);
				$("#promo-politica").val(data['politica']);
				$("#promo-desconto").val(data['desconto']);
				$("#promo-socio-mais").val(data['socioMais']);
				$("#destaque-promocao").val(data['destaque']);
				if (data['segundaFeira'])
					$("#promo-segunda").prop('checked', true);
				else
					$("#promo-segunda").prop('checked', false);

				if (data['tercaFeira'])
					$("#promo-terca").prop('checked', true);
				else
					$("#promo-terca").prop('checked', false);

				if (data['quartaFeira'])
					$("#promo-quarta").prop('checked', true);
				else
					$("#promo-quarta").prop('checked', false);

				if (data['quintaFeira'])
					$("#promo-quinta").prop('checked', true);
				else
					$("#promo-quinta").prop('checked', false);

				if (data['sextaFeira'])
					$("#promo-sexta").prop('checked', true);
				else
					$("#promo-sexta").prop('checked', false);

				if (data['sabado'])
					$("#promo-sabado").prop('checked', true);
				else
					$("#promo-sabado").prop('checked', false);

				if (data['domingo'])
					$("#promo-domingo").prop('checked', true);
				else
					$("#promo-domingo").prop('checked', false);

				$("#promo-horario1").val(data['horario1']);
				$("#promo-horario2").val(data['horario2']);
				$("#promo-horario3").val(data['horario3']);
				$("#promo-horario4").val(data['horario4']);
				$("#status-promocao").val(data['status']);
				if (data['validade']) {
					var validade = data['validade'].split('T');	
					validade = validade[0].split('-');
					validade = validade[2] + '/' + validade[1] + '/' + validade[0];
					$("#validade").val(validade);
				}

				$("#modal-promocao").modal();
			}
		});
	}

	$(".abrir-promocao").click(function(){		
		var id = $(this).attr('ciklus-promocao');
		// pesquisar ID e abrir promoção
		abrirPromocao(id);
		return false;
	});

	$('#btn-salvar-promocao').click(function() {
		var estId		= $("#hdnId").val();
		var promoId  	= $("#promocao-id").val();
		var titulo 		= $("#promo-titulo").val();
		var politica	= $("#promo-politica").val();
		var socioMais	= $("#promo-socio-mais").val();
		var segunda		= $("#promo-segunda").prop('checked');
		var terca		= $("#promo-terca").prop('checked');
		var quarta		= $("#promo-quarta").prop('checked');
		var quinta		= $("#promo-quinta").prop('checked');
		var sexta		= $("#promo-sexta").prop('checked');
		var sabado		= $("#promo-sabado").prop('checked');
		var domingo		= $("#promo-domingo").prop('checked');
		var horario1	= $("#promo-horario1").val();
		var horario2	= $("#promo-horario2").val();
		var horario3	= $("#promo-horario3").val();
		var horario4	= $("#promo-horario4").val();
		var desconto 	= $("#promo-desconto").val();
		var validade	= $("#validade").val();
		var status		= $("#status-promocao").val();
		var destaque	= $("#destaque-promocao").val();
		var update = false;

		if (!desconto) {
			desconto = 0;
		}

		if (promoId > 0) {
			update = true;
		}

		$.ajax({
			url: 'action/ajax/salvar-promocao.php',
			type: 'POST',
			data: {estabelecimento: estId, id:promoId, titulo: titulo,
					politica:politica, socioMais:socioMais, segunda:segunda,
					terca: terca, quarta:quarta, quinta: quinta, sexta: sexta,
					sabado: sabado, domingo: domingo, horario1: horario1, horario2: horario2,
					horario3: horario3, horario4: horario4, desconto: desconto, validade: validade, 
					status:status, destaque: destaque },
			success: function(data) {
				data = $.parseJSON(data);
				
				var stat = "Ativa";
				if (status == 0) {
					stat = "Inativa";
				}
				$("#tr-promocao-"+data['id']).remove();
				if (destaque == 'r') {
					destaque = "Randômico";
				} else if (destaque == 'f') {
					destaque = "Fixo";
				} else {
					destaque = "Sem destaque";
				}
				var tr = "<tr class='" + stat + "' id='tr-promocao-" + data['id'] + "'><td><a href='#' ciklus-promocao='" + data['id'] + "' class='abrir-promocao'>" + data['titulo'] + "</a></td><td>"+data['desconto']+"%</td><td>" + stat + "</td><td>" + destaque + "</td><td><a href='#' class='excluir-promocao' ciklus-promocao='" + data['id'] + "''>Excluir</a></td></tr>";
				$("#tbody-promocoes").append(tr);
				$(".abrir-promocao").click(function(){		
					var id = $(this).attr('ciklus-promocao');
					// pesquisar ID e abrir promoção
					abrirPromocao(id);
					return false;
				});
				
				swal("Promoção salva com sucesso");
				$("#modal-promocao").modal('toggle');
			}
		});
	});

	$('#btn-salvar').click(function() {
		var id		 = $("#hdnId").val();
		var nome 	 = $("#nome").val();
		var endereco = $("#endereco").val();
		var bairro	 = $("#bairro").val();
		var cidade 	 = $("#cidade").val();
		var numero   = $("#numero").val();
		var cep		 = $("#cep").val();
		var telefone = $("#telefone").val();
		var email	 = $("#email").val();
		var site	 = $("#site").val();
		var fb		 = $("#facebook").val();
		var desc	 = $("#descricao").val();
		var categ	 = $("#categoria").val();
		var subcateg = $("#subcategoria").val();
		var urlMaps  = $("#url-maps").val();
		var login	 = $("#login").val();
		var senha	 = $("#senha").val();
		var status   = $("#status").val();
		var lat 	 = $("#latitude").val();
		var lon 	 = $("#longitude").val();
		var metaDesc = $("#meta-descricao").val();
		var metaTit	 = $("#meta-titulo").val();

		$.ajax({
			url: 'action/ajax/salvar-estabelecimento.php',
			type: 'POST',
			data: { id:id,nome:nome, endereco:endereco, bairro:bairro, cidade:cidade,
					cep:cep,telefone:telefone,email:email,site:site,facebook:fb,
					descricao:desc, categoria:categ,subcategoria:subcateg, url: urlMaps,
					login: login, senha: senha, status: status, numero: numero,
					latitude: lat, longitude: lon, metaTitulo: metaTit, metaDescricao: metaDesc },
			success: function(data) {
				data = $.parseJSON(data);
				$("#hdnId").val(data['id']);
				$(".edit-mode").show();
				swal({
				  title: "Pronto",
				  text: "Estabelecimento salvo com sucesso",
				  type: "success",
				  showCancelButton: true,
				  cancelButtonText: "Ficar na edição",
				  confirmButtonColor: "#DD6B55",
				  confirmButtonText: "Ir para pesquisa",
				  closeOnConfirm: false
				},
				function(){
				  window.location="estabelecimentos.php?status=a";
				});
			}
		});
	});

	$("#todas-promocoes").click(function() {
		$(".Ativa").show();
		$(".Inativa").show();
	});

	$("#apenas-ativas").click(function() {
		$(".Ativa").show();
		$(".Inativa").hide();
	});

	$("#apenas-inativas").click(function() {
		$(".Ativa").hide();
		$(".Inativa").show();
	});
});