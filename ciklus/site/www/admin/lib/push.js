$(document).ready(function() {

	$("#btn-salvar").click(function() {

		var ids 		= $("#ids").val();
		var titulo 		= $("#titulo").val();
		var mensagem 	= $("#mensagem").val();
		var empresa 	= $("#empresa").val();
		$.ajax({
			url: 'action/ajax/enviar-push.php',
			data: { ids: ids, titulo: titulo, mensagem: mensagem, empresa:empresa},
			type: 'POST',
			success: function(data) {
				if (data) {		
					var ids 		= $("#ids").val();
					var texto 		= $("#msg_notificacao").val();
					var link 		= $("#link_notificacao").val();
					var parametro 	= $("#parametro_notificacao").val();
					var empresa 	= $("#empresa").val();
					if (texto) {
						$.ajax({
							url: 'action/ajax/salvar-notificacao.php',
							data: { ids:ids, texto: texto, link:link, parametro:parametro, empresa:empresa},
							type: 'POST',
							success: function(data) {
								if (data) {
									swal('Pronto!', 'O Push foi enviado e a Notificação foi salva com sucesso', 'success');
								}
							}
						});
					}else{
						swal('Pronto!', 'O Push foi enviado com sucesso', 'success');
					}
				}
			}
		});
	});
});