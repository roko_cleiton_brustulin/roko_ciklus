$(document).ready(function(){

	$("#inicio").mask('99/99/9999');
	$("#fim").mask('99/99/9999');
	
	$("#btn-relatorio").click(function() {
		$.ajax({
			url: 'action/ajax/gerar-relatorio.php',
			type: 'POST',
			data: { inicio: $("#inicio").val(), fim: $("#fim").val(), categoria: $("#categoria").val() },
			success: function(data) {
				if (data != "null") {
					$("#resultados").show();
					var res = JSON.parse(data);
					$("#vendas").text("R$ " + res[0].total_vendas);
					$("#economizado").text("R$ " + res[0].total_economizado);
					$("#transacoes").text(res[0].num_transacoes);
					$("#media").text(res[0].media_desconto);
					$("#socios").text(res[0].socios);
					$("#mediatransacoes").text(res[0].num_transacoes / res[0].socios);
				} else {
					$("#vendas").text("0");
					$("#economizado").text("0");
					$("#transacoes").text("0");
					$("#media").text("0");
					$("#socios").text("0");
				}
			}
		});
	});
});