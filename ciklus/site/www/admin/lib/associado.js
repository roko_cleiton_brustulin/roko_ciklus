$(document).ready(function() {

	var SPMaskBehavior = function (val) {
		return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	},
	spOptions = {
		onKeyPress: function(val, e, field, options) {
			field.mask(SPMaskBehavior.apply({}, arguments), options);
		}
	};

	$("#cep").mask('99999-999');
	$("#cpf").mask('999.999.999-99');
	$("#data_nasc").mask('99/99/9999');
	$("#telefone").mask(SPMaskBehavior, spOptions);
	$("#celular").mask(SPMaskBehavior, spOptions);

	$("#cep").blur(function() {
		if ($("#cep").val()) {
			var cep = $("#cep").val().replace("-", "");
			$.ajax({
				url: 'https://viacep.com.br/ws/' + cep + '/json/ ',
				type: 'GET',
				success: function(data) {
					$("#endereco").val(data.logradouro);
					$("#bairro").val(data.bairro);
					$("#cidade").val(data.localidade);
					$("#estado").val(data.uf);
				}
			});
		}
	});

	$("#cpf").blur(function() {
		if ($("#cpf").val()) {
			$.ajax({
				url: 'action/ajax/checar-cpf.php',
				data: { cpf: $("#cpf").val() },
				type: 'POST',
				success: function(res) {
					var socios = JSON.parse(res);
					if (socios.length > 0) {
						swal({
						  title: "Ops",
						  text: "Esse cpf já está cadastrado",
						  type: "error",
						  showCancelButton: true,
						  cancelButtonText: "Ok",
						  confirmButtonColor: "#DD6B55",
						  confirmButtonText: "Ir para associado",
						  closeOnConfirm: false
						},
						function(){
						  window.location="associado.php?id=" + socios[0].id;
						  $("#cpf").val('');
						});
					}
				}
			});
		}
	});

	$("#btn-salvar").click(function() {
		// if ($("#cpf").val() && $("#nome").val() && $("#email").val() && $("#data_nasc").val() && $("#sexo").val() &&
		// 	$("#cep").val() && $("#endereco").val() && $("#numero").val() && $("#bairro").val() && $("#cidade").val() &&
		// 	$("#estado").val() && $("#telefone").val() && $("#celular").val()) {
		if ($("#cpf").val() && $("#nome").val()) {
			$.ajax({
				url: 'action/ajax/salvar-associado.php',
				type: 'POST',
				data: { id: $("#id").val(), nome: $("#nome").val(), cpf: $("#cpf").val(), email: $("#email").val(),
						data: $("#data_nasc").val(), sexo: $("#sexo").val(), cep: $("#cep").val(), endereco: $("#endereco").val(),
						numero: $("#numero").val(), bairro: $("#bairro").val(), cidade: $("#cidade").val(), estado: $("#estado").val(),
						fone: $("#telefone").val(), celular: $("#celular").val(), senha: $('#senha').val() },
				success: function(res) {
					res = JSON.parse(res);
					$("#id").val(res.id);
					swal({
					  title: "Pronto",
					  text: "Associado salvo com sucesso",
					  type: "success",
					  showCancelButton: true,
					  cancelButtonText: "Ficar na edição",
					  confirmButtonColor: "#DD6B55",
					  confirmButtonText: "Ir para pesquisa",
					  closeOnConfirm: false
					},
					function(){
					  window.location="associados.php";
					});
				}
			});
		} else {
			swal("Ops", "Preencha todos os campos", "error");
		}
	});
});