$(document).ready(function() {

	if ($("#eventoid").val() > 0) {
		$("#form-foto").show();
	}

	$('#data').datepicker({
		format: 'dd/mm/yyyy'
	});
	$("#desconto").mask('99');

	$("#btn-salvar").click(function() {
		var polit = $("#politica").val();
		if (!polit || !$("#titulo").val() || !$("#desconto").val() || !$("#data").val() || !$("#categoria").val() || !$("#cidade").val() || !$("#estado").val() || !$("#local").val()) {
			swal('Preencha todos os campos.');
		} else {
			$.ajax({
				url: 'action/ajax/salvar-evento.php',
				data: { titulo: 	$("#titulo").val(), 
						politica: 	polit, 
						desconto: 	$("#desconto").val(),
						data: 		$("#data").val(),
						cidade: 	$("#cidade").val(),
						estado: 	$("#estado").val(),
						local: 		$("#local").val(),
						link: 		$("#link").val(),
						empresa: 	$("#empresa").val(),
						categoria: 	$("#categoria").val(), 
						id: 		$("#eventoid").val() 
					},
				type: 'POST',
				success: function(data) {
					var obj = JSON.parse(data);
					$("#eventoid").val(obj.id);
					$("#form-foto").show();
					$("#form-foto").attr('action', 'evento.php?id=' + obj.id);
					swal({
					  title: "Pronto",
					  text: "Evento salvo com sucesso",
					  type: "success",
					  showCancelButton: true,
					  cancelButtonText: "Ficar na edição",
					  confirmButtonColor: "#DD6B55",
					  confirmButtonText: "Ir para pesquisa",
					  closeOnConfirm: false
					},
					function(){
					  window.location="eventos.php?status=1";
					});
				}
			});
		}
	});

	$("#btn-excluir").click(function() {
		if (confirm('Confirma exclusão?')) {
			$.ajax({
				url: 'action/ajax/excluir-evento.php',
				data: { id: $("#eventoid").val() },
				type: 'POST',
				success: function(data) {
					window.location="eventos.php?status=1";
				}
			});
		}
	});

	$("#excluir-foto").click(function() {
		if (confirm("Deseja realmente excluir a foto?")) {
			$.ajax({
				url: 'action/ajax/excluir-foto.php',
				data: { id: $("#fotoid").val(), evento: $("#eventoid").val() },
				type: 'POST',
				success: function(data) {
					$("#foto").hide();
				}
			});
		}
	});		
});