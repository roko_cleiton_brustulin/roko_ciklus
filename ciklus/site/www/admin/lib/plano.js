$(document).ready(function() {
	$("#preco").maskMoney({ prefix: 'R$ ' });

	$("#btn-salvar").click(function(e) {
		e.preventDefault();
		
		var plano = $("#planoid").val();
		var frequencia = $("#frequencia").val();
		var tipo = $("#tipo").val();
		var ativo = $("#ativo").val();
		var preco = $("#preco").val();
		var titulo = $("#titulo").val();

		$.ajax({
			url: 'action/ajax/salvar-plano.php',
			data: { id: plano, frequencia: frequencia, tipo: tipo, ativo: ativo, preco: preco, titulo: titulo },
			type: 'POST',
			success: function(data) {
				data = JSON.parse(data);
				$("#planoid").val(data.id);
				swal({
				  title: "Pronto",
				  text: data.titulo + " salvo com sucesso",
				  type: "success",
				  showCancelButton: true,
				  cancelButtonText: "Ficar na edição",
				  confirmButtonColor: "#DD6B55",
				  confirmButtonText: "Ir para pesquisa",
				  closeOnConfirm: false
				},
				function(){
				  window.location="planos.php";
				});
			}
		});
	});
});