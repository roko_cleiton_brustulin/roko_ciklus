$(document).ready(function() {
	// $("#expiracao").mask('99/99/9999');
	$('#expiracao').datepicker({
		format: 'dd/mm/yyyy'
	});

	$("#btn-salvar").click(function() {
		var prefixo = $("#prefixo").val();
		var qtde = $("#qtde").val();
		var expiracao = $("#expiracao").val();
		var plano = $("#plano").val();
		var tipo = $("#tipo").val();

		$.ajax({
			url: 'action/ajax/gerar-vouchers.php',
			data: { prefixo: prefixo, quantidade: qtde, expiracao: expiracao, plano: plano, tipo:tipo},
			type: 'POST',
			success: function(data) {
				if (data) {
					swal('Pronto!', 'Os vouchers foram gerados com sucesso', 'success');
				}
			}
		});

	});
});