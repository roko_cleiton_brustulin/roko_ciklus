<?php 
include('includes/settings.php');
include('action/eventos.php');
include('widgets/header.php');
?>

<div class="page">
  	<div class="page-content">
    	<div class="panel">
      		<div class="panel-heading">
        		<h3 class="panel-title">Ações</h3>
      		</div>
      		<div class="panel-body container-fluid">
        		<a href="evento.php" class="btn btn-success" id="btn-novo">Cadastrar Evento</a>
            <?php if (!$todos) { ?>
              <a href="eventos.php?todos=1" class="btn btn-default" id="btn-ver-todos">Ver Todos os Eventos</a>
            <?php } else { ?>
              <a href="eventos.php" class="btn btn-default" id="btn-ver-todos">Ver Apenas Futuros</a>
            <?php } ?>
      		</div>
    	</div>
    	<!-- Panel Form Elements -->
    	<div class="panel">
	      <div class="panel-heading">
	        <h3 class="panel-title">Lista de Evento</h3>
	      </div>
	      <div class="panel-body container-fluid">
	      	<table class="table table-hover dataTable table-striped width-full" id="tbl-eventos">
	            <thead>
	              <tr>
	                <th>Título</th>
	                <th>Categoria</th>
	                <th>Desconto</th>
                  <th>Data</th>
	              </tr>
	            </thead>
	            <tbody>
            	<?php 
              foreach (@$rows as $row) { 
                $date = new DateTime($row->data);
                $date->add(new DateInterval('P1D'));
                if ((new DateTime() < $date) || $todos) {
              ?>
            		<tr>
            			<td><a href="evento.php?id=<?php echo $row->id; ?>"><?php echo @$row->titulo; ?></a></td>
            			<td><a href="evento.php?id=<?php echo $row->id; ?>"><?php echo @$row->categoria->nome; ?></a></td>
            			<td><a href="evento.php?id=<?php echo $row->id; ?>"><?php echo @$row->desconto; ?></a></td>
                  <td><a href="evento.php?id=<?php echo $row->id; ?>"><?php echo substr($row->data, 0, 10); ?></a></td>
            		</tr>
            	<?php
                }
              } ?>
	            </tbody>
        	</table>
	      </div>
  		</div>
	</div>
</div>

<?php 
$script = "<script type='text/javascript' src='lib/eventos.js'></script>";
include('widgets/footer.php');
