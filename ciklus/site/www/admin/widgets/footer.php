  <!-- Footer -->
  <footer class="site-footer">
    <span class="site-footer-legal">© 2017 Ciklus</span>
    <div class="site-footer-right">
    </div>
  </footer>

  <!-- Core  -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/bootstrap.min.js"></script>
  <script src="assets/vendor/animsition/jquery.animsition.min.js"></script>
  <script src="assets/vendor/asscroll/jquery-asScroll.min.js"></script>
  <script src="assets/vendor/mousewheel/jquery.mousewheel.min.js"></script>
  <script src="assets/vendor/asscrollable/jquery.asScrollable.all.min.js"></script>
  <script src="assets/vendor/ashoverscroll/jquery-asHoverScroll.min.js"></script>

  <!-- Plugins -->
  <script src="assets/vendor/switchery/switchery.min.js"></script>
  <script src="assets/vendor/intro-js/intro.min.js"></script>
  <script src="assets/vendor/screenfull/screenfull.js"></script>
  <script src="assets/vendor/slidepanel/jquery-slidePanel.min.js"></script>
  <script src="assets/js/sweetalert-master/dist/sweetalert.min.js"></script>

  <!-- Scripts -->
  <script src="assets/js/core.min.js"></script>
  <script src="assets/js/site.min.js"></script>
  <script src="assets/js/sections/menu.min.js"></script>
  <script src="assets/js/sections/menubar.min.js"></script>
  <script src="assets/js/sections/sidebar.min.js"></script>
  <script src="assets/js/configs/config-colors.min.js"></script>
  <script src="assets/js/configs/config-tour.min.js"></script>
  <script src="assets/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="assets/vendor/datatables-fixedheader/dataTables.fixedHeader.js"></script>
  <script src="assets/vendor/datatables-bootstrap/dataTables.bootstrap.min.js"></script>
  <script src="assets/vendor/datatables-responsive/dataTables.responsive.js"></script>
  <script src="assets/vendor/datatables-tabletools/dataTables.tableTools.js"></script>
  <script src="assets/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
  <script src="assets/js/components/asscrollable.min.js"></script>
  <script src="assets/js/components/animsition.min.js"></script>
  <script src="assets/js/components/slidepanel.min.js"></script>
  <script src="assets/js/components/switchery.min.js"></script>
  <script src="assets/js/jquery.mask.min.js"></script>
  <script src="assets/js/maskmoney.js"></script>
  <script src="assets/croppic.min.js"></script>

  <?php echo @$script; ?>
</body>
</html>