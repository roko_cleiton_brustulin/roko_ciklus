<?php

if (!isset($_COOKIE['admin-ciklus'])) {	
	echo "<script>window.location='login.php';</script>";
	die;
}
/* Produção */
// $rest = "http://Default-Environment.mjmzgfnwre.sa-east-1.elasticbeanstalk.com/"
/* Local */
//http://localhost:1337/
// http://ciklus-server.southcentralus.cloudapp.azure.com:1337/
function api($call, $data = null, $rest = "http://ciklus-server.westus2.cloudapp.azure.com/") {	
// function api($call, $data = null, $rest = "http://localhost:80/") {	
	// Get cURL resource
	$curl = curl_init();
	// Set some options - we are passing in a useragent too here
	curl_setopt_array($curl, array(
	    CURLOPT_RETURNTRANSFER => 1,
	    CURLOPT_URL => $rest . $call,
	    CURLOPT_USERAGENT => 'Ciklus',
	));

	if ($data != null) {
		$fields_string = "";
		foreach($data as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
			rtrim($fields_string, '&');
		curl_setopt($curl, CURLOPT_POST, count($data));
		curl_setopt($curl, CURLOPT_POSTFIELDS, $fields_string);
	}

	// Send the request & save response to $resp
	$response = curl_exec($curl);
	
	// Close request to clear up some resources
	curl_close($curl);

	return $response;
}

//include('functions.php');