<?php 
include('includes/settings.php');
include('action/evento.php'); 
include('widgets/header.php'); 
?>

  <div class="page">
    <div class="page-content">
      <div class="panel">
        <div class="panel-heading">
          <h3 class="panel-title">Ações</h3>
        </div>
        <div class="panel-body container-fluid">
          <a href="eventos.php" class="btn btn-default" id="btn-voltar">Voltar</a>
          <button class="btn btn-success" id="btn-salvar" type="submit">Salvar</button>
          <button class="btn btn-danger" id="btn-excluir" type="submit">Excluir</button>
        </div>
      </div>
      <div class="panel">
        <div class="panel-heading">
          <h3 class="panel-title">Cadastrar Evento</h3>
        </div>
        <div class="panel-body container-fluid">
          <div class="row row-lg">
            <input type="hidden" value="<?php echo @$evento->id; ?>" id="eventoid" />

            <div class="col-sm-12 col-md-12">  
              <h4 class="example-title">Empresa</h4>
              <div class="form-group">
                <select class="form-control" id="empresa">
                  <!-- <option value="01;">Ciklus</option> -->
                  <option value="02;" selected>Alô Ingressos</option>
                </select>
              </div>
            </div>

            <div class="col-sm-6 col-md-6" style="margin-bottom: 15px;">  
              <h4 class="example-title">Título</h4>
              <input type="text" id="titulo" class="form-control" value="<?php echo @$evento->titulo ?>" />
            </div>

            <div class="col-sm-6 col-md-3" style="margin-bottom: 15px;">  
              <h4 class="example-title">Cidade</h4>
              <input type="text" id="cidade" class="form-control" value="<?php echo @$evento->cidade; ?>" />
            </div>

            <div class="col-sm-6 col-md-3" style="margin-bottom: 15px;">  
              <h4 class="example-title">Estado</h4>
              <input type="text" id="estado" class="form-control" value="<?php echo @$evento->estado; ?>" />
            </div>

            <div class="col-sm-6 col-md-3" style="margin-bottom: 15px;">  
              <h4 class="example-title">Local</h4>
              <input type="text" id="local" class="form-control" value="<?php echo @$evento->local; ?>" />
            </div>

            <div class="col-sm-6 col-md-3" style="margin-bottom: 15px;">  
              <h4 class="example-title">Data Evento</h4>
              <input type="text" id="data" class="form-control" value="<?php echo @$evento->data; ?>" />
            </div>

            <div class="col-sm-6 col-md-3" style="margin-bottom: 15px;">  
              <h4 class="example-title">Desconto (%)</h4>
              <input type="text" id="desconto" class="form-control" value="<?php echo @$evento->desconto; ?>" />
            </div>

            <div class="col-sm-6 col-md-3">  
              <h4 class="example-title">Categoria</h4>
              <div class="form-group">
                <select class="form-control" id="categoria">
                  <option value="0">Selecione</option>
                  <?php 
                  $optGroupCount = 0;
                  $optGroup = "";
                  foreach ($categorias as $categoria) { 
                    if ($optGroup != $categoria->nomePai) {
                      if ($optGroupCount == 0) {
                        echo "</optgroup>";
                      }
                      $optGroupCount++;
                      echo "<optgroup label='$categoria->nomePai'>";
                      $optGroup = $categoria->nomePai;
                    }
                  ?>
                    <option <?php if (@$evento->categoria->id == $categoria->id) : echo "selected='selected'"; endif; ?> 
                      value="<?php echo $categoria->id; ?>">
                      <?php echo $categoria->nome; ?>
                    </option>
                  <?php } ?>
                </select>
              </div>
            </div>


            <div class="col-sm-12 col-md-12" style="margin-bottom: 15px;">  
              <h4 class="example-title">Link (botão "Comprar Ingresso")</h4>
              <input type="text" id="link" class="form-control" value="<?php echo @$evento->link; ?>" />
            </div>

            <div class="col-sm-12 col-md-12" style="margin-bottom: 15px;">  
              <h4 class="example-title">Política</h4>
              <textarea id="politica" class="form-control"><?php echo @$evento->politica; ?></textarea>
            </div>

            <div class="col-sm-12 col-md-12" style="margin-bottom: 15px;">  
              <form id="form-foto" action="evento.php?id=<?php echo $evento->id; ?>" method="POST" enctype="multipart/form-data" style="display:none;margin-bottom: 20px;">
                <div class="row">
                  <div class="col-sm-6" style="margin-bottom: 10px;">
                    <input type="file" class="form-control" name="imagem-upload" id="imagem-upload" />
                    <input type="hidden" id="imagem-upload-base" />
                  </div>
                  <div class="col-sm-6" style="margin-bottom: 10px;">
                    <input type="submit" class="btn btn-primary" value="Salvar" />
                    <!-- <button class="btn btn-primary" id="btn-salvar-imagem" type="button">Salvar</button> -->
                  </div>
                </div>
              </form>
            </div>

            <div class="col-lg-12">
                <?php if (@$evento->foto->id > 0) :?>
                <div class="col-lg-3 col-sm-6 text-center" id="foto">
                  <input type="hidden" id="fotoid" value="<?php echo $evento->foto->id; ?>" />
                  <img src="uploads/<?php echo $evento->foto->id . '.' . $evento->foto->extensao; ?>" style="width:150px;" /><br />
                  <button class="btn btn-danger" id="excluir-foto">Excluir</button>
                </div>
                <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php 
$script = "<script type='text/javascript' src='lib/evento.js'></script>";
include('widgets/footer.php'); ?>