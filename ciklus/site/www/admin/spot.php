<?php 
include('includes/settings.php');
include('action/spot.php'); 
include('widgets/header.php'); 
?>

<div class="page">
    <div class="page-content">

  		<div class="panel">
	        <div class="panel-heading">
	          <h3 class="panel-title">Gerenciar Spot</h3>
	        </div>
	        <div class="panel-body container-fluid">
	        	<div class="row">
		        	<form action="spot.php?id=<?php echo $spot->id; ?>" method="POST" enctype="multipart/form-data">
					    <div class="col-sm-6" style="margin-bottom: 10px;">
					        <input type="file" class="form-control" name="imagem-upload" id="imagem-upload" />
					        <input type="hidden" id="imagem-upload-base" />
					    </div>
					    <div class="col-sm-6" style="margin-bottom: 10px;">
					        <input type="submit" class="btn btn-primary" value="Salvar" />				        
					    </div>
		            </form>
		        </div>
	            <div class="row">
	            	<?php foreach($spot->fotos as $foto) : ?>
	            	<div class="col-lg-3">
	            		<img src="uploads/<?php echo $foto->id . '.' . $foto->extensao; ?>" style="width:150px;" />
	            	</div>
	            	<?php endforeach; ?>
	            </div>
	        </div>
    	</div>
	</div>
</div>