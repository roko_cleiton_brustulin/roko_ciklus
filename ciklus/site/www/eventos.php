<?php
include("includes/settings.php");
include("action/listar-eventos.php");
$scripts = "<script src='jsapp/eventos.js'></script>";
include("widgets/header.php"); 
?>
<div class="col-lg-12 titulo-topo conainter-fluid" style="background: url(http://www.91x.com/wp-content/uploads/2016/04/Sigur_Ro%CC%81s_2013.jpg)">
	<div class="overlay-topo">
	<span class="text-center titulo-pagina overlay-titulo">EVENTOS</span>
	</div>
</div>
<div class="container-fluid">
<div class="row">
		<?php if (count($eventos) == 0) {
			echo "<div class='col-lg-12 text-center' style='margin-top: 100px;'><h1>Nenhum evento agendado.</h1><h3>Mas fique ligado, em breve teremos novidades!</h3></div>";
		} else {
			?>
			<!--<div class="col-lg-12">
				<span class="text-center eventos-descricao-principal">Aproveite tudo que a Ciklus tem pra oferecer!</span>
			</div>-->
			<?php
			$count = 0;
			foreach ($eventos as $e) {
				$data = explode('-', $e->data);
				$data = $data[2] . '/' . $data[1] . '/' . $data[0];
				if ($count % 2 == 0) {
			?>
			<div class="container">
			<div class="row">
				<div class="col-md-6" id="<?php echo $e->id; ?>" style="float:left;background-color: #f1f1f1;margin-bottom:20px; border-radius: 10px; padding: 20px;">
						<div class="col-md-12">
							<img class="eventos-imagem" src="<?php echo $urlImagens . $e->foto . '.' . $e->extensao; ?>"/>
							<span class="discount-tag">
								<span class="oferta-tag-texto">Economize</span>
								<br>
							<!-- <span class="oferta-tag-desconto"><?php echo $e->desconto; ?>%</span> -->
								<span class="oferta-tag-desconto">
									<?php if($e->desconto == 0){
										echo '$';
									}else{
										echo $e->desconto . '%';	
									} ?>
								</span>
							</span>
						</div>
						<div class="col-md-12">
						<div class="eventos-borda" style="margin-bottom:0!important;">
						<div class="eventos-overlay">
							<span class="text-center eventos-titulo"><i class="fa fa-bullhorn"></i> <?php echo $e->titulo; ?></span>
							<span class="text-center eventos-data"><i class="fa fa-calendar"></i> <?php echo $data; ?></span><br>
							<!-- <span class="text-center eventos-descricao"><?php echo $e->politica; ?></span> -->
							<br><br>
							<a class="btn-default btn-large" href="evento?id=<?php echo $e->id; ?>">SAIBA MAIS</a>
						</div>
						</div>
						</div>
				</div>
			<?php
				} else {
			?>
				<div class="col-md-6" style="float:left;background-color: #f1f1f1;margin-bottom:20px; border-radius: 10px; padding: 20px;">
						<div class="col-md-12">
							<img class="eventos-imagem" src="<?php echo $urlImagens . $e->foto . '.' . $e->extensao; ?>"/>
							<span class="discount-tag">
								<span class="oferta-tag-texto">Economize</span>
								<br>
							<!-- <span class="oferta-tag-desconto"><?php echo $e->desconto; ?>%</span> -->
								<span class="oferta-tag-desconto">
									<?php if($e->desconto == 0){
										echo '$';
									}else{
										echo $e->desconto . '%';	
									} ?>
								</span>
							</span>
						</div>
						<div class="col-md-12">
						<div class="eventos-borda" style="margin-bottom:0!important;">
						<div class="eventos-overlay">
							<span class="text-center eventos-titulo"><i class="fa fa-bullhorn"></i> <?php echo $e->titulo; ?></span>
							<span class="text-center eventos-data"><i class="fa fa-calendar"></i> <?php echo $data; ?></span><br>
							<!-- <span class="text-center eventos-descricao"><?php echo $e->politica; ?></span> -->
							<br><br>
							<a class="btn-default btn-large" href="evento?id=<?php echo $e->id; ?>">SAIBA MAIS</a>
							</div>
							</div>
						</div>
				</div>
			</div>
			</div>
			
			<?php
				}
				$count++;
			}
		} ?>
		</div>
</div>
<?php include("widgets/footer.php"); ?>
