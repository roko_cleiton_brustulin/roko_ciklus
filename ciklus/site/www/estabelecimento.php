<?php
include("includes/settings.php");
include("action/detalhe-estabelecimento.php");
include("widgets/header.php"); ?>

<?php if (isset($capa)) {
	$urlCapa = $urlImagens . $capa->id . '.' . $capa->extensao;
	echo "<div class=\"col-lg-12 titulo-topo conainter-fluid\" style=\"background: url('" . $urlCapa . "');\">";
		} else {
			echo '<div class="col-lg-12 titulo-topo conainter-fluid">';
		} ?>
	<div class="overlay-topo" style="padding:220px 0 150px;">
		<span class="text-center titulo-pagina overlay-titulo"><?php echo $estabelecimento->nome; ?></span>
		</div>
</div>
<<div class="container" style="padding-bottom:50px;">
	<div class="row">
		<div class="estabelecimento-box-1 col-lg-7 col-xs-12">
			<div class="row">
				<div class="row">
					<div class="col-md-12">
						<p class="estabelecimento-promocao-titulo">Promoções</p>
					</div>
				</div>
				<?php
				$contPromo = count($estabelecimento->promocoes);

				if ($contPromo == 0) {
					echo "<div class='col-lg-12'><h3>Não há promoções no momento.</h3></div>";
				}

				 foreach ($estabelecimento->promocoes as $promo) {  ?>
				<div class="row estabelecimento-box">
					<div class="col-lg-12 ">
						<p class="estabelecimento-oferta"><span><?php echo $promo->titulo; ?></span></p>
						<span class="estabelecimento-tag">
							<span class="oferta-tag-texto">Economize</span><br>
							<!-- <span class="oferta-tag-desconto"><?php echo $promo->desconto; ?>%</span> -->
							<span class="oferta-tag-desconto">
								<?php if($promo->desconto == 0){
									echo '$';
								}else{
									echo $promo->desconto . '%';	
								} ?>
							</span>
						</span>
						<?php if ($promo->socioMais > 0) : ?>
						<p class="estabelecimento-socio"><i class="glyphicon glyphicon-user"></i> Sócio + <?php echo $promo->socioMais; ?> convidados</p>
						<?php else : ?>
						<p class="estabelecimento-socio"><i class="glyphicon glyphicon-user"></i> Apenas sócio</p>
						<?php endif; ?>
					</div>
					<div class="col-lg-12 estabelecimento-data">
					<div class="col-md-7">
						<h4 style="margin-bottom:10px;"><i class="fa fa-calendar"></i> DIAS VÁLIDOS</h4>
							<?php if ($promo->segundaFeira) { echo "<span class='estabelecimento-dia'>SEG</span>"; } ?>
							<?php if ($promo->tercaFeira) { echo "<span class='estabelecimento-dia'>TER</span>"; } ?>
							<?php if ($promo->quartaFeira) { echo "<span class='estabelecimento-dia'>QUA</span>"; } ?>
							<?php if ($promo->quintaFeira) { echo "<span class='estabelecimento-dia'>QUI</span>"; } ?>
							<?php if ($promo->sextaFeira) { echo "<span class='estabelecimento-dia'>SEX</span>"; } ?>
							<?php if ($promo->sabado) { echo "<span class='estabelecimento-dia'>SAB</span>"; } ?>
							<?php if ($promo->domingo) { echo "<span class='estabelecimento-dia'>DOM</span>"; } ?>
						</div>
						<div class="col-md-5">
						<h4 style="margin-bottom:10px;"><i class="fa fa-clock-o"></i> HORÁRIOS VÁLIDOS</h4>
						<span class="estabelecimento-dia"><?php 	echo $promo->horario1; ?> às <?php echo $promo->horario2; ?></span>
						<?php 	if (strlen(@$promo->horario3) > 0 && strlen(@$promo->horario4)) {
									echo " | <span class='estabelecimento-dia'>" . $promo->horario3 . " às " . $promo->horario4 . "</span>";

						} ?>
						</div>
					</div>
					<div>
						<h4 style="margin-bottom:5px;">POLÍTICA</h4>
						<p><?php echo nl2br($promo->politica); ?></p>
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="estabelecimento-descricao">
				<h3>Sobre o <?php echo $estabelecimento->nome; ?></h3>
				<p class="estabelecimento-institucional-descricao"><?php echo utf8_decode(nl2br($estabelecimento->descricao)); ?></p>
			</div>
		</div>
		<div class="col-lg-5 col-xs-12 estabelecimento-box-2">
		<h1 class="estabelecimento-nome"><?php echo $estabelecimento->nome; ?><span class="estabelecimento-categoria "><i class="fa fa-bookmark"></i> <?php echo $estabelecimento->categoria->nome; ?></span></h1>
		<div class="row">
			<div class="col-xs-12" style="padding-left: 20px;">
					<img class="estabelecimento-imagem" src="<?php echo $urlImagens . $logo->id . "." . $logo->extensao; ?>" style="width:100%;" />
			</div>
		</div>
		<div class="row" style="padding-top:20px;">
			<div class="row">
				<div class="col-lg-12">
					<!--<label>Endereço</label>-->
					<p><i class="fa fa-map-marker"></i> <?php echo $estabelecimento->endereco . ', ' . $estabelecimento->numero; ?><br><i class="fa fa-phone"></i> <?php echo $estabelecimento->telefone; ?></p>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6">
				<!--	<label>Site</label><br />-->
					<a href="<?php echo $estabelecimento->site; ?>" target="__blank"><i class="fa fa-link"></i>  Site</a>
				</div>
				<div class="col-lg-6">
					<!--<label>Facebook</label><br />-->
					 <a href="<?php echo $estabelecimento->facebook; ?>" target="__blank"><i class="fa fa-facebook"></i> Facebook</a>
				</div>
			</div>
				<div class="col-xs-12 map" style="padding-left: 20px;">
					<iframe src="<?php echo $estabelecimento->urlMaps; ?>" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include("widgets/footer.php"); ?>