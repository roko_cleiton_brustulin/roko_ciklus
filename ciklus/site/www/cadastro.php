<?php
include("includes/settings.php");
include("action/cadastro.php");
$scripts = '<script type="text/javascript" src="jsapp/cadastro.js"></script>';
include("widgets/header.php"); 
?>
<div class="col-lg-12 titulo-topo container-fluid">
	<div class="overlay-topo">
		<span class="text-center titulo-pagina overlay-titulo" style="text-transform:uppercase;">Entre para o Clube</span>
	</div>
</div>
<div class="container">	
	<div class="row text-center" id="loader" style="display:none;margin-bottom:200px;">
		<img src="assets/images/loader.gif" alt="">
		<h2>Aguarde, estamos processando o pagamento.</h2>
	</div>
	<div class="col-md-offset-2 col-md-8 text-center conteudo">
		<input type="hidden" id="hdn_socio_id" />
		<input type="hidden" id="hdn_plano_id" />
		<input type="hidden" id="hdn_voucher_id" />
		<div class="col-md-12">
			<div class="panel panel-success" id="passo-2" style="display:none;">
				<div class="panel-heading cadastro-aviso">
					<p>Digite os seus dados</p>
				</div>
				<div class="panel-body text-center">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<input type="text" id="nome" class="form-control text-center" placeholder="Digite seu nome completo" />
							</div>
						</div>
						<div class="col-md-8">
							<div class="form-group">
								<input type="email" id="email" class="form-control text-center" placeholder="Digite seu e-mail" />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<input type="text" id="cpf" class="form-control text-center" placeholder="Digite seu CPF" />
							</div>
						</div>
						<!-- <div class="col-md-4">
							<div class="form-group">
								<input type="text" id="telefone" class="form-control text-center" placeholder="Digite seu telefone" />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<input type="text" id="data-nascimento" class="form-control text-center" placeholder="Sua data de nascimento" />
							</div>
						</div> -->
						<div class="col-md-4">
							<div class="form-group">
								<input type="text" id="cep" class="form-control text-center" placeholder="Digite seu CEP" />
							</div>
						</div>
						<div class="col-md-8">
							<div class="form-control">
								Sexo:
								<input style="margin:10px;" type="radio" id="sexo-masc" class="radio-sexo" name="sexo" radio-group="sexo" value="mas" /> Masculino
								<input type="radio" id="sexo-fem" class="radio-sexo" name="sexo" radio-group="sexo" value="fem" /> Feminino
							</div>
						</div>
						<div class="col-md-offset-2 col-md-8 col-md-offset-2 col-xs-12">
							<div class="form-group">
								<input type="checkbox" id="termos" />
								<label>Aceito os <a href="#termos-modal" data-toggle="modal">termos de uso</a> da Ciklus</label>
							</div>
						</div>
						<div class="col-md-offset-2 col-md-8 col-md-offset-2 col-xs-12">
							<button class="btn btn-primary btn-large" id="voltar-passo-1"><< Voltar</button>&nbsp;
							<button class="btn btn-primary btn-large" style="display:none;" id="comecar-voucher">Começar >></button>
							<button class="btn btn-primary btn-large" id="ir-passo-3">Continuar >></button>
						</div>
					</div>
				</div>
				<div class="panel-footer"><a href="#login" data-toggle="modal" class="menu-user">Já faz parte? Clique aqui para acessar</a></div>
			</div>

			<div class="panel panel-success" id="passo-3" style="display:none;">
				<div class="panel-heading cadastro-aviso">
					<p>Preencha seu endereço</p>
				</div>
				<div class="panel-body text-center">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<input type="text" id="cep" class="form-control text-center" placeholder="Digite seu cep" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<input type="text" id="rua" class="form-control text-center" placeholder="Sua rua" readonly />
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<input type="text" id="numero" class="form-control text-center" placeholder="Número" />
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<input type="text" id="complemento" class="form-control text-center" placeholder="Complemento" maxlength="30" />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<input type="text" id="bairro" class="form-control text-center" placeholder="Seu bairro" readonly />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<input type="text" id="cidade" class="form-control text-center" placeholder="Sua cidade" readonly />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<input type="text" id="estado" class="form-control text-center" placeholder="Seu estado" readonly />
							</div>
						</div>
						<div class="col-md-12">
							<button class="btn btn-primary btn-large" id="voltar-passo-2"><< Voltar</button>
							<button class="btn btn-primary btn-large" id="ir-passo-4">Continuar >></button>
						</div>
					</div>
				</div>
				<div class="panel-footer"><a href="#login" data-toggle="modal" class="menu-user">Já faz parte? Clique aqui para acessar</a></div>
			</div>

			<div class="panel panel-success" id="passo-1">

				<div class="col-md-offset-2 col-md-8">

					<label for="basic-url">Se você tem um convite, digite o código no campo abaixo:</label>
					<br><br>
					<div class="col-md-9">
						<input type="text" class="form-control text-center" id="codigo-voucher" style="padding:15px;" PLACEHOLDER="Digite o código">
						</input>
					</div>

					<button class="col-md-3 btn btn-primary" style="text-transform:none;font-size:14px;" id="validar-voucher">Validar</button>
					
					<br><br><br>
					
					<a class="btn btn-default" style="text-transform:none;font-size:14px;" data-toggle="modal" data-target=".bs-example-modal-sm">Não tem um convite? Peça aqui</a>

					<br><br>

					<!-- <label for="basic-url">ou</label><br><br>

					<label for="basic-url">Escolha o plano de pagamento:</label>
					<div class="row">
			    		<?php
				    		if (count($planos) == 2) {
				    			echo "<div class='col-lg-2'></div>";
				    		}
		    		 		foreach ($planos as $plano) { ?>
					    		<div class="col-md-12 cadastro-plano">
				    				<button class="btn btn-primary btn-large btn-selecionar-plano" ciklus-plano="<?php echo $plano->id; ?>">Plano <?php echo $plano->nome; ?> - <strong>R$ <?php echo substr($plano->valor, 0, 2) . ',' . substr($plano->valor, 2, 2); ?></strong></button>
				    			</div>
		    				<?php } ?>
	    			</div> -->

	    			<br><br>

					<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
						<div class="modal-dialog modal-sm" role="document">
							<div class="modal-content" style="padding:20px;">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><br><br>
									<div>
										Faça parte da Ciklus.<br>Preencha os campos abaixo:
										<br><br>
										<div class="form-group">
											<input type="text" placeholder="Seu nome completo" class="form-control" id="nome-voucher" />
										</div>
										<div class="form-group">
											<input type="text" placeholder="Seu e-mail" class="form-control" id="email-voucher" />
										</div>
										<!-- <div class="form-group">
											<input type="text" placeholder="Seu telefone" class="form-control" id="telefone-voucher" />
										</div> -->
										<div class="form-group">
											<button class="btn btn-primary btn-large" id="btn-enviar-voucher">Entrar na lista de espera</button>
										</div>
									</div>
							</div>
						</div>
					</div>
				</div>		
			</div>
		</div>
		<div class="panel panel-success" id="passo-4" style="display:none;">
		<div class="panel-heading cadastro-aviso">
			<p>Digite os dados do cartão de crédito</p>
		</div>
		<div class="panel-body text-center">
			<div class="row">
				<div class="col-md-12">
					<div class='form-row'>
						<div class='col-md-12 form-group'>
							<!-- <label class='control-label'>Nome no Cartão (igual escrito no cartão)</label> -->
							<input class='form-control text-center' size='4' id='checkout-nome' type='text' placeholder='Nome (igual escrito no cartão)'>
						</div>
					</div>
					<div class='form-row'>
						<div class='col-xs-8 form-group card required'>
							<!-- <label class='control-label'>Número do Cartão</label> -->
							<input class='form-control text-center' size='16' maxlength="16" id='checkout-cartao' type='text' placeholder='Número do Cartão'>
						</div>
					</div>
					<div class='form-row'>
						<div class='col-xs-4 form-group cvc required'>
							<!-- <label class='control-label'>CVV (Código de Segurança)</label> -->
							<input class='form-control text-center' placeholder='CVV' size='4' maxlength="4" type='text' id='checkout-cvv'>
						</div>
						<div class='col-xs-6 form-group expiration required'>
							<label class='control-label'>Mês (Validade)</label>
							<select class="form-control" style="text-align-last:center;" id='checkout-mes'>
								<option value="01">01</option>
								<option value="02">02</option>
								<option value="03">03</option>
								<option value="04">04</option>
								<option value="05">05</option>
								<option value="06">06</option>
								<option value="07">07</option>
								<option value="08">08</option>
								<option value="09">09</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
							</select>
						</div>
						<div class='col-xs-6 form-group expiration required'>
							<label class='control-label'>Ano (Validade)</label>
							<select class="form-control" style="text-align-last:center;" id='checkout-ano'>
								<option value="17">17</option>
								<option value="18">18</option>
								<option value="19">19</option>
								<option value="20">20</option>
								<option value="21">21</option>
								<option value="22">22</option>
								<option value="23">23</option>
								<option value="24">24</option>
								<option value="25">25</option>
								<option value="26">26</option>
								<option value="27">27</option>
								<option value="28">28</option>
								<option value="29">29</option>
								<option value="30">30</option>
							</select>
						</div>
					</div>
					<div class="col-md-12 col-xs-12">
						<button class="btn btn-primary btn-large" id="voltar-passo-3"><< Voltar</button>
						<button class="btn btn-primary btn-large" id="iniciar-ciklus">Finalizar >></button>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>
</div>
</div>
<?php 
include("widgets/footer.php");