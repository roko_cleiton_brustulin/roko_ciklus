<?php
include("includes/settings.php");
include("action/minha-conta.php");
$scripts = "<script src='jsapp/minha-conta.js'></script>";
include("widgets/header.php");
?>
<div class="col-lg-12 titulo-topo conainter-fluid">
	<div class="overlay-topo">
	<span class="text-center titulo-pagina overlay-titulo" style="text-transform:uppercase;"><span style="font-size:0.7em;">Bem vindo,</span><br><?php echo $user->nome; ?></span></span>
	</div>
</div>
<div class="container">
	<div class="col-md-12" style="text-align:center;">
		<span class="conta-plano">Plano: <?php echo $plano; ?></span><a href="#modal-planos" data-toggle="modal"> Alterar</a><br><br>
		<span class="conta-validade">Validade:
				<?php
					if ($plano != "Expirado") {
						if (is_array($validade)) {
							if (strlen(@$validade[2]) == 2) 
		                        echo @$validade[2] . '/' . @$validade[1] . '/' . @$validade[0];
		                    else
		                        echo substr($validade[2], 0, 2) . '/' . $validade[1] . '/' . $validade[0];
						} else {
							echo $validade;
						}
					} else {
						echo "-";
					}
                ?>
        </span>
		<br>
	</div>
	<div class="col-md-8 col-md-offset-2 col-xs-12" style="margin-top:40px;">
	<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#home">ASSISTENTE FINANCEIRO</a></li>
		<li><a data-toggle="tab" href="#menu2" >FATURAS</a></li>
		<li><a data-toggle="tab" href="#menu1" >MEUS DADOS</a></li>
	</ul>
	<div class="tab-content">
		<div id="home" class="tab-pane fade in active">
			<div class="col-xs-12 col-md-12" style="background:#fff;margin-bottom:50px;">
				<!--<h2 class="text-center">Histórico de Economia</h3>-->
				<br />
				<div class="form-group text-center col-lg-12 col-xs-12">
					<h4 style="text-transform:uppercase;font-weight:bold;">Aqui você vê quanto já economizou com a Ciklus!</h4>
					<div class="table-responsive">
					<table class="table table-striped">
						<thead>
							<tr>
								<th class="text-left"><strong>Data</strong></th>
								<th class="text-center"><strong>Estabelecimento</strong></th>
								<th class="text-right"><strong>Desconto</strong></th>
							</tr>
						</thead>
						<tbody>
						<?php
							if (count($transacoes) == 0) {
								echo "<tr><td colspan='3' class='text-center'>Você ainda não utilizou seu Ciklus</td></tr>";
							}

							$descontoTotal = 0;
							foreach ($transacoes as $t) :
								$data = explode("-", $t->createdAt);
								$descontoTotal = $descontoTotal + $t->valorDesconto; ?>
								<tr>
									<td class="text-left"><?php echo substr($data[2], 0, 2) . '/' . $data[1] . '/' . $data[0]; ?></td>
									<td class="text-center"><?php echo @$t->estabelecimento->nome; ?></td>
									<td class="text-right">R$<?php echo number_format(@$t->valorDesconto, 2, ",", "."); ?></td>
								</tr>
						<?php 	endforeach; ?>
						</tbody>
						<tfoot>
							<tr>
						</tfoot>
					</table>
					</div>
					<br>
					<div class="conta-desconto">Total Economizado: R$<?php echo number_format($descontoTotal, 2, ",", "."); ?></div>
					<br>
					<br>
				</div>
			</div>
		</div>
		<div id="menu2" class="tab-pane fade">
			<div class="form-group text-center col-lg-12 col-xs-12" style="background:#fff;margin-bottom:50px;">
			<h4 class="text-center" style="text-transform:uppercase;font-weight:bold;"><br>Faturas Ciklus</h4>
			<div class="table-responsive">
			<table class="table table-striped">
				<thead>
					<tr>
						<th><strong>Data</strong></th>
						<th class="text-right"><strong>Valor</strong></th>
						<th class="text-center">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
						if (count($transactions) == 0) {
							echo "<tr><td colspan='3' class='text-center'>Você ainda não realizou pagamentos</td></tr>";
						}

						foreach ($transactions as $f) :
							$data = $f->getDateCreated()->format('d/m/Y'); ?>
							<tr>
								<td><?php echo $data; ?></td>
								<td class="text-right">R$ <?php echo substr($f->getAmount(), 0, 2) . ',' . substr($f->getAmount(), 2, 2); ?></td>
								<td><?php if ($f->getStatus() != "paid") : echo "<strong>Pendente</strong>"; else : echo "<strong>Pago</strong>"; endif; ?></td>
							</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			</div>
			<h4 class="text-center">Forma de pagamento</h4>
			<div class="form-group text-center col-lg-12 col-xs-12">
				<?php if ($card) : ?>
						<span class="cartao-<?php echo $card->getId(); ?>">XXXX-XXXX-XXXX-<?php echo $card->getLastDigits() . " - " . $card->getBrand(); ?></span><br /><br />
					<a class="btn btn-default btn-large" href="#modal-cartao" data-toggle="modal">Alterar cartão</a>
				<?php else: ?>
				<a class="btn btn-primary btn-large" href="#modal-cartao" data-toggle="modal">Cadastrar cartão</a>
				<?php endif; ?>
				</div>
			</div>
		</div>
		<div id="menu1" class="tab-pane fade">
			<!-- <h3>MEUS DADOS</h3>-->
			<div class="col-xs-12 col-md-12" style="background:#fff;margin-bottom:50px;">
				<br>
				<h4 class="text-center">Seus dados</h4>
				<div class="form-group text-center col-lg-6 col-xs-12">
					<label>Nome <font class="required">*</font></label>
					<input type="text" class="form-control text-center" id="nome" value="<?php echo @$user->nome; ?>" placeholder="Seu Nome" />
				</div>
				<div class="form-group text-center col-lg-6 col-xs-12">
					<label>E-mail <font class="required">*</font></label>
					<input type="text" class="form-control text-center" id="email" value="<?php echo @$user->email; ?>" placeholder="Seu E-mail" />
				</div>
				<div class="form-group text-center col-lg-6 col-xs-12">
					<label>CPF <font class="required">*</font></label>
					<input type="text" class="form-control text-center" id="cpf" value="<?php echo @$user->cpf; ?>" readonly />
				</div>
				<div class="form-group text-center col-lg-6 col-xs-12">
					<label>Senha (alterar) <font class="required"></font></label>
					<input type="password" class="form-control text-center" id="senha" placeholder="Nova senha" />
				</div>
				<div class="form-group text-center col-lg-6 col-xs-12">
					<label>Data de nascimento <font class="required">*</font></label>
					<input type="text" class="form-control text-center" id="data-nascimento" value="<?php echo @$user->dataNasc; ?>" />
				</div>
				<div class="form-group text-center col-lg-6 col-xs-12">
					<label>Sexo <font class="required">*</font></label>
					<div class="form-group">
						<input type="radio" id="sexo-fem" class="radio-sexo" name="sexo" radio-group="sexo" value="fem" <?php if (@$user->sexo == 'fem') : echo "checked"; endif; ?> /> Feminino
						<input type="radio" id="sexo-masc" class="radio-sexo" name="sexo" radio-group="sexo" value="mas" <?php if (@$user->sexo == 'mas') : echo "checked"; endif; ?> /> Masculino
					</div>
				</div>
				<div class="form-group text-center col-lg-6 col-xs-12">
					<label>Celular <font class="required">*</font></label>
					<input type="text" class="form-control text-center" id="celular" value="<?php echo @$user->celular; ?>" placeholder="Seu celular" />
				</div>
				<div class="form-group text-center col-lg-6 col-xs-12">
					<label>Telefone</label>
					<input type="text" class="form-control text-center" id="telefone" value="<?php echo @$user->telefone; ?>" placeholder="Seu telefone" />
				</div>
				<div class="form-group text-center col-lg-6 col-xs-12">
					<label>CEP <font class="required">*</font></label>
					<input type="text" class="form-control text-center" id="cep" value="<?php echo @$user->cep; ?>" placeholder="Seu CEP" />
				</div>
				<div class="form-group text-center col-lg-6 col-xs-12">
					<label>Número <font class="required">*</font></label>
					<input type="text" class="form-control text-center" id="numero" value="<?php echo @$user->numero; ?>" placeholder="Número Endereço" />
				</div>
				<div class="form-group text-center col-lg-6 col-xs-12">
					<label>Rua</label>
					<input type="text" class="form-control text-center" id="rua" value="<?php echo @$user->rua; ?>" placeholder="Rua" readonly />
				</div>
				<div class="form-group text-center col-lg-6 col-xs-12">
					<label>Bairro</label>
					<input type="text" class="form-control text-center" id="bairro" value="<?php echo @$user->bairro; ?>" placeholder="Bairro" readonly />
				</div>
				<div class="form-group text-center col-lg-6 col-xs-12">
					<label>Cidade</label>
					<input type="text" class="form-control text-center" id="cidade" value="<?php echo @$user->cidade; ?>" placeholder="Cidade" readonly />
				</div>
				<div class="form-group text-center col-lg-6 col-xs-12">
					<label>Estado</label>
					<input type="text" class="form-control text-center" id="uf" value="<?php echo @$user->uf; ?>" placeholder="Estado" readonly />
				</div>
				<br />
				<div class="form-group col-xs-12 text-center">
					<button class="btn btn-primary btn-large" id="btn-salvar-dados">Salvar</button>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>
<?php
include("widgets/footer.php");
