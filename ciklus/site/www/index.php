<?php
include("includes/settings.php");
include("action/index.php");
$scripts = "<script src='jsapp/index.js'></script>";
include("widgets/header.php");
?>
<div class="container-fluid home-top hidden-sm hidden-xs">
	<div class="row">
		<div class="carousel fade-carousel slide" data-ride="carousel" data-interval="4000" id="bs-carousel">
			<!--<ol class="carousel-indicators">
				<li data-target="#bs-carousel" data-slide-to="0" class="active"></li>
				<li data-target="#bs-carousel" data-slide-to="1"></li>
				<li data-target="#bs-carousel" data-slide-to="2"></li>
			</ol>-->
			<div class="carousel-inner">
				<div class="item slides active">
					<!-- <video autoplay loop muted class="overlay">
						<source src="video.mp4" alt="..." type="video/mp4" >
					</video> -->
					<video autoplay loop muted class="overlay">
						<?php
							$rand = rand(1, 3);
							switch ($rand) {
								case 1:?>
									<source src="video1.mp4" alt="..." type="video/mp4" >
								<?php     	break;
								case 2:  ?>
									<source src="video2.mp4" alt="..." type="video/mp4" >
								<?php     	break;
								case 3:  ?>
									<source src="video4.mp4" alt="..." type="video/mp4" >
								<?php     	break;
							}
						?>
					</video>


					<div class="slide-1">
						<div class="overlay"></div>
					</div>
					<div class="hero">
						<hgroup>
							<!-- <h1>CIKLUS</h1> -->
							<!-- <h2>Clube da Economia</h2> -->
							<h3><strong>Comece a economizar agora</strong></h3>
						</hgroup>
						<!-- <img src="assets/images/logo-branco.png" style="width:800px;"><br><br> -->
						<a href="cadastro" class="btn btn-primary btn-lg" role="button">ENTRE PARA O CLUBE</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid app visible-xs visible-sm" style="padding: 150px 0 30px!important;">
	<div class="container">
		<div class="col-md-8">
			<p class="app-titulo" style="font-size:3em;">BAIXE NOSSO APP</p>
			<div class="app-destaques">
				<p>Encontre os lugares próximos para economizar</p>
				<p>Sua economia registrada em cada utilização</p>
				<p>Fique por dentro das promoções</p>
				<p></br></p>
			</div>
			<div>
				<a href="https://play.google.com/store/apps/details?id=com.ionicframework.ciklus992232" class="btn btn-default app-botao" style="margin:10px 0"><i class="glyphicon glyphicon-play"></i> Play Storeee</a>
				<a href="https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=1271807606&mt=8" class="btn btn-default app-botao" style="margin:10px 0"><i class="fa fa-apple"></i> Apple Store</a>
			</div>
		</div>
		<div class="col-md-4 col-sx-12">
			<img src="assets/images/app.png" style="width:300px;height:auto;margin-top:20px;"/>
		</div>
	</div>
</div>
<div class="container-fluid muted">
	<div class="container">
		<div class="steps text-center">
		<h1 class="titulo-borda">Como funciona</h1>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-3 step-by-step">
					<i class="glyphicon glyphicon-thumbs-up"></i>
					<p>É fácil e rápido entrar para o primeiro clube da economia de Curitiba.</p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 step-by-step">
					<i class="glyphicon glyphicon-ok-circle"></i>
					<p>Economize no dia a dia, escolha o estabelecimento que queira usar no momento pelo nosso APP ou site.</p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 step-by-step">
					<i class="glyphicon glyphicon-phone"></i>
					<p>No local, identifique-se como sócio ciklus com seu CPF ou cartão digital.</p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 step-by-step">
					<i class="glyphicon glyphicon-heart-empty"></i>
					<p>Agora é só economizar em até 50%, conforme o estabelecimento.</p>
				</div>
			</div>
			<div class="row text-center visible-xs visible-sm">
			<a class="btn btn-large btn-primary" href="/cadastro.php">ENTRE PARA O CLUBE</a>
			</div>
		</div>
	</div>
</div>
<div class="container padding-secao">
	<div id="agenda">
		<div class="col-xs-12 text-center">
			<h1 class="titulo-borda">AGENDA DA SEMANA</h1>
		</div>
	</div>
	<br />
	<br />
	<div>
		<div class="col-xs-12 text-center" style="line-height: 80px;">
			<input type="hidden" id="dia-semana" value="<?php echo date('w'); ?>" />
			<span ciklus-dia="1" class="col-md-1 col-xs-3 col-centered agenda-span">Segunda</span>
			<span ciklus-dia="2" class="col-md-1 col-xs-3 col-centered agenda-span">Terça</span>
			<span ciklus-dia="3" class="col-md-1 col-xs-3 col-centered agenda-span">Quarta</span>
			<span ciklus-dia="4" class="col-md-1 col-xs-3 col-centered agenda-span">Quinta</span>
			<span ciklus-dia="5" class="col-md-1 col-xs-3 col-centered agenda-span">Sexta</span>
			<span ciklus-dia="6" class="col-md-1 col-xs-3 col-centered agenda-span">Sábado</span>
			<span ciklus-dia="0" class="col-md-1 col-xs-3 col-centered agenda-span">Domingo</span>
		</div>
	</div>
	<br /><br /><br />
	<div id="agenda-do-dia-loader" style="display:none;min-height: 500px;">
		<div class="col-xs-12 text-center">
			<img src="assets/images/loader.gif" alt="" style="margin-top: 50px;">
		</div>
	</div>
	<div id="agenda-do-dia-conteudo">
		<div class="col-xs-12">
			<div id="conteudo-categoria-agenda">
			</div>
		</div>
		<div class="col-xs-12">
			<div>
				<div class="col-lg-12">
					<h2 class="text-center">Eventos</h2>
					<div>
						<?php
						if (count($eventos) < 1) {
							echo "<div class='col-lg-12'><h3 class='text-center'>Nenhum evento agendado.</h3></div>";
						}

						foreach ($eventos as $e) :
							$data = explode('-', $e->data);
							$data = $data[2] . '/' . $data[1] . '/' . $data[0]; ?>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="thumbnail">
								<div class="discount-tag"><span class="oferta-tag-texto">Economize</span><br>
									<span class="oferta-tag-desconto">
										<?php if($e->desconto == 0){
											echo '$';
										}else{
											echo $e->desconto + '%';	
										} ?>
									</span>
								</div>
								<a href="eventos#<?php echo $e->id; ?>">
									<img src="<?php echo $urlImagens . $e->foto . '.' . $e->extensao; ?>" data-holder-rendered="true" class="home-thumbnail" style="display: block;">
								</a>
								<div class="caption col-xs-12">
									<p class="evento-titulo"><?php echo $e->titulo; ?></p>
									<p class="evento-data"><?php echo $data; ?></p>
									<!-- <p class="evento-informacoes"><?php echo $e->politica; ?></p> -->
									<a href="evento?id=<?php echo $e->id; ?>" class="btn btn-default oferta-botao col-xs-8 col-xs-offset-2" role="button">Mais Informações</a>
								</div>
							</div>
						</div>
						<?php
						endforeach;

						if (count($eventos) > 0) {
							echo "<div class='col-lg-12 text-center'><br /><a href='eventos' class='botao-todos' style='padding-bottom:15px;'><i class='glyphicon glyphicon-arrow-right'></i>  Ver todas as opções de economia</a></div>";
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br />
	<br />
</div>
<div class="container-fluid app hidden-sm hidden-xs">
	<div class="container">
		<div class="col-md-8 col-xs-12">
			<p class="app-titulo">BAIXE NOSSO APP</p>
			<div class="app-destaques">
				<p>Encontre os lugares próximos para economizar</p>
				<p>Sua economia registrada em cada utilização</p>
				<p>Fique por dentro das promoções</p>
				<p></br></p>
			</div>
			<div class="col-xs-12">
				<a href="https://play.google.com/store/apps/details?id=com.ionicframework.ciklus992232" class="btn btn-default app-botao" style="margin:10px 0"><i class="glyphicon glyphicon-play"></i> Play Storeee</a>
				<a href="https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=1271807606&mt=8" class="btn btn-default app-botao" style="margin:10px 0"><i class="fa fa-apple"></i> Apple Store</a>
			</div>
		</div>
		<div class="col-md-4 col-sx-12">
			<img src="assets/images/app.png" style="width:400px;height:auto;"/>
		</div>
	</div>
</div>
<div class="container-fluid depoimentos">
	<div class="container">
		<br>
		<div class="col-md-6 col-xs-12">
			<div class="jumbotron depoimento-texto-1">
				<div class="depoimento-overlay">
					<h3 style="color:#fff;">Porque para viver mais,<br>você não precisa gastar mais</h3>
					<a href="cadastro" class="btn btn-primary btn-lg" role="button">Comece já</a>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-xs-12">
			<div class="jumbotron depoimento-texto-2">
				<div class="depoimento-overlay">
					<h3 style="color:#fff;">Mais economia para<br>você e seus amigos</h3>
					<a href="cadastro" class="btn btn-primary btn-lg" role="button">Planos</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include("widgets/footer.php"); ?>
