<?php
include("includes/settings.php");
include("action/painel-estabelecimento.php");
$scripts = "<script src='jsapp/painel-estabelecimento.js'></script>";
include("widgets/header.php");
?>
<style>
.pre-validacao {
	display: none;
}
</style>

<div class="container">
	<div class="row text-center" style="margin-top: 100px;margin-bottom:20px;">
		<div class="col-xs-12">
			<h3><strong>Bem vindo <?php echo $estabelecimento->nome; ?></strong></h3>
		</div>
	</div>
<!-- 	<div class="row">
		<div class="col-xs-12 text-center">
			<button class="btn btn-default" href="#modal-novidade" data-toggle="modal">Postar novidade</button>
		</div>
	</div>
	<hr> -->
	<div class="row text-center">
		<div class="panel panel-default">
  			<div class="panel-body text-center">
  				<div class="row">
	  				<div class="col-lg-offset-1 col-lg-10 col-xs-12">
	  					<div class="col-xs-12">
	  						<h4>Registrar Lucro / Economia</h4>
	  					</div>
						<div class="form-group text-center col-lg-4 col-xs-12">
							<label>CPF do Associado</label>
							<input type="text" class="form-control text-center" id="cpf-socio" placeholder="CPF do associado" />
							<input type="hidden" id="hdn_socio" value="false" />
							ou
							<br>
						<!-- </div> -->
						<!-- <div class="form-group text-center col-lg-4 col-xs-12"> -->
							<label>Código do Associado</label>
							<input type="text" class="form-control text-center" id="codigo-socio" placeholder="Código do associado" />
							<input type="hidden" id="hdn_socio" value="false" />
						</div>
						<div class="form-group text-left col-lg-4 col-xs-12 validar-socio" style="padding-top:25px;">
							<button class="btn btn-success" id="validar-socio">Validar Associado</button>
						</div>
						<div class="form-group text-center col-lg-4 col-xs-12 pre-validacao">
							<label>Valor Total</label>
							<input type="text" class="form-control text-center" id="valor-desconto" placeholder="Valor da compra sem desconto" />
						</div>
						<div class="form-group text-center col-lg-4 col-xs-12 pre-validacao">
							<label>Promoção</label>
							<select class="form-control" id="promocao">
								<?php foreach($promocoes as $promo) { ?>
								<option value="<?php echo $promo->id; ?>"><?php echo $promo->desconto . "%" . " - " . $promo->titulo; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group col-xs-12 text-center pre-validacao">
							<button class="btn btn-default" id="btn-cancelar-pgto">Cancelar</button>
							<button class="btn btn-primary pre-validacao" id="btn-salvar-pgto">Salvar</button>
						</div>
	  				</div>
	  			</div>
			</div>
		</div>
	</div>
	<div class="row text-center">
		<div class="panel panel-default">
  			<div class="panel-body text-center">
  				<div class="row">
  					<div class="col-xs-12">
  						<h4>Histórico</h4>
  					</div>
  				</div>
  				<div class="row">
	  				<div class="col-lg-offset-1 col-lg-10 col-xs-12">
	  					<table class="table table-stripped">
	  						<thead>
	  							<tr>
	  								<th class="text-center">Data</th>
	  								<th class="text-center">Promoção</th>
	  								<th class="text-right">Valor Total</th>
	  								<th class="text-right">Desconto</th>
	  								<th class="text-right">Valor Final</th>
	  							</tr>
	  						</thead>
	  						<tbody id="transacoes">
	  							<?php
	  								$valorTotal = 0;
	  								foreach ($transacoes as $t) :
	  									$data = explode("-", $t->createdAt);
	  									$valorTotal = $valorTotal + ($t->valor - $t->valorDesconto);
	  							?>
	  							<tr>
	  								<td><?php echo substr($data[2], 0, 2) . '/' . $data[1] . '/' . $data[0]; ?></td>
	  								<td class="text-center"><?php echo $t->promocao->titulo; ?></td>
	  								<td class="text-right">R$ <?php echo number_format($t->valor, 2, ",", "."); ?></td>
	  								<td class="text-right">R$ <?php echo number_format($t->valorDesconto, 2, ",", "."); ?></td>
	  								<td class="text-right">R$ <?php echo number_format($t->valor - $t->valorDesconto, 2, ",", "."); ?></td>
	  							</tr>
	  							<?php endforeach; ?>
	  						</tbody>
	  					</table>
	  				</div>
	  			</div>

	  			<div class="conta-desconto">Seu lucro total com a Ciklus: R$ <strong><?php echo number_format($valorTotal, 2, ",", "."); ?></strong>
	  			</div>
				<br>

	  		</div>
	  	</div>
	  </div>
</div>

<?php
include('widgets/modal-novidade.php');
include('widgets/footer.php');
?>
