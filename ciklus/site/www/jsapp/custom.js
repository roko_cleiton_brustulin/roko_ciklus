function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

$(document).ready(function() {
	$("#login-cpf").mask('999.999.999-99');

	$("#login-senha").keypress(function(e) {
	    if(e.which == 13) {
	        $("#btn-login").trigger('click');
	    }
	});

	$("#senha-estabelecimento").keypress(function(e) {
	    if(e.which == 13) {
	        $("#btn-login-estabelecimento").trigger('click');
	    }
	});

	$("#btn-email-newsletter").click(function() {
		var email = $("#email-newsletter").val();
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  		
		if (re.test(email)) {
			$.ajax({
				url: 'action/ajax/salvar-newsletter.php',
				data: {email: email},
				type: 'POST',
				success: function(data) {
					swal('Sucesso', 'Obrigado, seu e-mail será adicionado à nossa newsletter.', 'success');
				}
			});
		} else {
			swal('Ops', 'Esse e-mail é inválido.', 'warning');
		}
	});

	$("#btn-login").click(function(){
		var cpf = $("#login-cpf").val();
		var senha = $("#login-senha").val();

		$.ajax({
			url: 'action/ajax/login-socio.php',
			data: { cpf: cpf, senha: senha },
			type: "POST",
			success: function(data) {
				console.log(data);
				data = JSON.parse(data);
				data = data[0];
				if (data) {
					$.cookie('usuario-ciklus', JSON.stringify(data));
					window.location = "minha-conta.php";
				} else {
					swal('Ops', 'Sócio não encontrado, verifique os dados.', 'warning');
				}
			}
		});
	});

	$("#entre-para-o-clube").click(function(){
		window.location = "cadastro.php";
	});

	$("#btn-logout").click(function() {
		$.removeCookie('usuario-ciklus', { path: '*' });
		setCookie('estabelecimento-ciklus', '', -1);
		window.location = 'index.php';
	});

	$("#acessar-login-estabelecimento").click(function() {
		$(".login-usuario").hide();
		$("#acessar-login-estabelecimento").hide();
		$(".login-estabelecimento").show();
		$("#acessar-login-usuario").show();
		return false;
	});

	$("#acessar-login-usuario").click(function() {
		$(".login-estabelecimento").hide();
		$("#acessar-login-estabelecimento").show();
		$("#acessar-login-usuario").hide();
		$(".login-usuario").show();
	});

	$("#btn-login-estabelecimento").click(function() {
		var login = $("#login-estabelecimento").val();
		var senha = $("#senha-estabelecimento").val();

		if (login.length > 0 && senha.length > 0) {
			$.ajax({
				url: 'action/ajax/login-estabelecimento.php',
				data: { login: login, senha: senha },
				type: 'POST',
				success: function(data) {
					if (data && data != "[]") {
						data = JSON.parse(data);
						setCookie('estabelecimento-ciklus', data[0].id, 15);
						window.location = "painel-estabelecimento.php";
					} else {
						swal("Login/senha inválido(s).");
					}
				}
			});
		} else {
			swal('Ops!', 'Você precisa preencher todos os campos para continuar.', 'warning');
		}
	});
});
