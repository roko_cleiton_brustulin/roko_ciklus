function validarEmail(email){
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
 
  var input = document.createElement('input');
 
  input.type = 'email';
  input.value = email;
 
  return typeof input.checkValidity == 'function' ? input.checkValidity() : re.test(email);
}

$(document).ready(function() {
	$("#email").blur(function() {
		var email = $(this).val();
		if (!validarEmail(email)) {
			swal('Ops', 'O e-mail digitado não é válido', 'warning');
			$("#email").val();
		} else {
			$.ajax({
					url: 'action/ajax/verificar-email.php',
					data: { email: email },
					type: 'POST',
					success: function(data) {
						if (data == "f") {
							swal('Ops', 'Esse e-mail já está cadastrado em outra conta', 'error');
							$("#email").val('');
						}
					}
				});
		}
	});

	$("#confirmar-conta").click(function() {
		var senha = $("#senha").val();
		var email = $("#email").val();
		if ($("#senha").val().length > 4) {
			if ($("#senha").val() == $("#confirmar-senha").val()) {
				if ($("#email").val().length > 0) {
					$.ajax({
						url: 'action/ajax/confirmar-conta.php',
						type: 'POST',
						data: { email: email, senha: senha },
						success: function(data) {
							swal({
							  	title: "Parabéns!",
							  	text: "Você agora é um associado Ciklus. <span style='font-weight: bold;'>Algumas dicas rápidas:</span><br><ul><li>Para procurar um estabelecimento, clique no link Estabelecimentos.</li><li><a href='#'>Não esqueça de baixar nosso app.</a></li><li>Quando for pagar a conta em um estabelecimento parceiro, informe que é um associado Ciklus e informe seu CPF para economizar.</li></ul>.",
							  	html: true,
							  	type: 'success',
							  	showCancelButton: false,
							  	confirmButtonText: "Entendi!",
  								closeOnConfirm: false
							},
							function(){
							  window.location="index.php";
							});
						}
					});
				}
			} else {
				swal('Ops', 'A senha e a confirmação de senha não coincidem', 'warning');	
			}
		} else {
			swal('Ops', 'Sua senha deve ter mais de 4 caracteres', 'warning');
		}
	});

});