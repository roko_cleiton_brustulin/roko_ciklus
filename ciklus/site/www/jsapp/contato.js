$(document).ready(function(){
	$("#btn-enviar").click(function() {
		var nome = $("#nome").val();
		var email = $("#email").val();
		var msg = $("#mensagem").val();

		if (nome && email && msg) {
			$.ajax({
				url: 'action/ajax/enviar-contato.php',
				data: { nome: nome, email: email, mensagem: msg },
				type: 'POST',
				success: function(data) {
					swal('Recebemos a sua mensagem', 'Responderemos em breve', 'success');
					setTimeout(function() {
						window.location = 'index.php';
					}, 2000);
				}
			});
		} else {
			swal('Ops', 'Por favor, preencha todos os campos', 'warning');
		}
	});
});