function validarCPF(cpf) {
    var soma;
    var resto;
    soma = 0;
	if (cpf == "00000000000")
		return false;

	for (i=1; i<=9; i++)
		soma = soma + parseInt(cpf.substring(i-1, i)) * (11 - i);

	resto = (soma * 10) % 11;

    if ((resto == 10) || (resto == 11))
    	resto = 0;
    if (resto != parseInt(cpf.substring(9, 10)) )
    	return false;

	soma = 0;
    for (i = 1; i <= 10; i++)
    	soma = soma + parseInt(cpf.substring(i-1, i)) * (12 - i);
    resto = (soma * 10) % 11;

    if ((resto == 10) || (resto == 11))
    	resto = 0;
    if (resto != parseInt(cpf.substring(10, 11) ) )
    	return false;
    return true;
}

function validarEmail(email){
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  return re.test(email);
}

var SPMaskBehavior = function (val) {
		return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	},
	spOptions = {
		onKeyPress: function(val, e, field, options) {
			field.mask(SPMaskBehavior.apply({}, arguments), options);
		}
	};


$(document).ready(function(){
	$("#cpf").mask('999.999.999-99');
	$("#cep").mask('99999-999');
	$("#data-nascimento").mask('99/99/9999');
	$("#telefone").mask(SPMaskBehavior, spOptions);
	$("#checkout-cartao").mask('9999999999999999');

	$("#email").blur(function() {
		if (!validarEmail($(this).val())) {
			swal('Ops', 'O e-mail digitado é inválido.', 'warning');
			$("#email").val('');
		}
	});

	$("#cep").blur(function() {
		if ($("#cep").val()) {
			var cep = $("#cep").val().replace("-", "");
			$.ajax({
				url: 'https://viacep.com.br/ws/' + cep + '/json/ ',
				type: 'GET',
				success: function(data) {
					if (data) {
						$("#rua").val(data.logradouro);
						$("#bairro").val(data.bairro);
						$("#cidade").val(data.localidade);
						$("#estado").val(data.uf);
					} else {
						swal('Ops', 'O cep digitado é inválido.', 'warning');
						$("#cep").val('');
					}
				}
			});
		}
	});

	$("#voltar-passo-2").click(function() {
		$("#passo-3").hide();
		$("#passo-2").show();
	});

	$("#voltar-passo-3").click(function() {
		$("#passo-4").hide();
		$("#passo-3").show();
	});

	$("#ir-passo-3").click(function(){
		var nome 	= $("#nome").val();
		var email 	= $("#email").val();
		var cpf		= $("#cpf").val();
		var fone 	= $("#telefone").val();
		var dataNasc = $("#data-nascimento").val();
		var sexo	= $(".radio-sexo:checked").val();
		var termos  = $("#termos").is(":checked");

		if (termos == true) {
			if (nome.length > 0 && email.length > 0 && cpf.length == 14 && fone.length > 0 && dataNasc.length > 0 && (sexo == 'fem' || sexo == 'mas')) {
				if (validarCPF(cpf.replace('.', '').replace('.', '').replace('.', '').replace('-', ''))) {
					$.ajax({
						url: 'action/ajax/buscar-cpf.php',
						data: { validar: cpf, email: email },
						type: 'POST',
						success: function(data) {
							if (data == "t") {
								$.ajax({
									url: 'action/ajax/cadastrar-socio.php',
									data: { nome: nome, email: email, cpf: cpf, fone: fone, data: dataNasc, sexo: sexo },
									type: 'POST',
									success: function (data) {
										var socio = $.parseJSON(data);
										$("#hdn_socio_id").val(socio['id']);
										$("#passo-2").hide();
										$("#passo-3").show();
									}
								});
							} else if (data == "f") {
								swal({   title: "Ops!", type: 'warning',  text: "Você já é um sócio Ciklus (CPF já cadastrado).", confirmButtonText: 'Acessar' }, function() {
									$("#login").modal();
									$("#login-cpf").val(cpf);
								});
							} else {
								swal({   title: "Ops!", type: 'warning',  text: "Você já é um sócio Ciklus (E-mail já cadastrado).", confirmButtonText: 'Acessar' }, function() {
									$("#login").modal();
								});
							}
						}
					});
				} else {
					swal('Ops!', 'O CPF é inválido.', 'warning');
				}
			} else {
				swal('Ops!', 'Você precisa preencher todos os campos para continuar.', 'warning');
			}
		} else {
			swal('Ops!', 'Você precisa aceitar os termos para fazer parte da Ciklus.', 'warning');
		}
	});

	$("#ir-passo-4").click(function(){
		var cep = $("#cep").val();
		var rua = $("#rua").val();
		var numero = $("#numero").val();
		numero = numero + ' ' + $("#complemento").val();
		var bairro = $("#bairro").val();
		var cidade = $("#cidade").val();
		var estado = $("#estado").val();
		var socio = $("#hdn_socio_id").val();

		if (cep.length >= 8 && numero.length > 0) {
			if (bairro.length > 0 && rua.length > 0 && cidade.length > 0) {
				$.ajax({
					url: 'action/ajax/atualizar-socio-auxiliar.php',
					data: { cep: cep, rua: rua, numero: numero, bairro: bairro, cidade: cidade, estado: estado, socio: socio },
					type: 'POST',
					success: function(data) {
						$("#passo-3").hide();
						$("#passo-4").show();
					}
				});
			} else {
				swal('Ops!', 'O CEP é inválido.', 'warning');
			}
		}
	});

	$("#plano-trial").click(function() {
		var socio = $("#hdn_socio_id").val();

		$.ajax({
			url: 'action/ajax/cadastrar-trial.php',
			data: { id: socio },
			type: 'POST',
			success: function (data) {
				$.cookie('usuario-ciklus', data);
				swal('Eba!', 'Você está muito perto de começar a economizar com o seu Ciklus! Falta só terminar de preencher seus dados.', 'success');
				window.location = "minha-conta.php";
			}
		});
	});

	$(".btn-selecionar-plano").click(function() {
		var socioId = $("#hdn_socio_id").val();
		$("#hdn_plano_id").val($(this).attr('ciklus-plano'));

		$("#passo-1").hide();
		$("#passo-2").show();
	});

	$("#voltar-passo-1").click(function() {
		$("#hdn_voucher_id").val('');
		$("#comecar-voucher").hide();
		$("#passo-1").show();
		$("#passo-2").hide();
		$("#ir-passo-3").show();
	});

	$("#btn-enviar-voucher").click(function() {
		var nome = $("#nome-voucher").val();
		var email = $("#email-voucher").val();
		var msg = 'Solicitação de convite via site';
		if (email) {
			// $.ajax({
			// 	url: 'action/ajax/enviar-solicitacao-voucher.php',
			// 	data: { nome: nome, email: email, tel: tel },
			// 	type: 'POST',
			// 	success: function(data) {
			// 		swal('Recebemos a sua solicitação', 'Seu  nome já está na nossa lista de espera. Agora é só aguardar! Obrigado pelo interesse na Ciklus live - Clube da economia', 'success');
			// 	}
			// });
			// $.ajax({
			// 	url: 'action/ajax/enviar-retorno-voucher.php',
			// 	data: { nome: nome, email: email},
			// 	type: 'POST'
			// });

			$.ajax({
				url: 'action/ajax/enviar-solicitacao-voucher.php',
				data: { nome: nome, email: email, msg: msg },
				type: 'POST',
				success: function(data) {
					swal('Recebemos a sua solicitação', 'Seu  nome já está na nossa lista de espera. Agora é só aguardar! Obrigado pelo interesse na Ciklus live - Clube da economia', 'success');
				}
			});
		} else {
			swal('Ops', 'Por favor, preencha o e-mail ou o telefone para contato', 'warning');
		}
	});

	$("#validar-voucher").click(function() {
		var voucher = $("#codigo-voucher").val();
		if (voucher.length > 0) {
			$.ajax({
				url: 'action/ajax/validar-voucher.php',
				data: { voucher: voucher },
				type: 'POST',
				success: function(resultado) {
					if (!resultado) {
						swal('Código inválido', 'Código de convite inválido', 'warning');
					} else {
						var cupom = JSON.parse(resultado);
						var expiracao = cupom.expiracao.split('-');
				        var expiracaoObj = new Date(expiracao[0], expiracao[1], expiracao[2].substring(0, 2));
			        	var hoje = new Date();
			        	if (expiracaoObj.valueOf() < hoje.valueOf()) {
			        		swal('Código expirado', 'Esse código está expirado.', 'error');
			        	} else {
			        		if (cupom.utilizado) {
			        			swal('Código já utilizado', 'Esse código já foi utilizado.', 'error');
			        		} else {
			        			swal('Parabéns', 'O código que você digitou é válido e dá direito a ' + cupom.plano.frequencia + ' dias grátis. Continue preenchendo os seus dados para fazer parte da Ciklus agora mesmo.', 'success');
			        			$("#hdn_voucher_id").val(cupom.id);
			        			$("#passo-1").hide();
			        			// $("#voltar-passo-1").hide();
			        			$("#ir-passo-3").hide();
			        			$("#comecar-voucher").show();
								$("#passo-2").show();

			        		}
			        	}
					}
				}
			});
		} else {
			swal('Ops', 'Preencha o código do convite', 'warning');
		}
	});

	$("#comecar-voucher").click(function() {
		var nome 	= $("#nome").val();
		var email 	= $("#email").val();
		var cpf		= $("#cpf").val();
		// var fone 	= $("#telefone").val();
		// var dataNasc = $("#data-nascimento").val();
		var cep 	= $("#cep").val();
		var sexo	= $(".radio-sexo:checked").val();
		var termos  = $("#termos").is(":checked");

		if (termos == true) {
			if (nome.length > 0 && email.length > 0 && cpf.length == 14 && cep.length == 9 && (sexo == 'fem' || sexo == 'mas')) {
				if (validarCPF(cpf.replace('.', '').replace('.', '').replace('.', '').replace('-', ''))) {
					var voucherId = $("#hdn_voucher_id").val();

					$.ajax({
						url: 'action/ajax/buscar-cpf.php',
						data: { validar: cpf, email: email },
						type: 'POST',
						success: function(data) {
							if (data == "t") {
								$.ajax({
									url: 'action/ajax/validar-voucher-id.php',
									data: { voucher: voucherId },
									type: 'POST',
									success: function(resultado) {
										if (!resultado) {
											swal('Código inválido', 'Código de convite inválido', 'warning');
										} else {
											var cupom = JSON.parse(resultado);
											var hoje =  new Date();
								            hoje.setDate(hoje.getDate() + cupom.plano.frequencia);
								            var mes = hoje.getMonth() + 1;
								            var dia = hoje.getDate();
								            var ano = hoje.getFullYear();
								            if (mes.length < 2) mes = '0' + mes;
								            if (dia.length < 2) dia = '0' + dia;
								            var ativoAte = [ano, mes, dia].join('-');
								   			$.ajax({
												url: 'action/ajax/cadastrar-socio-voucher.php',
												// data: { nome: nome, email: email, cpf: cpf, fone: fone, data: dataNasc, sexo: sexo, ativoAte: ativoAte, voucher: cupom.id },
												data: { nome: nome, email: email, cpf: cpf, cep: cep, sexo: sexo, ativoAte: ativoAte, voucher: cupom.id },
												type: 'POST',
												success: function (data) {
													$.cookie('usuario-ciklus', data);
													var socio = $.parseJSON(data);
													setTimeout(function() {
											  			window.location = "confirmar-conta.php";
											  		}, 1000);
												}
											});
										}
									}
								});
							} else if (data == "f") {
								swal({   title: "Ops!", type: 'warning',  text: "Você já é um sócio Ciklus (CPF já cadastrado).", confirmButtonText: 'Acessar' }, function() {
									$("#login").modal();
									$("#login-cpf").val(cpf);
								});
							} else {
								swal({   title: "Ops!", type: 'warning',  text: "Você já é um sócio Ciklus (E-mail já cadastrado).", confirmButtonText: 'Acessar' }, function() {
									$("#login").modal();
								});
							}
						}
					});
				} else {
					swal('Ops!', 'O CPF é inválido.', 'warning');
				}
			} else {
				swal('Dados inválidos', 'Por favor, verifique os dados preenchidos.', 'warning');
			}
		} else {
			swal('Termos de uso', 'Você precisa aceitar os termos de uso da Ciklus.', 'warning');
		}
	});

	$("#iniciar-ciklus").click(function() {
		var plano = $("#hdn_plano_id").val();
		var socioId = $("#hdn_socio_id").val();

		var nomecompleto = $("#checkout-nome").val();
		nome = nomecompleto.split(' ')[0];
		var sobrenome = nomecompleto.split(' ')[1];
		var numero = $("#checkout-cartao").val();
		var cvv = $("#checkout-cvv").val();
		var mes = $("#checkout-mes").val();
		var ano = $("#checkout-ano").val();

		if (nome.length > 0 && numero.length == 16 && (cvv.length == 3 || cvv.length == 4) && mes.length == 2 && ano.length == 2) {
			$(".conteudo").hide();
			$("#loader").show();

			//Pagar.me
			var card = {};
	        card.card_holder_name = nomecompleto;
	        card.card_expiration_date = mes + '/' + ano;
	        card.card_number = numero;
	        card.card_cvv = cvv;

	        // pega os erros de validação nos campos do form e a bandeira do cartão
	        var cardValidations = pagarme.validate({card: card})
					
	        //Então você pode verificar se algum campo não é válido
	        if(!cardValidations.card.card_number || !cardValidations.card.card_cvv) {
	        	swal("Ops!", "Houve um problema ao efetuar o pagamento, por favor, verifique os dados do cartão.", "error");
				$("#loader").hide();
				$(".conteudo").show();
	        } else {
	          	$.ajax({
					url: 'action/ajax/associar-cliente.php',
					type: 'POST',
					data: { plano: plano, socio: socioId, nome: nome,
							sobrenome: sobrenome, numero: numero,
							cvv: cvv, mes: mes, ano: ano },
					success: function(data) {
						var ret = JSON.parse(data);
						if (ret['resultado']) {
							swal({
							  title: "Parabéns",
							  text: "Você agora é um sócio Ciklus, termine de preencher seus dados e comece a economizar!",
							  type: "success",
							  confirmButtonColor: "#D31E86",
							  confirmButtonText: "Ok!",
							  closeOnConfirm: false
							},
							function(){
						  		$.cookie('usuario-ciklus', JSON.stringify(ret['cliente']));
						  		setTimeout(function() {
						  			window.location = "confirmar-conta.php";
						  		}, 1000);
							});
						} else {
							swal("Ops!", "Houve um problema ao efetuar o pagamento, por favor, tente novamente.", "error");
							$("#loader").hide();
							$(".conteudo").show();
						}
					}
				});
	        }
		} else {
			swal('Ops', 'Preencha todos os dados de pagamento', 'warning');
		}
	});
});
