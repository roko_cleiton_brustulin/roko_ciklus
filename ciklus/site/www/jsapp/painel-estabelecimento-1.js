$(document).ready(function() {
	$("#cpf-socio").mask("999.999.999-99");

	// $('#valor-desconto').mask("0,00");
	$('#valor-desconto').maskMoney({
		prefix: 'R$ ',
		decimal: ",",
		thousands: "."
	});
	// $('#valor-desconto').maskMoney();
	
	$('#postar').click(function() {
		$("#modal-novidade").modal();
	});

	$("#cpf-socio").keypress(function(e) {
		if (e.which == 13) {
	        $("#validar-socio").trigger('click');
	    }
	});

	$("#codigo-socio").keypress(function(e) {
		if (e.which == 13) {
	        $("#validar-socio").trigger('click');
	    }
	});

	$("#validar-socio").click(function() {
		var cpf = $("#cpf-socio").val();
	    cpf = cpf.replace( /\D/g , ""); //Remove tudo o que não é dígito
	    cpf = cpf.replace( /(\d{3})(\d)/ , "$1.$2"); //Coloca um ponto entre o terceiro e o quarto dígitos
	    cpf = cpf.replace( /(\d{3})(\d)/ , "$1.$2"); //Coloca um ponto entre o terceiro e o quarto dígitos
	    cpf = cpf.replace( /(\d{3})(\d{1,2})$/ , "$1-$2"); //Coloca um hífen entre o terceiro e o quarto dígitos

		var codigo = $("#codigo-socio").val();
		
		if (cpf.length == 14 || codigo.length > 1) {
			$.ajax({
				url: 'action/ajax/validar-cpf.php',
				type: 'POST',
				data: { cpf: cpf, codigo: codigo },
				success: function(data) {
					if (data == "true") {
						if(cpf == ''){
							$("#hdn_socio").val(codigo);
						} else {
							$("#hdn_socio").val(cpf);
						};
						$(".pre-validacao").show();
						$(".validar-socio").hide();
						$("#cpf-socio").attr('readonly', 'readonly');
						$("#codigo-socio").attr('readonly', 'readonly');
						$("#valor-desconto").focus();
					} else if (data == "false") {
						swal("A assinatura deste usuário está expirada.");
					} else if (data == "semassinatura") {
						swal("Conta de usuário bloqueada.");
					} else if (data == "invalido") {
						swal("Sócio não encontrado.");
					} else {
						swal("Sócio não encontrado.");
					}
				}
			});
		} else {
			swal('Por favor, preencha o CPF completo.');
		}
	});

	$("#btn-cancelar-pgto").click(function() {
		$("#cpf-socio").val('');
		$("#cpf-socio").removeAttr('readonly');
		$("#codigo-socio").val('');
		$("#codigo-socio").removeAttr('readonly');
		$("#hdn_socio").val('');
		$(".validar-socio").show();
		$(".pre-validacao").hide();
	});

	$("#btn-salvar-pgto").click(function() {
		var valor = $("#valor-desconto").val();
		var promocao = $("#promocao").val();

		if ($("#hdn_socio").val() != "socio") {
			// var rep = valor.substring(0, 2);
			// valor = valor.replace(rep, '');
			valor = valor.replace('.', '');
			valor = valor.replace(',', '.');
			console.log(valor);
			if (valor > 0) {
				$.ajax({ 
					url: 'action/ajax/salvar-transacao.php',
					data: { socio: $("#hdn_socio").val(), valor: valor, promocao: promocao },
					type: 'POST',
					success: function(data) {
						var transacao = JSON.parse(data);
						valor_final	= valor - transacao['valorDesconto'];
						swal({
							title: 'Transação Salva',
							text: 'Desconto: ' + transacao['desconto'] + '% - R$ ' + transacao['valorDesconto'].toFixed(2).replace('.', ',') + '\r\n\r\nValor Final: R$ ' + valor_final.toFixed(2).replace('.', ','),
							type: 'success',
						}, function(isConfirm) {
						     location.reload();
						});
					}
				});
			} else {
				swal('Ops', 'Valor inválido.', 'warning');
			}
		} else {
			swal("Sócio inválido.");
		}
	});

	$("#btn-postar-novidade").click(function() {
		var formdata = new FormData($("form[name='form-novidade']")[0]);
		$(this).text('Postando...');
		$.ajax({
			url: 'action/ajax/postar-novidade.php',
			data: formdata,
	        processData: false,
	        contentType: false,
	        type: 'POST',
	        success: function(data) {
	        	swal({
	        		title: 'Novidade postada',
	        		text: 'A sua novidade foi postada com sucesso!',
	        		type: 'success'
	        	}, function() {
	        		$("#modal-novidade").modal('hide');
	        		$(this).text('Postar');
	        	});
	        }
		})

		return false;
	});
});