$(document).ready(function() {
	$("#cpf").mask("999.999.999-99");

	$("#criar-senha").click(function() {
		var cpf = $("#cpf").val();
		var email = $("#email").val();

		if (cpf && email) {
			$.ajax({
				url: 'action/ajax/criar-senha.php',
				data: { cpf: cpf, email: email },
				type: 'POST',
				success: function(data) {
					if (data) {
						$("#hdn_socio").val(data);
						swal({   
							title: "Pronto!", 
							type: 'success',  
							text: "Uma senha provisória foi enviada ao seu e-mail.", 
							confirmButtonText: 'Ok' },
						function() {
							$("#login").modal();
						});
					} else {
						swal('Ops', 'Sócio não encontrado', 'warning');
					}
				}
			});
		}
		return false;
	});
});