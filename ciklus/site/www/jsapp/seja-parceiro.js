$(document).ready(function() {
	var SPMaskBehavior = function (val) {
		return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	},
	spOptions = {
		onKeyPress: function(val, e, field, options) {
			field.mask(SPMaskBehavior.apply({}, arguments), options);
		}
	};

	$("#fone").mask(SPMaskBehavior, spOptions);
	$("#cep").mask('99999-999');

	$("#enviar").click(function() {
		var nome = $("#nome").val();
		var email = $("#email").val();
		var fone = $("#fone").val();
		var categoria = $("#categoria").val();
		var estabelecimento = $("#estabelecimento").val();
		var cep = $("#cep").val();
		var complemento = $("#complemento").val();
		var fb = $("#facebook-site").val();

		if (nome && email && fone && estabelecimento && cep && fb && categoria) {
			$.ajax({
				url: 'action/ajax/seja-parceiro.php',
				type: 'POST',
				data: { nome: nome, email: email, fone: fone, estabelecimento: estabelecimento, cep: cep, fb: fb, complemento: complemento, categoria: categoria },
				success: function(data) {
					swal("Obrigado por entrar em contato.", "Retornaremos o mais breve possível.", "success");
				}
			});
		} else {
			swal("Todos os campos são obrigatórios");
		}
	});
});