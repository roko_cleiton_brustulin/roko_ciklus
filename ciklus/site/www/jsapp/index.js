var agendaCat = 21;

function popularCategoria(categoria) {
	$("#conteudo-categoria-agenda").append('<div class="row"><div class="col-xs-12 text-center"><h2>' + categoria + '</h2></div>');
}

function popularWidget(content, dia, categoriaObj) {
	var categoria = 0;
	var desconto = "";
	if (content) {
		content.forEach(function(x, y) {
			categoria = x.categoria;
			var html = '<div class="col-md-4 col-sm-6 col-xs-12">';
			if(x.desconto == 0){
				desconto = "$";
			}else{
				desconto = x.desconto + "%";
			};
			html +=	' <div class="discount-tag"><span class="oferta-tag-texto">Economize</span><br><span class="oferta-tag-desconto">' + desconto + '<span></div>';
			html += '<a href="estabelecimento.php?id=' + x.id + '"><img src="http://admin.cikluslive.com.br/uploads/' + x.fotoid + '.' + x.fotoextensao + '" data-holder-rendered="true" class="home-thumbnail" style="display: block;"></a> ';
			html += '<div class="caption"> ';
			html +=	'<p class="oferta-estabelecimento">' + x.nome + '</p> ';
			html += '<p class="oferta-titulo">' + x.titulo + '</p> ';
			if (x.socioMais > 0) {
				html += '<div class="oferta-informacoes"><span class="oferta-socio">Sócio + ' + x.socioMais + '</span><br><span class="oferta-horario">' + x.horario1 + '-' + x.horario2 + '</span></div>';	
			} else {
				html += '<div class="oferta-informacoes"><span class="oferta-socio">Apenas sócio</span><br><span class="oferta-horario">' + x.horario1 + '-' + x.horario2 + '</span></div>';
			}				
			html += '<a href="estabelecimento.php?id=' + x.id + '" class="btn btn-default oferta-botao col-xs-8 col-xs-offset-2" role="button">Mais Informações</a><br /><br /></div></div></div>';
			$("#conteudo-categoria-agenda").append(html);
		});
	} else {
		$("#conteudo-categoria-agenda").append('<div class="col-xs-12 text-center" style="height: 200px;"><h3>Nenhuma promoção no momento.</h3></div>');
	}
	if (dia == 0)
		dia = 7;
	$("#conteudo-categoria-agenda").append('<div class="col-xs-12 text-center" style="margin: 20px 0 0;"><a class="botao-todos" href="estabelecimentos.php?dia=' + dia + '&categoria=' + categoriaObj.id + '"><i class="glyphicon glyphicon-arrow-right"></i> Ver todas as opções de economia</a></div>');
	$("#conteudo-categoria-agenda").append("</div>");
}

$(document).ready(function() {
	var d = new Date();
	var n = d.getDay();
	$("[ciklus-dia='"+n+"']").addClass('active');

	$.ajax({
		url: 'action/ajax/buscar-agenda.php',
		data: { agendaDia: n },
		type: 'POST',
		success: function(data) {
			localStorage.setItem('agenda', data);
			$("#conteudo-categoria-agenda").html('');
			var est = $.parseJSON(data);
			var categorias = Object.keys(est.agenda);
			categorias.forEach(function(x, y) {
				popularCategoria(x);
				popularWidget(est.agenda[x], n, est.categorias[x]);
			});
		}	
	});

	$("#entre-para-o-clube-index").click(function(){
		window.location = "cadastro.php";
	});

	$("#quero-comecar").click(function(){
		window.location = "cadastro.php";
	});

	$(".agenda-span").click(function() {

		$(".agenda-span").removeClass('active');
		var dia = $(this).attr("ciklus-dia");
		$(this).addClass('active');
		$("#agenda-do-dia-conteudo").hide();
		$("#agenda-do-dia-loader").show();
		$.ajax({
			url: 'action/ajax/buscar-agenda.php',
			data: { agendaDia: dia },
			type: 'POST',
			success: function(data) {
				$("#agenda-do-dia-loader").hide();
				$("#agenda-do-dia-conteudo").show();				
				localStorage.setItem('agenda', data);
				$("#conteudo-categoria-agenda").html('');
				var est = $.parseJSON(data);
				var categorias = Object.keys(est.agenda);
				categorias.forEach(function(x, y) {
					popularCategoria(x);
					popularWidget(est.agenda[x], dia, est.categorias[x]);
				});
			}
		});
	});

});