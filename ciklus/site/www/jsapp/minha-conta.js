var SPMaskBehavior = function (val) {
		return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	},
	spOptions = {
		onKeyPress: function(val, e, field, options) {
			field.mask(SPMaskBehavior.apply({}, arguments), options);
		}
	};


$(document).ready(function() {
	$("#cep").mask("99999-999");
	$("#data-nascimento").mask("99/99/9999");
	$("#celular").mask(SPMaskBehavior, spOptions);
	$("#telefone").mask(SPMaskBehavior, spOptions);

	$("#cep").blur(function() {
		if ($("#cep").val()) {
			var cep = $("#cep").val().replace("-", "");
			$.ajax({
				url: 'https://viacep.com.br/ws/' + cep + '/json/ ',
				type: 'GET',
				success: function(data) {
					$("#rua").val(data.logradouro);
					$("#bairro").val(data.bairro);
					$("#cidade").val(data.localidade);
					$("#uf").val(data.uf);
				}
			});
		}
	});

	$('#modal-planos').on('shown.bs.modal', function (e) {
	  	$(".plano-cartao").hide();
		$(".plano-plano").show();
	})

	$("#voltar-planos").click(function() {
		$(".plano-cartao").hide();
		$(".plano-plano").show();
	});

	$(".alterar-plano").click(function() {
		var plano = $(this).attr('ciklus-plano');
		var paymentId = $("#hdn_payment_id").val();
		if (paymentId) {
			swal({
			  	title: "Tem certeza?",
			  	text: "Deseja realmente alterar o seu plano?",
			  	type: "warning",
			  	showCancelButton: true,
			  	confirmButtonColor: "#DD6B55",
			  	confirmButtonText: "Sim, alterar!",
			  	cancelButtonText: "Agora não",
			  	closeOnConfirm: false
			},	function(){
			  	$.ajax({
			  		url: 'action/ajax/alterar-plano.php',
			  		data: { plano: plano },
			  		type: 'POST',
			  		success: function(data) {
			  			window.location.reload();
			  		}
			  	});
			});
		} else {
			$("#hdn_plano_selecionado").val(plano);
			$(".plano-plano").hide();
			$(".plano-cartao").show();
		}
	});

	$(".remover-cartao").click(function() {
		var cartao = $(this).attr('ciklus-cartao');
		$.ajax({
			url: 'action/ajax/remover-cartao.php',
			data: { cartao: cartao },
			type: 'POST',
			success: function(data) {
				if (data) {
					$(".cartao-" + cartao).hide();
					$(this).hide();
				}
			}
		});
	});

	$("#salvar-cartao-plano").click(function() {
		$(this).text('Processando...');
		var numero = $("#numero-cartao").val();
		var nome = $("#nome-cartao").val();
		var cvv = $("#cvv-cartao").val();
		var mes = $("#mes-cartao").val();
		var ano = $("#ano-cartao").val();
		if (numero.length == 16 && nome.length > 4 && (cvv.length == 3 || cvv.length == 4) && (mes.length == 1 || mes.length == 2) && (ano.length == 2)) {
			$.ajax({
				url: 'action/ajax/criar-cartao.php',
				data: { numero: numero, nome: nome, cvv: cvv, mes: mes, ano: ano, plano: 'setar' },
				type: "POST",
				success: function(data) {
					if (data) {
						var plano = $("#hdn_plano_selecionado").val();
						$.ajax({
							url: 'action/ajax/associar-cliente-trial.php',
							data: { plano: plano, cartao: data },
							type: 'POST',
							success: function(status) {
								if (status == "paid") {
									swal({
									  	title: "Sucesso",
									  	text: "Seu plano foi alterado com sucesso.",
									  	type: "success",
									  	showCancelButton: false,
									  	confirmButtonText: "Ok!",
									  	closeOnConfirm: false
									},	function(){
									  	window.location.reload();
									});
								} else {
									swal({
									  	title: "Ops",
									  	text: "Houve um problema com o pagamento, por favor, tente novamente.",
									  	type: "warning",
									  	showCancelButton: false,
									  	confirmButtonText: "Tentar novamente",
									  	closeOnConfirm: false
									});
								}
							}
						});
					} else {
						$(this).text('Salvar');
						swal('Dados inválidos', 'Houve um erro ao cadastrar o seu cartão, por favor, verifique os dados e tente novamente.', 'error');
					}
				}
			});
		} else {
			$(this).text('Salvar');
			swal("Dados inválidos", "Por favor, verifique os dados do cartão.", "warning");
		}
	});

	$("#salvar-cartao").click(function() {
		$(this).text('Processando...');
		var numero = $("#numero-cartao-modal").val();
		var nome = $("#nome-cartao-modal").val();
		var cvv = $("#cvv-cartao-modal").val();
		var mes = $("#mes-cartao-modal").val();
		var ano = $("#ano-cartao-modal").val();
		if (numero.length == 16 && nome.length > 4 && (cvv.length == 3 || cvv.length == 4) && (mes.length == 1 || mes.length == 2) && (ano.length == 2)) {
			$.ajax({
				url: 'action/ajax/criar-cartao.php',
				data: { numero: numero, nome: nome, cvv: cvv, mes: mes, ano: ano },
				type: "POST",
				success: function(data) {
					if (data) {
						swal({
						  	title: "Sucesso",
						  	text: "Cartão cadastrado com sucesso",
						  	type: "success",
						  	showCancelButton: false,
						  	confirmButtonColor: "#D31E86",
						  	confirmButtonText: "Ok!",
						},	function(){
						  	window.location.reload();
						});
					} else {
						$(this).text('Salvar');
						swal('Dados inválidos', 'Houve um erro ao cadastrar o seu cartão, por favor, verifique os dados e tente novamente.', 'error');
					}
				}
			});
		} else {
			$(this).text('Salvar');
			swal("Dados inválidos", "Por favor, verifique os dados do cartão.", "warning");
		}
	});

	$("#btn-salvar-dados").click(function(){
		var cep = $("#cep").val();
		var celular = $("#celular").val();
		var telefone = $("#telefone").val();
		var estado = $("#uf").val();
		var cidade = $("#cidade").val();
		var bairro = $("#bairro").val();
		var rua = $("#rua").val();
		var numero = $("#numero").val();
		var email = $("#email").val();
		var nome = $("#nome").val();
		var senha = $("#senha").val();
		var dataNascimento = $("#data-nascimento").val();
		var sexo = $(".radio-sexo:checked").val();

		if (celular != "" && cep != "" && numero != "" && email != "" && nome != "" && dataNascimento != "" && sexo != "") {
			$.ajax({
				url: 'action/ajax/atualizar-dados.php',
				data: { cep: cep, celular: celular, telefone: telefone, estado: estado, cidade: cidade,
						bairro: bairro, rua: rua, numero: numero, email: email, nome: nome, senha: senha,
						dataNasc: dataNascimento, sexo: sexo },
				type: 'POST',
				success: function(data) {
					$.cookie('usuario-ciklus', data);
					swal('Sucesso!', 'Seus dados foram atualizados com sucesso!', 'success');
				}
			});
		} else {
			swal('Ops!', 'Alguns dados obrigatórios (*) não foram preenchidos, por favor, verifique.', 'warning');
		}
	});
});