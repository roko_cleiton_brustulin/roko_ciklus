function UpdateQueryString(key, value, url) {
    if (!url) url = window.location.href;
    var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi"),
        hash;

    if (re.test(url)) {
    	if (key !== "nome" && url.indexOf('nome') > 0) {
    		var re2 = new RegExp("([?&])nome=.*?(&|#|$)(.*)", "gi"),
    		url = url.replace(re2, '');
    	}

        if (typeof value !== 'undefined' && value !== null)
            return url.replace(re, '$1' + key + "=" + value + '$2$3');
        else {
            hash = url.split('#');
            url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
            if (typeof hash[1] !== 'undefined' && hash[1] !== null) 
                url += '#' + hash[1];
            return url;
        }
    }
    else {
    	if (key !== "nome" && url.indexOf('nome') > 0) {
    		var re2 = new RegExp("([?&])nome=.*?(&|#|$)(.*)", "gi"),
    		url = url.replace(re2, '');
    	}
        if (typeof value !== 'undefined' && value !== null) {
            var separator = url.indexOf('?') !== -1 ? '&' : '?';
            hash = url.split('#');
            url = hash[0] + separator + key + '=' + value;
            if (typeof hash[1] !== 'undefined' && hash[1] !== null) 
                url += '#' + hash[1];
            return url;
        }
        else
            return url;
    }
}

$(document).ready(function(){
	$("#ver-tudo-categoria").click(function(){
		if ($(".categoria-hide").is(":visible")) {
			$(".categoria-hide").hide();
			$(this).text("+ Ver todos");
		} else {
			$(".categoria-hide").show();
			$(this).text("- Ver menos");
		}
	});

	$("#ver-tudo-bairro").click(function(){
		if ($(".bairro-hide").is(":visible")) {
			$(".bairro-hide").hide();
			$(this).text("+ Ver todos");
		} else {
			$(".bairro-hide").show();
			$(this).text("- Ver menos");
		}
	});

	$(".add-filtro-categoria").click(function(){
		var categoria = $(this).attr('ciklus-categoria');
		var url = UpdateQueryString('subcategoria', '');
		window.location = UpdateQueryString('categoria', categoria, url);
	});

	$(".add-filtro-subcategoria").click(function(){
		var categoria = $(this).attr('ciklus-categoria');
		window.location = UpdateQueryString('subcategoria', categoria);
	});

	$(".filtro-dia").click(function(){
		var dia = $(this).attr('ciklus-dia');
		window.location = UpdateQueryString('dia', dia);
	});

	$(".add-filtro-bairro").click(function(){
		var bairro = $(this).attr('ciklus-bairro');
		window.location = UpdateQueryString('bairro', bairro);
	});

	$("#buscar-por-nome").click(function() {
		var nome = $("#buscar-nome").val();
		if (nome.length > 1) {
			window.location = UpdateQueryString('nome', nome);
		}
	});

	$("#buscar-nome").keypress(function(e) {
	    if(e.which == 13) {
	        $("#buscar-por-nome").trigger('click');
	    }
	});
});