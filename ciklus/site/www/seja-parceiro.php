<?php
include("includes/settings.php");
include("action/seja-parceiro.php");
$scripts = "<script src='jsapp/seja-parceiro.js'></script>";
include("widgets/header.php");
?>
<div class="col-lg-12 titulo-topo conainter-fluid">
	<div class="overlay-topo">
	<span class="text-center titulo-pagina overlay-titulo" style="text-transform:uppercase;">Seja Parceiro da Ciklus</span>
	</div>
</div>
<div class="container pagina">
	<div class="col-md-6">
		<h1>SÃO MUITAS AS VANTAGENS:</h1>
		<ul>
			<li>Participação em um grupo seleto de estabelecimentos. </li>
			<li>Grande visibilidade através do nosso aplicativo, site e redes sociais. </li>
			<li>Captação de novos clientes qualificados e fidelização de quem já frequenta seu estabelecimento. </li>
			<li>Conhecimento detalhado do seu público-alvo. </li>
			<li>Alto potencial de retorno financeiro. </li>
		</ul>
		<p>Preencha o formulário que em breve entraremos em contato. Obrigado.</p>
		<img src="http://unilance.com.br/blog/wp-content/uploads/2016/12/como-investir-bem-empreendedorismo.jpg" style="width: 100%;  height:auto;">
		<br><br>
	</div>
	<div class="col-md-6" style="background:#fff;padding: 50px 0 30px;box-shadow:1px 1px 5px rgba(51, 51, 51, 0.32)">
		<div class="form-group col-md-12">
			<label>Seu Nome *</label>
			<input type="text" placeholder="Seu nome" class="form-control" id="nome" />
		</div>
		<div class="form-group col-md-7">
			<label>E-mail *</label>
			<input type="email" placeholder="Seu e-mail" class="form-control" id="email" />
		</div>
		<div class="form-group col-md-5">
			<label>Telefone *</label>
			<input type="text" placeholder="(99) 99999-9999" class="form-control" id="fone" />
		</div>
		<div class="form-group col-md-12">
			<label>Nome do Estabelecimento *</label>
			<input type="text" placeholder="Nome do Estabelecimento" class="form-control" id="estabelecimento" />
		</div>
		<div class="form-group col-md-6">
			<label>CEP *</label>
			<input type="text" placeholder="99999-999" class="form-control" id="cep" />
		</div>
		<div class="form-group col-md-6">
			<label>Núm. & Complem. *</label>
			<input type="text" placeholder="Ex: 100, Loja B" class="form-control" id="complemento" />
		</div>
		<div class="form-group col-md-12">
			<label>Facebook | Site</label>
			<input type="text" placeholder="Ex: http://meuestabelecimento.com.br ou facebook.com/meuestabelecimento" class="form-control" id="facebook-site" />
		</div>

		<div class="form-group col-md-6">
			<label>Categoria *</label>
			<select class="form-control col-md-6" id="categoria">
	                <option value="">Selecione</option>
	                <?php
	                $optGroupCount = 0;
	                $optGroup = "";
	                foreach ($categorias as $categoria) {
	                  if ($optGroup != $categoria->nomePai) {
	                    if ($optGroupCount == 0) {
	                      echo "</optgroup>";
	                    }
	                    $optGroupCount++;
	                    echo "<optgroup label='$categoria->nomePai'>";
	                    $optGroup = $categoria->nomePai;
	                  }
	                ?>
	                  <option <?php if (@$estabelecimento->categoria->id == $categoria->id) : echo "selected='selected'"; endif; ?>
	                    value="<?php echo $categoria->id; ?>">
	                    <?php echo $categoria->nome; ?>
	                  </option>
	                <?php } ?>
			</select>
		</div>

		<!-- <div class="form-group col-md-12">

		</div> -->

		<div class="form-group text-center col-md-6">
			<button class="btn btn-primary btn-large" id="enviar">Enviar</button><br><br>
		</div>

		<div class="form-group text-center col-md-12">
			<small>  * Campos obrigatórios</small>
		</div>

	</div>
</div>
<?php
include("widgets/footer.php");
?>
