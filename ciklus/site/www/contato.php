<?php 
include("includes/settings.php");
$scripts = "<script src='jsapp/contato.js'></script>";
include("widgets/header.php");
?>
<div class="col-lg-12 titulo-topo conainter-fluid">
	<div class="overlay-topo">
	<span class="text-center titulo-pagina overlay-titulo" style="text-transform:uppercase;">Contato</span>
	</div>
</div>
<div class="container">
	<div class="row text-center" style="margin-bottom:20px;">
		<div class="col-md-8 col-md-offset-2">
		<h4>Preencha o formulário abaixo que responderemos o mais breve possível.</h4>
		</div>
	</div>
	<div class="row text-center" style="margin-bottom:20px;">
		<div class="col-lg-offset-3 col-lg-6 col-xs-12">
			<div class="form-group">
				<label>Nome</label>
				<input type="text" placeholder="Seu nome" class="form-control" id="nome" />
			</div>
			<div class="form-group">
				<label>E-mail</label>
				<input type="email" placeholder="Seu e-mail" class="form-control" id="email" />
			</div>
			<div class="form-group">
				<label>Sua mensagem</label>
				<textarea cols="20" rows="7" class="form-control" id="mensagem"></textarea>
			</div>
			<div class="form-group">
				<button class="btn btn-primary btn-large" id="btn-enviar">Enviar</button>
			</div>
		</div>
	</div>
</div>
<?php
include("widgets/footer.php");
?>