/**
 * EventoController
 *
 * @description :: Server-side logic for managing Eventoes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	
	buscarCidades: function(req, res)
	{
		var empresa = req.param('empresa');
		if(empresa == undefined){
			var empresa = "";
		};

		var query = "SELECT DISTINCT(cidade) FROM evento WHERE empresa LIKE ('%" + empresa + "%')";
		
		Evento.query(query, function (err, cidades){
			if (err || !cidades.rows.length) {
			  	return res.json({"status": 0, "error": err});
			}
			else {
				return res.json(cidades);
			}
		});
	},

};

