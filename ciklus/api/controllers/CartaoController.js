/**
 * CartaoController
 *
 * @description :: Server-side logic for managing cartaos
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	adicionar: function(req, res) {
		var api_key = req.param("api_key");
		var customer_id = req.param("customer_id");
		var uri = "https://api.iugu.com/v1/customers/:ID_DO_CLIENTE/payment_methods";
		var otherParams = req.param("otherParams") || {};

		if (!api_key) {
			return res.serverError({ message: "O api_key não foi especificado." });
		}		
		if (!customer_id) {
			return res.serverError({ message: "O customer_id não foi especificado." });
		}		
		uri = uri.replace(":ID_DO_CLIENTE", customer_id);			
		IuguService.sendRequest('POST', uri, api_key,  otherParams, function(err, results) {
			if (err) {
				return res.negotiate(err);
			}
			return res.json(results);
		});;
	},

	remover: function(req, res) {
		var api_key = req.param("api_key");
		var customer_id = req.param("customer_id");
		var payment_method_id = req.param("payment_method_id");
		var uri =  "https://api.iugu.com/v1/customers/ID_DO_CLIENTE/payment_methods/ID_DA_FORMA_PAGAMENTO";
		var otherParams = req.param("otherParams") || {};

		if (!api_key) {
			return res.serverError({ message: "O api_key não foi especificado." });
		}		
		if (!customer_id) {
			return res.serverError({ message: "O customer_id não foi especificado." });
		}	
		if (!payment_method_id) {
			return res.serverError({ message: "O payment_method_id não foi especificado." });
		}		
		uri = uri.replace(":ID_DO_CLIENTE", customer_id).replace(":ID_DA_FORMA_PAGAMENTO", payment_method_id);			
		IuguService.sendRequest('DELETE', uri, api_key,  otherParams, function(err, results) {
			if (err) {
				return res.negotiate(err);
			}
			return res.json(results);
		});;
	}
};

