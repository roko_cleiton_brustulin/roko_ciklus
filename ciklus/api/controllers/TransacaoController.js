/**
 * TransacaoController
 *
 * @description :: Server-side logic for managing Transacaos
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	buscarPorSocio: function(req, res) {
		Transacao.find({ socio: req.param('socio') }).populate('estabelecimento').populate('promocao').sort('createdAt DESC').exec(function(err, results) {
			res.json(results);
		});
	}
};

