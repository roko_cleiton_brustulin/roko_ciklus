/**
 * EstabelecimentoController
 *
 * @description :: Server-side logic for managing 
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	fullSearch: function(req, res)
	{
		var myQuery = "SELECT json_agg(row_to_json(row)) FROM (" + req.query.where + " ) row";
		Estabelecimento.query(myQuery, function (err, activities){
			if(err || !activities.rows.length){
			  return res.json({"status": 0, "error": err});
			}
			else
			{
			  return res.json(activities);
			}
		});
	},

	buscarPorCategoria: function(req, res)
	{
		var estabelecimentosArr = [];
		var empresa = req.param('empresa');
		if(empresa == undefined){
			var empresa = "";
		};
		var query = "SELECT * FROM estabelecimento WHERE (categoria = " + req.param('categoria') + " OR categoria IN (SELECT id FROM categoria WHERE \"categoriaPai\" = " + req.param('categoria') + ")) AND status = 'a' AND empresa LIKE ('%" + empresa + "%')";

		Estabelecimento.query(query, function (err, estabelecimentos){
			if (err || !estabelecimentos.rows.length) {
			  	return res.json({"status": 0, "error": err});
			}
			else {
				async.eachSeries(estabelecimentos.rows, function(x, cb) {
					Estabelecimento.findOne({ id: x.id }).populate('imagens').populate('categoria').populate('promocoes').exec(function(err, result) {
						estabelecimentosArr.push(result);
						cb();
					});
				}, function (err) {
					return res.json(estabelecimentosArr);
				});
			}
		});
	},

	buscarCidades: function(req, res)
	{
		var empresa = req.param('empresa');
		if(empresa == undefined){
			var empresa = "";
		};

		var query = "SELECT DISTINCT(cidade) FROM estabelecimento WHERE status = 'a' AND empresa LIKE ('%" + empresa + "%')";
		
		Estabelecimento.query(query, function (err, cidades){
			if (err || !cidades.rows.length) {
			  	return res.json({"status": 0, "error": err});
			}
			else {
				return res.json(cidades);
			}
		});
	},

	buscar: function(req, res) {
		var estabelecimentos = [];
		var empresa = req.param('empresa');
		if(empresa == undefined){
			var empresa = "";
		};
		if (req.param('nome')) {
			Estabelecimento.find({ nome: { 'like': '%' + req.param('nome') + '%' }, status: 'a', empresa: {'like': '%' + empresa + '%'} }).sort('nome ASC').populate('promocoes').populate('imagens').populate('categoria').exec(function(err, results) {
				async.eachSeries(results, function(x, cb) {
					estabelecimentos.push(x);
					cb();
				}, function(err) {
					res.json(estabelecimentos);
				});
			});
		} else {
			if (req.param('categoria')) {
				Categoria.findOne({ id: req.param('categoria') }).exec(function(err, result){
					if (result) {
						Estabelecimento.find({ categoria: result.id, status: 'a', empresa: {'like': '%' + empresa + '%'} } ).sort('nome ASC').populate('promocoes').populate('imagens').populate('categoria').exec(function(err, estabelecimentosRes) {
							async.eachSeries(estabelecimentosRes, function(x, cb) {
								estabelecimentos.push(x);
								cb();
							}, function(err) {
								Categoria.find({ categoriaPai: result.id }).exec(function(err, results) {
									if (results.length > 0) {
										async.eachSeries(results, function(x, cb) {
											Estabelecimento.find({ categoria: x.id, status: 'a', empresa: {'like': '%' + empresa + '%'} } ).sort('nome ASC').populate('promocoes').populate('imagens').populate('categoria').exec(function(err, estabelecimentosRes) {
												async.eachSeries(estabelecimentosRes, function(x, cb1) {
													estabelecimentos.push(x);
													cb1();
												}, function(err) {
													cb();
												});
											});
										}, function (err) {
											return res.json(estabelecimentos);	
										});
									} else {
										return res.json(estabelecimentos);
									}
								});
							});
						});
					} else {
						return res.json([]);
					}
				});
			} else {
				Estabelecimento.find({status: 'a', empresa: {'like': '%' + empresa + '%'}}).sort('nome ASC').populate('promocoes').populate('imagens').populate('categoria').exec(function(err, estabelecimentosRes) {					
					res.json(estabelecimentosRes);
				});
			}
		}
	},

	buscarTodosApp: function(req, res) {
		var empresa = req.param('empresa');
		if(empresa == undefined){
			var empresa = "";
		};
		Estabelecimento.find({empresa: { 'like': '%' + empresa + '%' }})
						.populate('promocoes')
						.populate('imagens')
						.populate('categoria')
						.exec(function(err, estabelecimentos) {
			if (err) res.negotiate(err);

			res.json(estabelecimentos);
		});
	}
};