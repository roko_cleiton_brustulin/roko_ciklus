/**
 * EstabelecimentoFotoController
 *
 * @description :: Server-side logic for managing Estabelecimentofotoes
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	alterarOrdem: function(req, res) {
		EstabelecimentoFoto.update({ ordem: req.param('tipo'), estabelecimento: req.param('estabelecimento') }, { ordem: 0 }).exec(function(err, results) {
			EstabelecimentoFoto.update({ id: req.param('id') }, { ordem: req.param('tipo') }).exec(function(err, results) {
				res.ok();
			});
		});
	}
};

