/**
 * CategoriaController
 *
 * @description :: Server-side logic for managing Categorias
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	buscarTodas: function(req, res) {
		Categoria.find().exec(function(err, results) {
			if (err) res.negotiate(err);

			res.json(results);
		});
	},

	buscarAtivas: function(req, res) {
		Categoria.find().where({ categoriaPai: null, status: 1 }).exec(function(err, results) {
			if (err) res.negotiate(err);

			res.json(results);
		});
	}
};

