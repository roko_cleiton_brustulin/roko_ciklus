/**
 * CupomController
 *
 * @description :: Server-side logic for managing Cupoms
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	baixar: function (req, res) {
		Cupom.find().exec(function(err, results) {
			if (err) res.negotiate(err);

			res.json(results);
		});
	}
};

