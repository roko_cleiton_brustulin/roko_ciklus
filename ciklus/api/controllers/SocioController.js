/**
 * SocioController
 *
 * @description :: Server-side logic for managing socios
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	buscarTodos: function(req, res) {
		Socio.find().exec(function (err, results) {
			if (err) res.negotiate(err);

			res.json(results);
		});
	},

	buscarTodosApp: function(req, res) {
		var empresa = req.param('empresa');
		if(empresa == undefined){
			var empresa = "";
		};
		Socio.find({empresa: { 'like': '%' + empresa + '%' }}).exec(function(err, results) {
			if (err) res.negotiate(err);

			res.json(results);
		});
	},

	criarClienteIugu: function(req, res) {
		res.ok();
	},

	removerCartao: function(req, res) {
		var api_key = req.param("api_key");
		var cartao = req.param("cartaoId");
		var socio = req.param("socioId");
		var uri = "https://api.iugu.com/v1/customers/" + socio + "/payment_methods/" + cartao;
		var otherParams = req.param("otherParams") || {};
		
		IuguService.sendRequest('DELETE', uri, api_key,  otherParams, function(err, results) {
			if (err) {
				return res.negotiate(err);
			}
			return res.json(results);
		});
	},

	alterarClienteIugu: function(req, res) {
		var api_key = req.param("api_key");
		var subscriptionId = req.param("subscriptionId");
		var uri = "https://api.iugu.com/v1/subscriptions/:ID_DA_ASSINATURA";
		var otherParams = req.param("otherParams") || {};

		if (!api_key) {
			return res.serverError({ message: "O api_key não foi especificado." });
		}		
		if (!subscriptionId) {
			return res.serverError({ message: "O subscriptionId não foi especificado." });
		}		
		uri = uri.replace(":ID_DA_ASSINATURA", subscriptionId);			
		IuguService.sendRequest('PUT', uri, api_key,  otherParams, function(err, results) {
			if (err) {
				return res.negotiate(err);
			}
			return res.json(results);
		});
	},

	removerClienteIugu: function(req, res) {
		var api_key = req.param("api_key");
		var subscriptionId = req.param("subscriptionId");
		var uri = "https://api.iugu.com/v1/subscriptions/:ID_DA_ASSINATURA";
		var otherParams = req.param("otherParams") || {};

		if (!api_key) {
			return res.serverError({ message: "O api_key não foi especificado." });
		}		
		if (!subscriptionId) {
			return res.serverError({ message: "O subscriptionId não foi especificado." });
		}		
		uri = uri.replace(":ID_DA_ASSINATURA", subscriptionId);			
		IuguService.sendRequest('DELETE', uri, api_key,  otherParams, function(err, results) {
			if (err) {
				return res.negotiate(err);
			}
			return res.json(results);
		});
	}
};

