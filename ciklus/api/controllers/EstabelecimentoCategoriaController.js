/**
 * EstabelecimentoCategoriaController
 *
 * @description :: Server-side logic for managing Estabelecimentocategorias
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	buscarAgenda: function(req, res) {
		var dia 	= req.param('dia');
		var empresa = req.param('empresa');
		if(empresa == undefined){
			var empresa = "";
		};
		var result 	= [];
		CategoriaService.buscarAtivas(function(err, results) {
			if (err) res.negotiate(err);
			
			async.eachSeries(results, function(item, callback) {
				
				var promocao = { id: item.id, categoria: item.nome, promocoes: [] };
				var sql = "SELECT titulo, politica, desconto, \"socioMais\", horario1, horario2, horario3, horario4, e.nome, e.id, ef.id as fotoid, ef.extensao as fotoextensao, e.categoria, e.destaque, e.endereco, e.numero, e.bairro, e.cidade, p.\"segundaFeira\", p.\"tercaFeira\", p.\"quartaFeira\", p.\"quintaFeira\", p.\"sextaFeira\", p.sabado, p.domingo, e.latitude, e.longitude ";
				sql += "FROM promocao p ";
				sql += "INNER JOIN estabelecimento e ON p.estabelecimento = e.id ";
				sql += "INNER JOIN estabelecimentofoto ef ON e.id = ef.estabelecimento ";
				sql += "INNER JOIN categoria c ON e.categoria = c.id ";
				sql += "WHERE (e.categoria IN (" + item.id + ") OR c.\"categoriaPai\" IN (" + item.id + ")) AND \"" + dia + "\" = 't' AND c.status = 1 AND p.status = 1 AND (validade > NOW() OR validade IS NULL) AND e.status = 'a' AND p.destaque IN ('f', 'r') AND ef.ordem = 1 AND e.empresa LIKE ('%" + empresa + "%')";
				sql += "ORDER BY p.destaque ASC, random() ";
				// sql += "LIMIT 3";
				Estabelecimento.query(sql, function(err, promocoes) {
					if (err) res.negotiate(err);
					if (promocoes.rows) {
						promocao.promocoes = promocoes.rows;
						result.push(promocao);
						callback();
					} else {
						callback();
					}						
				});

			}, function() {
				return res.json(result);
			});
		});
	}
};

