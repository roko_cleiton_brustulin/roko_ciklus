/**
* EstabelecimentoFoto.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	nome: {
  		type: 'text'
  	},
    extensao: {
      type: 'string'
    },
  	estabelecimento: {
  		model: 'estabelecimento'
  	},
  	ordem: {
  		type: 'integer'
  	},
    spot: {
      model: 'sitespot'
    },
    categoria: {
      model: 'categoria'
    }
  }
};

