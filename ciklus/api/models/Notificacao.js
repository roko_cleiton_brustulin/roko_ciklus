/**
 * Novidade.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
  	texto: {
  		type: 'text'
  	},
    socio: {
      type: 'text'
    },
  	socioLido: {
  		type: 'text'
  	},
    link: {
      type: 'text'
    },
    parametro: {
      type: 'text'
    },
    empresa: {
      type: 'text'
    }
  }
};

