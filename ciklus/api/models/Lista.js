module.exports = {

  attributes: {
    evento: {
      model: 'Evento'
    },
  	nome: {
  		type: 'text'
  	},
  	validade: {
  		type: 'date'
  	},
  	status: {
  		type: 'boolean'
  	},
  	descricao: {
  		type: 'text'
  	},
    planos: {
      type: 'text'
    },
    empresa: {
      type: 'text'
    },
    socios: {
      type: 'text'
    },
  }
};

