/**
* Pagamento.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	socio: {
  		model: 'Socio'
  	},
  	tipo: {
  		type: 'string'
  	},
  	formaPgto: {
  		type: 'string'
  	},
  	tokenPgto: {
  		type: 'string'
  	},
  	valor: {
  		type: 'float'
  	},
    cupom: {
      model: 'Cupom'
    }
  }
};

