/**
* Socio.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    idIugu: {
      type: 'string'
    },
  	nome: {
  		type: 'string'
  	},
  	email: {
  		type: 'string'
  	},
  	cpf: {
  		type: 'string'
  	},
  	celular: {
  		type: 'string'
  	},
  	telefone: {
  		type: 'string'
  	},
  	cep: {
  		type: 'string'
  	},
  	rua: {
  		type: 'string'
  	},
  	numero: {
  		type: 'integer'
  	},
  	bairro: {
  		type: 'string'
  	},
  	cidade: {
  		type: 'string'
  	},
  	uf: {
  		type: 'string'
  	},
  	cartao: {
  		model: 'Cartao'
  	},
  	pagamentos: {
  		collection: 'Pagamento',
  		via: 'socio'
  	},
    ativoAte: {
      type: 'date'
    },
    senha: {
      type: 'string'
    },
    dataNasc: {
      type: 'string'
    },
    sexo: {
      type: 'string'
    },
    termos: {
      type: 'boolean'
    },
    cupom: {
      model: 'Cupom'
    },
    onesignalId: {
      type: 'text'
    },
    codigo: {
      type: 'text'
    },
    empresa: {
      type: 'text'
    },
    origem: {
      type: 'text'
    },
    emailValido: {
      type: 'boolean'
    },
    estilo: {
      type: 'text'
    }
  }, afterCreate: function(value, cb) {
    // var cod1 = Math.floor((Math.random() * 10) + 1);
    // var cod2 = Math.floor((Math.random() * 10) + 1);
    min = Math.ceil(10);
    max = Math.floor(100);
    cod1 = Math.floor(Math.random() * (max - min)) + min;
    cod2 = Math.floor(Math.random() * (max - min)) + min;
    var codigoSocio = cod1.toString() + value.id.toString() + cod2.toString();
    console.log(codigoSocio);
    Socio.update({ id: value.id }, { codigo: codigoSocio.toString() }).exec(function(err, res) {
      if (err) {
        cb(err);
      }
      console.log(res);
      cb();
    });
  }
};
