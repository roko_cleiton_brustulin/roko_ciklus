/**
 * Cupom.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
  	plano: {
  		model: 'Plano'
  	},
  	codigo: {
  		type: 'string'
  	},
  	expiracao: {
  		type: 'date'
  	},
    utilizado: {
      type: 'boolean'
    },
    empresa: {
      type: 'text'
    }
  }
};

