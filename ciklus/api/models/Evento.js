/**
 * Evento.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
  	titulo: {
  		type: 'string'
  	},
  	politica: {
  		type: 'text'
  	},
    desconto: {
      type: 'integer'
    },
    data: {
    	type: 'date'
    },
    empresa: {
      type: 'string'
    },
    categoria: {
    	model: 'categoria'
    },
    foto: {
    	model: 'estabelecimentofoto'
    },
    cidade: {
      type: 'text'
    },
    estado: {
      type: 'text'
    },
    local: {
      type: 'text'
    },
    link: {
      type: 'text'
    },
    estilo: {
      type: 'text'
    },
    listas: {
      collection: 'lista',
      via: 'evento'
    },
  }
};

