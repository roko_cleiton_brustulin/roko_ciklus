/**
 * Novidade.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
  	titulo: {
  		type: 'string'
  	},
  	texto: {
  		type: 'text'
  	},
  	estabelecimento: {
  		model: 'Estabelecimento'
  	},
  	imagem: {
  		model: 'EstabelecimentoFoto'
  	},
    curtidas: {
      type: 'text'
    },
    link: {
      type: 'text'
    },
    parametro: {
      type: 'text'
    },
    empresa: {
      type: 'text'
    },
    cidade: {
      type: 'text'
    }
  }
};

