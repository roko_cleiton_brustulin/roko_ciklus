/**
* Transacao.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	socio: {
  		model: 'socio'
  	},
  	estabelecimento: {
  		model: 'estabelecimento'
  	},
  	valor: {
  		type: 'float'
  	},
    desconto: {
      type: 'float'
    },
    valorDesconto: {
      type: 'float'
    },
    promocao: {
      model: 'Promocao'
    },
    nota: {
      type: 'integer'
    },
    observacao: {
      type: 'string'
    }
  },

  afterCreate: function(value, cb) {
    Socio.findOne({id: value.socio}).exec(function(err, user) {
      if (err) return cb(null, true);

      Estabelecimento.findOne({ id: value.estabelecimento }).exec(function (err, estabelecimento) {
        if (err) return cb(null, true);

        if (user.onesignalId) {
          var onesignal = user.onesignalId.split(';')[0];
          var primeironome = user.nome.split(' ').slice(0, -1).join(' ');
          OnesignalService.sendNotification(
            OnesignalService.createMessage([onesignal], 
              primeironome + ', sua conta no ' + estabelecimento.nome + ' foi de ' + value.valor.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }) + ' e com a Ciklus você economizou ' + value.valorDesconto.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }) + ' - ' + value.desconto + '%! Não esqueça de avaliar.', null, {}));
          return cb(null, true);
        } else {
          return cb(null, true);
        }
      });
    });
  }
  
};

