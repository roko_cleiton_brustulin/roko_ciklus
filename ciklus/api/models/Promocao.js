/**
* Promocao.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	titulo: {
  		type: 'string'
  	},
  	politica: {
  		type: 'text'
  	},
    desconto: {
      type: 'integer'
    },
  	socioMais: {
  		type: 'integer'
  	},
  	segundaFeira: {
  		type: 'boolean'
  	},
  	tercaFeira: {
  		type: 'boolean'
  	},
  	quartaFeira: {
  		type: 'boolean'
  	},
  	quintaFeira: {
  		type: 'boolean'
  	},
  	sextaFeira: {
  		type: 'boolean'
  	},
  	sabado: {
  		type: 'boolean'
  	},
  	domingo: {
  		type: 'boolean'
  	},
  	estabelecimento: {
  		model: 'Estabelecimento'
  	},
    horario1: {
      type: 'string'
    },
    horario2: {
      type: 'string'
    },
    horario3: {
      type: 'string'
    },
    horario4: {
      type: 'string'
    },
    validade: {
      type: 'date'
    },
    status: {
      type: 'integer'
    },
    destaque: {
      type: 'string'
    }
  }
};