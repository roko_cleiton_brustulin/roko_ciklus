/**
* Estabelecimento.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/
module.exports = {
  attributes: {
  	nome: {
  		type: 'text'
  	},
  	endereco: {
  		type: 'string'
  	},
    numero: {
      type: 'string'
    },
  	bairro: {
  		type: 'string'
  	},
  	cidade: {
  		type: 'string'
  	},
  	cep: {
  		type: 'string'
  	},
  	telefone: {
  		type: 'string'
  	},
  	email: {
  		type: 'string'
  	},
  	site: {
  		type: 'string'
  	},
  	facebook: {
  		type: 'string'
  	},
  	descricao: {
  		type: 'text'
  	},
    categoria: {
      model: 'categoria'
    },
  	promocoes: {
  		collection: 'promocao',
  		via: 'estabelecimento'
  	},
    imagens: {
      collection: 'EstabelecimentoFoto',
      via: 'estabelecimento'
    },
    produtos: {
      collection: 'produto',
      via: 'estabelecimento'
    },
    urlMaps: {
      type: 'string'
    },
    login: {
      type: 'string'
    },
    senha: {
      type: 'string'
    },
    status: {
      type: 'string'
    },
    destaque: {
      type: 'string'
    },
    latitude: {
      type: 'string'
    },
    longitude: {
      type: 'string'
    },
    metaTitulo: {
      type: 'string'
    },
    metaDescricao: {
      type: 'text'
    },
    empresa: {
      type: 'text'
    },
    perfil: {
      type: 'text'
    }
  }
};