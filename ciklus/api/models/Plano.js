/**
 * Plano.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
  	frequencia: {
  		type: 'integer'
  	},
  	tipo: {
  		type: 'string'
  	},
  	preco: {
  		type: 'float'
  	},
  	titulo: {
  		type: 'string'
  	},
    ativo: {
      type: 'boolean'
    },
    empresa: {
      type: 'string'
    },
    idPlataforma: {
      type: 'integer'
    }
  }
};

