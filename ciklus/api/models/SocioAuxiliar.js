/**
 * SocioAuxiliar.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    nome: {
  		type: 'string'
  	},
  	email: {
  		type: 'string'
  	},
  	cpf: {
  		type: 'string'
  	},
  	celular: {
  		type: 'string'
  	},
  	telefone: {
  		type: 'string'
  	},
  	cep: {
  		type: 'string'
  	},
  	rua: {
  		type: 'string'
  	},
  	numero: {
  		type: 'integer'
  	},
  	bairro: {
  		type: 'string'
  	},
  	cidade: {
  		type: 'string'
  	},
  	uf: {
  		type: 'string'
  	},
    dataNasc: {
      type: 'string'
    },
    sexo: {
      type: 'string'
    },
    empresa: {
      type: 'string'
    },
    origem: {
      type: 'string'
    }
  }
}