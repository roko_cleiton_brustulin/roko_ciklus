module.exports = {
  sendNotification: function(data) {
    var headers = {
      "Content-Type": "application/json",
      "Authorization": "MTkzYjQzZDMtY2M1Zi00YmM4LWI1N2UtN2YyODllNTMxZGFm"
    };

    var options = {
      host: "onesignal.com",
      port: 443,
      path: "/api/v1/notifications",
      method: "POST",
      headers: headers
    };

    var https = require('https');
    var req = https.request(options, function(res) {
      res.on('data', function(data) {
      });
    });

    req.on('error', function(e) {
      console.log("ERROR:");
      console.log(e);
    });

    req.write(JSON.stringify(data));
    req.end();
  },

  createMessage: function(playerIds, content, photo, state) {
    var m = {
      // app_id: "a7783d58-2788-4b59-95a5-638a220badea", - CIKLUS
      app_id: "f8b8d4f7-d32d-4abc-92d1-d3a707f9a760", // ALÔ INGRESSOS
      contents: {"en": content },
      include_player_ids: playerIds,
      small_icon: 'ic_stat_onesignal_default',
      large_icon: 'ic_onesignal_large_icon_default',
      android_accent_color: 'E6177E',
      ios_badgeType: 'Increase',
      ios_badgeCount: 1,
      android_group: "ciklus",
      android_group_message: "$[notif_count] novidades, toque para ver.",
      data: { state: state }
    };
    if (photo) {
      m.big_picture = photo;
      m.ios_attachments = { "id1": photo };      
    }
    return m;
  }
};