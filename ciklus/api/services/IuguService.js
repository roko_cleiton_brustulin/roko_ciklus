var request = require('request')

module.exports = {
	sendRequest: function(method, uri, api_key, otherParams, cb) {
		request({ 
			method: method, 
			'auth': {
			    'user': api_key,
			    'pass': ''
			    // , 'sendImmediately': false
			},
			url: uri, 
			json: otherParams
			
		}, function (error, response, body) {
			if (error){
				return cb(error, null);
			}
			if (body) {
				// body = JSON.parse(body);
				return cb(null, body);
			} else {
				return cb();
			}
		});
	}
};
