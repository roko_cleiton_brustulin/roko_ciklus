/**
 * CategoriaController
 *
 * @description :: Server-side logic for managing Categorias
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	buscarAtivas: function(cb) {
		Categoria.find().where({ categoriaPai: null, status: 1 }).exec(function(err, results) {
			return cb(err, results);
		});
	}
};

